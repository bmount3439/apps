using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

//namespace RequestForQuoteSharePointWeb.App_Start
//{
//    public class WebApiConfig : ApiController
//    {
//    }
//}
class WebApiConfig
{

  
  public static void Register(HttpConfiguration configuration)
  {
    configuration.MapHttpAttributeRoutes();

    //configuration.Routes.MapHttpRoute("API Default", "api/{controller}/{id}",
    //new { id = RouteParameter.Optional,action="DefaultAction"});
    configuration.Routes.MapHttpRoute(
            name: "DefaultApi",
            routeTemplate: "api/{controller}/{id}",
            defaults: new { id = RouteParameter.Optional }
        );
    //new { controller="values",action="Index",BranchPlant = RouteParameter.Optional, CustomerNumber= RouteParameter.Optional, PartNumber= RouteParameter.Optional, Quantity= RouteParameter.Optional });
    //new { BranchPlant = RouteParameter.Optional});


    //public static void Register(HttpConfiguration configuration)
    //{
    //  // Web API configuration and services

    //  // Web API routes
    //  configuration.MapHttpAttributeRoutes();

    //  configuration.Routes.MapHttpRoute(
    //      name: "DefaultApi",
    //      routeTemplate: "api/{controller}/{id}",
    //      defaults: new { id = RouteParameter.Optional }
    //  );

    configuration.Routes.MapHttpRoute(
  name: "PricetApi",
    routeTemplate: "api/{controller}/{BranchPlant}/{CustomerNumber}/{PartNumber}/{Quantity}"
    );
  }


}

