
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RequestForQuoteSharePointWeb.Models
{

using System;
    using System.Collections.Generic;
    
public partial class WORKFLOW
{

    public int WORKFLOW_ID { get; set; }

    public int STATUS_ID { get; set; }

    public System.DateTime LAST_UPDATED { get; set; }

    public int LAST_UPDATED_BY { get; set; }

    public int ASSIGNED_TO_ROLE_ID { get; set; }

    public Nullable<int> CREATED_BY_USER_ID { get; set; }

    public Nullable<System.DateTime> CREATION_DATE { get; set; }

    public Nullable<int> WORKFLOW_TYPE_ID { get; set; }

}

}
