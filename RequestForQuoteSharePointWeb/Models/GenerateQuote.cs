using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace RequestForQuoteSharePointWeb.Models
{
  public class RefreshSavedModel
  {
    public string PartNumber { get; set; }
    public string CustomerName { get; set; }
    public string CustomerNum { get; set; }
  }
    public class GenerateQuoteHeader
    {
        public int QuoteID { get; set; }
        public string QuoteNum { get; set; }
        public string PartDescription { get; set; }
        public string PartStatus { get; set; }
        public decimal UnitPrice { get; set; }
        public int Item_ID { get; set; }

        public int UserID { get; set; }
        public string Username { get; set; }
        public string RoleDesc { get; set; }

        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal UnitCost { get; set; }

        public DateTime LastChangedCostDate { get; set; }
        public string DisplayedDate
        {
            get
            {
                return LastChangedCostDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
        }
        public decimal Wins { get; set; }
        public decimal WinsCalc { get; set; }
        public decimal Losses { get; set; }
        public decimal LossesCalc { get; set; }
        public decimal TotalOrders { get; set; }
        public decimal WinsPercent { get; set; }
        public string CostReference { get; set; }
        public int QuoteQuantity { get; set; }
        public string MultipleQuoteQuantity { get; set; }

        public List<SelectListItem> Costing { get; set; }
        public string CostYearDesc { get; set; }
        public string CostYearID { get; set; }

        public string PartNumber { get; set; }
        public decimal SalesPriceOverride { get; set; }
        public string Division { get; set; }
        public string CustomerPartNum { get; set; }
        //public string CustodianPartNum { get; set; }
        public string CustomerName { get; set; }
        public string CustomerNum { get; set; }

        public int ShipToNum { get; set; }
        public string ShipToName { get; set; }
        public string SoldToNum { get; set; }
        public string SoldToName { get; set; }
        public string line_num_check { get; set; }


        public int CompetitorCount { get; set; }
        public Int32 ReserveQty { get; set; }
        public Int32 QuantityOnHand { get; set; }
        public bool BlacklistFlag { get; set; }
        public string BlacklistReasoning { get; set; }
        public decimal EOExtCost { get; set; }
        public string GLClass { get; set; }
        //public string CompetitorNames { get; set; }
        public int ComponentCount { get; set; }
        public int MfgLT { get; set; }
        public int CumLT { get; set; }
        public int RmLT { get; set; }
        public int ActLT { get; set; }
        public int ACQ { get; set; }
        public decimal AverageCostAdjFactor { get; set; }
        public decimal ActualCostMin { get; set; }
        public decimal FifteenPercentCostMin { get; set; }


        public string FactorLastEditComments { get; set; }
        public string FactorLastEditUser { get; set; }
        public DateTime FactorLastEditDate { get; set; }


        public int LTACount { get; set; }
        public string LTAPartNumber { get; set; }
        public decimal LTAPartNumberPrice { get; set; }
        public int LTACustomerNum { get; set; }
        public string LTACustomerName { get; set; }
        public int LoadingPricingValue { get; set; }

        //show sim and fzn
        public decimal calc_500_fzn_unit_price { get; set; }
        public decimal calc_1000_fzn_unit_price { get; set; }
        public decimal calc_2000_fzn_unit_price { get; set; }
        public decimal calc_2500_fzn_unit_price { get; set; }
        public decimal calc_5000_fzn_unit_price { get; set; }
        public decimal calc_10000_fzn_unit_price { get; set; }
        public decimal calc_20000_fzn_unit_price { get; set; }
        public decimal calc_25000_fzn_unit_price { get; set; }
        public decimal calc_50000_fzn_unit_price { get; set; }
        public decimal calc_100000_fzn_unit_price { get; set; }
        public int calc_dynam_qty { get; set; }
        public decimal calc_dynam_fzn_unit_cost { get; set; }
        public decimal Margin { get; set; }

        public decimal calc_10_fzn_unit_cost { get; set; }
        public decimal calc_25_fzn_unit_cost { get; set; }
        public decimal calc_50_fzn_unit_cost { get; set; }
        public decimal calc_75_fzn_unit_cost { get; set; }
        public decimal calc_100_fzn_unit_cost { get; set; }
        public decimal calc_250_fzn_unit_cost { get; set; }
        public decimal calc_500_fzn_unit_cost { get; set; }
        public decimal calc_750_fzn_unit_cost { get; set; }
        public decimal calc_1000_fzn_unit_cost { get; set; }
        public decimal calc_2000_fzn_unit_cost { get; set; }
        public decimal calc_2500_fzn_unit_cost { get; set; }
        public decimal calc_5000_fzn_unit_cost { get; set; }
        public decimal calc_10000_fzn_unit_cost { get; set; }
        public decimal calc_20000_fzn_unit_cost { get; set; }
        public decimal calc_25000_fzn_unit_cost { get; set; }
        public decimal calc_50000_fzn_unit_cost { get; set; }
        public decimal calc_100000_fzn_unit_cost { get; set; }


        public decimal margin_10 { get; set; }
        public decimal margin_25 { get; set; }
        public decimal margin_50 { get; set; }
        public decimal margin_75 { get; set; }
        public decimal margin_100 { get; set; }
        public decimal margin_250 { get; set; }
        public decimal margin_500 { get; set; }
        public decimal margin_750 { get; set; }
        public decimal margin_1000 { get; set; }
        public decimal margin_2500 { get; set; }
        public decimal margin_2000 { get; set; }
        public decimal margin_5000 { get; set; }
        public decimal margin_10000 { get; set; }
        public decimal margin_20000 { get; set; }
        public decimal margin_25000 { get; set; }
        public decimal margin_50000 { get; set; }
        public decimal margin_100000 { get; set; }



        public decimal factor_adj_10 { get; set; }
        public decimal factor_adj_25 { get; set; }
        public decimal factor_adj_50 { get; set; }
        public decimal factor_adj_75 { get; set; }
        public decimal factor_adj_100 { get; set; }
        public decimal factor_adj_250 { get; set; }
        public decimal factor_adj_500 { get; set; }
        public decimal factor_adj_750 { get; set; }
        public decimal factor_adj_1000 { get; set; }
        public decimal factor_adj_2000 { get; set; }
        public decimal factor_adj_2500 { get; set; }
        public decimal factor_adj_5000 { get; set; }
        public decimal factor_adj_10000 { get; set; }
        public decimal factor_adj_20000 { get; set; }
        public decimal factor_adj_25000 { get; set; }
        public decimal factor_adj_50000 { get; set; }
        public decimal factor_adj_100000 { get; set; }

        public int CUSTOMER_RECENT_PRICE_FLAG { get; set; }
        public int BASE_PRICE_FLAG { get; set; }
        public decimal BASE_PRICE { get; set; }
        public int RESERVE_FLAG { get; set; }
        public int APPLY_VARIANCE_ADJUSTMENT { get; set; }
        public string CUSTOMER_NUMBER { get; set; }
        public string CUSTOMER_PART_NUMBER { get; set; }
        public string VALUE_STREAM_ADJ { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PRODUCT_GROUP { get; set; }
        public string INTERNAL_PART_NUMBER { get; set; }
        public string OTHER_ADJ { get; set; }
        public int LEAD_TIME_FLAG { get; set; }
        public int MINIMUM_ORDER_QUANTITY_FLAG { get; set; }
        public int MINIMUM_ORDER_QUANTITY { get; set; }
        public int? QUANTITY_BREAK_RANGE_END { get; set; }
        public int? QUANTITY_BREAK_RANGE_START { get; set; }


        public decimal Demand { get; set; }
        public string CostTypeFlag { get; set; }

        public string ProductGroup { get; set; }
        public string ProductCode { get; set; }
        public string FocusFactory { get; set; }
        public int AvailQty { get; set; }
        public string GetFirstOp { get; set; }
        public string ValueStream { get; set; }
        public string ProductType { get; set; }
        public string Other { get; set; }
        public int QtyAvail { get; set; }
        public int ibitm { get; set; }
        public string PartRevision { get; set; }


        //additional costing details
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal total { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal material { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal setup { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal labor { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal lbr_voh { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal mch_voh { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal lbr_foh { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal mch_foh { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal osp { get; set; }

        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal fznA1Net { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal fznA2Net { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal fznB1Net { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal fznB2Net { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal fznc1net { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal fznc3net { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal fznD0Net { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal fznD2Net { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal fznc2net { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal fznc4net { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal fznSUTotalNet { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00000}", ApplyFormatInEditMode = true)]
        public decimal fznLCTotal { get; set; }

        public decimal totalCPQtyCostVar { get; set; }
        public decimal totalCPQtyCostSU { get; set; }
        public decimal totalCPQtyCostFOH { get; set; }

        public string HeaderComment { get; set; }
        public string CustomerNo { get; set; }
        public string WBCustomerName { get; set; }
        public string ShipTooName { get; set; }
        public int ShipTooNum { get; set; }
        public string SoldTooName { get; set; }
        public int SoldTooNum { get; set; }
        public string CustPO { get; set; }
        public string LeadTimeComment { get; set; }
        public string DueDate { get; set; }
        //public string QuoteUploadMethod {get;set;}
        public string ECFlag { get; set; }
        public int CapacityThroughput { get; set; }

    
    public List<PartDescription> PartDescs { get; set; }

        public DateTime EarliestStartDate { get; set; }
        public string DisplayeEarliestStartdDate
        {
            get
            {
                return EarliestStartDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
        }
        public DateTime EstimatedFinishDate { get; set; }
        public string DisplayedEstimatedFinishDate
        {
            get
            {
                return EstimatedFinishDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
        }
        public int TotalEstimatedLTDays { get; set; }
        public int TotalEstimatedLTWeeks { get; set; }

    }

  public class QuoteListItem
  {
    //"Sold To,Ship To,Qty Ordered,Part Number,Request Date,Sched Pick Date,Customer PO,Unit Price");
    public string division_id { get; set; }
    public int workflow_id { get; set; }
    public string customer_po { get; set; }
    public int line_num { get; set; }
    public int quantity { get; set; }
    public string part_number { get; set; }
    public decimal unit_price { get; set; }
    public string sold_to_num { get; set; }
    public string ship_to_num { get; set; }
        public DateTime approval_date { get; set; }
  }

  public class QuoteWorkBench
  {

    //public string CustPO { get; set; }
    //public string HeaderComment { get; set; }
    //public string CustomerNo { get; set; }
    //public string CustomerName { get; set; }
    //public string ShipTooName { get; set; }
    //public int ShipTooNum { get; set; }
    //public string SoldTooName { get; set; }
    //public int SoldTooNum { get; set; }
    //public string CustPO { get; set; }
    //public string LeadTimeComment { get; set; } //added

    public int QCLineNum { get; set; }

    public string QCPartNumber { get; set; }
    public string QCCustPartNumber { get; set; }
    public string QCLeadTime { get; set; }
    public string QCLineComments { get; set; }
    public int QCQuoteQty { get; set; }
    public decimal QCQuoteUnitPrice { get; set; }
    public decimal QCQuoteUnitCost { get; set; }
        public decimal QCQuoteFznUnitCost { get; set; }
        public decimal QCExtPrice { get; set; }
        public decimal QCCalcCost { get; set; }
        
    

  }

  public class QuoteWorkbenchItems
  {

    [Key]
    public int LineID { get; set; }
    public int QCLineNum { get; set; }
    public string QCPartNumber { get; set; }
    public string QCCustPartNumber { get; set; }
    public string QCLeadTime { get; set; }
    public string QCLineComments { get; set; }
    public int QCQuoteQty { get; set; }
    public decimal QCQuoteUnitPrice { get; set; }
    public decimal QCUnitCost { get; set; }
        public decimal QCFznUnitCost { get; set; }
        public decimal QCExtPrice { get; set; }
        public decimal QCCalcCost { get; set; }
    }

    public class ApprovalGenerate
  {
    public string QuoteNum { get; set; }

    public string CustPO { get; set; }
    public string HeaderComment { get; set; }
    public string CustomerNo { get; set; }
    public string CustomerName { get; set; }
    public string DueDate { get; set; }

    public int QCLineNum { get; set; }

    public string QCPartNumber { get; set; }
    public string QCLineComments { get; set; }
    public int QCQuoteQty { get; set; }
    public decimal QCQuoteUnitPrice { get; set; }
    public decimal QCExtPrice { get; set; }


  }

  public class CostReferenceList
  {
    public Int32 Cost_ID { get; set; }
    public string Cost_Description { get; set; }
  }
  public class LTAInfo
  {
    public int MinQty { get; set; }
    public int MaxQty { get; set; }
    public decimal AMU { get; set; }
    public string CustomerName { get; set; }
    public int CurrentInventory { get; set; }

    public string LTAType { get; set; }

  }
  public class LTAPart
  {
   
    public decimal Price { get; set; }
    public string CustomerName { get; set; }
    public int CustomerNumber { get; set; }
    public string PartNumber { get; set; }
    

  }
  public class Comment
  {
    public string PartComment { get; set; }
    public int PostID { get; set; }
    public DateTime? PostDate { get; set; }
    public string PartNumber { get; set; }
    public string Division { get; set; }
    public string UserName { get; set; }
    public string RoleDesc { get; set; }
    public string SalesOrderNum { get; set; }
    public string QuoteNum { get; set; }
    public string OrderVal { get; set; }
    //public string current { get; set; }
  }
    //public class DeleteComment
    //{
    //    public int PostID { get; set; }
    //}

    public class CurrentLoadByWorkorder
    {
        [Key]

        public int Load_id { get; set; }
        public int Workorder { get; set; }
        public int Item_id { get; set; }
        public string ValueStream { get; set; }
        public DateTime StartDate { get; set; }
        public string DisplayedStartDate
        {
            get
            {
                return StartDate.ToString("dd MMMMM yyyy", CultureInfo.InvariantCulture);
            }
        }
        public DateTime EndDate { get; set; }
        public string DisplayedEndDate
        {
            get
            {
                return EndDate.ToString("dd MMMM yyyy", CultureInfo.InvariantCulture);
            }
        }
        public int Quantity { get; set; }

    }

    public class SalesOrder
  {
    [Key]

    public int SOLineNum { get; set; }
    public string SOOrderNum { get; set; }
    //public string SOOrderType { get; set; }
    //public DateTime SOOrderDate { get; set; }
    public DateTime SOOrderDate { get; set; }
    public string DisplayedDate
    {
      get
      {
        return SOOrderDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
      }
    }
    public int SOQuantity { get; set; }
    public decimal SOUnitPrice { get; set; }
    public string SOStatus { get; set; }
    public string SOCustomerName { get; set; }
    public int SOCustomerNum { get; set; }
    //public string PartComment { get; set; }
    public decimal SOExtPrice { get; set; }
    public decimal SOUnitCost { get; set; }
  
  public bool IsNegative { get; set; }
	}

    
 
    public class Competitor
  {

    public string CompetitorName { get; set; }
    public string CompetitorStatus { get; set; }
    public decimal CompetitorPrice { get; set; }
    public string CompetitorPartNum { get; set; }
  }

  public class CustomerPart
  {

    public string CustomerPartNum { get; set; }
    public string CustomerPartNumRev { get; set; }
    //public string CustomerNumber { get; set; }
  }

  public class GetPricingInfo
  {
    public GenerateQuoteHeader GenerateQuoteHeader { get; set; }
    public List<SalesOrder> SalesOrders { get; set; }

    // public List<Competitor> Competitors { get; set; }
    public List<Quote> Quotes { get; set; }
    public List<Competitor> Competitors { get; set; }
    public string CompetitorConcat { get; set; }
    public List<ComponentFound> Components { get; set; }
    public List<Comment> Comments { get; set; }
    //public Comment ComDelete { get; set; }
    public NewCommentDetail NewComment { get; set; }
    public int CommentCount { get; set; }
    //public int LTACount { get; set; }
    public List<LTAInfo> LTAs { get; set; }
    public List<ActualCost> ActualCosts { get; set; }
    //public GenerateQuote GenerateQuote { get; set; }
    public QuoteWorkBench QuoteWorkbench { get; set; }

    public List<Demand> DemandList { get; set; }
  }
  public class Demand
  {
    public string order_num { get; set; }
    public int line_num { get; set; }

    public int remaining_qty { get; set; }
  }

  public class ActualCost
  {
    public string OrderType { get; set; }
    public string OrderNum { get; set; }
    public DateTime CompletionDate { get; set; }
    public string DisplayedDate
    {
      get
      {
        return CompletionDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
      }
    }
    public int QtyShipped { get; set; }
    public decimal StdQtyWOQty { get; set; }
    public decimal ActCost { get; set; }
    public decimal CostAdjFactor { get; set; }

  }

  public class NewCommentDetail
  {
    public string NewPartComment { get; set; }
  }


  public class GetStatusData
  {
    public int eo_list { get; set; }
    public int active_flag { get; set; }
    public int competition { get; set; }
    public string lead_time { get; set; }
    public int components { get; set; }
    public int demand { get; set; }
    public decimal actual_cost { get; set; }
  }

  public class Quote
  {
    [Key]
    public int QHLineNum { get; set; }
    public string QHQuoteNum { get; set; }
    public DateTime QHQuoteDate { get; set; }
    public int QHQuantity { get; set; }
    public decimal QHUnitPrice { get; set; }
    public decimal QHExtPrice { get; set; }
    public string QHMargin { get; set; }
    public string QHCustomerName { get; set; }
    public int QHCustomerNum { get; set; }
    public string QHStatus { get; set; }
    public string QHPartNumber { get; set; }
    public DateTime WQuoteDate { get; set; }
    public decimal QHUnitCost { get; set; }
  }
  public class ComponentFound
  {
    [Key]
    //public int CompID { get; set; }
    public string ComponentNum { get; set; }
    public string ComponentDesc { get; set; }
    public string ComponentStock { get; set; }
    public decimal ComponentACQ { get; set; }
    public decimal ComponentMfgLT { get; set; }
    public int ComponentCumLT { get; set; }

    public int ComponentQty { get; set; }
    public int ComponentOnHand { get; set; }
    public int ComponentAvailQt { get; set; }
    public decimal ComponentCost { get; set; }
    public decimal ComponentCostFzn { get; set; }
    public decimal ComponentCostFznRecalc { get; set; }
    public string ComponentUOM { get; set; }
    //added to list
    public decimal CostPerEachQuoteQty { get; set; }
    public decimal QuoteQty { get; set; }
    //public string ComponentCostRef { get; set; }

    public int ItemKey { get; set; }
    public decimal CompTotal { get; set; }
    public decimal CompMtlCost { get; set; }
    public decimal CompSetup { get; set; }
    public decimal CompLabor { get; set; }
    public decimal CompLbrVOH { get; set; }
    public decimal CompMchVOH { get; set; }
    public decimal CompLbrFOH { get; set; }
    public decimal CompMchFOH { get; set; }
    public decimal CompOSP { get; set; }
    public decimal CompSUTotal { get; set; }
    public decimal FznCPQtyCostSU { get; set; }
    public decimal FznCPQtyCostVar { get; set; }
    public decimal FznCPQtyCostFOH { get; set; }

    public decimal ChangeQuantity { get; set; }

  }

    public class UploadWorkbenchPricing
    {
        public string item_num { get; set; }
        public int qty_break { get; set; }
        public decimal unit_price { get; set; }
    }
    //public class UploadFile
    //{
    //    public string QuoteUploadMethod {get;set;}
    //    public string file_path_var { get; set; }
    //}

    public class OriginalPricingTool
  {
    public GenerateQuoteHeader GenerateQuoteHeader { get; set; }
    public List<SalesOrder> SalesOrders { get; set; }
    public List<CurrentLoadByWorkorder> cl { get; set; }
    public List<QuoteWorkbenchItems> QuoteWorkbenchList { get; set; }
        public List<UploadWorkbenchPricing> AutoPriceList { get; set; }
    public List<Quote> Quotes { get; set; }
    public List<Competitor> Competitors { get; set; }
    public List<CustomerPart> CustParts { get; set; }
    public string CompetitorConcat { get; set; }
    public List<ComponentFound> Components { get; set; }
    public List<Comment> Comments { get; set; }
    public List<LTAPart> LTAParts { get; set; }
    public NewCommentDetail NewComment { get; set; }
    public int CommentCount { get; set; }
    //public int LTACount { get; set; }
    public List<LTAInfo> LTAs { get; set; }
    public List<ActualCost> ActualCosts { get; set; }
    //public GenerateQuote GenerateQuote { get; set; }
    public QuoteWorkBench QuoteWorkbench { get; set; }
    //public List<SelectListItem> Costing { get; set; }
    //public string CostYearDesc { get; set; }
    //public string CostYearID { get; set; }
    public List<Demand> DemandList { get; set; }
    //public AdditionalPartInfo AdditionalPartInfo {get;set;}

    //added


  }

  public class MobilePricingTool
  {
    public GenerateQuoteHeader GenerateQuoteHeader { get; set; }
    public List<SalesOrder> SalesOrders { get; set; }
    public List<Quote> Quotes { get; set; }
    public List<Competitor> Competitors { get; set; }
    public List<CustomerPart> CustParts { get; set; }
    public string CompetitorConcat { get; set; }
    public List<ComponentFound> Components { get; set; }
    public List<Comment> Comments { get; set; }
    public NewCommentDetail NewComment { get; set; }
    public int CommentCount { get; set; }
    //public int LTACount { get; set; }
    public List<LTAInfo> LTAs { get; set; }
    public List<ActualCost> ActualCosts { get; set; }
    public List<Demand> DemandList { get; set; }
    //public AdditionalPartInfo AdditionalPartInfo {get;set;}


  }

  public class CostYears
  {
    //List<CostYears> Costing { get; set; }
    public string CostYearDesc { get; set; }
    public Int32 CostYearID { get; set; }
  }

  public class PartDescription
  {
    [Key]
    public int PartID { get; set; }
    public string PartNumber { get; set; }
    //public string CustomerPartNumber { get; set; }
    public string PartDesc { get; set; }
  }

    public class GetOrderThreshold
    {
        public string BranchPlant { get; set; }
        public decimal OrderMinimum { get; set; }
    }
        public class GetPricingAPI
  {
    public string BranchPlant { get; set; }
    public string CustomerNumber {get;set;}
    public string ItemNumber { get; set; }
    public string CustomerPartNumber { get; set; }
    public string Price { get; set; }
    public int Quantity { get; set; }
    public int MOQ { get; set; }
    public decimal LeadTime { get; set; }
        public int StockQuantity { get; set; }

  }
    public class GetPricingAPIList
    {
        public List<GetPricingAPI> pricingList { get; set; }
    }



}
   


