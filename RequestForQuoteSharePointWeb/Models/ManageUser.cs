﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace RequestForQuoteSharePointWeb.Models
{
    public class ManageUser
    {

        [Key]
        public int UserId { get; set; }
        
        public string Username { get; set; }
        public string Email { get; set; }
        public int Org_ID { get; set; }
        public string Organization { get; set; }
        public int Role_ID { get; set; }
        public string Role { get; set; }

        [Display(Name ="Active Status")]
        public string ActiveFlag { get; set; }
        public int multi_org_id { get; set; }
        [Display(Name = "Access To Multiple Organizations")]
        
        public string MultiOrg { get; set; }
        
    }
    public class UserList
    {
        public List<ManageUser> Users { get; set; }
        public UserList()
        {
            Users = new List<ManageUser>();
        }
    }

}
