﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RequestForQuoteSharePointWeb.Models
{
    public class Roles : Controller
    {
        public List<SelectListItem> RoleList { get; set; }
        public int RoleID { get; set; }
            public string RoleDesc { get; set; }
        
    }
}