using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RequestForQuoteSharePointWeb.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace RequestForQuoteSharePointWeb.Models
{
    public class ViewQueue
    {
        [Key]
        
        public int Request_Id { get; set; }
        public string Series { get; set; }
        public string Priority { get; set; }
        //public DateTime? DueDate { get; set; }
        public DateTime? DueDate { get; set; }
        public string DisplayedDate
        {
            get
            {
                return DueDate.Value.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
        }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public string CustomerName { get; set; }
        public string OpportunityType { get; set; }
        public string ProductType { get; set; }
        public string Comments { get; set; }
        public string CurrentAssignment { get; set; }
        //public string OptionalAssignment { get; set; }
        public string Organization { get; set; }

    }

    public class ViewQuoteQueue
    {
        [Key]

        public int QuoteID { get; set; }
        public string QuoteNum { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DueDate { get; set; }
        public string DisplayedDueDate
        {
            get
            {
              return DueDate.Value.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
        }
        public DateTime CreationDate { get; set; }
        public string DisplayedDate
        {
            get
            {
                return CreationDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
        }
        public string CustomerName { get; set; }
        public string AssignedRoleDesc { get; set; }
        public string WorkflowTypeDesc { get; set; }
        public string StatusDesc { get; set; }
        public string Division { get; set; }
    }
}
