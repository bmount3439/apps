using System;
using System.Data.SqlClient;

namespace RequestForQuoteSharePointWeb.Models
{
    public class RequestLineViewModel
    {
        public int request_line_id;
        public string size_num;
        public string other_source;
        public string platform;
        public string material_type_desc;
        public decimal est_revenue_y1;
        public decimal market_price;
        public decimal est_qty_y1;
        public decimal est_revenue_y2;
        public decimal est_qty_y2;
        public decimal est_revenue_y3;
        public decimal est_qty_y3;
        public decimal est_revenue_y4;
        public decimal est_qty_y4;
        public decimal est_revenue_y5;
        public decimal est_qty_y5;
        public DateTime? due_date;
        public string priority_desc;
        public int material_type_id;
        public int priority_id;

        public RequestLineViewModel()
        {
            request_line_id = 0;
            size_num = "";
            other_source = "";
            platform = "";
            material_type_desc = "";
            est_revenue_y1 = 0;
            market_price = 0;
            est_qty_y1 = 0;
            est_revenue_y2 = 0;
            est_qty_y2 = 0;
            est_revenue_y3 = 0;
            est_qty_y3 = 0;
            est_revenue_y4 = 0;
            est_qty_y4 = 0;
            est_revenue_y5 = 0;
            est_qty_y5 = 0;
            due_date = DateTime.Now;
            priority_desc = "";
            material_type_id = 0;
        }

        public void ReadFrom(SqlDataReader reader)
        {
            request_line_id = reader.GetInt32(1);
            size_num = reader.GetString(2);
            other_source = reader.GetString(3);
            platform = reader.GetString(4);
            material_type_desc = reader.GetString(5);
            est_revenue_y1 = reader.GetDecimal(6);
            market_price = reader.GetDecimal(7);
            est_qty_y1 = reader.GetInt32(8);
            est_revenue_y2 = reader.GetDecimal(9);
            est_qty_y2 = reader.GetInt32(10);
            est_revenue_y3 = reader.GetDecimal(11);
            est_qty_y3 = reader.GetInt32(12);
            est_revenue_y4 = reader.GetDecimal(13);
            est_qty_y4 = reader.GetInt32(14);
            est_revenue_y5 = reader.GetDecimal(15);
            est_qty_y5 = reader.GetInt32(16);
            due_date = reader.GetDateTime(17);
            priority_desc = reader.GetString(18);
        }

    }
}