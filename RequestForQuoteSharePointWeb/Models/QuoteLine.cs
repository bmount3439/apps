﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RequestForQuoteSharePointWeb.Models
{
    public class QuoteLine
    {
        public int Qty { get; set; }
        public double Cost { get; set; }
        public double Margin { get; set; }
        public bool IsDynamic { get; set; }
    }
}