﻿

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RequestForQuoteSharePointWeb.Models
{

using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

using System.Data.Entity.Core.Objects;
using System.Linq;


public partial class SharePointConnectionUpdated : DbContext
{
    public SharePointConnectionUpdated()
        : base("name=SharePointConnectionUpdated")
    {

    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
        throw new UnintentionalCodeFirstException();
    }


    public virtual DbSet<ORGANIZATION> ORGANIZATIONs { get; set; }

    public virtual DbSet<PROCESS_TYPE> PROCESS_TYPE { get; set; }

    public virtual DbSet<ROLE> ROLES { get; set; }

    public virtual DbSet<ROUTE_BACK> ROUTE_BACK { get; set; }

    public virtual DbSet<STATUS> STATUS { get; set; }

    public virtual DbSet<sysdiagram> sysdiagrams { get; set; }

    public virtual DbSet<ATTACHMENT> ATTACHMENTS { get; set; }

    public virtual DbSet<MATERIAL_TYPE> MATERIAL_TYPE { get; set; }

    public virtual DbSet<OPPORTUNITY_TYPE> OPPORTUNITY_TYPE { get; set; }

    public virtual DbSet<PRIORITY> PRIORITies { get; set; }

    public virtual DbSet<PRODUCT_TYPE> PRODUCT_TYPE { get; set; }

    public virtual DbSet<REQUEST_QUOTE_HEADER> REQUEST_QUOTE_HEADER { get; set; }

    public virtual DbSet<REQUEST_QUOTE_LINES> REQUEST_QUOTE_LINES { get; set; }

    public virtual DbSet<SQL_STATEMENTS> SQL_STATEMENTS { get; set; }

    public virtual DbSet<USER> USERS { get; set; }

    public virtual DbSet<vCheck_Your_Requests> vCheck_Your_Requests { get; set; }

    public virtual DbSet<vCreate_Quote_Routing> vCreate_Quote_Routing { get; set; }

    public virtual DbSet<vManage_Users> vManage_Users { get; set; }

    public virtual DbSet<COMMENT_LINES> COMMENT_LINES { get; set; }

    public virtual DbSet<QUOTE_HEADER> QUOTE_HEADER { get; set; }

    public virtual DbSet<QUOTE_LINES> QUOTE_LINES { get; set; }

    public virtual DbSet<APPROVED_PARTS> APPROVED_PARTS { get; set; }

    public virtual DbSet<COMMENT_HEADER> COMMENT_HEADER { get; set; }

    public virtual DbSet<IMPORT_QUOTE_LINES> IMPORT_QUOTE_LINES { get; set; }

    public virtual DbSet<QUALIFICATION_TESTING_TYPE> QUALIFICATION_TESTING_TYPE { get; set; }

    public virtual DbSet<WORKFLOW_TYPE> WORKFLOW_TYPE { get; set; }

    public virtual DbSet<vQuote_View_status> vQuote_View_status { get; set; }

    public virtual DbSet<vUpdate_Status> vUpdate_Status { get; set; }

    public virtual DbSet<vView_Quote_Routing> vView_Quote_Routing { get; set; }

    public virtual DbSet<config_MARGIN> config_MARGIN { get; set; }

    public virtual DbSet<MASTER_COST> MASTER_COST { get; set; }

    public virtual DbSet<WORKBENCH_HEADER> WORKBENCH_HEADER { get; set; }

    public virtual DbSet<WORKBENCH_LINES> WORKBENCH_LINES { get; set; }

    public virtual DbSet<dT_Finance_CostYearLookup> dT_Finance_CostYearLookup { get; set; }

    public virtual DbSet<vT_Sales_LTAModifications> vT_Sales_LTAModifications { get; set; }

    public virtual DbSet<LOGIN_ACTIVITY> LOGIN_ACTIVITY { get; set; }

    public virtual DbSet<sT_ComponentCosting_JDE> sT_ComponentCosting_JDE { get; set; }

    public virtual DbSet<sT_PartDetails_JDE> sT_PartDetails_JDE { get; set; }

    public virtual DbSet<dT_ConfigurableDimensions> dT_ConfigurableDimensions { get; set; }

    public virtual DbSet<STRATEGIC_PRICING_ADJUSTMENTS> STRATEGIC_PRICING_ADJUSTMENTS { get; set; }

    public virtual DbSet<iT_Sales_LTAUpload> iT_Sales_LTAUpload { get; set; }

    public virtual DbSet<REPORTS> REPORTS { get; set; }

    public virtual DbSet<tV_Strategic_Pricing_Adjustments> tV_Strategic_Pricing_Adjustments { get; set; }

    public virtual DbSet<WORKFLOW> WORKFLOW { get; set; }

    public virtual DbSet<WORKFLOW_HISTORY> WORKFLOW_HISTORY { get; set; }


    public virtual ObjectResult<sp_InsertLTAInfo_Result> sp_InsertLTAInfo(Nullable<int> user_ID, string division, string customer_Number, string item_Number_3, string item_Number_2, string type, Nullable<System.DateTime> effective_Start_Date, Nullable<System.DateTime> effective_End_Date, Nullable<decimal> price, string uOM, Nullable<int> customer_AMU)
    {

        var user_IDParameter = user_ID.HasValue ?
            new ObjectParameter("User_ID", user_ID) :
            new ObjectParameter("User_ID", typeof(int));


        var divisionParameter = division != null ?
            new ObjectParameter("Division", division) :
            new ObjectParameter("Division", typeof(string));


        var customer_NumberParameter = customer_Number != null ?
            new ObjectParameter("Customer_Number", customer_Number) :
            new ObjectParameter("Customer_Number", typeof(string));


        var item_Number_3Parameter = item_Number_3 != null ?
            new ObjectParameter("Item_Number_3", item_Number_3) :
            new ObjectParameter("Item_Number_3", typeof(string));


        var item_Number_2Parameter = item_Number_2 != null ?
            new ObjectParameter("Item_Number_2", item_Number_2) :
            new ObjectParameter("Item_Number_2", typeof(string));


        var typeParameter = type != null ?
            new ObjectParameter("Type", type) :
            new ObjectParameter("Type", typeof(string));


        var effective_Start_DateParameter = effective_Start_Date.HasValue ?
            new ObjectParameter("Effective_Start_Date", effective_Start_Date) :
            new ObjectParameter("Effective_Start_Date", typeof(System.DateTime));


        var effective_End_DateParameter = effective_End_Date.HasValue ?
            new ObjectParameter("Effective_End_Date", effective_End_Date) :
            new ObjectParameter("Effective_End_Date", typeof(System.DateTime));


        var priceParameter = price.HasValue ?
            new ObjectParameter("Price", price) :
            new ObjectParameter("Price", typeof(decimal));


        var uOMParameter = uOM != null ?
            new ObjectParameter("UOM", uOM) :
            new ObjectParameter("UOM", typeof(string));


        var customer_AMUParameter = customer_AMU.HasValue ?
            new ObjectParameter("Customer_AMU", customer_AMU) :
            new ObjectParameter("Customer_AMU", typeof(int));


        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_InsertLTAInfo_Result>("sp_InsertLTAInfo", user_IDParameter, divisionParameter, customer_NumberParameter, item_Number_3Parameter, item_Number_2Parameter, typeParameter, effective_Start_DateParameter, effective_End_DateParameter, priceParameter, uOMParameter, customer_AMUParameter);
    }

}

}

