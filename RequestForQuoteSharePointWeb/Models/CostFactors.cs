﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace RequestForQuoteSharePointWeb.Models
{
    public class CostFactors
    {
        [Key]
        public int Factor_ID  {get;set;}
        public string PartNumber { get; set; }
        public string Description { get; set; }
        //public decimal C500_COST { get; set; }
        //public Nullable<decimal> Cost_1000 { get; set; }
        //public Nullable<decimal> Cost_2500 { get; set; }
        //public Nullable<decimal> Cost_5000 { get; set; }
        //public Nullable<decimal> Cost_10000 { get; set; }
        //public Nullable<decimal> Cost_25000 { get; set; }
        //public Nullable<decimal> Cost_50000 { get; set; }
        //public Nullable<decimal> Cost_100000 { get; set; }
        public Nullable<decimal> Factor_500 { get; set; }
        public Nullable<decimal> Factor_1000 { get; set; }
        public Nullable<decimal> Factor_2500 { get; set; }
        public Nullable<decimal> Factor_5000 { get; set; }
        public Nullable<decimal> Factor_10000 { get; set; }
        public Nullable<decimal> Factor_25000 { get; set; }
        public Nullable<decimal> Factor_50000 { get; set; }
        public Nullable<decimal> Factor_100000 { get; set; }
        public string LastEditBy { get; set; }
        //public DateTime LastEditDate { get; set; }
        public string Comments { get; set; }
        public DateTime LastEditDate { get; set; }
    }
}