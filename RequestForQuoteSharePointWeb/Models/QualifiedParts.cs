using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace RequestForQuoteSharePointWeb.Models
{
    public class QualifiedParts
    {
        [Key]
    [Display(Name = "ID")]
    public int Product_Code_ID { get; set; }
        [Display(Name = "Specification Standard")]
        public string SpecificationStandard { get; set; }
        [Display(Name = "Customer Part")]
        public string CustodianPart { get; set; }
        public string Custodian { get; set; }
        public string Manufacturer { get; set; }
        [Display(Name = "Material Type")]
        public string MaterialType { get; set; }
        public string Size { get; set; }
        public string Range { get; set; }
        public string Finish { get; set; }
    [Display(Name = "Status")]
        public string Status { get; set; }
        public string Organization { get; set; }
        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }
        public string DisplayedDate
        {
            get
            {
                return DateCreated.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
        }
        [Display(Name ="Entered By")]
        public string EnteredBy { get; set; }
        [Display(Name = "Modified By")]
        public string ModifiedBy { get; set; }
        [Display(Name = "Competitor Price")]
        public decimal? CompetitorPrice { get; set; }
        //public string CompetitorPrice { get; set; }
    }
}
