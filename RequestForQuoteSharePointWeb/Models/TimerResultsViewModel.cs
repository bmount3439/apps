﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RequestForQuoteSharePointWeb.Models
{
    public class TimerResultsViewModel
    {
        public string Name { get; set; }
        public List<long> Values { get; set; }
     }
}