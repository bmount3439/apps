
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RequestForQuoteSharePointWeb.Models
{

using System;
    using System.Collections.Generic;
    
public partial class vManage_Users
{

    public int user_id { get; set; }

    public string username { get; set; }

    public string email { get; set; }

    public string organization { get; set; }

    public string role { get; set; }

    public string active_flag { get; set; }

    public string multi_org { get; set; }

}

}
