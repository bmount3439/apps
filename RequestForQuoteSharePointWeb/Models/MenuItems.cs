﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RequestForQuoteSharePointWeb.Models
{
	public class MenuItems
	{
		public string Name { get; set; }
		public string Path { get; set; }
		public string Icon { get; set; }
		public string Description { get; set; }
		public bool IsHeroTool { get; set; }
	}
}