using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace RequestForQuoteSharePointWeb.Models
{
    public class StrategicPricingAdjustments
    {
        [Key]
        [Display(Name = "Adjustment ID")]
        public int ADJUSTMENT_ID  {get;set;}
        [Display(Name = "Division")]
        public string DIVISION { get; set; }
        [Display(Name = "Value Stream")]
        public string VALUE_STREAM { get; set; }
        [Display(Name = "Product Group")]
        public string PRODUCT_GROUP { get; set; }
        [Display(Name = "Product Code")]
        public string PRODUCT_CODE { get; set; }
        [Display(Name = "Customer Num")]
        public string CUSTOMER_NUMBER { get; set; }
        [Display(Name = "Part Number")]
        public string INTERNAL_PART_NUMBER { get; set; }
        [Display(Name = "Customer Part Number")]
        public string CUSTOMER_PART_NUMBER { get; set; }
        [Display(Name = "Recent Price Flag")]
        public bool CUSTOMER_RECENT_PRICE_FLAG { get; set; }
        [Display(Name = "Quantity Start")]
        public int QUANTITY_BREAK_RANGE_START { get; set; }
        [Display(Name = "Quantity End")]
        public int QUANTITY_BREAK_RANGE_END { get; set; }
        [Display(Name = "Other")]
        public string OTHER { get; set; }
        [Display(Name = "Base Price Flag")]
        public bool BASE_PRICE_FLAG { get; set; }
        [Display(Name = "Base Price")]
        public decimal BASE_PRICE { get; set; }
        [Display(Name = "Reserve Flag")]
        public bool RESERVE_FLAG { get; set; }
        [Display(Name ="Lead Time Flag")]
        public bool LEAD_TIME_FLAG { get; set; }
        [Display(Name = "Blacklist Flag")]
    public bool BLACKLIST_FLAG { get; set; }
    [Display(Name = "Blacklist Reasoning")]
    public string BLACKLIST_REASONING { get; set; }
    [Display(Name = "Minimum Order Quantity Flag")]
    public bool MINIMUM_ORDER_QUANTITY_FLAG { get; set; }
    [Display(Name = "Minimum Order Quantity")]
    public int MINIMUM_ORDER_QUANTITY { get; set; }
    [Display(Name = "Status")]
    public string STATUS { get; set; }
        [Display(Name = "Comment")]
        public string COMMENT { get; set; }
        [Display(Name = "Adjustment")]
        public decimal ADJUSTMENT { get; set; }
        [Display(Name = "Apply Variance Adjustment?")]
        public bool APPLY_VARIANCE_ADJUSTMENT { get; set; }
        [Display(Name = "Created By")]
        public int CreatedByID { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        [Display(Name = "Creation Date")]
        public string DisplayedCreationDate
        {
            get
            {
                return CreationDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
        }
        [Display(Name = "Last Edited By")]
        public int LastEditedByID { get; set; }
        public string LastEditedBy { get; set; }
        public DateTime LastEditDate { get; set; }
        [Display(Name = "Last Edited Date")]
        public string DisplayedEditDate
        {
            get
            {
                return LastEditDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
        }
    }
    public class SpList
    {
        public List<StrategicPricingAdjustments> StrategicAdjustments { get; set; }
        public SpList()
        {
            StrategicAdjustments = new List<StrategicPricingAdjustments>();
        }
    }
}
