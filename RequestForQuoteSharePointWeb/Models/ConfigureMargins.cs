﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace RequestForQuoteSharePointWeb.Models
{
    
    public class ConfigureMargins
    {
        [Key]
        [Display(Name ="ID")]
        public int margin_id { get; set; }

        [Display(Name = "Division")]
        public int division_id { get; set; }

        [Display(Name = "Division Applied For")]
        public int division_id_select { get; set; }
        [Display(Name = "Division Applied")]
        public int? division_id_val { get; set; }

        [Display(Name = "Apply Value Stream")]
        public int value_stream_select { get; set; }
        [Display(Name = "Value Stream Applied")]
        public string value_stream_val { get; set; }

        [Display(Name = "Apply Product Group")]
        public int product_group_select { get; set; }
        [Display(Name = "Product Group Applied")]
        public string product_group_val { get; set; }

        [Display(Name = "Apply Product Code")]
        public int product_code_select { get; set; }
        [Display(Name = "Product Code Applied")]
        public string product_code_val { get; set; }

        [Display(Name = "Apply Customer Number")]
        public int customer_number_select { get; set; }
        [Display(Name = "Customer Number Applied")]
        public string customer_number_val { get; set; }

        [Display(Name = "Apply Internal Part Number")]
        public int internal_part_number_select { get; set; }
        [Display(Name = "Internal Part Number Applied")]
        public string internal_part_number_val { get; set; }

        [Display(Name = "Apply Customer Part Number")]
        public int customer_part_number_select { get; set; }
        [Display(Name = "Customer Part Number Applied")]
        public string customer_part_number_val { get; set; }

        [Display(Name = "Apply Other")]
        public int other_select { get; set; }
        [Display(Name = "Other Applied")]
        public string other_val { get; set; }

        [Display(Name = "Margin Value")]
        public decimal? margin_value { get; set; }

        [Display(Name = "Quantity Break Range Start")]
        public int quantity_break_range_start { get; set; }
        [Display(Name = "Quantity Break Range Finish")]
        public int quantity_break_range_finish { get; set; }
    }

}
