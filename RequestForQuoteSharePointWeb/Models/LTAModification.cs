﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace RequestForQuoteSharePointWeb.Models
{
    public class LTAModifcations
    {
        [Key]
        public int LTA_ID { get; set; }

        public string Division { get; set; }

        [Display(Name ="Customer ID")]
        public int Customer_ID { get; set; }
        public string Customer_Name { get; set; }

        [Display(Name = "Customer Part Number")]
        public string Customer_Part_Number { get; set; }
        [Display(Name = "Internal Part Number")]
        public string Internal_Part_Number { get; set; }

        public int Type_ID { get; set; }
        [Display(Name = "Type")]
        public string Type { get; set; }

        [Display(Name = "Effective Start Date")]
        public DateTime StartDate { get; set; }
        public string DisplayedDate_Start
        {
            get
            {
                return StartDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
        }
        [Display(Name = "Effective End Date")]
        public DateTime EndDate { get; set; }
        public string DisplayedDate_End
        {
            get
            {
                return EndDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
        }
        public decimal Price { get; set; }
        public string UOM { get; set; }
        public int AMU { get; set; }
        public int Open_Sales_Orders { get; set; }
        public int Total_Usage { get; set; }

        [Display(Name = "Total Forecasted")]
        public int? Total_Forecast { get; set; }

        [Display(Name = "Last Updated")]
        public DateTime Last_Updated_Date { get; set; }

        [Display(Name = "Last Updated")]
        public string DisplayedDate_LastUpdate
        {
            get
            {
                return Last_Updated_Date.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
        }
        public int Last_Updated_By { get; set; }
        [Display(Name = "Last Updated By")]
        public string Last_Updated_By_Name { get; set; }
        
    }
}