using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RequestForQuoteSharePointWeb.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace RequestForQuoteSharePointWeb.Models
{
    public class ViewQuoteModel
    {
    [Key]
        public int QuoteID { get; set; }
        public string QuoteNum { get; set; }
        public string ShipTooName { get; set; }
        public string ShipTooNum { get; set; }
        public string SoldTooName { get; set; }
        public string SoldTooNum { get; set; }
        public string LeadTimeComments { get; set; }
        public string CustPO { get; set; }
        public string HeaderComment { get; set; }
        public string DueDate { get; set; }
        //public string DisplayedDueDate
        //{
        //    get
        //    {
        //        return DueDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
        //    }
        //}
        public string DueDateBackFromCustomer { get; set; }
    public string BuyerName { get; set; }
    public string BuyerEmail { get; set; }
    public string Notice { get; set; }
    public string FooterString { get; set; }
    
    //public string DisplayedDueDate
    //{
    //  get
    //  {
    //    return DueDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
    //  }
    //}
    public DateTime CreationDate { get; set; }
        public string DisplayedDate
        {
            get
            {
                return CreationDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
        }

        public string CreatedBy { get; set; }
        public string WorkflowStatus { get; set; }
        public string AssignedRoleDesc { get; set; }

        public List<SelectListItem> AssignedStatusList;
        public int Status_ID { get; set; }
        public string Status_Desc { get; set; }

        //public List<QuoteListItem> QuoteLines;
    //public List<QuoteLineItems> QuoteDisplayLines;
    public decimal TotalPrice { get; set; }
    public decimal TotalCost { get; set; }
    public decimal QuoteExtendedValue { get; set; }
    public string WorkflowComments { get; set; }
    public string ShipTooAddress { get; set; }
    public string ShipTooZipCode { get; set; }
    public string ShipTooState { get; set; }
    public string ShipTooCountry { get; set; }
    public string SoldTooAddress { get; set; }
    public string SoldTooZipCode { get; set; }
    public string SoldTooState { get; set; }
    public string SoldTooCountry { get; set; }

  }

  public class QuoteLineItems
    {

    [Key]
    public int QCLineNum { get; set; }
      public string QCPartNumber { get; set; }
    public string QCCustPartNumber { get; set; }
    public string QCLineComments { get; set; }
    public string QCPartDescription { get; set; }
    public int QCQuoteQty { get; set; }
    public decimal QCQuoteUnitCost { get; set; }
    public decimal QCQuoteUnitPrice { get; set; }
    public decimal QCExtCost { get; set; }
    public decimal QCExtPrice { get; set; }
    public decimal QCExtMargin { get; set; }
    public decimal QCMargin { get; set; }
    public decimal QCMarginPerc { get; set; }
    public string QCLeadTime { get; set; }
        public string QCCostReference { get; set; }
        public decimal QCQuoteCalcUnitCost { get; set; }

  }
        public class QuoteOverview
  {
    public List<QuoteLineItems> QuoteDisplayLines;
    public ViewQuoteModel ViewQuoteModel;

  }

    

    //public class QuoteListItem
    //    {
    //    public int workflow_id { get; set; }
    //        //public int line_num { get; set; }
    //        //public int quantity { get; set; }
    //        //public string part_number { get; set; }
    //        //public decimal unit_price { get; set; }
    //        //public string customer_po { get; set; }

            
    //    }
}
