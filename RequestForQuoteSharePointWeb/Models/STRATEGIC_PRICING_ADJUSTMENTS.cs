
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RequestForQuoteSharePointWeb.Models
{

using System;
    using System.Collections.Generic;
    
public partial class STRATEGIC_PRICING_ADJUSTMENTS
{

    public int ADJUSTMENT_ID { get; set; }

    public string DIVISION { get; set; }

    public string VALUE_STREAM { get; set; }

    public string PRODUCT_GROUP { get; set; }

    public string PRODUCT_CODE { get; set; }

    public string CUSTOMER_NUMBER { get; set; }

    public string INTERNAL_PART_NUMBER { get; set; }

    public string CUSTOMER_PART_NUMBER { get; set; }

    public string OTHER { get; set; }

    public string STATUS { get; set; }

    public string COMMENT { get; set; }

    public decimal ADJUSTMENT { get; set; }

    public int APPLY_VARIANCE_ADJUSTMENT { get; set; }

    public int BLACKLIST_FLAG { get; set; }

    public string BLACKLIST_REASONING { get; set; }

    public int CREATED_BY_ID { get; set; }

    public System.DateTime CREATION_DATE { get; set; }

    public int LAST_EDIT_ID { get; set; }

    public System.DateTime LAST_EDIT_DATE { get; set; }

    public int QUANTITY_BREAK_RANGE_START { get; set; }

    public int QUANTITY_BREAK_RANGE_END { get; set; }

    public int CUSTOMER_RECENT_PRICE_FLAG { get; set; }

    public int BASE_PRICE_FLAG { get; set; }

    public decimal BASE_PRICE { get; set; }

    public int RESERVE_FLAG { get; set; }

    public int LEAD_TIME_FLAG { get; set; }

    public int MINIMUM_ORDER_QTY_FLAG { get; set; }

    public int MINIMUM_ORDER_QUANTITY { get; set; }

}

}
