﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RequestForQuoteSharePointWeb.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace RequestForQuoteSharePointWeb.Models
{
    public class ViewRequestModel
    {
        
        public int RequestID { get; set; }
        public string RequestedBy { get; set; }
        public string PartSeries { get; set; }
        public string CustomerName { get; set; }
        public string Comments { get; set; }
        public bool PrototypeRequired { get; set; }
        //public string PrototypeRequiredDesc { get; set; }
        // public string QualificationRequired { get; set; }
        public string OptionalAssignment { get; set; }
        public string BuyersName { get; set; }
        public string ContactInfo { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public string OptionalAssignee { get; set; }
        public List<SelectListItem> ProductTypecList { get; set; }
        public int Product_Type_ID { get; set; } //set to high or low when passed to supervisor
        public string Product_Type_Desc { get; set; }
        public List<SelectListItem> RoleTypeList { get; set; }
        public int Role_ID { get; set; }
        public string Role_Desc { get; set; }

        //allow user to see available statuses and update them as applicable
        public List<SelectListItem> UpdateStatusList { get; set; }
        public int Status_ID { get; set; }
        public string Status_Desc { get; set; }

        public List<SelectListItem> QualificationTestingList { get; set; }
        public int Qualification_Testing_Type_ID { get; set; }
        public string Qualification_Testing_Type_Desc { get; set; }

        //public List<SelectListItem> EmailList { get; set; }
        //public string email_string { get; set; }

        public List<SelectListItem> OpportunityTypesList;
        public int Opportunity_Type_ID { get; set; }
        public string Opportunity_Type_Desc { get; set; }

        //lines
        public List<SelectListItem> LineList { get; set; }
        public int LineID { get; set; }

        //public DateTime? CreationDate { get; set; }
        //public string Size { get; set; }
        //public string OtherSource { get; set; }
        //public string Platform { get; set; }
        //public decimal MarketPrice { get; set; }
        //public decimal EstRevenueYr1 { get; set; }
        //public decimal EstRevenueYr2 { get; set; }
        //public decimal EstRevenueYr3 { get; set; }
        //public decimal EstRevenueYr4 { get; set; }
        //public decimal EstRevenueYr5 { get; set; }
        //public int EstQtyYr1 { get; set; }
        //public int EstQtyYr2 { get; set; }
        //public int EstQtyYr3 { get; set; }
        //public int EstQtyYr4 { get; set; }
        //public int EstQtyYr5 { get; set; }
        //public DateTime? DueDate { get; set; }
        ////public string BluePrintAttachment { get; set; }
        ////public string OtherAttachment { get; set; }
        //public List<SelectListItem> MaterialTypeList { get; set; }
        //public int Material_Type_ID { get; set; }
        //public string Material_Type_Desc { get; set; }
        ////public Dictionary<int, string> MaterialTypeDescList { get; set; }
        //public List<SelectListItem> PriorityTypeDescList;
        //public string Priority_Type_Desc { get; set; }
        //public int Priority_Type_ID { get; set; }

        //lines
        public string Size1 { get; set; }
        public string OtherSource1 { get; set; }
        public string Platform1 { get; set; }
        public decimal MarketPrice1 { get; set; }
        public decimal EstRevenueYr11 { get; set; }
        public decimal EstRevenueYr21 { get; set; }
        public decimal EstRevenueYr31 { get; set; }
        public decimal EstRevenueYr41 { get; set; }
        public decimal EstRevenueYr51 { get; set; }
        public decimal EstQtyYr11 { get; set; }
        public decimal EstQtyYr21 { get; set; }
        public decimal EstQtyYr31 { get; set; }
        public decimal EstQtyYr41 { get; set; }
        public decimal EstQtyYr51 { get; set; }
        public DateTime? DueDate1 { get; set; }

        public List<SelectListItem> MaterialTypeList1 { get; set; }
        public int Material_Type_ID1 { get; set; }
        public string Material_Type_Desc1 { get; set; }

        public List<SelectListItem> PriorityTypeDescList1 { get; set; }
        public int Priority_Type_ID1 { get; set; }
        public string Priority_Type_Desc1 { get; set; }

        //lines2
        public string Size2 { get; set; }
        public string OtherSource2 { get; set; }
        public string Platform2 { get; set; }
        public decimal MarketPrice2 { get; set; }
        public decimal EstRevenueYr12 { get; set; }
        public decimal EstRevenueYr22 { get; set; }
        public decimal EstRevenueYr32 { get; set; }
        public decimal EstRevenueYr42 { get; set; }
        public decimal EstRevenueYr52 { get; set; }
        public decimal EstQtyYr12 { get; set; }
        public decimal EstQtyYr22 { get; set; }
        public decimal EstQtyYr32 { get; set; }
        public decimal EstQtyYr42 { get; set; }
        public decimal EstQtyYr52 { get; set; }
        public DateTime? DueDate2 { get; set; }

        public List<SelectListItem> MaterialTypeList2 { get; set; }
        public int Material_Type_ID2 { get; set; }
        public string Material_Type_Desc2 { get; set; }

        public List<SelectListItem> PriorityTypeDescList2 { get; set; }
        public int Priority_ID2 { get; set; }
        public string Priority_Desc2 { get; set; }

        //lines3
        public string Size3 { get; set; }
        public string OtherSource3 { get; set; }
        public string Platform3 { get; set; }
        public decimal MarketPrice3 { get; set; }
        public decimal EstRevenueYr13 { get; set; }
        public decimal EstRevenueYr23 { get; set; }
        public decimal EstRevenueYr33 { get; set; }
        public decimal EstRevenueYr43 { get; set; }
        public decimal EstRevenueYr53 { get; set; }
        public decimal EstQtyYr13 { get; set; }
        public decimal EstQtyYr23 { get; set; }
        public decimal EstQtyYr33 { get; set; }
        public decimal EstQtyYr43 { get; set; }
        public decimal EstQtyYr53 { get; set; }
        public DateTime? DueDate3 { get; set; }

        public List<SelectListItem> MaterialTypeList3 { get; set; }
        public int Material_Type_ID3 { get; set; }
        public string Material_Type_Desc3 { get; set; }

        public List<SelectListItem> PriorityTypeDescList3 { get; set; }
        public int Priority_ID3 { get; set; }
        public string Priority_Desc3 { get; set; }

        //lines4
        public string Size4 { get; set; }
        public string OtherSource4 { get; set; }
        public string Platform4 { get; set; }
        public decimal MarketPrice4 { get; set; }
        public decimal EstRevenueYr14 { get; set; }
        public decimal EstRevenueYr24 { get; set; }
        public decimal EstRevenueYr34 { get; set; }
        public decimal EstRevenueYr44 { get; set; }
        public decimal EstRevenueYr54 { get; set; }
        public decimal EstQtyYr14 { get; set; }
        public decimal EstQtyYr24 { get; set; }
        public decimal EstQtyYr34 { get; set; }
        public decimal EstQtyYr44 { get; set; }
        public decimal EstQtyYr54 { get; set; }
        public DateTime? DueDate4 { get; set; }

        public List<SelectListItem> MaterialTypeList4 { get; set; }
        public int Material_Type_ID4 { get; set; }
        public string Material_Type_Desc4 { get; set; }

        public List<SelectListItem> PriorityTypeDescList4 { get; set; }
        public int Priority_ID4 { get; set; }
        public string Priority_Desc4 { get; set; }
        //lines5
        public string Size5 { get; set; }
        public string OtherSource5 { get; set; }
        public string Platform5 { get; set; }
        public decimal MarketPrice5 { get; set; }
        public decimal EstRevenueYr15 { get; set; }
        public decimal EstRevenueYr25 { get; set; }
        public decimal EstRevenueYr35 { get; set; }
        public decimal EstRevenueYr45 { get; set; }
        public decimal EstRevenueYr55 { get; set; }
        public decimal EstQtyYr15 { get; set; }
        public decimal EstQtyYr25 { get; set; }
        public decimal EstQtyYr35 { get; set; }
        public decimal EstQtyYr45 { get; set; }
        public decimal EstQtyYr55 { get; set; }
        public DateTime? DueDate5 { get; set; }

        public List<SelectListItem> MaterialTypeList5 { get; set; }
        public int Material_Type_ID5 { get; set; }
        public string Material_Type_Desc5 { get; set; }

        public List<SelectListItem> PriorityTypeDescList5 { get; set; }
        public int Priority_ID5 { get; set; }
        public string Priority_Desc5 { get; set; }



    }
}