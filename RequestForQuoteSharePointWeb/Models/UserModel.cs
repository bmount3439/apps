﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RequestForQuoteSharePointWeb.Models
{
    public class UserModel
    {
        public  string username { get; set; }
        public  string PartNumber { get; set; }
        public  int user_id { get; set; }
        public  string QCPartNumber { get; set; }
        public string division { get; set; }
        
    }
}