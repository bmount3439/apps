
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RequestForQuoteSharePointWeb.Models
{

using System;
    using System.Collections.Generic;
    
public partial class ROUTE_BACK
{

    public int ROUTE_BACK_ID { get; set; }

    public string ROUTE_BACK_REASON { get; set; }

    public Nullable<int> ORG_ID { get; set; }

    public Nullable<int> ROUTE_BACK_TO_USER_ID { get; set; }

    public Nullable<int> PREVIOUS_ASSIGNED_TO_USER_ID { get; set; }

    public Nullable<int> REV_ID { get; set; }

    public Nullable<System.DateTime> ROUTE_BACK_DATE { get; set; }

}

}
