﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace RequestForQuoteSharePointWeb.Models
{
    //public class CostYears
    //{
    //    //List<CostYears> Costing { get; set; }
    //    public string Cost_Year_Desc { get; set; }
    //    public string Cost_Year_ID { get; set; }
    //}
    public class OrigPricing
    {
        public List<OrigPricing> Costing { get; set; }
        public string CustomerPartNum { get; set; }
        public string PartNumber { get; set; }
        public int QuoteQuantity { get; set; }

    }

}