﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace RequestForQuoteSharePointWeb.Services
{
  
    public class CodeTimer
    {
        public const string TIMER_PREFIX = "_TimerList:";
        protected const int MAX_STORAGE = 300;
        protected string Name {  get ; set ; }
        protected long CounterValue { get; set; }

        protected Stopwatch Counter { get; set; }
        public CodeTimer( string name )
        {
            this.Name = TIMER_PREFIX + name;
            this.Counter = new Stopwatch();
        }

        public void Start()
        {
            this.Counter.Start();
        }

        public void Stop( HttpContextBase ctx )
        {
            this.Counter.Stop();

            List<long> values = ctx.Application[this.Name] as List<long>;
            if( values == null )
                values = new List<long>();
            if (values.Count + 1 > MAX_STORAGE)
                values.RemoveAt(0);
            values.Add(this.Counter.ElapsedMilliseconds);
            ctx.Application[this.Name] = values;
        }

    }
}