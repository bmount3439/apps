using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Configuration;
using System.Data.Odbc;
using System.Data.SqlClient;
using RequestForQuoteSharePointWeb.Models;
using RequestForQuoteSharePointWeb.Services;


namespace RequestForQuoteSharePointWeb.Controllers
{
    public class ValuesController : ApiController
    {


        [AllowAnonymous]
        public IEnumerable<GetPricingAPI> Get(int requestID = 0, string BranchPlant = "", string CustomerNumber = null, string PartNumber = null, int Quantity = 0, string CustomerPartNumber = null, int StockQuantity = 0) //, string CustomerNumber, string PartNumber,int Quantity
        {



            OriginalPricingTool model = new OriginalPricingTool();
            //GenerateQuoteHeader gqhmodel = new GenerateQuoteHeader();


            try
            {
                OdbcConnection cnn_odbc;
                cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString);
                cnn_odbc.Open();
                cnn_odbc.Dispose();
                cnn_odbc.Close();
            }
            catch (Exception)
            {
                // return an error
            }

            string item_num = PartNumber;
            string division = BranchPlant;
            //string CustomerPartNumber = "";
            int history_flag = 0;
            string true_price;
            decimal leadtime = 25;
            int MOQ = 10;
            GenerateQuoteHeader gqh = new GenerateQuoteHeader();
            List<LTAInfo> LTAs = new List<LTAInfo>();
            List<LTAPart> LTAParts = new List<LTAPart>();
            List<Quote> quotes = new List<Quote>();
            List<ComponentFound> Components = new List<ComponentFound>();
            List<SalesOrder> model2 = new List<SalesOrder>();
            List<Comment> Comments = new List<Comment>();
            List<ActualCost> ActCost = new List<ActualCost>();
            List<Competitor> Comps = new List<Competitor>();
            List<CustomerPart> CustParts = new List<CustomerPart>();
            List<QuoteWorkBench> quoteworkbench = new List<QuoteWorkBench>();
            List<QuoteWorkbenchItems> bench = new List<QuoteWorkbenchItems>();

            gqh.MINIMUM_ORDER_QUANTITY = 10;
            MOQ = gqh.MINIMUM_ORDER_QUANTITY;
            string cost_reference_id = "07";
            gqh.QuoteQuantity = Quantity;

            if (Quantity < MOQ)
            {
                Quantity = 10;
            }

            //what is the customer part number
            using (OdbcConnection cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString))
            {
                cnn_odbc.Open();
                string sqlScript = "";

                if (CustomerPartNumber == null)
                {


                    sqlScript = @"select	DISTINCT ifnull(xref.ivcitm,ibaitm) CustPart, xref.ivxrt ibmerl
                                        from PRODDTA.f4102 ib join PRODDTA.f4101 itm on ibitm = imitm
                              left outer join proddta.F4104 xref on ibitm = xref.ivitm
                                        where  1 = 1
                                        and trim(iblitm)=? and ivan8=?";

                    using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                    {
                        command.Parameters.Add(new OdbcParameter("@item_num", item_num.Trim()));

                        command.Parameters.Add(new OdbcParameter("@customer_number", CustomerNumber));


                        using (var reader = command.ExecuteReader())
                        {
                            try
                            {
                                if (reader.HasRows)
                                {

                                    while (reader.Read())
                                    {
                                        CustomerPartNumber = reader.IsDBNull(0) ? "" : (string)reader.GetValue(0);
                                    }
                                }
                            }



                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
                else
                {
                    sqlScript = @"select	DISTINCT iblitm item_num, xref.ivxrt ibmerl
                                        from PRODDTA.f4102 ib join PRODDTA.f4101 itm on ibitm = imitm
                              left outer join proddta.F4104 xref on ibitm = xref.ivitm
                                        where  1 = 1
                                        and xref.ivcitm=? ";

                    using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                    {
                        command.Parameters.Add(new OdbcParameter("@customer_part_num", CustomerPartNumber.Trim()));


                        using (var reader = command.ExecuteReader())
                        {
                            try
                            {
                                if (reader.HasRows)
                                {

                                    while (reader.Read())
                                    {
                                        PartNumber = reader.IsDBNull(0) ? "" : (string)reader.GetValue(0);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
            }

            PartNumber = PartNumber.Trim();
            item_num = PartNumber;
            if (CustomerPartNumber != null)
            {
                CustomerPartNumber = CustomerPartNumber.Trim();
            }
            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                cnn.Open();

                string sqlScript = "";



                int LTACount = 0;
                if (CustomerNumber != null)
                {
                    sqlScript = @"select count(*) from iT_Sales_LTAUpload where (item_number_2=ISNULL(@item_num,'') Or Item_Number_3=ISNULL(@cust_item_num,''))
                                and division_id in ('710000','730000','750000','720000','Q720000','734000','780000') and convert(date,effective_start_date)<=convert(date,getdate())
                                    and convert(date, Effective_End_Date)>= convert(date, getdate()) and customer_number=@customer_number";
                }
                else
                {
                    sqlScript = @"select count(*) from iT_Sales_LTAUpload where (item_number_2=ISNULL(@item_num,'') Or Item_Number_3=ISNULL(@cust_item_num,''))
                                and division_id in ('710000','730000','750000','720000','Q720000','734000','780000') and convert(date,effective_start_date)<=convert(date,getdate())
                                    and convert(date, Effective_End_Date)>= convert(date, getdate()) ";
                }

                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    command.Parameters.Add(new SqlParameter("@item_num", item_num));
                    command.Parameters.Add(new SqlParameter("@cust_item_num", CustomerPartNumber));

                    if (CustomerNumber != null)
                    {
                        command.Parameters.Add(new SqlParameter("@customer_number", CustomerNumber));
                    }

                    try
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                if (reader.Read())
                                {
                                    LTACount = reader.GetInt32(0);
                                }
                            }
                            else
                            {
                                Console.WriteLine("Failed to obtain LTA count information or no long term agreements exist.");
                            }
                            reader.Close();
                        }
                    }
                    catch
                    {
                        //return api with error
                        //    if (CustomerNumber == null)
                        //    {
                        //        CustomerNumber = "ERROR";
                        //    }
                        //    if (PartNumber == null)
                        //    {
                        //        PartNumber = "ERROR";
                        //    }
                        //    if (BranchPlant == null)
                        //    {
                        //        BranchPlant = "ERROR";
                        //    }
                        //    if (CustomerPartNumber == null)
                        //    {
                        //        CustomerPartNumber = "ERROR";
                        //    }

                        //    return new[]
                        //{
                        //              new GetPricingAPI { Price=null, MOQ=MOQ, LeadTime=leadtime,Quantity=0,CustomerPartNumber=CustomerPartNumber,BranchPlant=BranchPlant,ItemNumber=PartNumber,CustomerNumber=CustomerNumber}
                        //            };

                        //continue without LTA

                    }
                }



                if (gqh.CustomerName != null)
                {
                    sqlScript = @"select top 1 Item_Number_3, Price, Customer_Number , c.abalph as Customer_Name
                                            from iT_Sales_LTAUpload u
                                            left outer join  [PRODDTA].[Cam404].[PRODDTA].[F0101] c on 
	                                            c.aban8=u.Customer_Number
                                            where 1=1 and (item_number_2=isnull(@item_num,'') Or item_number_3=isnull(@cust_item_num,''))
                                            and convert(char,division_id) in ('710000','730000','750000','720000','Q720000','780000','734000') 
                                            and convert(date,effective_start_date)<=convert(date,getdate())
                                            and convert(date, Effective_End_Date)>= convert(date, getdate()) and customer_number=@customer_number";
                }
                else if (gqh.CustomerPartNum != null)
                {
                    sqlScript = @"select top 1 Item_Number_3, Price, Customer_Number , c.abalph as Customer_Name
                                            from iT_Sales_LTAUpload u
                                            left outer join  [PRODDTA].[Cam404].[PRODDTA].[F0101] c on 
	                                            c.aban8=u.Customer_Number
                                            where 1=1 and item_number_3=isnull(@cust_item_num,'')
                                            and convert(char,division_id) in ('710000','730000','750000','720000','Q720000','780000','734000') 
                                            and convert(date,effective_start_date)<=convert(date,getdate())
                                            and convert(date, Effective_End_Date)>= convert(date, getdate()) ;";
                }
                else
                {
                    sqlScript = @"select top 1 Item_Number_3, Price, Customer_Number , c.abalph as Customer_Name
                                            from iT_Sales_LTAUpload u
                                            left outer join  [PRODDTA].[Cam404].[PRODDTA].[F0101] c on 
	                                            c.aban8=u.Customer_Number
                                            where 1=1 and item_number_2=isnull(@item_num,'')
                                            and convert(char,division_id) in ('710000','730000','750000','720000','Q720000','780000','734000') 
                                            and convert(date,effective_start_date)<=convert(date,getdate())
                                            and convert(date, Effective_End_Date)>= convert(date, getdate()) ;";
                }
                //decimal LTAPrice = 0;
                using (var command = new SqlCommand(sqlScript, cnn))
                {

                    if (CustomerPartNumber != null)
                    {
                        SqlParameter cust_item_param = new SqlParameter
                        {
                            ParameterName = "@cust_item_num",
                            Value = CustomerPartNumber
                        };

                        command.Parameters.Add(cust_item_param);
                    }
                    if (CustomerNumber != null)
                    {
                        command.Parameters.Add(new SqlParameter("@customer_number", CustomerNumber));
                        command.Parameters.Add(new SqlParameter("@item_num", item_num));
                    }
                    using (var reader = command.ExecuteReader())
                    {
                        try
                        {
                            if (reader.HasRows)
                            {

                                while (reader.Read())
                                {

                                    gqh.LTAPartNumberPrice = reader.GetDecimal(1);

                                }
                            }
                            else
                            {
                                Console.WriteLine("Failed to obtain LTA count information.");
                            }
                        }
                        catch (Exception ex)
                        {
                        }



                        reader.Close();
                    }
                }

                int[] qty_breaks = { Quantity };
                foreach (int y in qty_breaks)
                {
                    if (CustomerNumber != null)
                    {


                        sqlScript = @"select top 1 m.MARGIN_VALUE, m.other from config_MARGIN m 
                                 left outer join organization o on m.DIVISION_ID = o.ORG_ID 
                                 WHERE 1 = 1 
                                 AND (o.DIVISION = isnull(@division_id,o.DIVISION) or m.VALUE_STREAM=isnull(@value_stream,m.Value_stream) or m.PRODUCT_GROUP=isnull(@product_group,m.product_group)
								or m.PRODUCT_CODE=isnull(@product_code,m.product_code)
								or m.CUSTOMER_NUMBER=isnull(@customer_number,m.customer_number)
								or m.INTERNAL_PART_NUMBER=isnull(@part_number,m.INTERNAL_PART_NUMBER)
                                or m.CUSTOMER_PART_NUMBER=isnull(@customer_part_number,m.CUSTOMER_PART_NUMBER)
								or m.other=isnull(@other, m.OTHER))
								and @qty_of_interest between m.QUANTITY_BREAK_RANGE_START and m.QUANTITY_BREAK_RANGE_END order by m.margin_value; ";
                    }
                    else
                    {
                        sqlScript = @"select top 1 m.MARGIN_VALUE, m.other from config_MARGIN m 
                                 left outer join organization o on m.DIVISION_ID = o.ORG_ID 
                                 WHERE 1 = 1 
                                 AND (o.DIVISION = isnull(@division_id,o.DIVISION) or m.VALUE_STREAM=isnull(@value_stream,m.Value_stream) or m.PRODUCT_GROUP=isnull(@product_group,m.product_group)
								or m.PRODUCT_CODE=isnull(@product_code,m.product_code)
								or m.INTERNAL_PART_NUMBER=isnull(@part_number,m.INTERNAL_PART_NUMBER)
                                or m.CUSTOMER_PART_NUMBER=isnull(@customer_part_number,m.CUSTOMER_PART_NUMBER)
								or m.other=isnull(@other, m.OTHER))
								and @qty_of_interest between m.QUANTITY_BREAK_RANGE_START and m.QUANTITY_BREAK_RANGE_END order by m.margin_value; ";
                    }
                    using (var command = new SqlCommand(sqlScript, cnn))
                    {
                        SqlParameter division_val = new SqlParameter
                        {
                            ParameterName = "@division_id",
                            Value = BranchPlant
                        };
                        command.Parameters.Add(division_val);

                        SqlParameter value_stream_param = new SqlParameter
                        {
                            ParameterName = "@value_stream",
                            Value = String.IsNullOrEmpty(gqh.ValueStream) ? "" : gqh.ValueStream.ToString()
                        };
                        command.Parameters.Add(value_stream_param);
                        SqlParameter product_group_param = new SqlParameter
                        {
                            ParameterName = "@product_group",
                            Value = String.IsNullOrEmpty(gqh.ProductGroup) ? "" : gqh.ProductGroup.ToString()
                        };
                        command.Parameters.Add(product_group_param);
                        SqlParameter product_code_param = new SqlParameter
                        {
                            ParameterName = "@product_code",
                            Value = String.IsNullOrEmpty(gqh.ProductCode) ? "" : gqh.ProductCode.ToString()
                        };
                        command.Parameters.Add(product_code_param);
                        if (CustomerNumber != null)
                        {
                            SqlParameter customer_number_param = new SqlParameter
                            {
                                ParameterName = "@customer_number",
                                Value = CustomerNumber
                            };
                            command.Parameters.Add(customer_number_param);
                        }
                        SqlParameter internal_part_number_param = new SqlParameter
                        {
                            ParameterName = "@part_number",
                            Value = String.IsNullOrEmpty(gqh.PartNumber) ? "" : gqh.PartNumber.ToString()
                        };
                        command.Parameters.Add(internal_part_number_param);
                        SqlParameter customer_part_number_param = new SqlParameter
                        {
                            ParameterName = "@customer_part_number",
                            Value = String.IsNullOrEmpty(CustomerPartNumber) ? "" : CustomerPartNumber.ToString()
                        };
                        command.Parameters.Add(customer_part_number_param);
                        SqlParameter other_param = new SqlParameter
                        {
                            ParameterName = "@other",
                            Value = String.IsNullOrEmpty(gqh.Other) ? "" : gqh.Other.ToString()
                        };
                        command.Parameters.Add(other_param);
                        SqlParameter qty_param = new SqlParameter
                        {
                            ParameterName = "@qty_of_interest",
                            Value = y
                        };
                        command.Parameters.Add(qty_param);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            try
                            {

                                while (reader.Read())
                                {
                                    if (reader.HasRows)
                                    {

                                        gqh.Margin = reader.GetDecimal(0);

                                    }
                                    //y++;
                                }
                            }

                            catch (Exception ex)
                            {
                                if (CustomerNumber == null)
                                {
                                    CustomerNumber = "ERROR";
                                }
                                if (PartNumber == null)
                                {
                                    PartNumber = "ERROR";
                                }
                                if (BranchPlant == null)
                                {
                                    BranchPlant = "ERROR";
                                }
                                if (CustomerPartNumber == null)
                                {
                                    CustomerPartNumber = "ERROR";
                                }

                                //return api with error
                                return new[]
                            {
                                  new GetPricingAPI { Price=null, MOQ=MOQ, LeadTime=leadtime,Quantity=Quantity,CustomerPartNumber=CustomerPartNumber,BranchPlant=BranchPlant,ItemNumber=PartNumber,CustomerNumber=CustomerNumber,StockQuantity=0}
                                };
                            }

                            finally
                            {
                                reader.Close();
                            }

                        }

                    }
                }


            }



            decimal adj_amount = 0;


            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                cnn.Open();



                int[] qty_breaks = { Quantity };
                int n = 1;
                string sqlScript;
                foreach (int y in qty_breaks)
                {
                    if (CustomerNumber != null)
                    {
                        sqlScript = @"SELECT isnull(SUM(ADJUSTMENT),0) TOTAL_ADJUSTMENT, CUSTOMER_RECENT_PRICE_FLAG, BASE_PRICE_FLAG, BASE_PRICE, RESERVE_FLAG, APPLY_VARIANCE_ADJUSTMENT
                                          , VALUE_STREAM, PRODUCT_GROUP, PRODUCT_CODE, CUSTOMER_NUMBER, INTERNAL_PART_NUMBER, CUSTOMER_PART_NUMBER, QUANTITY_BREAK_RANGE_START
                                        , QUANTITY_BREAK_RANGE_END, OTHER, LEAD_TIME_FLAG, MINIMUM_ORDER_QTY_FLAG, isnull(MINIMUM_ORDER_QUANTITY,0) MINIMUM_ORDER_QUANTITY
                                      FROM STRATEGIC_PRICING_ADJUSTMENTS where [status]=2 and DIVISION=@DIVISION 
                                   and @qty_of_interest between QUANTITY_BREAK_RANGE_START and QUANTITY_BREAK_RANGE_END
                                and internal_part_number=@internal_part_number
                                and customer_number=@customer_number
                            GROUP BY CUSTOMER_RECENT_PRICE_FLAG, BASE_PRICE_FLAG, BASE_PRICE, RESERVE_FLAG, APPLY_VARIANCE_ADJUSTMENT,VALUE_STREAM, PRODUCT_GROUP, PRODUCT_CODE
                              , CUSTOMER_NUMBER, INTERNAL_PART_NUMBER, CUSTOMER_PART_NUMBER, QUANTITY_BREAK_RANGE_START, QUANTITY_BREAK_RANGE_END, OTHER, LEAD_TIME_FLAG, MINIMUM_ORDER_QTY_FLAG, isnull(MINIMUM_ORDER_QUANTITY,0) ";
                    }
                    else
                    {
                        sqlScript = @"SELECT isnull(SUM(ADJUSTMENT),0) TOTAL_ADJUSTMENT, CUSTOMER_RECENT_PRICE_FLAG, BASE_PRICE_FLAG, BASE_PRICE, RESERVE_FLAG, APPLY_VARIANCE_ADJUSTMENT
                                          , VALUE_STREAM, PRODUCT_GROUP, PRODUCT_CODE, CUSTOMER_NUMBER, INTERNAL_PART_NUMBER, CUSTOMER_PART_NUMBER, QUANTITY_BREAK_RANGE_START
                                        , QUANTITY_BREAK_RANGE_END, OTHER, LEAD_TIME_FLAG, MINIMUM_ORDER_QTY_FLAG, isnull(MINIMUM_ORDER_QUANTITY,0) MINIMUM_ORDER_QUANTITY
                                      FROM STRATEGIC_PRICING_ADJUSTMENTS where [status]=2 and DIVISION=@DIVISION 
                                   and @qty_of_interest between QUANTITY_BREAK_RANGE_START and QUANTITY_BREAK_RANGE_END
                                and internal_part_number=@internal_part_number
                            GROUP BY CUSTOMER_RECENT_PRICE_FLAG, BASE_PRICE_FLAG, BASE_PRICE, RESERVE_FLAG, APPLY_VARIANCE_ADJUSTMENT,VALUE_STREAM, PRODUCT_GROUP, PRODUCT_CODE
                              , CUSTOMER_NUMBER, INTERNAL_PART_NUMBER, CUSTOMER_PART_NUMBER, QUANTITY_BREAK_RANGE_START, QUANTITY_BREAK_RANGE_END, OTHER, LEAD_TIME_FLAG, MINIMUM_ORDER_QTY_FLAG, isnull(MINIMUM_ORDER_QUANTITY,0) ";
                    }

                    using (var command = new SqlCommand(sqlScript, cnn))
                    {

                        if (CustomerNumber != null)
                        {
                            command.Parameters.Add(new SqlParameter("@customer_number", String.IsNullOrEmpty(CustomerNumber) ? "" : CustomerNumber.ToString()));
                        }

                        command.Parameters.Add(new SqlParameter("@internal_part_number", PartNumber));
                        command.Parameters.Add(new SqlParameter("@DIVISION", BranchPlant));
                        SqlParameter qty_param = new SqlParameter
                        {
                            ParameterName = "@qty_of_interest",
                            Value = y
                        };
                        command.Parameters.Add(qty_param);

                        using (var reader = command.ExecuteReader())
                        {
                            try
                            {

                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {

                                        if (y <= 17.5)
                                        {

                                            gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                            gqh.BASE_PRICE = reader.GetDecimal(3);
                                            gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);


                                        }
                                        else if (y > 17.5 && y <= 37.5 && n == 1)
                                        {

                                            gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                            gqh.BASE_PRICE = reader.GetDecimal(3);
                                            gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                        }
                                        else if (y > 37.5 && y <= 62.5 && n == 1)
                                        {

                                            gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                            gqh.BASE_PRICE = reader.GetDecimal(3);
                                            gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                        }
                                        else if (y > 62.5 && y <= 87.5 && n == 1)
                                        {

                                            gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                            gqh.BASE_PRICE = reader.GetDecimal(3);
                                            gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                        }
                                        else if (y > 87.5 && y <= 187.5 && n == 1)
                                        {

                                            gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                            gqh.BASE_PRICE = reader.GetDecimal(3);
                                            gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                        }
                                        else if (y > 187.5 && y <= 375 && n == 1)
                                        {

                                            gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                            gqh.BASE_PRICE = reader.GetDecimal(3);
                                            gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                        }
                                        else if (y > 375 && y <= 675 && n == 1)
                                        {

                                            gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                            gqh.BASE_PRICE = reader.GetDecimal(3);
                                            gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                        }
                                        else if (y > 675 && y <= 875 && n == 1)
                                        {

                                            gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                            gqh.BASE_PRICE = reader.GetDecimal(3);
                                            gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                        }
                                        else if (y > 875 && y <= 1500 && n == 1)
                                        {

                                            gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                            gqh.BASE_PRICE = reader.GetDecimal(3);
                                            gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                        }
                                        else if (y > 1500 && y <= 2250 && n == 1)
                                        {

                                            gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                            gqh.BASE_PRICE = reader.GetDecimal(3);
                                            gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                        }
                                        else if (y > 2250 && y <= 3750 && n == 1)
                                        {

                                            gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                            gqh.BASE_PRICE = reader.GetDecimal(3);
                                            gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                        }
                                        else if (y > 3750 && y <= 7500 && n == 1)
                                        {

                                            gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                            gqh.BASE_PRICE = reader.GetDecimal(3);
                                            gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                                        }
                                        else if (y > 7500 && y <= 15000 && n == 1)
                                        {

                                            gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                            gqh.BASE_PRICE = reader.GetDecimal(3);
                                            gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                        }
                                        else if (y > 15000 && y <= 22500 && n == 1)
                                        {

                                            gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                            gqh.BASE_PRICE = reader.GetDecimal(3);
                                            gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                        }
                                        else if (y > 22500 && y <= 37500 && n == 1)
                                        {

                                            gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                            gqh.BASE_PRICE = reader.GetDecimal(3);
                                            gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                        }
                                        else if (y > 37500 && y <= 87500 && n == 1)
                                        {

                                            gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                            gqh.BASE_PRICE = reader.GetDecimal(3);
                                            gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                        }
                                        else if (y > 87500 && n == 1)
                                        {

                                            gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                            gqh.BASE_PRICE = reader.GetDecimal(3);
                                            gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                        }

                                    }
                                }

                            }


                            catch (Exception ex)
                            {
                                if (CustomerNumber == null)
                                {
                                    CustomerNumber = "ERROR";
                                }
                                if (PartNumber == null)
                                {
                                    PartNumber = "ERROR";
                                }
                                if (BranchPlant == null)
                                {
                                    BranchPlant = "ERROR";
                                }
                                if (CustomerPartNumber == null)
                                {
                                    CustomerPartNumber = "ERROR";
                                }


                                //return api with error
                                return new[]
                            {
                                  //new GetPricingAPI { Price=0, MOQ=MOQ, LeadTime=0,Quantity=0,CustomerPartNumber="ERROR",BranchPlant="ERROR",ItemNumber="ERROR",CustomerNumber="ERROR" }
                                  new GetPricingAPI { Price=null, MOQ=MOQ, LeadTime=leadtime,Quantity=Quantity,CustomerPartNumber=CustomerPartNumber,BranchPlant=BranchPlant,ItemNumber=PartNumber,CustomerNumber=CustomerNumber,StockQuantity=0}
                                };
                            }


                            finally
                            {
                                reader.Close();
                            }

                        }

                    }
                }

            }

            //

            using (OdbcConnection cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString))
            {
                string sqlScript;
                cnn_odbc.Open();
                if (cnn_odbc == null || cnn_odbc.State == ConnectionState.Closed)
                {

                    cnn_odbc.Open();
                }

                int i = 1;
                if (CustomerNumber != null)
                {
                    sqlScript = @"select trim(soh.Division) Division
                                , soh.Unit_Price
                              from(select sdmcu Division, sddoco Order_Num, so.sddcto Order_Type
                                                                  , dba.juliantodate(sdtrdj) as Order_Date
                                                                                  , trim(sdlitm) as Part_Number
                                                                  , sduorg Quantity
                                                                  , dec(sduprc * .000001, 15, 4) as Unit_Price
                                                                                  , case when sdnxtr = '999' then 'Closed' else 'Open' end as Status
                                                                                   , sdan8 Customer_No
                                                                  , (abalph) as Customer_Name
                                                                                       from PRODDTA.f4211 so join PRODDTA.f0101 on sdan8 = aban8

                                                                                    left outer join zpemjde.calendar_dim cal on
                                                                                               so.sdtrdj = cal.juliandate
                                                                                       where    1 = 1            and trim(sdlitm) = ? and trim(sdmcu) in ('730000', '710000', '750000', '720000','734000','780000')
                                                                                         and sddcto in ('SO')
                                                                                       and sdlttr <> '980'
                                                                                       and DATE(cal.date) <= (current date)
                                                                                       union all
                                                                                       select sdmcu Division, sddoco Order_Num, sddcto Order_Type,  dba.juliantodate(sdtrdj) Order_Date, 
                                                                                               trim(sdlitm) as Part_Number, sduorg Quantity, dec(sduprc * .000001, 15, 4) as Unit_Price,  
                                                                                               case when sdnxtr = '999' then 'Closed' else 'Open' end as Status, 
                                                                                               sdan8 Customer_No, trim(abalph) as Customer_Name
                                                                                       from PRODDTA.f42119 soh join PRODDTA.f0101 on sdan8 = aban8
                                                                                    left outer join
                                                                                           zpemjde.calendar_dim cal2 on
                                                                                           soh.sdtrdj = cal2.juliandate
                                                                                     where trim(sdlitm) = ?
                                                                                           and trim(sdmcu) in ('730000', '710000', '750000', '720000','734000','780000')
                                                                                     and sddcto in ('SO')
                                                                                       and sdlttr<> '980' ) soh WHERE soh.customer_no = ?
                                                              group by Division, Order_Num, Unit_Price, Status, Customer_Name, Customer_No ,soh.unit_price
                              order by soh.unit_price desc
                               fetch first row only";
                }
                else
                {
                    sqlScript = @"select trim(soh.Division) Division
                                , soh.Unit_Price
                              from(select sdmcu Division, sddoco Order_Num, so.sddcto Order_Type
                                                                  , dba.juliantodate(sdtrdj) as Order_Date
                                                                                  , trim(sdlitm) as Part_Number
                                                                  , sduorg Quantity
                                                                  , dec(sduprc * .000001, 15, 4) as Unit_Price
                                                                                  , case when sdnxtr = '999' then 'Closed' else 'Open' end as Status
                                                                                   , sdan8 Customer_No
                                                                  , (abalph) as Customer_Name
                                                                                       from PRODDTA.f4211 so join PRODDTA.f0101 on sdan8 = aban8
                                                                                    left outer join zpemjde.calendar_dim cal on
                                                                                               so.sdtrdj = cal.juliandate
                                                                                       where    1 = 1            and trim(sdlitm) = ? and trim(sdmcu) in ('730000', '710000', '750000', '720000','734000','780000')
                                                                                         and sddcto in ('SO')
                                                                                       and sdlttr <> '980'
                                                                                       and DATE(cal.date) <= (current date)
                                                                                       union all
                                                                                       select sdmcu Division, sddoco Order_Num, sddcto Order_Type,  dba.juliantodate(sdtrdj) Order_Date, 
                                                                                               trim(sdlitm) as Part_Number, sduorg Quantity, dec(sduprc * .000001, 15, 4) as Unit_Price,  
                                                                                               case when sdnxtr = '999' then 'Closed' else 'Open' end as Status, 
                                                                                               sdan8 Customer_No, trim(abalph) as Customer_Name
                                                                                       from PRODDTA.f42119 soh join PRODDTA.f0101 on sdan8 = aban8
                                                                                    left outer join
                                                                                           zpemjde.calendar_dim cal2 on
                                                                                           soh.sdtrdj = cal2.juliandate
                                                                                     where trim(sdlitm) = ?
                                                                                           and trim(sdmcu) in ('730000', '710000', '750000', '720000','734000','780000')
                                                                                     and sddcto in ('SO')
                                                                                       and sdlttr<> '980' ) soh WHERE 1=1
                                                              group by Division, Order_Num, Unit_Price, Status, Customer_Name, Customer_No ,soh.unit_price
                              order by soh.unit_price desc
                               fetch first row only";
                }

                using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                {
                    command.Parameters.Add(new OdbcParameter("@item_num", item_num));
                    //if (customer_num != null)
                    //{
                    //    command.Parameters.Add(new OdbcParameter("@customer_num", customer_num));
                    //}

                    command.Parameters.Add(new OdbcParameter("@item_num2", item_num));
                    if (CustomerNumber != null)
                    {
                        command.Parameters.Add(new OdbcParameter("@customer_num", CustomerNumber));
                    }
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                gqh.calc_dynam_fzn_unit_cost = Convert.ToDecimal(string.Format("{0:0.00}", Math.Round(reader.GetDecimal(1), 2)));
                            }
                            //if (gqh.Wins > 0)

                            if (gqh.BASE_PRICE_FLAG == 1)
                            {
                                gqh.calc_dynam_fzn_unit_cost = gqh.BASE_PRICE;
                            }

                        }

                        else
                        {
                            //i = 1;
                            if (CustomerNumber != null)
                            {
                                sqlScript = @"select sd.Unit_Price as Unit_Price, sd.sdshan from (select	dec(sduprc * .000001, 15, 4) as Unit_Price, sdshan
                                 from PRODDTA.f4211 so join PRODDTA.f0101 on sdshan = aban8 
                          left outer join zpemjde.calendar_dim cal on 
                                                     so.sdtrdj = cal.juliandate 
                                 where 1=1
                                 and trim(so.sdlitm) =?
                                and trim(sdmcu) in ('730000','710000','750000','720000','734000','780000')
                                 and sddcto in ('SQ') 
                                 and sdlttr<> '980' 
                                                and date(cal.date)<= (current date)  
                                    and dec(sdan8,15,0)=?
                                 union all 
                                 select  dec(sduprc * .000001, 15, 4) as Unit_Price , sdshan
                                from PRODDTA.f42119 soh join PRODDTA.f0101 on sdshan = aban8  
                          left outer join zpemjde.calendar_dim cal2 on soh.sdtrdj = cal2.juliandate 
                                 where 1=1
                                 and trim(sdlitm) =?
                                    and trim(sdmcu) in ('730000','710000','750000','720000','734000','780000')
                                 and sddcto in ('SQ') 
                                and  dec(sdan8,15,0)=?
                                 and sdlttr<> '980' ) sd
                                order by Unit_Price desc
                                fetch first row only";
                            }
                            else
                            {
                                sqlScript = @"select sd.Unit_Price as Unit_Price, sd.sdshan from (select	dec(sduprc * .000001, 15, 4) as Unit_Price, sdshan
                                 from PRODDTA.f4211 so join PRODDTA.f0101 on sdshan = aban8 
                          left outer join zpemjde.calendar_dim cal on 
                                                     so.sdtrdj = cal.juliandate 
                                 where 1=1
                                 and trim(so.sdlitm) =?
                                and trim(sdmcu) in ('730000','710000','750000','720000','734000','780000')
                                 and sddcto in ('SQ') 
                                 and sdlttr<> '980' 
                                                and date(cal.date)<= (current date)  
                                 union all 
                                 select  dec(sduprc * .000001, 15, 4) as Unit_Price , sdshan
                                from PRODDTA.f42119 soh join PRODDTA.f0101 on sdshan = aban8  
                          left outer join zpemjde.calendar_dim cal2 on soh.sdtrdj = cal2.juliandate 
                                 where 1=1
                                 and trim(sdlitm) =?
                                    and trim(sdmcu) in ('730000','710000','750000','720000','734000','780000')
                                 and sddcto in ('SQ') 
                                 and sdlttr<> '980' ) sd
                                order by Unit_Price desc
                                fetch first row only";
                            }

                            using (var command2 = new OdbcCommand(sqlScript, cnn_odbc))
                            {
                                command2.Parameters.Add(new OdbcParameter("@item_num", item_num));
                                if (CustomerNumber != null)
                                {
                                    command2.Parameters.Add(new OdbcParameter("@customer_num", CustomerNumber));
                                }
                                command2.Parameters.Add(new OdbcParameter("@item_num2", item_num));
                                command2.Parameters.Add(new OdbcParameter("@customer_num2", CustomerNumber));
                                using (var reader2 = command2.ExecuteReader())
                                {
                                    if (reader2.HasRows)
                                    {
                                        while (reader2.Read())
                                        {
                                            gqh.calc_dynam_fzn_unit_cost = Convert.ToDecimal(string.Format("{0:0.00}", Math.Round(reader2.GetDecimal(0), 2)));
                                        }

                                    }

                                }
                            }
                        }
                        reader.Close();

                        if (gqh.BASE_PRICE_FLAG == 1)
                        {
                            gqh.calc_dynam_fzn_unit_cost = gqh.BASE_PRICE;
                        }

                    }
                }

                decimal price = gqh.calc_dynam_fzn_unit_cost;
                if (price != 0)
                {
                    true_price = Convert.ToString(price);
                }
                else
                {
                    true_price = null;
                }
                //cnn_odbc.Open();
                if (cnn_odbc == null || cnn_odbc.State == ConnectionState.Closed)
                {

                    cnn_odbc.Open();
                }
                //check history flag
                sqlScript = @"select count(*) history_int from (select distinct trim(soh.Division) Division
                                , soh.part_number , soh.order_num 
                            , soh.customer_no
                              from(select sdmcu Division, sddoco Order_Num, so.sddcto Order_Type
                                                                  , dba.juliantodate(sdtrdj) as Order_Date
                                                                                  , trim(sdlitm) as Part_Number
                                                                  , sduorg Quantity
                                                                  , dec(sduprc * .000001, 15, 4) as Unit_Price
                                                                                  , case when sdnxtr = '999' then 'Closed' else 'Open' end as Status
                                                                                   , sdan8 Customer_No
                                                                  , (abalph) as Customer_Name
                                                                                       from PRODDTA.f4211 so join PRODDTA.f0101 on sdan8 = aban8
                                                                                    left outer join zpemjde.calendar_dim cal on
                                                                                               so.sdtrdj = cal.juliandate
                                                                                       where    1 = 1           and trim(sdlitm) =?
                                and trim(sdmcu) in ?
                                                                                         and sddcto in ('SO')
                                                                                       and sdlttr <> '980'
                                                                                       and DATE(cal.date) <= (current date)
                                                                                       union all
                                                                                       select sdmcu Division, sddoco Order_Num, sddcto Order_Type,  dba.juliantodate(sdtrdj) Order_Date, 
                                                                                               trim(sdlitm) as Part_Number, sduorg Quantity, dec(sduprc * .000001, 15, 4) as Unit_Price,  
                                                                                               case when sdnxtr = '999' then 'Closed' else 'Open' end as Status, 
                                                                                               sdan8 Customer_No, trim(abalph) as Customer_Name
                                                                                       from PRODDTA.f42119 soh join PRODDTA.f0101 on sdan8 = aban8
                                                                                    left outer join
                                                                                           zpemjde.calendar_dim cal2 on
                                                                                           soh.sdtrdj = cal2.juliandate
                                                                                     where 1=1  and trim(sdlitm) = ?
                                                                                           and trim(sdmcu) in ?
                                                                                     and sddcto in ('SO')
                                                                                       and sdlttr<> '980' ) soh WHERE 1=1
                                                              group by Division , Order_Num, customer_no
                                                                , Part_number) cdo
                                                                group by part_number, division";

                using (var command2 = new OdbcCommand(sqlScript, cnn_odbc))
                {
                    command2.Parameters.Add(new OdbcParameter("@item_num", item_num));

                    command2.Parameters.Add(new OdbcParameter("@branch_plant", BranchPlant));
                    command2.Parameters.Add(new OdbcParameter("@item_num2", item_num));

                    command2.Parameters.Add(new OdbcParameter("@branch_plant2", BranchPlant));

                    using (var reader2 = command2.ExecuteReader())
                    {
                        if (reader2.HasRows)
                        {
                            while (reader2.Read())
                            {
                                history_flag = reader2.GetInt32(0);
                            }

                        }

                    }
                }

                if (MOQ == 0)
                {
                    MOQ = 10;
                }
                //decimal leadtime = 25;

                //if (true_price="0")
                //{
                //    true_price = null;
                //}

                if (Convert.ToDecimal(price) > 0 && history_flag > 1)
                {
                    return new[]
                    {
          //new GetPricingAPI { Price=true_price, MOQ=MOQ, LeadTime=leadtime,Quantity=Quantity,CustomerPartNumber=CustomerPartNumber,BranchPlant=BranchPlant }
          new GetPricingAPI { Price=true_price, MOQ=MOQ, LeadTime=leadtime,Quantity=Quantity,CustomerPartNumber=CustomerPartNumber,BranchPlant=BranchPlant,ItemNumber=PartNumber,CustomerNumber=CustomerNumber,StockQuantity=0}
        };
                }

                else
                {

                    //go get cost

                    //using (OdbcConnection cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString))
                    //{

                    //cnn_odbc.Open();
                    if (cnn_odbc == null || cnn_odbc.State == ConnectionState.Closed)
                    {

                        cnn_odbc.Open();
                    }

                    int int_num = 1;

                    gqh.totalCPQtyCostVar = 0;
                    gqh.totalCPQtyCostSU = 0;
                    gqh.totalCPQtyCostFOH = 0;

                    if (BranchPlant == "730000" || BranchPlant == "734000")
                    {
                        sqlScript = @"select	ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ixum, ibacq, ibstkt, ibltmf, ibltcm, imdsc1,  
                  dec(ixqnty / 1000000, 15, 6) as ixqnty, ixcpnb,  
                  (select sum(lipqoh) from proddta.f41021 where limcu = ixmmcu and liitm = ixitm) as pqoh,  
                  (select sum(lipqoh) - sum(lipcom) - sum(lihcom) - sum(lifcom) - sum(liqowo) from proddta.f41021 
                  where limcu = ixmmcu and liitm = ixitm) as avail, 
                  sum(dec(iexscr / 1000000, 15, 7)) as simCost
                , sum(dec(iecsl / 1000000, 15, 7)) as fznCost ,
                        coalesce(sum(case when iecost = 'A1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznA1,
                        coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB2,
						coalesce(sum(case when iecost = 'B1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB1,
						coalesce(sum(case when iecost = 'C3' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC3,
						coalesce(sum(case when iecost = 'C1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC1,
                        coalesce(sum(case when iecost = 'C4' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC4,
						coalesce(sum(case when iecost = 'C2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC2,
						coalesce(sum(case when left(iecost,1) = 'D' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznD0,
                        coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000*ibacq as decimal(15,7)) else 0 end),0) as fznSUTotal
                  from proddta.f3002 join proddta.f4102 on ixitm = ibitm and ixmmcu = ibmcu and left(ibglpt,2) in ('FG', 'CP') 
                  join proddta.f4101 on imitm = ibitm 
                  left join proddta.f30026 on iemmcu = ibmcu and ieitm = ibitm and ieledg = ?
                  where ixtbm in ('M')
                and ixkit = ? 
                  and trim(ixmmcu) = ? 
                  group by    ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ibacq, imdsc1, ixqnty, ibstkt, ixcpnb, ibltmf, ibltcm, ixum";
                    }
                    else if (BranchPlant == "710000")
                    {
                        sqlScript = @"select	ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ixum, ibacq, ibstkt, ibltmf, ibltcm, imdsc1,  
                  dec(ixqnty / 1000000, 15, 6) as ixqnty, ixcpnb,  
                  (select sum(lipqoh) from proddta.f41021 where limcu = ixmmcu and liitm = ixitm) as pqoh,  
                  (select sum(lipqoh) - sum(lipcom) - sum(lihcom) - sum(lifcom) - sum(liqowo) from proddta.f41021 
                  where limcu = ixmmcu and liitm = ixitm) as avail, 
                  sum(dec(iexscr / 1000000, 15, 7)) as simCost
                , sum(dec(iecsl / 1000000, 15, 7)) as fznCost ,
                        coalesce(sum(case when left(iecost,1) = 'A' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simA1,
                        coalesce(sum(case when iecost = 'B2' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB2,
						coalesce(sum(case when iecost = 'B1' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB1,
						coalesce(sum(case when iecost = 'C3' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC3,
						coalesce(sum(case when iecost = 'C1' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC1,
                       coalesce(sum(case when iecost = 'C4' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC4,
						coalesce(sum(case when iecost = 'C2' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC2,
						coalesce(sum(case when left(iecost,1) = 'D' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simD0,
                        coalesce(sum(case when iecost = 'B2' then cast(iexscr/1000000*ibacq as decimal(15,7)) else 0 end),0) simSUTotal
                  from proddta.f3002 join proddta.f4102 on ixitm = ibitm and ixmmcu = ibmcu and left(ibglpt,2) in ('FG', 'CP', 'RM') 
                  join proddta.f4101 on imitm = ibitm 
                  left join proddta.f30026 on iemmcu = ibmcu and ieitm = ibitm and ieledg = ?
                  where ixtbm in ('M')
                and ixkit = ? 
                  and trim(ixmmcu) = ? 
                  group by    ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ibacq, imdsc1, ixqnty, ibstkt, ixcpnb, ibltmf, ibltcm, ixum";
                    }
                    else
                    {
                        sqlScript = @"select	ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ixum, case when trim(ixmmcu)='780000' then case when ibacq=0 then 1 else ibacq end else ibacq end as ibacq, ibstkt, ibltmf, ibltcm,imdsc1,
                  CAST(dec(ixqnty / 1000000, 15, 6) AS INTEGER) as ixqnty, ixcpnb,  
                  ifnull((select sum(lipqoh) from proddta.f41021 where limcu = ixmmcu and liitm = ixitm),0) as pqoh,  
                  ifnull((select sum(lipqoh) - sum(lipcom) - sum(lihcom) - sum(lifcom) - sum(liqowo) from proddta.f41021 
                  where limcu = ixmmcu and liitm = ixitm),0) as avail, 
                  sum(dec(iexscr / 1000000, 15, 7)) as simCost
                , sum(dec(iecsl / 1000000, 15, 7)) as fznCost ,
                        coalesce(sum(case when iecost = 'A1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznA1,
                        coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB2,
						coalesce(sum(case when iecost = 'B1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB1,
						coalesce(sum(case when iecost = 'C3' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC3,
						coalesce(sum(case when iecost = 'C1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC1,
                        coalesce(sum(case when iecost = 'C4' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC4,
						coalesce(sum(case when iecost = 'C2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC2,
						coalesce(sum(case when left(iecost,1) = 'D' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznD0,
                        coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000*ibacq as decimal(15,7)) else 0 end),0) as fznSUTotal
                  from proddta.f3002 join proddta.f4102 on ixitm = ibitm and ixmmcu = ibmcu and left(ibglpt,2) in ('FG', 'CP', 'RM') 
                  join proddta.f4101 on imitm = ibitm 
                  left join proddta.f30026 on iemmcu = ibmcu and ieitm = ibitm and ieledg = ?
                  where ixtbm in ('M')
                and ixkit = ? 
                  and trim(ixmmcu) = ? 
                  group by    ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ibacq, imdsc1, ixqnty, ibstkt, ixcpnb, ibltmf, ibltcm, ixum";
                    }
                    //and dec(ixqnty / 1000000, 15, 6)>=1
                    using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                    {
                        command.Parameters.Add(new OdbcParameter("@cost_year", "07")); //only current standard cost year
                        command.Parameters.Add(new OdbcParameter("@item_num", gqh.ibitm));
                        command.Parameters.Add(new OdbcParameter("@division", BranchPlant));

                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    ComponentFound item = new ComponentFound()
                                    {
                                        //reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                                        //ItemKey = int_num,
                                        ComponentNum = reader.GetString(4), //ixitm
                                        ComponentUOM = reader.GetString(5), //ixum
                                        ComponentDesc = reader.GetString(10), //imdsc1
                                        ComponentStock = reader.GetString(7), //ibstkt
                                        ComponentACQ = reader.IsDBNull(6) ? 0 : (decimal)reader.GetValue(6),
                                        ComponentMfgLT = reader.IsDBNull(8) ? 0 : (decimal)reader.GetValue(8),
                                        ComponentQty = reader.GetInt32(11),
                                        ChangeQuantity = reader.GetInt32(11),
                                        ComponentOnHand = reader.GetInt32(13),
                                        ComponentAvailQt = reader.GetInt32(14),
                                        ComponentCostFzn = reader.IsDBNull(16) ? 0 : (decimal)reader.GetValue(16),
                                        CompMtlCost = reader.IsDBNull(17) ? 0 : (decimal)reader.GetValue(17),
                                        CompSetup = reader.IsDBNull(18) ? 0 : (decimal)reader.GetValue(18),
                                        CompLabor = reader.IsDBNull(19) ? 0 : (decimal)reader.GetValue(19),
                                        CompLbrVOH = reader.IsDBNull(20) ? 0 : (decimal)reader.GetValue(20),
                                        CompMchVOH = reader.IsDBNull(21) ? 0 : (decimal)reader.GetValue(21),
                                        CompLbrFOH = reader.IsDBNull(22) ? 0 : (decimal)reader.GetValue(22),
                                        CompMchFOH = reader.IsDBNull(23) ? 0 : (decimal)reader.GetValue(23),
                                        CompOSP = reader.IsDBNull(24) ? 0 : (decimal)reader.GetValue(24),
                                        CompSUTotal = reader.IsDBNull(25) ? 0 : (decimal)reader.GetValue(25),
                                        FznCPQtyCostSU = Math.Round((reader.IsDBNull(25) ? 0 : (decimal)reader.GetValue(25)), 5) / (reader.IsDBNull(6) ? 0 : (decimal)reader.GetValue(6)),
                                        FznCPQtyCostVar = (reader.IsDBNull(17) ? 0 : (decimal)reader.GetValue(17)) + (reader.IsDBNull(19) ? 0 : (decimal)reader.GetValue(19)) + (reader.IsDBNull(20) ? 0 : (decimal)reader.GetValue(20)) + (reader.IsDBNull(21) ? 0 : (decimal)reader.GetValue(21)) + (reader.IsDBNull(24) ? 0 : (decimal)reader.GetValue(24)), //fzna1 fznb1 fznD0 fznc1 fznc3
                                        FznCPQtyCostFOH = (((reader.IsDBNull(23) ? 0 : (decimal)reader.GetValue(23)) + (reader.IsDBNull(22) ? 0 : (decimal)reader.GetValue(22))) * (reader.IsDBNull(6) ? 0 : (decimal)reader.GetValue(6))) / (reader.IsDBNull(6) ? 0 : (decimal)reader.GetValue(6))
                                        //loop through?
                                    };

                                    if (Convert.ToDecimal(item.ChangeQuantity) > 0)
                                    {
                                        string test_compNum = item.ComponentNum;
                                        int test_compqty = item.ComponentQty;// = reader.GetInt32(8), //ixqnty
                                        int test_availqt = item.ComponentAvailQt; //= reader.GetInt32(11), //avail
                                        decimal test_CostFzn = item.ComponentCostFzn; //= reader.GetDecimal(12), //fzna1
                                        decimal test_FznRecalc = item.ComponentCostFznRecalc; //= reader.GetDecimal(12), //fzna1
                                        decimal test_MtlCost = item.CompMtlCost; //= reader.GetDecimal(14), //fznb2
                                        decimal test_ComLabor = item.CompLabor; //= reader.GetDecimal(13), //fzna1
                                        decimal test_ComSetup = item.CompSetup;
                                        decimal test_CompLbrVOH = item.CompLbrVOH;
                                        decimal test_CompMchVOH = item.CompMchVOH;
                                        decimal test_CompLbrFOH = item.CompLbrFOH;
                                        decimal test_CompMchFOH = item.CompMchFOH;
                                        decimal test_CompOSP = item.CompOSP; // = reader.GetDecimal(20), //fznd0
                                        decimal test_compSU = item.CompSUTotal; //= reader.GetDecimal(21), //fznsutotal
                                        gqh.totalCPQtyCostVar = (item.FznCPQtyCostVar * item.ChangeQuantity) + gqh.totalCPQtyCostVar;
                                        gqh.totalCPQtyCostSU = (item.FznCPQtyCostSU * item.ChangeQuantity) + gqh.totalCPQtyCostSU;
                                        gqh.totalCPQtyCostFOH = (item.FznCPQtyCostFOH * item.ChangeQuantity) + gqh.totalCPQtyCostFOH;

                                    }
                                    else
                                    {
                                        if (item.FznCPQtyCostVar != 0 && item.FznCPQtyCostSU != 0)
                                        {
                                            gqh.totalCPQtyCostVar = (item.FznCPQtyCostVar) + gqh.totalCPQtyCostVar;
                                            gqh.totalCPQtyCostSU = (item.FznCPQtyCostSU) + gqh.totalCPQtyCostSU;
                                            gqh.totalCPQtyCostFOH = (item.FznCPQtyCostFOH) + gqh.totalCPQtyCostFOH;
                                        }
                                        else
                                        {
                                            gqh.totalCPQtyCostVar = 0 + gqh.totalCPQtyCostVar;
                                            gqh.totalCPQtyCostSU = 0 + gqh.totalCPQtyCostSU;
                                            gqh.totalCPQtyCostFOH = 0 + gqh.totalCPQtyCostFOH;
                                        }
                                    }
                                    int_num = int_num + 1;
                                    Components.Add(item);
                                }
                            }

                            else
                            {
                                foreach (var item in Components)
                                {
                                    gqh.totalCPQtyCostVar = 0;
                                    gqh.totalCPQtyCostSU = 0;
                                    gqh.totalCPQtyCostFOH = 0;
                                    gqh.totalCPQtyCostVar = (item.FznCPQtyCostVar * item.ChangeQuantity) + gqh.totalCPQtyCostVar;
                                    gqh.totalCPQtyCostSU = (item.FznCPQtyCostSU * item.ChangeQuantity) + gqh.totalCPQtyCostSU;
                                    gqh.totalCPQtyCostFOH = (item.FznCPQtyCostFOH * item.ChangeQuantity) + gqh.totalCPQtyCostFOH;

                                }
                            }
                            reader.Close();
                        }
                    }

                    decimal fznMatl; //fzna1
                    decimal fznLbr; //fznb1
                    decimal fznb2;
                    decimal fznc1;
                    decimal fznc2;
                    decimal fznc3;
                    decimal fznc4;
                    decimal fznOS; //fznd0
                    decimal fznlc;
                    decimal fznSUTotal;
                    decimal fznLCTotal;

                    if (BranchPlant == "710000")
                    {
                        sqlScript = @"select coalesce(sum(case when left(iecost,1) = 'A' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simA1,
						coalesce(sum(case when iecost = 'B1' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB1,
						coalesce(sum(case when iecost = 'B2' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB2,
						coalesce(sum(case when iecost = 'C1' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC1,
						coalesce(sum(case when iecost = 'C2' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC2,
						coalesce(sum(case when iecost = 'C3' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC3,
						coalesce(sum(case when iecost = 'C4' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC4,
                                         coalesce(sum(case when left(iecost,1) = 'D' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simD0,
                                         coalesce(sum(case when trim(iemcu) = 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simLC,
                                         coalesce(sum(case when iecost = 'B2' then cast(iexscr/1000000*CAST(? AS INTEGER) as decimal(15,7)) else 0 end),0) 
							as simSUTotal, 
                                        decimal(coalesce( sum(case when iecost = 'A1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznA1Net,
                         decimal(coalesce( sum(case when iecost = 'B1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB1Net,
                         decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB2Net,
                         decimal(coalesce( sum(case when iecost = 'C1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC1Net,
                         decimal(coalesce( sum(case when iecost = 'C2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC2Net,
                         decimal(coalesce( sum(case when iecost = 'C3' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC3Net,
                         decimal(coalesce( sum(case when iecost = 'C4' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC4Net,
                         decimal(coalesce( sum(case when left(iecost,1) = 'D' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznD0Net,
                         decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 * CAST(? AS INTEGER) as decimal(15,5)) else 0 end),0),10,5) 
                          as fznSUTotalNet , coalesce(sum(case when trim(iemcu) = 'V78LOT001' then cast(iexscr/1000000*CAST(? as INTEGER) as decimal(15,7)) else 0 end),0) simLCTotal, 
                         decimal(coalesce( sum(case when iecost = 'A2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznA2Net,
                            decimal(coalesce( sum(case when iecost = 'D3' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznD3Net
                                            from proddta.f300261
                                            where ieledg = ?
                                            and trim(iemmcu)=?
                                            and ieitm = ?";
                    }

                    else
                    {
                        sqlScript = @"select decimal(coalesce(sum(case when iecost = 'A1'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznA1, 
                                         decimal(coalesce(sum(case when iecost = 'B1'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznB1, 
                                         decimal(coalesce(sum(case when iecost = 'B2'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznB2, 
                                         decimal(coalesce(sum(case when iecost = 'C1'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC1, 
                                         decimal(coalesce(sum(case when iecost = 'C2'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC2, 
                                         decimal(coalesce(sum(case when iecost = 'C3'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC3, 
                                         decimal(coalesce(sum(case when iecost = 'C4'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC4, 
                                         decimal(coalesce(sum(case when left(iecost, 1) = 'D' then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznD0,    
                                         decimal(coalesce(sum(case when iecost = 'B2' then cast((iecsl * .000001 * CAST(? AS INTEGER)) as decimal(15, 5)) else 0 end), 0),10,5) as fznSUTotal, 
                                        decimal(coalesce( sum(case when iecost = 'A1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznA1Net,
					                    decimal(coalesce( sum(case when iecost = 'B1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB1Net,
					                    decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB2Net,
					                    decimal(coalesce( sum(case when iecost = 'C1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC1Net,
					                    decimal(coalesce( sum(case when iecost = 'C2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC2Net,
					                    decimal(coalesce( sum(case when iecost = 'C3' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC3Net,
					                    decimal(coalesce( sum(case when iecost = 'C4' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC4Net,
					                    decimal(coalesce( sum(case when left(iecost,1) = 'D' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznD0Net,
					                    decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 * CAST(? AS INTEGER) as decimal(15,5)) else 0 end),0),10,5) as fznSUTotalNet
                                            from proddta.f30026
                                            where ieledg = ?
                                            and trim(iemmcu) in (?)
                                            and trim(ielitm) = ?";
                    }
                    using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                    {

                        command.Parameters.Add(new OdbcParameter("@ACQ", gqh.ACQ));


                        command.Parameters.Add(new OdbcParameter("@ACQ2", gqh.ACQ));

                        if (BranchPlant != "730000")
                        {
                            command.Parameters.Add(new OdbcParameter("@ACQ3", gqh.ACQ));
                        }
                        command.Parameters.Add(new OdbcParameter("@year", "07"));
                        command.Parameters.Add(new OdbcParameter("@division_num", BranchPlant));
                        if (BranchPlant == "710000")
                        {
                            command.Parameters.Add(new OdbcParameter("@item_num", gqh.ibitm));
                        }
                        else
                        {
                            command.Parameters.Add(new OdbcParameter("@item_num", item_num));
                        }

                        try
                        {
                            using (var reader = command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        if (BranchPlant == "710000")
                                        {

                                            fznMatl = reader.GetDecimal(0);
                                            fznLbr = reader.GetDecimal(1);
                                            fznb2 = reader.GetDecimal(2);
                                            fznc1 = reader.GetDecimal(3);
                                            fznc2 = reader.GetDecimal(4);
                                            fznc3 = reader.GetDecimal(5);
                                            fznc4 = reader.GetDecimal(6);
                                            fznOS = reader.GetDecimal(7);
                                            fznlc = reader.GetDecimal(8);
                                            fznSUTotal = reader.GetDecimal(9);
                                            gqh.fznA1Net = reader.GetDecimal(10); //fznmatlnet
                                            gqh.fznB1Net = reader.GetDecimal(11); //fznlbrnet
                                            gqh.fznB2Net = reader.GetDecimal(12);
                                            gqh.fznc1net = reader.GetDecimal(13);
                                            gqh.fznc2net = reader.GetDecimal(14);
                                            gqh.fznc3net = reader.GetDecimal(15);
                                            gqh.fznc4net = reader.GetDecimal(16);
                                            gqh.fznD0Net = reader.GetDecimal(17); //fznosnet
                                            gqh.fznSUTotalNet = reader.IsDBNull(18) ? 0 : (decimal)reader.GetValue(18);
                                            gqh.fznLCTotal = reader.GetDecimal(19);
                                            gqh.material = fznMatl;
                                            gqh.setup = fznb2; //setup
                                            gqh.labor = fznLbr;
                                            gqh.lbr_voh = gqh.fznc3net;
                                            gqh.mch_voh = gqh.fznc1net;
                                            gqh.lbr_foh = gqh.fznc4net;
                                            gqh.mch_foh = fznc2;
                                            gqh.osp = gqh.fznD0Net;

                                        }
                                        else
                                        {
                                            fznMatl = reader.GetDecimal(0); //a1
                                            fznLbr = reader.GetDecimal(1); //b1
                                            fznb2 = reader.GetDecimal(2);
                                            fznc1 = reader.GetDecimal(3);
                                            fznc2 = reader.GetDecimal(4);
                                            fznc3 = reader.GetDecimal(5);
                                            fznc4 = reader.GetDecimal(6);
                                            fznOS = reader.GetDecimal(7); //d0
                                            fznSUTotal = reader.GetDecimal(2) * gqh.ACQ;
                                            gqh.fznA1Net = reader.GetDecimal(9); //fznmatlnet
                                            gqh.fznB1Net = reader.GetDecimal(10); //fznlbrnet
                                            gqh.fznB2Net = reader.GetDecimal(11);
                                            gqh.fznc1net = reader.GetDecimal(12);
                                            gqh.fznc2net = reader.GetDecimal(13);
                                            gqh.fznc3net = reader.GetDecimal(14);
                                            gqh.fznc4net = reader.GetDecimal(15);
                                            gqh.fznD0Net = reader.GetDecimal(16); //fznosnet
                                            gqh.fznSUTotalNet = reader.IsDBNull(17) ? 0 : (decimal)reader.GetValue(17);
                                            if (BranchPlant == "710000")
                                            {
                                                gqh.fznLCTotal = reader.GetDecimal(19); //gqh.total = fznMatl + fznLbr + fznOS + fznb2 + fznc1 + fznc3 + fznc2 + fznc4; //fznfoh fznvoh
                                            }
                                            gqh.material = fznMatl;
                                            gqh.setup = fznb2; //setup
                                            gqh.labor = fznLbr;
                                            gqh.lbr_voh = fznc3;
                                            gqh.mch_voh = fznc1;
                                            gqh.lbr_foh = fznc4;
                                            gqh.mch_foh = fznc2;
                                            gqh.osp = fznOS;
                                            gqh.UnitCost = fznMatl + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS + fznb2;
                                        }
                                        if (BranchPlant == "730000")
                                        {
                                            //if (item_var.item_num == "BH01816-4") 
                                            //{
                                            //    int check_me=1;
                                            //    //check_me = 1;
                                            //    check_me = 1;
                                            //}
                                            if (Components.Count() > 0)
                                            {
                                                gqh.calc_dynam_qty = Quantity;
                                                if (Quantity > 0)
                                                {
                                                    gqh.calc_dynam_fzn_unit_cost = (((Quantity * (gqh.fznA1Net + adj_amount + gqh.fznB1Net + gqh.fznc1net + gqh.fznc3net + gqh.fznD0Net + ((gqh.fznc2net + gqh.fznc4net) / Quantity) + gqh.totalCPQtyCostVar + gqh.totalCPQtyCostSU + gqh.totalCPQtyCostFOH)) + gqh.fznSUTotalNet)) / Quantity;
                                                }
                                                else
                                                {
                                                    gqh.calc_dynam_fzn_unit_cost = (((Quantity * (gqh.fznA1Net + adj_amount + gqh.fznB1Net + gqh.fznc1net + gqh.fznc3net + gqh.fznD0Net + ((gqh.fznc2net + gqh.fznc4net)) + gqh.totalCPQtyCostVar + gqh.totalCPQtyCostSU + gqh.totalCPQtyCostFOH + (gqh.fznSUTotalNet)))));
                                                }
                                            }
                                            else
                                            {
                                                gqh.calc_dynam_qty = Quantity;
                                                if (Quantity > 0)
                                                {
                                                    gqh.calc_dynam_fzn_unit_cost = (((Quantity * (fznMatl + gqh.fznB1Net + adj_amount + gqh.fznc1net + gqh.fznc3net + gqh.fznD0Net + ((gqh.fznc2net + gqh.fznc4net) / Quantity) + gqh.totalCPQtyCostVar + gqh.totalCPQtyCostSU + gqh.totalCPQtyCostFOH)) + gqh.fznSUTotalNet)) / Quantity;
                                                }
                                                else
                                                {
                                                    gqh.calc_dynam_fzn_unit_cost = (((Quantity * (fznMatl + gqh.fznB1Net + adj_amount + gqh.fznc1net + gqh.fznc3net + gqh.fznD0Net + ((gqh.fznc2net + gqh.fznc4net)) + gqh.totalCPQtyCostVar + gqh.totalCPQtyCostSU + gqh.totalCPQtyCostFOH + (gqh.fznSUTotalNet)))));
                                                }
                                            }
                                        }
                                        else if (Components.Count() > 0 && BranchPlant == "720000")
                                        {
                                            gqh.calc_dynam_qty = Quantity;

                                            if (Quantity > 0)
                                            {
                                                gqh.calc_dynam_fzn_unit_cost = (adj_amount) + (fznMatl + fznLbr + fznc2 + fznc1 + fznc3 + fznc4 + fznOS) + fznSUTotal / Quantity;
                                            }
                                        }
                                        else if (Components.Count() == 0 && BranchPlant == "720000")
                                        {
                                            gqh.calc_dynam_qty = Quantity;
                                            if (Quantity > 0)
                                            {
                                                gqh.calc_dynam_fzn_unit_cost = (adj_amount) + ((Quantity * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal) / Quantity;
                                            }
                                            else
                                            {
                                                gqh.calc_dynam_fzn_unit_cost = (adj_amount) + (((fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal);
                                            }
                                        }
                                        else
                                        {
                                            gqh.calc_dynam_qty = Quantity;
                                            if (Quantity > 0)
                                            {
                                                gqh.calc_dynam_fzn_unit_cost = (fznMatl + adj_amount + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS + (fznSUTotal / Quantity) + (gqh.fznLCTotal / Quantity));
                                            }
                                            else
                                            {
                                                gqh.calc_dynam_fzn_unit_cost = (fznMatl + adj_amount + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS + (fznSUTotal) + (gqh.fznLCTotal));
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Failed to obtain frozen cost.");
                                }
                                reader.Close();
                            }
                        }
                        catch
                        {
                            //return api with error
                            if (CustomerNumber == null)
                            {
                                CustomerNumber = "ERROR";
                            }
                            if (PartNumber == null)
                            {
                                PartNumber = "ERROR";
                            }
                            if (BranchPlant == null)
                            {
                                BranchPlant = "ERROR";
                            }
                            if (CustomerPartNumber == null)
                            {
                                CustomerPartNumber = "ERROR";
                            }

                            return new[]
                        {
                                  new GetPricingAPI { Price=null, MOQ=MOQ, LeadTime=leadtime,Quantity=Quantity,CustomerPartNumber=CustomerPartNumber,BranchPlant=BranchPlant,ItemNumber=PartNumber,CustomerNumber=CustomerNumber,StockQuantity=0}
                                };
                        }

                    }

                    //MOQ = gqh.MINIMUM_ORDER_QUANTITY;

                    if (MOQ == 0)
                    {
                        MOQ = 10;
                    }
                    leadtime = 25;

                    if (Quantity < MOQ)
                    {
                        Quantity = MOQ;
                    }

                    if (Convert.ToDecimal(Math.Round(gqh.calc_dynam_fzn_unit_cost / (1 - gqh.Margin), 2)) > 0)
                    {
                        true_price = Convert.ToString(Math.Round(gqh.calc_dynam_fzn_unit_cost / (1 - gqh.Margin), 2));
                        price = Convert.ToDecimal(true_price);
                    }
                    else
                    {
                        true_price = null;
                    }
                }
                //if (price > 0)
                //{
                return new[]
                {
          new GetPricingAPI { Price=true_price, MOQ=MOQ, LeadTime=leadtime,Quantity=Quantity,CustomerPartNumber=CustomerPartNumber,BranchPlant=BranchPlant,ItemNumber=PartNumber,CustomerNumber=CustomerNumber,StockQuantity=0 }

        };
                //}


                //end to get cost 
                //var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                //{
                //  Content = new StringContent(string.Format("Price is 0", price)),
                //  ReasonPhrase = "Price is $0"
                //};
                //throw new HttpResponseException(resp);
            }
        }

        [AllowAnonymous]
        public GetPricingAPIList Post([FromBody] GetPricingAPIList pricingList) //, string CustomerNumber, string PartNumber,int Quantity
        {

            //List<OriginalPricingTool> pricingListing = new List<OriginalPricingTool>();
            string seperator = ",";
            // string data = string.Join(seperator, pricingList.ToList<GetPricingAPI>());


            try
            {
                OdbcConnection cnn_odbc;
                cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString);
                cnn_odbc.Open();
                cnn_odbc.Dispose(); 
                cnn_odbc.Close();
            }
            catch (Exception)
            {
                // return an error
            }

            GetPricingAPIList returnPriceList = new GetPricingAPIList();
            returnPriceList.pricingList = new List<GetPricingAPI>();

            foreach (var obj in pricingList.pricingList)
            {

                string item_num = obj.ItemNumber;
                string division = obj.BranchPlant;
                //string CustomerPartNumber = "";
                int history_flag = 0;
                string true_price;
                decimal leadtime = 25;
                int MOQ = 10;
                GenerateQuoteHeader gqh = new GenerateQuoteHeader();
                List<LTAInfo> LTAs = new List<LTAInfo>();
                List<LTAPart> LTAParts = new List<LTAPart>();
                List<Quote> quotes = new List<Quote>();
                List<ComponentFound> Components = new List<ComponentFound>();
                List<SalesOrder> model2 = new List<SalesOrder>();
                List<Comment> Comments = new List<Comment>();
                List<ActualCost> ActCost = new List<ActualCost>();
                List<Competitor> Comps = new List<Competitor>();
                List<CustomerPart> CustParts = new List<CustomerPart>();
                List<QuoteWorkBench> quoteworkbench = new List<QuoteWorkBench>();
                List<QuoteWorkbenchItems> bench = new List<QuoteWorkbenchItems>();

                gqh.MINIMUM_ORDER_QUANTITY = 10;
                MOQ = gqh.MINIMUM_ORDER_QUANTITY;
                string cost_reference_id = "07";
                gqh.QuoteQuantity = obj.Quantity;
                gqh.PartNumber = obj.ItemNumber;

                if (obj.Quantity < MOQ)
                {
                    obj.Quantity = 10;
                }

                //what is the customer part number
                using (OdbcConnection cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString))
                {
                    cnn_odbc.Open();
                    string sqlScript = "";

                    if (obj.CustomerPartNumber == null)
                    {


                        sqlScript = @"select	DISTINCT ifnull(xref.ivcitm,ibaitm) CustPart, xref.ivxrt ibmerl, ibitm
                                              from PRODDTA.f4102 ib join PRODDTA.f4101 itm on ibitm = imitm
                                    left outer join proddta.F4104 xref on ibitm = xref.ivitm
                                              where  1 = 1
                                              and trim(iblitm)=? and ivan8=?";

                        using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                        {
                            command.Parameters.Add(new OdbcParameter("@item_num", item_num.Trim()));
                            command.Parameters.Add(new OdbcParameter("@customer_number", obj.CustomerNumber));
                            using (var reader = command.ExecuteReader())
                            {
                                try
                                {
                                    if (reader.HasRows)
                                    {

                                        while (reader.Read())
                                        {
                                            obj.CustomerPartNumber = reader.IsDBNull(0) ? "" : (string)reader.GetValue(0);
                                            gqh.ibitm = reader.GetInt32(2);
                                        }
                                    }
                                    else
                                    {
                                        reader.Close();
                                        sqlScript = @"select DISTINCT ifnull(xref.ivcitm,ibaitm) CustPart, xref.ivxrt ibmerl, ibitm
                                              from PRODDTA.f4102 ib join PRODDTA.f4101 itm on ibitm = imitm
                                    left outer join proddta.F4104 xref on ibitm = xref.ivitm
                                              where  1 = 1
                                              and trim(iblitm)=? fetch first row only";
                                        using (var new_command = new OdbcCommand(sqlScript, cnn_odbc))
                                        {
                                            new_command.Parameters.Add(new OdbcParameter("@item_num", item_num.Trim()));

                                            //command.Parameters.Add(new OdbcParameter("@customer_number", obj.CustomerNumber));


                                            using (var new_reader = new_command.ExecuteReader())
                                            {
                                                try
                                                {
                                                    if (new_reader.HasRows)
                                                    {

                                                        while (new_reader.Read())
                                                        {
                                                            obj.CustomerPartNumber = (new_reader.IsDBNull(0) ? "" : (string)new_reader.GetValue(0)).Trim();
                                                            gqh.ibitm = new_reader.GetInt32(2); // ? 0 : (int)new_reader.GetValue(2);
                                                        }
                                                    }

                                                }
                                                catch (Exception ex)
                                                {
                                                    //who knows
                                                }
                                            }
                                        }
                                    }
                                }

                                catch (Exception ex)
                                {

                                }
                            }
                        }
                    }
                    else
                    {
                        sqlScript = @"select	DISTINCT iblitm item_num, xref.ivxrt ibmerl, ibitm
                                              from PRODDTA.f4102 ib join PRODDTA.f4101 itm on ibitm = imitm
                                    left outer join proddta.F4104 xref on ibitm = xref.ivitm
                                              where  1 = 1
                                              and xref.ivcitm=? ";

                        using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                        {
                            command.Parameters.Add(new OdbcParameter("@customer_part_num", obj.CustomerPartNumber.Trim()));


                            using (var reader = command.ExecuteReader())
                            {
                                try
                                {
                                    if (reader.HasRows)
                                    {

                                        while (reader.Read())
                                        {
                                            obj.ItemNumber = reader.IsDBNull(0) ? "" : (string)reader.GetValue(0);
                                            gqh.ibitm = reader.IsDBNull(2) ? 0 : (int)reader.GetValue(2);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                        }
                    }
                }



                obj.ItemNumber = obj.ItemNumber.Trim();
                item_num = obj.ItemNumber;
                if (obj.CustomerPartNumber != null)
                {
                    obj.CustomerPartNumber = obj.CustomerPartNumber.Trim();
                }
                using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                {
                    cnn.Open();

                    string sqlScript = "";



                    int LTACount = 0;
                    if (obj.CustomerNumber != null)
                    {
                        sqlScript = @"select count(*) from iT_Sales_LTAUpload where (item_number_2=ISNULL(@item_num,'') Or Item_Number_3=ISNULL(@cust_item_num,''))
                                      and division_id in ('710000','730000','750000','720000','Q720000','734000','780000') and convert(date,effective_start_date)<=convert(date,getdate())
                                          and convert(date, Effective_End_Date)>= convert(date, getdate()) and customer_number=@customer_number";
                    }
                    else
                    {
                        sqlScript = @"select count(*) from iT_Sales_LTAUpload where (item_number_2=ISNULL(@item_num,'') Or Item_Number_3=ISNULL(@cust_item_num,''))
                                      and division_id in ('710000','730000','750000','720000','Q720000','734000','780000') and convert(date,effective_start_date)<=convert(date,getdate())
                                          and convert(date, Effective_End_Date)>= convert(date, getdate()) ";
                    }

                    using (var command = new SqlCommand(sqlScript, cnn))
                    {
                        command.Parameters.Add(new SqlParameter("@item_num", item_num));
                        command.Parameters.Add(new SqlParameter("@cust_item_num", obj.CustomerPartNumber));

                        if (obj.CustomerNumber != null)
                        {
                            command.Parameters.Add(new SqlParameter("@customer_number", obj.CustomerNumber));
                        }

                        try
                        {
                            using (var reader = command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    if (reader.Read())
                                    {
                                        LTACount = reader.GetInt32(0);
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Failed to obtain LTA count information or no long term agreements exist.");
                                }
                                reader.Close();
                            }
                        }
                        catch
                        {
                            //return api with error
                            //    if (CustomerNumber == null)
                            //    {
                            //        CustomerNumber = "ERROR";
                            //    }
                            //    if (PartNumber == null)
                            //    {
                            //        PartNumber = "ERROR";
                            //    }
                            //    if (BranchPlant == null)
                            //    {
                            //        BranchPlant = "ERROR";
                            //    }
                            //    if (CustomerPartNumber == null)
                            //    {
                            //        CustomerPartNumber = "ERROR";
                            //    }

                            //    return new[]
                            //{
                            //              new GetPricingAPI { Price=null, MOQ=MOQ, LeadTime=leadtime,Quantity=0,CustomerPartNumber=CustomerPartNumber,BranchPlant=BranchPlant,ItemNumber=PartNumber,CustomerNumber=CustomerNumber}
                            //            };

                            //continue without LTA

                        }
                    }



                    if (gqh.CustomerName != null)
                    {
                        sqlScript = @"select top 1 Item_Number_3, Price, Customer_Number , c.abalph as Customer_Name
                                                  from iT_Sales_LTAUpload u
                                                  left outer join  [PRODDTA].[Cam404].[PRODDTA].[F0101] c on 
                                                   c.aban8=u.Customer_Number
                                                  where 1=1 and (item_number_2=isnull(@item_num,'') Or item_number_3=isnull(@cust_item_num,''))
                                                  and convert(char,division_id) in ('710000','730000','750000','720000','Q720000','780000','734000') 
                                                  and convert(date,effective_start_date)<=convert(date,getdate())
                                                  and convert(date, Effective_End_Date)>= convert(date, getdate()) and customer_number=@customer_number";
                    }
                    else if (gqh.CustomerPartNum != null)
                    {
                        sqlScript = @"select top 1 Item_Number_3, Price, Customer_Number , c.abalph as Customer_Name
                                                  from iT_Sales_LTAUpload u
                                                  left outer join  [PRODDTA].[Cam404].[PRODDTA].[F0101] c on 
                                                   c.aban8=u.Customer_Number
                                                  where 1=1 and item_number_3=isnull(@cust_item_num,'')
                                                  and convert(char,division_id) in ('710000','730000','750000','720000','Q720000','780000','734000') 
                                                  and convert(date,effective_start_date)<=convert(date,getdate())
                                                  and convert(date, Effective_End_Date)>= convert(date, getdate()) ;";
                    }
                    else
                    {
                        sqlScript = @"select top 1 Item_Number_3, Price, Customer_Number , c.abalph as Customer_Name
                                                  from iT_Sales_LTAUpload u
                                                  left outer join  [PRODDTA].[Cam404].[PRODDTA].[F0101] c on 
                                                   c.aban8=u.Customer_Number
                                                  where 1=1 and item_number_2=isnull(@item_num,'')
                                                  and convert(char,division_id) in ('710000','730000','750000','720000','Q720000','780000','734000') 
                                                  and convert(date,effective_start_date)<=convert(date,getdate())
                                                  and convert(date, Effective_End_Date)>= convert(date, getdate()) ;";
                    }
                    //decimal LTAPrice = 0;
                    using (var command = new SqlCommand(sqlScript, cnn))
                    {

                        if (obj.CustomerPartNumber != null)
                        {
                            SqlParameter cust_item_param = new SqlParameter
                            {
                                ParameterName = "@cust_item_num",
                                Value = obj.CustomerPartNumber
                            };

                            command.Parameters.Add(cust_item_param);
                        }
                        if (obj.CustomerNumber != null)
                        {
                            command.Parameters.Add(new SqlParameter("@customer_number", obj.CustomerNumber));
                            command.Parameters.Add(new SqlParameter("@item_num", item_num));
                        }
                        using (var reader = command.ExecuteReader())
                        {
                            try
                            {
                                if (reader.HasRows)
                                {

                                    while (reader.Read())
                                    {

                                        gqh.LTAPartNumberPrice = reader.GetDecimal(1);

                                    }
                                }
                                else
                                {
                                    gqh.LTAPartNumberPrice = 0;
                                }
                            }
                            catch (Exception ex)
                            {
                            }



                            reader.Close();
                        }
                    }

                    if (obj.BranchPlant != "750000" || obj.BranchPlant != "780000")
                    {
                        sqlScript = @"select top 1 pd.ibmcu, pd.IBACQ ACQ
                        , pd.ibltmf MfgLT
                        , pd.IBLTCM CumLT
                        , pd.QOH
                        , pd.PRODUCT_CODE
                        , pd.PRODUCT_GROUP
                        , pd.IBSRP5 ValueStream
                        , pd.IBSRP1 ProductType   
                        , convert(int,ltrim(pd.ibitm)) ibitm
                        , pd.IBMERL PartRevision
                        , isnull(vs.throughput,0) CapacityThroughput
                        From sT_PartDetails_JDE  pd left outer join [DataWarehouse].[dbo].[sT_ThroughputCapacityByValueStream] vs
							on pd.ibsrp2=vs.VALUE_STREAM and pd.ibmcu=vs.DIVISION
                            where pd.ibitm=@item and pd.ibmcu=@division";
                    }
                    else
                    {
                        sqlScript = @"select top 1 ibmcu, IBACQ ACQ
                        , ibltmf MfgLT
                        , pd.IBLTCM CumLT
                        , pd.QOH
                        , pd.PRODUCT_CODE
                        , pd.PRODUCT_GROUP
                        , pd.IBSRP2 ValueStream
                        , pd.IBSRP1 ProductType   
                        , convert(int,ltrim(pd.ibitm)) ibitm
                        , pd.IBMERL PartRevision
						, isnull(vs.throughput,0) CapacityThroughput
                        From sT_PartDetails_JDE pd left outer join [DataWarehouse].[dbo].[sT_ThroughputCapacityByValueStream] vs
							on pd.ibsrp2=vs.VALUE_STREAM and pd.ibmcu=vs.DIVISION
						where pd.ibitm=@item and pd.ibmcu=@division";
                    }
                    using (var command = new SqlCommand(sqlScript, cnn))
                    {
                        SqlParameter item_val = new SqlParameter
                        {
                            ParameterName = "@item",
                            Value = gqh.ibitm
                        };
                        command.Parameters.Add(item_val);
                        SqlParameter div = new SqlParameter
                        {
                            ParameterName = "@division",
                            Value = obj.BranchPlant
                        };
                        command.Parameters.Add(div);

                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    string division_check;
                                    division_check = reader.GetString(0);

                                    gqh.ACQ = Convert.ToInt32(reader.GetDecimal(1));
                                    gqh.MfgLT = Convert.ToInt32(reader.GetDecimal(2));
                                    gqh.CumLT = Convert.ToInt32(reader.GetDecimal(3));
                                    obj.StockQuantity = Convert.ToInt32(reader.GetDecimal(4));
                                    gqh.ProductCode = reader.IsDBNull(5) ? "" : (string)reader.GetValue(5);

                                    //gqh.ProductGroup = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                                    //gqh.ValueStream = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                                    //gqh.ProductType = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                                    //gqh.ibitm = reader.GetInt32(9);
                                    //gqh.PartRevision = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                                    //gqh.CapacityThroughput = Convert.ToInt32(reader.GetDecimal(11));

                                }
                            }
                            else
                            {

                                obj.LeadTime = 0;
                            }
                            reader.Close();
                        }

                    }

                    int bi_lead_time = 0;
                    int bi_rm_lead_time = 0;

                    if (gqh.MfgLT != null)
                    {
                        if (gqh.Division == "710000")
                        {
                            gqh.MfgLT = (gqh.MfgLT / 5);
                            //gqh.CumLT = ((gqh.MfgLT) + ((gqh.CumLT - (gqh.MfgLT * 5)) / 7));
                            obj.LeadTime = ((gqh.MfgLT) + ((gqh.CumLT - (gqh.MfgLT * 5)) / 7));
                            gqh.ActLT = bi_lead_time / 5;
                        }
                        else if (gqh.Division == "730000")// || model.GenerateQuoteHeader.Division=="710000")
                        {
                            gqh.MfgLT = bi_lead_time / 5;
                            //gqh.CumLT = ((gqh.CumLT - bi_lead_time) / 7);
                            obj.LeadTime = ((gqh.CumLT - bi_lead_time) / 7);
                            gqh.RmLT = bi_rm_lead_time;
                        }
                        else
                        {
                            int mfg_lt_var = 0;
                            mfg_lt_var = gqh.MfgLT / 5;
                            gqh.ActLT = bi_lead_time / 5;
                            int mfg_keep = gqh.MfgLT;
                            gqh.MfgLT = mfg_lt_var;
                            //gqh.CumLT = ((mfg_lt_var) + ((gqh.CumLT - mfg_keep) / 7));
                            obj.LeadTime = ((mfg_lt_var) + ((gqh.CumLT - mfg_keep) / 7));
                        }
                    }

                    int[] qty_breaks = { obj.Quantity };
                    foreach (int y in qty_breaks)
                    {
                        if (obj.CustomerNumber != null)
                        {


                            sqlScript = @"select top 1 m.MARGIN_VALUE, m.other from config_MARGIN m 
                                       left outer join organization o on m.DIVISION_ID = o.ORG_ID 
                                       WHERE 1 = 1 
                                       AND (o.DIVISION = isnull(@division_id,o.DIVISION) or m.VALUE_STREAM=isnull(@value_stream,m.Value_stream) or m.PRODUCT_GROUP=isnull(@product_group,m.product_group)
            		or m.PRODUCT_CODE=isnull(@product_code,m.product_code)
            		or m.CUSTOMER_NUMBER=isnull(@customer_number,m.customer_number)
            		or m.INTERNAL_PART_NUMBER=isnull(@part_number,m.INTERNAL_PART_NUMBER)
                                      or m.CUSTOMER_PART_NUMBER=isnull(@customer_part_number,m.CUSTOMER_PART_NUMBER)
            		or m.other=isnull(@other, m.OTHER))
            		and @qty_of_interest between m.QUANTITY_BREAK_RANGE_START and m.QUANTITY_BREAK_RANGE_END order by m.margin_value desc; ";
                        }
                        else
                        {
                            sqlScript = @"select top 1 m.MARGIN_VALUE, m.other from config_MARGIN m 
                                       left outer join organization o on m.DIVISION_ID = o.ORG_ID 
                                       WHERE 1 = 1 
                                       AND (o.DIVISION = isnull(@division_id,o.DIVISION) or m.VALUE_STREAM=isnull(@value_stream,m.Value_stream) or m.PRODUCT_GROUP=isnull(@product_group,m.product_group)
            		or m.PRODUCT_CODE=isnull(@product_code,m.product_code)
            		or m.INTERNAL_PART_NUMBER=isnull(@part_number,m.INTERNAL_PART_NUMBER)
                                      or m.CUSTOMER_PART_NUMBER=isnull(@customer_part_number,m.CUSTOMER_PART_NUMBER)
            		or m.other=isnull(@other, m.OTHER))
            		and @qty_of_interest between m.QUANTITY_BREAK_RANGE_START and m.QUANTITY_BREAK_RANGE_END order by m.margin_value desc; ";
                        }
                        using (var command = new SqlCommand(sqlScript, cnn))
                        {
                            SqlParameter division_val = new SqlParameter
                            {
                                ParameterName = "@division_id",
                                Value = obj.BranchPlant
                            };
                            command.Parameters.Add(division_val);

                            SqlParameter value_stream_param = new SqlParameter
                            {
                                ParameterName = "@value_stream",
                                Value = String.IsNullOrEmpty(gqh.ValueStream) ? "" : gqh.ValueStream.ToString()
                            };
                            command.Parameters.Add(value_stream_param);
                            SqlParameter product_group_param = new SqlParameter
                            {
                                ParameterName = "@product_group",
                                Value = String.IsNullOrEmpty(gqh.ProductGroup) ? "" : gqh.ProductGroup.ToString()
                            };
                            command.Parameters.Add(product_group_param);
                            SqlParameter product_code_param = new SqlParameter
                            {
                                ParameterName = "@product_code",
                                Value = String.IsNullOrEmpty(gqh.ProductCode) ? "" : gqh.ProductCode.ToString()
                            };
                            command.Parameters.Add(product_code_param);
                            if (obj.CustomerNumber != null)
                            {
                                SqlParameter customer_number_param = new SqlParameter
                                {
                                    ParameterName = "@customer_number",
                                    Value = obj.CustomerNumber
                                };
                                command.Parameters.Add(customer_number_param);
                            }
                            SqlParameter internal_part_number_param = new SqlParameter
                            {
                                ParameterName = "@part_number",
                                Value = String.IsNullOrEmpty(gqh.PartNumber) ? "" : gqh.PartNumber.ToString()
                            };
                            command.Parameters.Add(internal_part_number_param);
                            SqlParameter customer_part_number_param = new SqlParameter
                            {
                                ParameterName = "@customer_part_number",
                                Value = String.IsNullOrEmpty(obj.CustomerPartNumber) ? "" : obj.CustomerPartNumber.ToString()
                            };
                            command.Parameters.Add(customer_part_number_param);
                            SqlParameter other_param = new SqlParameter
                            {
                                ParameterName = "@other",
                                Value = String.IsNullOrEmpty(gqh.Other) ? "" : gqh.Other.ToString()
                            };
                            command.Parameters.Add(other_param);
                            SqlParameter qty_param = new SqlParameter
                            {
                                ParameterName = "@qty_of_interest",
                                Value = y
                            };
                            command.Parameters.Add(qty_param);

                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                try
                                {

                                    while (reader.Read())
                                    {
                                        if (reader.HasRows)
                                        {

                                            gqh.Margin = reader.GetDecimal(0);

                                        }
                                        //y++;
                                    }
                                }

                                catch (Exception ex)
                                {
                                    if (obj.CustomerNumber == null)
                                    {
                                        obj.CustomerNumber = "ERROR";
                                    }
                                    if (obj.ItemNumber == null)
                                    {
                                        obj.ItemNumber = "ERROR";
                                    }
                                    if (obj.BranchPlant == null)
                                    {
                                        obj.BranchPlant = "ERROR";
                                    }
                                    if (obj.CustomerPartNumber == null)
                                    {
                                        obj.CustomerPartNumber = "ERROR";
                                    }


                                    //pricingList.pricingList.Add(obj);
                                    //returnPriceList.pricingList.Add(obj);
                                }

                                finally
                                {
                                    reader.Close();
                                }

                            }

                        }
                    }


                }



                decimal adj_amount = 0;


                using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                {
                    cnn.Open();



                    int[] qty_breaks = { obj.Quantity };
                    int n = 1;
                    string sqlScript;
                    foreach (int y in qty_breaks)
                    {
                        if (obj.CustomerNumber != null)
                        {
                            sqlScript = @"SELECT isnull(SUM(ADJUSTMENT),0) TOTAL_ADJUSTMENT, CUSTOMER_RECENT_PRICE_FLAG, BASE_PRICE_FLAG, BASE_PRICE, RESERVE_FLAG, APPLY_VARIANCE_ADJUSTMENT
                                                , VALUE_STREAM, PRODUCT_GROUP, PRODUCT_CODE, CUSTOMER_NUMBER, INTERNAL_PART_NUMBER, CUSTOMER_PART_NUMBER, QUANTITY_BREAK_RANGE_START
                                              , QUANTITY_BREAK_RANGE_END, OTHER, LEAD_TIME_FLAG, MINIMUM_ORDER_QTY_FLAG, isnull(MINIMUM_ORDER_QUANTITY,0) MINIMUM_ORDER_QUANTITY
                                            FROM STRATEGIC_PRICING_ADJUSTMENTS where [status]=2 and DIVISION=@DIVISION 
                                         and @qty_of_interest between QUANTITY_BREAK_RANGE_START and QUANTITY_BREAK_RANGE_END
                                      and internal_part_number=@internal_part_number
                                      and customer_number=@customer_number
                                  GROUP BY CUSTOMER_RECENT_PRICE_FLAG, BASE_PRICE_FLAG, BASE_PRICE, RESERVE_FLAG, APPLY_VARIANCE_ADJUSTMENT,VALUE_STREAM, PRODUCT_GROUP, PRODUCT_CODE
                                    , CUSTOMER_NUMBER, INTERNAL_PART_NUMBER, CUSTOMER_PART_NUMBER, QUANTITY_BREAK_RANGE_START, QUANTITY_BREAK_RANGE_END, OTHER, LEAD_TIME_FLAG, MINIMUM_ORDER_QTY_FLAG, isnull(MINIMUM_ORDER_QUANTITY,0) ";
                        }
                        else
                        {
                            sqlScript = @"SELECT isnull(SUM(ADJUSTMENT),0) TOTAL_ADJUSTMENT, CUSTOMER_RECENT_PRICE_FLAG, BASE_PRICE_FLAG, BASE_PRICE, RESERVE_FLAG, APPLY_VARIANCE_ADJUSTMENT
                                                , VALUE_STREAM, PRODUCT_GROUP, PRODUCT_CODE, CUSTOMER_NUMBER, INTERNAL_PART_NUMBER, CUSTOMER_PART_NUMBER, QUANTITY_BREAK_RANGE_START
                                              , QUANTITY_BREAK_RANGE_END, OTHER, LEAD_TIME_FLAG, MINIMUM_ORDER_QTY_FLAG, isnull(MINIMUM_ORDER_QUANTITY,0) MINIMUM_ORDER_QUANTITY
                                            FROM STRATEGIC_PRICING_ADJUSTMENTS where [status]=2 and DIVISION=@DIVISION 
                                         and @qty_of_interest between QUANTITY_BREAK_RANGE_START and QUANTITY_BREAK_RANGE_END
                                      and internal_part_number=@internal_part_number
                                  GROUP BY CUSTOMER_RECENT_PRICE_FLAG, BASE_PRICE_FLAG, BASE_PRICE, RESERVE_FLAG, APPLY_VARIANCE_ADJUSTMENT,VALUE_STREAM, PRODUCT_GROUP, PRODUCT_CODE
                                    , CUSTOMER_NUMBER, INTERNAL_PART_NUMBER, CUSTOMER_PART_NUMBER, QUANTITY_BREAK_RANGE_START, QUANTITY_BREAK_RANGE_END, OTHER, LEAD_TIME_FLAG, MINIMUM_ORDER_QTY_FLAG, isnull(MINIMUM_ORDER_QUANTITY,0) ";
                        }

                        using (var command = new SqlCommand(sqlScript, cnn))
                        {

                            if (obj.CustomerNumber != null)
                            {
                                command.Parameters.Add(new SqlParameter("@customer_number", String.IsNullOrEmpty(obj.CustomerNumber) ? "" : obj.CustomerNumber.ToString()));
                            }

                            command.Parameters.Add(new SqlParameter("@internal_part_number", obj.ItemNumber));
                            command.Parameters.Add(new SqlParameter("@DIVISION", obj.BranchPlant));
                            SqlParameter qty_param = new SqlParameter
                            {
                                ParameterName = "@qty_of_interest",
                                Value = y
                            };
                            command.Parameters.Add(qty_param);

                            using (var reader = command.ExecuteReader())
                            {
                                try
                                {

                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {

                                            if (y <= 17.5)
                                            {

                                                gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                                gqh.BASE_PRICE = reader.GetDecimal(3);
                                                gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);


                                            }
                                            else if (y > 17.5 && y <= 37.5 && n == 1)
                                            {

                                                gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                                gqh.BASE_PRICE = reader.GetDecimal(3);
                                                gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                            }
                                            else if (y > 37.5 && y <= 62.5 && n == 1)
                                            {

                                                gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                                gqh.BASE_PRICE = reader.GetDecimal(3);
                                                gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                            }
                                            else if (y > 62.5 && y <= 87.5 && n == 1)
                                            {

                                                gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                                gqh.BASE_PRICE = reader.GetDecimal(3);
                                                gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                            }
                                            else if (y > 87.5 && y <= 187.5 && n == 1)
                                            {

                                                gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                                gqh.BASE_PRICE = reader.GetDecimal(3);
                                                gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                            }
                                            else if (y > 187.5 && y <= 375 && n == 1)
                                            {

                                                gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                                gqh.BASE_PRICE = reader.GetDecimal(3);
                                                gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                            }
                                            else if (y > 375 && y <= 675 && n == 1)
                                            {

                                                gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                                gqh.BASE_PRICE = reader.GetDecimal(3);
                                                gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                            }
                                            else if (y > 675 && y <= 875 && n == 1)
                                            {

                                                gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                                gqh.BASE_PRICE = reader.GetDecimal(3);
                                                gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                            }
                                            else if (y > 875 && y <= 1500 && n == 1)
                                            {

                                                gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                                gqh.BASE_PRICE = reader.GetDecimal(3);
                                                gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                            }
                                            else if (y > 1500 && y <= 2250 && n == 1)
                                            {

                                                gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                                gqh.BASE_PRICE = reader.GetDecimal(3);
                                                gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                            }
                                            else if (y > 2250 && y <= 3750 && n == 1)
                                            {

                                                gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                                gqh.BASE_PRICE = reader.GetDecimal(3);
                                                gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                            }
                                            else if (y > 3750 && y <= 7500 && n == 1)
                                            {

                                                gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                                gqh.BASE_PRICE = reader.GetDecimal(3);
                                                gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                                            }
                                            else if (y > 7500 && y <= 15000 && n == 1)
                                            {

                                                gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                                gqh.BASE_PRICE = reader.GetDecimal(3);
                                                gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                            }
                                            else if (y > 15000 && y <= 22500 && n == 1)
                                            {

                                                gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                                gqh.BASE_PRICE = reader.GetDecimal(3);
                                                gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                            }
                                            else if (y > 22500 && y <= 37500 && n == 1)
                                            {

                                                gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                                gqh.BASE_PRICE = reader.GetDecimal(3);
                                                gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                            }
                                            else if (y > 37500 && y <= 87500 && n == 1)
                                            {

                                                gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                                gqh.BASE_PRICE = reader.GetDecimal(3);
                                                gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                            }
                                            else if (y > 87500 && n == 1)
                                            {

                                                gqh.BASE_PRICE_FLAG = reader.GetInt32(2);
                                                gqh.BASE_PRICE = reader.GetDecimal(3);
                                                gqh.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                                            }

                                        }
                                    }

                                }


                                catch (Exception ex)
                                {
                                    if (obj.CustomerNumber == null)
                                    {
                                        obj.CustomerNumber = "ERROR";
                                    }
                                    if (obj.ItemNumber == null)
                                    {
                                        obj.ItemNumber = "ERROR";
                                    }
                                    if (obj.BranchPlant == null)
                                    {
                                        obj.BranchPlant = "ERROR";
                                    }
                                    if (obj.CustomerPartNumber == null)
                                    {
                                        obj.CustomerPartNumber = "ERROR";
                                    }


                                    //pricingList.pricingList.Add(obj);
                                    //returnPriceList.pricingList.Add(obj);
                                }


                                finally
                                {
                                    reader.Close();
                                }

                            }

                        }
                    }

                }

                //

                using (OdbcConnection cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString))
                {
                    string sqlScript;
                    cnn_odbc.Open();
                    if (cnn_odbc == null || cnn_odbc.State == ConnectionState.Closed)
                    {

                        cnn_odbc.Open();
                    }

                    int i = 1;
                    if (obj.CustomerNumber != null)
                    {
                        sqlScript = @"select trim(soh.Division) Division
                                      , soh.Unit_Price
                                    from(select sdmcu Division, sddoco Order_Num, so.sddcto Order_Type
                                                                        , dba.juliantodate(sdtrdj) as Order_Date
                                                                                        , trim(sdlitm) as Part_Number
                                                                        , sduorg Quantity
                                                                        , dec(sduprc * .000001, 15, 4) as Unit_Price
                                                                                        , case when sdnxtr = '999' then 'Closed' else 'Open' end as Status
                                                                                         , sdan8 Customer_No
                                                                        , (abalph) as Customer_Name
                                                                                             from PRODDTA.f4211 so join PRODDTA.f0101 on sdan8 = aban8

                                                                                          left outer join zpemjde.calendar_dim cal on
                                                                                                     so.sdtrdj = cal.juliandate
                                                                                             where    1 = 1            and trim(sdlitm) = ? and trim(sdmcu) in ('730000', '710000', '750000', '720000','734000','780000')
                                                                                               and sddcto in ('SO','SQ')
                                                                                             and sdlttr <> '980'
                                                                                             and DATE(cal.date) <= (current date)
                                                                                             union all
                                                                                             select sdmcu Division, sddoco Order_Num, sddcto Order_Type,  dba.juliantodate(sdtrdj) Order_Date, 
                                                                                                     trim(sdlitm) as Part_Number, sduorg Quantity, dec(sduprc * .000001, 15, 4) as Unit_Price,  
                                                                                                     case when sdnxtr = '999' then 'Closed' else 'Open' end as Status, 
                                                                                                     sdan8 Customer_No, trim(abalph) as Customer_Name
                                                                                             from PRODDTA.f42119 soh join PRODDTA.f0101 on sdan8 = aban8
                                                                                          left outer join
                                                                                                 zpemjde.calendar_dim cal2 on
                                                                                                 soh.sdtrdj = cal2.juliandate
                                                                                           where trim(sdlitm) = ?
                                                                                                 and trim(sdmcu) in ('730000', '710000', '750000', '720000','734000','780000')
                                                                                           and sddcto in ('SO','SQ')
                                                                                             and sdlttr<> '980' ) soh WHERE soh.customer_no = ?
                                                                    group by Division, Order_Num, Unit_Price, Status, Customer_Name, Customer_No ,soh.unit_price
                                    order by soh.unit_price desc
                                     fetch first row only";
                    }
                    else
                    {
                        sqlScript = @"select trim(soh.Division) Division
                                      , soh.Unit_Price
                                    from(select sdmcu Division, sddoco Order_Num, so.sddcto Order_Type
                                                                        , dba.juliantodate(sdtrdj) as Order_Date
                                                                                        , trim(sdlitm) as Part_Number
                                                                        , sduorg Quantity
                                                                        , dec(sduprc * .000001, 15, 4) as Unit_Price
                                                                                        , case when sdnxtr = '999' then 'Closed' else 'Open' end as Status
                                                                                         , sdan8 Customer_No
                                                                        , (abalph) as Customer_Name
                                                                                             from PRODDTA.f4211 so join PRODDTA.f0101 on sdan8 = aban8
                                                                                          left outer join zpemjde.calendar_dim cal on
                                                                                                     so.sdtrdj = cal.juliandate
                                                                                             where    1 = 1            and trim(sdlitm) = ? and trim(sdmcu) in ('730000', '710000', '750000', '720000','734000','780000')
                                                                                               and sddcto in ('SO','SQ')
                                                                                             and sdlttr <> '980'
                                                                                             and DATE(cal.date) <= (current date)
                                                                                             union all
                                                                                             select sdmcu Division, sddoco Order_Num, sddcto Order_Type,  dba.juliantodate(sdtrdj) Order_Date, 
                                                                                                     trim(sdlitm) as Part_Number, sduorg Quantity, dec(sduprc * .000001, 15, 4) as Unit_Price,  
                                                                                                     case when sdnxtr = '999' then 'Closed' else 'Open' end as Status, 
                                                                                                     sdan8 Customer_No, trim(abalph) as Customer_Name
                                                                                             from PRODDTA.f42119 soh join PRODDTA.f0101 on sdan8 = aban8
                                                                                          left outer join
                                                                                                 zpemjde.calendar_dim cal2 on
                                                                                                 soh.sdtrdj = cal2.juliandate
                                                                                           where trim(sdlitm) = ?
                                                                                                 and trim(sdmcu) in ('730000', '710000', '750000', '720000','734000','780000')
                                                                                           and sddcto in ('SO','SQ')
                                                                                             and sdlttr<> '980' ) soh WHERE 1=1
                                                                    group by Division, Order_Num, Unit_Price, Status, Customer_Name, Customer_No ,soh.unit_price
                                    order by soh.unit_price desc
                                     fetch first row only";
                    }

                    using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                    {
                        command.Parameters.Add(new OdbcParameter("@item_num", item_num));
                        //if (customer_num != null)
                        //{
                        //    command.Parameters.Add(new OdbcParameter("@customer_num", customer_num));
                        //}

                        command.Parameters.Add(new OdbcParameter("@item_num2", item_num));
                        if (obj.CustomerNumber != null)
                        {
                            command.Parameters.Add(new OdbcParameter("@customer_num", obj.CustomerNumber));
                        }
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    gqh.calc_dynam_fzn_unit_cost = Convert.ToDecimal(string.Format("{0:0.00}", Math.Round(reader.GetDecimal(1), 2)));
                                }
                                //if (gqh.Wins > 0)

                                if (gqh.BASE_PRICE_FLAG == 1)
                                {
                                    gqh.calc_dynam_fzn_unit_cost = gqh.BASE_PRICE;
                                }
                                history_flag = 1;
                            }

                            else
                            {
                                //i = 1;
                                if (obj.CustomerNumber != null)
                                {
                                    sqlScript = @"select sd.Unit_Price as Unit_Price, sd.sdshan from (select	dec(sduprc * .000001, 15, 4) as Unit_Price, sdshan
                                       from PRODDTA.f4211 so join PRODDTA.f0101 on sdshan = aban8 
                                left outer join zpemjde.calendar_dim cal on 
                                                           so.sdtrdj = cal.juliandate 
                                       where 1=1
                                       and trim(so.sdlitm) =?
                                      and trim(sdmcu) in ('730000','710000','750000','720000','734000','780000')
                                       and sddcto in ('SQ') 
                                       and sdlttr<> '980' 
                                                      and date(cal.date)<= (current date)  
                                          and dec(sdan8,15,0)=?
                                       union all 
                                       select  dec(sduprc * .000001, 15, 4) as Unit_Price , sdshan
                                      from PRODDTA.f42119 soh join PRODDTA.f0101 on sdshan = aban8  
                                left outer join zpemjde.calendar_dim cal2 on soh.sdtrdj = cal2.juliandate 
                                       where 1=1
                                       and trim(sdlitm) =?
                                          and trim(sdmcu) in ('730000','710000','750000','720000','734000','780000')
                                       and sddcto in ('SQ') 
                                      and  dec(sdan8,15,0)=?
                                       and sdlttr<> '980' ) sd
                                      order by Unit_Price desc
                                      fetch first row only";
                                }
                                else
                                {
                                    sqlScript = @"select sd.Unit_Price as Unit_Price, sd.sdshan from (select	dec(sduprc * .000001, 15, 4) as Unit_Price, sdshan
                                       from PRODDTA.f4211 so join PRODDTA.f0101 on sdshan = aban8 
                                left outer join zpemjde.calendar_dim cal on 
                                                           so.sdtrdj = cal.juliandate 
                                       where 1=1
                                       and trim(so.sdlitm) =?
                                      and trim(sdmcu) in ('730000','710000','750000','720000','734000','780000')
                                       and sddcto in ('SQ') 
                                       and sdlttr<> '980' 
                                                      and date(cal.date)<= (current date)  
                                       union all 
                                       select  dec(sduprc * .000001, 15, 4) as Unit_Price , sdshan
                                      from PRODDTA.f42119 soh join PRODDTA.f0101 on sdshan = aban8  
                                left outer join zpemjde.calendar_dim cal2 on soh.sdtrdj = cal2.juliandate 
                                       where 1=1
                                       and trim(sdlitm) =?
                                          and trim(sdmcu) in ('730000','710000','750000','720000','734000','780000')
                                       and sddcto in ('SQ') 
                                       and sdlttr<> '980' ) sd
                                      order by Unit_Price desc
                                      fetch first row only";
                                }

                                using (var command2 = new OdbcCommand(sqlScript, cnn_odbc))
                                {
                                    command2.Parameters.Add(new OdbcParameter("@item_num", item_num));
                                    if (obj.CustomerNumber != null)
                                    {
                                        command2.Parameters.Add(new OdbcParameter("@customer_num", obj.CustomerNumber));
                                    }
                                    command2.Parameters.Add(new OdbcParameter("@item_num2", item_num));
                                    command2.Parameters.Add(new OdbcParameter("@customer_num2", obj.CustomerNumber));
                                    using (var reader2 = command2.ExecuteReader())
                                    {
                                        if (reader2.HasRows)
                                        {
                                            while (reader2.Read())
                                            {
                                                gqh.calc_dynam_fzn_unit_cost = Convert.ToDecimal(string.Format("{0:0.00}", Math.Round(reader2.GetDecimal(0), 2)));
                                            }
                                            history_flag = 1;
                                        }

                                    }
                                }
                            }
                            reader.Close();

                            if (gqh.BASE_PRICE_FLAG == 1)
                            {
                                gqh.calc_dynam_fzn_unit_cost = gqh.BASE_PRICE;
                            }

                        }
                    }

                    decimal price = gqh.calc_dynam_fzn_unit_cost;
                    if (price != 0)
                    {
                        true_price = Convert.ToString(price);
                    }
                    else
                    {

                        //cnn_odbc.Open();
                        if (cnn_odbc == null || cnn_odbc.State == ConnectionState.Closed)
                        {

                            cnn_odbc.Open();
                        }
                        //check history flag
                        sqlScript = @"select count(*) history_int from (select distinct trim(soh.Division) Division
                                      , soh.part_number , soh.order_num 
                                  , soh.customer_no
                                    from(select sdmcu Division, sddoco Order_Num, so.sddcto Order_Type
                                                                        , dba.juliantodate(sdtrdj) as Order_Date
                                                                                        , trim(sdlitm) as Part_Number
                                                                        , sduorg Quantity
                                                                        , dec(sduprc * .000001, 15, 4) as Unit_Price
                                                                                        , case when sdnxtr = '999' then 'Closed' else 'Open' end as Status
                                                                                         , sdan8 Customer_No
                                                                        , (abalph) as Customer_Name
                                                                                             from PRODDTA.f4211 so join PRODDTA.f0101 on sdan8 = aban8
                                                                                          left outer join zpemjde.calendar_dim cal on
                                                                                                     so.sdtrdj = cal.juliandate
                                                                                             where    1 = 1           and trim(sdlitm) =?
                                      and trim(sdmcu) in ?
                                                                                               and sddcto in ('SO')
                                                                                             and sdlttr <> '980'
                                                                                             and DATE(cal.date) <= (current date)
                                                                                             union all
                                                                                             select sdmcu Division, sddoco Order_Num, sddcto Order_Type,  dba.juliantodate(sdtrdj) Order_Date, 
                                                                                                     trim(sdlitm) as Part_Number, sduorg Quantity, dec(sduprc * .000001, 15, 4) as Unit_Price,  
                                                                                                     case when sdnxtr = '999' then 'Closed' else 'Open' end as Status, 
                                                                                                     sdan8 Customer_No, trim(abalph) as Customer_Name
                                                                                             from PRODDTA.f42119 soh join PRODDTA.f0101 on sdan8 = aban8
                                                                                          left outer join
                                                                                                 zpemjde.calendar_dim cal2 on
                                                                                                 soh.sdtrdj = cal2.juliandate
                                                                                           where 1=1  and trim(sdlitm) = ?
                                                                                                 and trim(sdmcu) in ?
                                                                                           and sddcto in ('SO')
                                                                                             and sdlttr<> '980' ) soh WHERE 1=1
                                                                    group by Division , Order_Num, customer_no
                                                                      , Part_number) cdo
                                                                      group by part_number, division";

                        using (var command2 = new OdbcCommand(sqlScript, cnn_odbc))
                        {
                            command2.Parameters.Add(new OdbcParameter("@item_num", item_num));

                            command2.Parameters.Add(new OdbcParameter("@branch_plant", obj.BranchPlant));
                            command2.Parameters.Add(new OdbcParameter("@item_num2", item_num));

                            command2.Parameters.Add(new OdbcParameter("@branch_plant2", obj.BranchPlant));

                            using (var reader2 = command2.ExecuteReader())
                            {
                                if (reader2.HasRows)
                                {
                                    while (reader2.Read())
                                    {
                                        history_flag = reader2.GetInt32(0);
                                    }

                                }

                            }
                        }



                        //decimal leadtime = 25;

                        //if (true_price="0")
                        //{
                        //    true_price = null;
                        //}

                        //if (Convert.ToDecimal(price) > 0 && history_flag > 1)
                        //{

                        //    returnPriceList.pricingList.Add(obj);

                        //}

                        //else
                        //{

                        //go get cost

                        //using (OdbcConnection cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString))
                        //{

                        //cnn_odbc.Open();
                        if (cnn_odbc == null || cnn_odbc.State == ConnectionState.Closed)
                        {

                            cnn_odbc.Open();
                        }

                        int int_num = 1;

                        gqh.totalCPQtyCostVar = 0;
                        gqh.totalCPQtyCostSU = 0;
                        gqh.totalCPQtyCostFOH = 0;

                        if (obj.BranchPlant == "730000" || obj.BranchPlant == "734000")
                        {
                            sqlScript = @"select	ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ixum, ibacq, ibstkt, ibltmf, ibltcm, imdsc1,  
                        dec(ixqnty / 1000000, 15, 6) as ixqnty, ixcpnb,  
                        (select sum(lipqoh) from proddta.f41021 where limcu = ixmmcu and liitm = ixitm) as pqoh,  
                        (select sum(lipqoh) - sum(lipcom) - sum(lihcom) - sum(lifcom) - sum(liqowo) from proddta.f41021 
                        where limcu = ixmmcu and liitm = ixitm) as avail, 
                        sum(dec(iexscr / 1000000, 15, 7)) as simCost
                      , sum(dec(iecsl / 1000000, 15, 7)) as fznCost ,
                              coalesce(sum(case when iecost = 'A1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznA1,
                              coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB2,
            coalesce(sum(case when iecost = 'B1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB1,
            coalesce(sum(case when iecost = 'C3' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC3,
            coalesce(sum(case when iecost = 'C1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC1,
                              coalesce(sum(case when iecost = 'C4' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC4,
            coalesce(sum(case when iecost = 'C2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC2,
            coalesce(sum(case when left(iecost,1) = 'D' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznD0,
                              coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000*ibacq as decimal(15,7)) else 0 end),0) as fznSUTotal
                        from proddta.f3002 join proddta.f4102 on ixitm = ibitm and ixmmcu = ibmcu and left(ibglpt,2) in ('FG', 'CP') 
                        join proddta.f4101 on imitm = ibitm 
                        left join proddta.f30026 on iemmcu = ibmcu and ieitm = ibitm and ieledg = ?
                        where ixtbm in ('M')
                      and ixkit = ? 
                        and trim(ixmmcu) = ? 
                        group by    ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ibacq, imdsc1, ixqnty, ibstkt, ixcpnb, ibltmf, ibltcm, ixum";
                        }
                        else if (obj.BranchPlant == "710000")
                        {
                            sqlScript = @"select	ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ixum, ibacq, ibstkt, ibltmf, ibltcm, imdsc1,  
                        dec(ixqnty / 1000000, 15, 6) as ixqnty, ixcpnb,  
                        (select sum(lipqoh) from proddta.f41021 where limcu = ixmmcu and liitm = ixitm) as pqoh,  
                        (select sum(lipqoh) - sum(lipcom) - sum(lihcom) - sum(lifcom) - sum(liqowo) from proddta.f41021 
                        where limcu = ixmmcu and liitm = ixitm) as avail, 
                        sum(dec(iexscr / 1000000, 15, 7)) as simCost
                      , sum(dec(iecsl / 1000000, 15, 7)) as fznCost ,
                              coalesce(sum(case when left(iecost,1) = 'A' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simA1,
                              coalesce(sum(case when iecost = 'B2' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB2,
            coalesce(sum(case when iecost = 'B1' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB1,
            coalesce(sum(case when iecost = 'C3' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC3,
            coalesce(sum(case when iecost = 'C1' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC1,
                             coalesce(sum(case when iecost = 'C4' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC4,
            coalesce(sum(case when iecost = 'C2' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC2,
            coalesce(sum(case when left(iecost,1) = 'D' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simD0,
                              coalesce(sum(case when iecost = 'B2' then cast(iexscr/1000000*ibacq as decimal(15,7)) else 0 end),0) simSUTotal
                        from proddta.f3002 join proddta.f4102 on ixitm = ibitm and ixmmcu = ibmcu and left(ibglpt,2) in ('FG', 'CP', 'RM') 
                        join proddta.f4101 on imitm = ibitm 
                        left join proddta.f30026 on iemmcu = ibmcu and ieitm = ibitm and ieledg = ?
                        where ixtbm in ('M')
                      and ixkit = ? 
                        and trim(ixmmcu) = ? 
                        group by    ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ibacq, imdsc1, ixqnty, ibstkt, ixcpnb, ibltmf, ibltcm, ixum";
                        }
                        else
                        {
                            sqlScript = @"select	ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ixum, case when trim(ixmmcu)='780000' then case when ibacq=0 then 1 else ibacq end else ibacq end as ibacq, ibstkt, ibltmf, ibltcm,imdsc1,
                        CAST(dec(ixqnty / 1000000, 15, 6) AS INTEGER) as ixqnty, ixcpnb,  
                        ifnull((select sum(lipqoh) from proddta.f41021 where limcu = ixmmcu and liitm = ixitm),0) as pqoh,  
                        ifnull((select sum(lipqoh) - sum(lipcom) - sum(lihcom) - sum(lifcom) - sum(liqowo) from proddta.f41021 
                        where limcu = ixmmcu and liitm = ixitm),0) as avail, 
                        sum(dec(iexscr / 1000000, 15, 7)) as simCost
                      , sum(dec(iecsl / 1000000, 15, 7)) as fznCost ,
                              coalesce(sum(case when iecost = 'A1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznA1,
                              coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB2,
            coalesce(sum(case when iecost = 'B1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB1,
            coalesce(sum(case when iecost = 'C3' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC3,
            coalesce(sum(case when iecost = 'C1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC1,
                              coalesce(sum(case when iecost = 'C4' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC4,
            coalesce(sum(case when iecost = 'C2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC2,
            coalesce(sum(case when left(iecost,1) = 'D' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznD0,
                              coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000*ibacq as decimal(15,7)) else 0 end),0) as fznSUTotal
                        from proddta.f3002 join proddta.f4102 on ixitm = ibitm and ixmmcu = ibmcu and left(ibglpt,2) in ('FG', 'CP', 'RM') 
                        join proddta.f4101 on imitm = ibitm 
                        left join proddta.f30026 on iemmcu = ibmcu and ieitm = ibitm and ieledg = ?
                        where ixtbm in ('M')
                      and ixkit = ? 
                        and trim(ixmmcu) = ? 
                        group by    ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ibacq, imdsc1, ixqnty, ibstkt, ixcpnb, ibltmf, ibltcm, ixum";
                        }
                        //and dec(ixqnty / 1000000, 15, 6)>=1
                        using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                        {
                            command.Parameters.Add(new OdbcParameter("@cost_year", "07")); //only current standard cost year
                            command.Parameters.Add(new OdbcParameter("@item_num", gqh.ibitm));
                            command.Parameters.Add(new OdbcParameter("@division", obj.BranchPlant));

                            using (var reader = command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        ComponentFound item = new ComponentFound()
                                        {
                                            //reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                                            //ItemKey = int_num,
                                            ComponentNum = reader.GetString(4), //ixitm
                                            ComponentUOM = reader.GetString(5), //ixum
                                            ComponentDesc = reader.GetString(10), //imdsc1
                                            ComponentStock = reader.GetString(7), //ibstkt
                                            ComponentACQ = reader.IsDBNull(6) ? 0 : (decimal)reader.GetValue(6),
                                            ComponentMfgLT = reader.IsDBNull(8) ? 0 : (decimal)reader.GetValue(8),
                                            ComponentQty = reader.GetInt32(11),
                                            ChangeQuantity = reader.GetInt32(11),
                                            ComponentOnHand = reader.GetInt32(13),
                                            ComponentAvailQt = reader.GetInt32(14),
                                            ComponentCostFzn = reader.IsDBNull(16) ? 0 : (decimal)reader.GetValue(16),
                                            CompMtlCost = reader.IsDBNull(17) ? 0 : (decimal)reader.GetValue(17),
                                            CompSetup = reader.IsDBNull(18) ? 0 : (decimal)reader.GetValue(18),
                                            CompLabor = reader.IsDBNull(19) ? 0 : (decimal)reader.GetValue(19),
                                            CompLbrVOH = reader.IsDBNull(20) ? 0 : (decimal)reader.GetValue(20),
                                            CompMchVOH = reader.IsDBNull(21) ? 0 : (decimal)reader.GetValue(21),
                                            CompLbrFOH = reader.IsDBNull(22) ? 0 : (decimal)reader.GetValue(22),
                                            CompMchFOH = reader.IsDBNull(23) ? 0 : (decimal)reader.GetValue(23),
                                            CompOSP = reader.IsDBNull(24) ? 0 : (decimal)reader.GetValue(24),
                                            CompSUTotal = reader.IsDBNull(25) ? 0 : (decimal)reader.GetValue(25),
                                            FznCPQtyCostSU = Math.Round((reader.IsDBNull(25) ? 0 : (decimal)reader.GetValue(25)), 5) / (reader.IsDBNull(6) ? 0 : (decimal)reader.GetValue(6)),
                                            FznCPQtyCostVar = (reader.IsDBNull(17) ? 0 : (decimal)reader.GetValue(17)) + (reader.IsDBNull(19) ? 0 : (decimal)reader.GetValue(19)) + (reader.IsDBNull(20) ? 0 : (decimal)reader.GetValue(20)) + (reader.IsDBNull(21) ? 0 : (decimal)reader.GetValue(21)) + (reader.IsDBNull(24) ? 0 : (decimal)reader.GetValue(24)), //fzna1 fznb1 fznD0 fznc1 fznc3
                                            FznCPQtyCostFOH = (((reader.IsDBNull(23) ? 0 : (decimal)reader.GetValue(23)) + (reader.IsDBNull(22) ? 0 : (decimal)reader.GetValue(22))) * (reader.IsDBNull(6) ? 0 : (decimal)reader.GetValue(6))) / (reader.IsDBNull(6) ? 0 : (decimal)reader.GetValue(6))
                                            //loop through?
                                        };

                                        if (Convert.ToDecimal(item.ChangeQuantity) > 0)
                                        {
                                            string test_compNum = item.ComponentNum;
                                            int test_compqty = item.ComponentQty;// = reader.GetInt32(8), //ixqnty
                                            int test_availqt = item.ComponentAvailQt; //= reader.GetInt32(11), //avail
                                            decimal test_CostFzn = item.ComponentCostFzn; //= reader.GetDecimal(12), //fzna1
                                            decimal test_FznRecalc = item.ComponentCostFznRecalc; //= reader.GetDecimal(12), //fzna1
                                            decimal test_MtlCost = item.CompMtlCost; //= reader.GetDecimal(14), //fznb2
                                            decimal test_ComLabor = item.CompLabor; //= reader.GetDecimal(13), //fzna1
                                            decimal test_ComSetup = item.CompSetup;
                                            decimal test_CompLbrVOH = item.CompLbrVOH;
                                            decimal test_CompMchVOH = item.CompMchVOH;
                                            decimal test_CompLbrFOH = item.CompLbrFOH;
                                            decimal test_CompMchFOH = item.CompMchFOH;
                                            decimal test_CompOSP = item.CompOSP; // = reader.GetDecimal(20), //fznd0
                                            decimal test_compSU = item.CompSUTotal; //= reader.GetDecimal(21), //fznsutotal
                                            gqh.totalCPQtyCostVar = (item.FznCPQtyCostVar * item.ChangeQuantity) + gqh.totalCPQtyCostVar;
                                            gqh.totalCPQtyCostSU = (item.FznCPQtyCostSU * item.ChangeQuantity) + gqh.totalCPQtyCostSU;
                                            gqh.totalCPQtyCostFOH = (item.FznCPQtyCostFOH * item.ChangeQuantity) + gqh.totalCPQtyCostFOH;

                                        }
                                        else
                                        {
                                            if (item.FznCPQtyCostVar != 0 && item.FznCPQtyCostSU != 0)
                                            {
                                                gqh.totalCPQtyCostVar = (item.FznCPQtyCostVar) + gqh.totalCPQtyCostVar;
                                                gqh.totalCPQtyCostSU = (item.FznCPQtyCostSU) + gqh.totalCPQtyCostSU;
                                                gqh.totalCPQtyCostFOH = (item.FznCPQtyCostFOH) + gqh.totalCPQtyCostFOH;
                                            }
                                            else
                                            {
                                                gqh.totalCPQtyCostVar = 0 + gqh.totalCPQtyCostVar;
                                                gqh.totalCPQtyCostSU = 0 + gqh.totalCPQtyCostSU;
                                                gqh.totalCPQtyCostFOH = 0 + gqh.totalCPQtyCostFOH;
                                            }
                                        }
                                        int_num = int_num + 1;
                                        Components.Add(item);
                                    }
                                }

                                else
                                {
                                    foreach (var item in Components)
                                    {
                                        gqh.totalCPQtyCostVar = 0;
                                        gqh.totalCPQtyCostSU = 0;
                                        gqh.totalCPQtyCostFOH = 0;
                                        gqh.totalCPQtyCostVar = (item.FznCPQtyCostVar * item.ChangeQuantity) + gqh.totalCPQtyCostVar;
                                        gqh.totalCPQtyCostSU = (item.FznCPQtyCostSU * item.ChangeQuantity) + gqh.totalCPQtyCostSU;
                                        gqh.totalCPQtyCostFOH = (item.FznCPQtyCostFOH * item.ChangeQuantity) + gqh.totalCPQtyCostFOH;

                                    }
                                }
                                reader.Close();
                            }
                        }

                        decimal fznMatl; //fzna1
                        decimal fznLbr; //fznb1
                        decimal fznb2;
                        decimal fznc1;
                        decimal fznc2;
                        decimal fznc3;
                        decimal fznc4;
                        decimal fznOS; //fznd0
                        decimal fznlc;
                        decimal fznSUTotal;
                        decimal fznLCTotal;

                        if (obj.BranchPlant == "710000")
                        {
                            sqlScript = @"select coalesce(sum(case when left(iecost,1) = 'A' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simA1,
            coalesce(sum(case when iecost = 'B1' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB1,
            coalesce(sum(case when iecost = 'B2' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB2,
            coalesce(sum(case when iecost = 'C1' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC1,
            coalesce(sum(case when iecost = 'C2' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC2,
            coalesce(sum(case when iecost = 'C3' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC3,
            coalesce(sum(case when iecost = 'C4' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC4,
                                               coalesce(sum(case when left(iecost,1) = 'D' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simD0,
                                               coalesce(sum(case when trim(iemcu) = 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simLC,
                                               coalesce(sum(case when iecost = 'B2' then cast(iexscr/1000000*CAST(? AS INTEGER) as decimal(15,7)) else 0 end),0) 
            	as simSUTotal, 
                                              decimal(coalesce( sum(case when iecost = 'A1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznA1Net,
                               decimal(coalesce( sum(case when iecost = 'B1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB1Net,
                               decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB2Net,
                               decimal(coalesce( sum(case when iecost = 'C1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC1Net,
                               decimal(coalesce( sum(case when iecost = 'C2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC2Net,
                               decimal(coalesce( sum(case when iecost = 'C3' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC3Net,
                               decimal(coalesce( sum(case when iecost = 'C4' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC4Net,
                               decimal(coalesce( sum(case when left(iecost,1) = 'D' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznD0Net,
                               decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 * CAST(? AS INTEGER) as decimal(15,5)) else 0 end),0),10,5) 
                                as fznSUTotalNet , coalesce(sum(case when trim(iemcu) = 'V78LOT001' then cast(iexscr/1000000*CAST(? as INTEGER) as decimal(15,7)) else 0 end),0) simLCTotal, 
                               decimal(coalesce( sum(case when iecost = 'A2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznA2Net,
                                  decimal(coalesce( sum(case when iecost = 'D3' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznD3Net
                                                  from proddta.f300261
                                                  where ieledg = ?
                                                  and trim(iemmcu)=?
                                                  and ieitm = ?";
                        }

                        else
                        {
                            sqlScript = @"select decimal(coalesce(sum(case when iecost = 'A1'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznA1, 
                                               decimal(coalesce(sum(case when iecost = 'B1'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznB1, 
                                               decimal(coalesce(sum(case when iecost = 'B2'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznB2, 
                                               decimal(coalesce(sum(case when iecost = 'C1'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC1, 
                                               decimal(coalesce(sum(case when iecost = 'C2'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC2, 
                                               decimal(coalesce(sum(case when iecost = 'C3'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC3, 
                                               decimal(coalesce(sum(case when iecost = 'C4'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC4, 
                                               decimal(coalesce(sum(case when left(iecost, 1) = 'D' then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznD0,    
                                               decimal(coalesce(sum(case when iecost = 'B2' then cast((iecsl * .000001 * CAST(? AS INTEGER)) as decimal(15, 5)) else 0 end), 0),10,5) as fznSUTotal, 
                                              decimal(coalesce( sum(case when iecost = 'A1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznA1Net,
                               decimal(coalesce( sum(case when iecost = 'B1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB1Net,
                               decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB2Net,
                               decimal(coalesce( sum(case when iecost = 'C1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC1Net,
                               decimal(coalesce( sum(case when iecost = 'C2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC2Net,
                               decimal(coalesce( sum(case when iecost = 'C3' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC3Net,
                               decimal(coalesce( sum(case when iecost = 'C4' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC4Net,
                               decimal(coalesce( sum(case when left(iecost,1) = 'D' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznD0Net,
                               decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 * CAST(? AS INTEGER) as decimal(15,5)) else 0 end),0),10,5) as fznSUTotalNet
                                                  from proddta.f30026
                                                  where ieledg = ?
                                                  and trim(iemmcu) in (?)
                                                  and trim(ielitm) = ?";
                        }
                        using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                        {

                            command.Parameters.Add(new OdbcParameter("@ACQ", gqh.ACQ));


                            command.Parameters.Add(new OdbcParameter("@ACQ2", gqh.ACQ));

                            if (obj.BranchPlant != "730000")
                            {
                                command.Parameters.Add(new OdbcParameter("@ACQ3", gqh.ACQ));
                            }
                            command.Parameters.Add(new OdbcParameter("@year", "07"));
                            command.Parameters.Add(new OdbcParameter("@division_num", obj.BranchPlant));
                            if (obj.BranchPlant == "710000")
                            {
                                command.Parameters.Add(new OdbcParameter("@item_num", gqh.ibitm));
                            }
                            else
                            {
                                command.Parameters.Add(new OdbcParameter("@item_num", item_num));
                            }

                            try
                            {
                                using (var reader = command.ExecuteReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            if (obj.BranchPlant == "710000")
                                            {

                                                fznMatl = reader.GetDecimal(0);
                                                fznLbr = reader.GetDecimal(1);
                                                fznb2 = reader.GetDecimal(2);
                                                fznc1 = reader.GetDecimal(3);
                                                fznc2 = reader.GetDecimal(4);
                                                fznc3 = reader.GetDecimal(5);
                                                fznc4 = reader.GetDecimal(6);
                                                fznOS = reader.GetDecimal(7);
                                                fznlc = reader.GetDecimal(8);
                                                fznSUTotal = reader.GetDecimal(9);
                                                gqh.fznA1Net = reader.GetDecimal(10); //fznmatlnet
                                                gqh.fznB1Net = reader.GetDecimal(11); //fznlbrnet
                                                gqh.fznB2Net = reader.GetDecimal(12);
                                                gqh.fznc1net = reader.GetDecimal(13);
                                                gqh.fznc2net = reader.GetDecimal(14);
                                                gqh.fznc3net = reader.GetDecimal(15);
                                                gqh.fznc4net = reader.GetDecimal(16);
                                                gqh.fznD0Net = reader.GetDecimal(17); //fznosnet
                                                gqh.fznSUTotalNet = reader.IsDBNull(18) ? 0 : (decimal)reader.GetValue(18);
                                                gqh.fznLCTotal = reader.GetDecimal(19);
                                                gqh.material = fznMatl;
                                                gqh.setup = fznb2; //setup
                                                gqh.labor = fznLbr;
                                                gqh.lbr_voh = gqh.fznc3net;
                                                gqh.mch_voh = gqh.fznc1net;
                                                gqh.lbr_foh = gqh.fznc4net;
                                                gqh.mch_foh = fznc2;
                                                gqh.osp = gqh.fznD0Net;

                                            }
                                            else
                                            {
                                                fznMatl = reader.GetDecimal(0); //a1
                                                fznLbr = reader.GetDecimal(1); //b1
                                                fznb2 = reader.GetDecimal(2);
                                                fznc1 = reader.GetDecimal(3);
                                                fznc2 = reader.GetDecimal(4);
                                                fznc3 = reader.GetDecimal(5);
                                                fznc4 = reader.GetDecimal(6);
                                                fznOS = reader.GetDecimal(7); //d0
                                                fznSUTotal = reader.GetDecimal(2) * gqh.ACQ;
                                                gqh.fznA1Net = reader.GetDecimal(9); //fznmatlnet
                                                gqh.fznB1Net = reader.GetDecimal(10); //fznlbrnet
                                                gqh.fznB2Net = reader.GetDecimal(11);
                                                gqh.fznc1net = reader.GetDecimal(12);
                                                gqh.fznc2net = reader.GetDecimal(13);
                                                gqh.fznc3net = reader.GetDecimal(14);
                                                gqh.fznc4net = reader.GetDecimal(15);
                                                gqh.fznD0Net = reader.GetDecimal(16); //fznosnet
                                                gqh.fznSUTotalNet = reader.IsDBNull(17) ? 0 : (decimal)reader.GetValue(17);
                                                if (obj.BranchPlant == "710000")
                                                {
                                                    gqh.fznLCTotal = reader.GetDecimal(19); //gqh.total = fznMatl + fznLbr + fznOS + fznb2 + fznc1 + fznc3 + fznc2 + fznc4; //fznfoh fznvoh
                                                }
                                                gqh.material = fznMatl;
                                                gqh.setup = fznb2; //setup
                                                gqh.labor = fznLbr;
                                                gqh.lbr_voh = fznc3;
                                                gqh.mch_voh = fznc1;
                                                gqh.lbr_foh = fznc4;
                                                gqh.mch_foh = fznc2;
                                                gqh.osp = fznOS;
                                                gqh.UnitCost = fznMatl + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS + fznb2;
                                            }
                                            if (obj.BranchPlant == "730000")
                                            {
                                                //if (item_var.item_num == "BH01816-4") 
                                                //{
                                                //    int check_me=1;
                                                //    //check_me = 1;
                                                //    check_me = 1;
                                                //}
                                                if (Components.Count() > 0)
                                                {
                                                    gqh.calc_dynam_qty = obj.Quantity;
                                                    if (obj.Quantity > 0)
                                                    {
                                                        gqh.calc_dynam_fzn_unit_cost = (((obj.Quantity * (gqh.fznA1Net + adj_amount + gqh.fznB1Net + gqh.fznc1net + gqh.fznc3net + gqh.fznD0Net + ((gqh.fznc2net + gqh.fznc4net) / obj.Quantity) + gqh.totalCPQtyCostVar + gqh.totalCPQtyCostSU + gqh.totalCPQtyCostFOH)) + gqh.fznSUTotalNet)) / obj.Quantity;
                                                    }
                                                    else
                                                    {
                                                        gqh.calc_dynam_fzn_unit_cost = (((obj.Quantity * (gqh.fznA1Net + adj_amount + gqh.fznB1Net + gqh.fznc1net + gqh.fznc3net + gqh.fznD0Net + ((gqh.fznc2net + gqh.fznc4net)) + gqh.totalCPQtyCostVar + gqh.totalCPQtyCostSU + gqh.totalCPQtyCostFOH + (gqh.fznSUTotalNet)))));
                                                    }
                                                }
                                                else
                                                {
                                                    gqh.calc_dynam_qty = obj.Quantity;
                                                    if (obj.Quantity > 0)
                                                    {
                                                        gqh.calc_dynam_fzn_unit_cost = (((obj.Quantity * (fznMatl + gqh.fznB1Net + adj_amount + gqh.fznc1net + gqh.fznc3net + gqh.fznD0Net + ((gqh.fznc2net + gqh.fznc4net) / obj.Quantity) + gqh.totalCPQtyCostVar + gqh.totalCPQtyCostSU + gqh.totalCPQtyCostFOH)) + gqh.fznSUTotalNet)) / obj.Quantity;
                                                    }
                                                    else
                                                    {
                                                        gqh.calc_dynam_fzn_unit_cost = (((obj.Quantity * (fznMatl + gqh.fznB1Net + adj_amount + gqh.fznc1net + gqh.fznc3net + gqh.fznD0Net + ((gqh.fznc2net + gqh.fznc4net)) + gqh.totalCPQtyCostVar + gqh.totalCPQtyCostSU + gqh.totalCPQtyCostFOH + (gqh.fznSUTotalNet)))));
                                                    }
                                                }
                                            }
                                            else if (Components.Count() > 0 && obj.BranchPlant == "720000")
                                            {
                                                gqh.calc_dynam_qty = obj.Quantity;

                                                if (obj.Quantity > 0)
                                                {
                                                    gqh.calc_dynam_fzn_unit_cost = (adj_amount) + (fznMatl + fznLbr + fznc2 + fznc1 + fznc3 + fznc4 + fznOS) + fznSUTotal / obj.Quantity;
                                                }
                                            }
                                            else if (Components.Count() == 0 && obj.BranchPlant == "720000")
                                            {
                                                gqh.calc_dynam_qty = obj.Quantity;
                                                if (obj.Quantity > 0)
                                                {
                                                    gqh.calc_dynam_fzn_unit_cost = (adj_amount) + ((obj.Quantity * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal) / obj.Quantity;
                                                }
                                                else
                                                {
                                                    gqh.calc_dynam_fzn_unit_cost = (adj_amount) + (((fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal);
                                                }
                                            }
                                            else
                                            {
                                                gqh.calc_dynam_qty = obj.Quantity;
                                                if (obj.Quantity > 0)
                                                {
                                                    gqh.calc_dynam_fzn_unit_cost = (fznMatl + adj_amount + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS + (fznSUTotal / obj.Quantity) + (gqh.fznLCTotal / obj.Quantity));
                                                }
                                                else
                                                {
                                                    gqh.calc_dynam_fzn_unit_cost = (fznMatl + adj_amount + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS + (fznSUTotal) + (gqh.fznLCTotal));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Failed to obtain frozen cost.");
                                    }
                                    reader.Close();
                                }
                            }
                            catch
                            {
                                //return api with error
                                if (obj.CustomerNumber == null)
                                {
                                    obj.CustomerNumber = "ERROR";
                                }
                                if (obj.ItemNumber == null)
                                {
                                    obj.ItemNumber = "ERROR";
                                }
                                if (obj.BranchPlant == null)
                                {
                                    obj.BranchPlant = "ERROR";
                                }
                                if (obj.CustomerPartNumber == null)
                                {
                                    obj.CustomerPartNumber = "ERROR";
                                }

                                //pricingList.pricingList.Add(obj);
                            }

                        }

                        MOQ = gqh.MINIMUM_ORDER_QUANTITY;
                        obj.MOQ = gqh.MINIMUM_ORDER_QUANTITY;
                        if (obj.MOQ == 0)
                        {
                            obj.MOQ = 10;
                        }
                        //obj.LeadTime = 25;

                    }

                    obj.MOQ = gqh.MINIMUM_ORDER_QUANTITY;
                        if (obj.Quantity < obj.MOQ)
                        {
                            obj.Quantity = MOQ;
                        obj.Quantity = obj.MOQ;
                    }
                    

                        if (obj.MOQ == 0)
                        {
                            obj.MOQ = 10;
                        }

         


                    if(history_flag>0)
                    {
                        true_price = Convert.ToString(Math.Round(gqh.calc_dynam_fzn_unit_cost * (1 + gqh.Margin / 100), 2) );
                        price = Convert.ToDecimal(true_price);
                        obj.Price = true_price;
                        if (obj.ItemNumber == "QC301")
                        {
                            //QC301 --FRIST ARTCLE INSPECTION
                            //QC501 -- CHEMICAL PHYSICAL

                            true_price = "500";
                            price = Convert.ToDecimal(true_price);
                            obj.Price = true_price;
                        }
                        else if (obj.ItemNumber == "QC501")
                        {
                            true_price = "250";
                            price = Convert.ToDecimal(true_price);
                            obj.Price = true_price;
                        }
         
                    }
                    else if ((Convert.ToDecimal(Math.Round(gqh.calc_dynam_fzn_unit_cost / (1 - (gqh.Margin/100)), 2)) > 0) && obj.ItemNumber!= "QC301" && obj.ItemNumber!= "QC501")
                        {
                            true_price = Convert.ToString(Math.Round(gqh.calc_dynam_fzn_unit_cost / (1 - (gqh.Margin/100)), 2));
                            price = Convert.ToDecimal(true_price);
                            obj.Price = true_price;
                        }
                        else
                        {
                        if (obj.ItemNumber == "QC301")
                        {
                            //QC301 --FRIST ARTCLE INSPECTION
                            //QC501 -- CHEMICAL PHYSICAL

                            true_price = "500";
                            price = Convert.ToDecimal(true_price);
                            obj.Price = true_price;
                        }
                        else if (obj.ItemNumber == "QC501")
                        {
                            true_price = "250";
                            price = Convert.ToDecimal(true_price);
                            obj.Price = true_price;
                        }
                        else
                        {
                            true_price = null;
                        }
                        }
                    
                    
                        //return new[]
                        //{
                        //      new GetPricingAPI { Price = true_price, MOQ = MOQ, LeadTime = leadtime, Quantity = Quantity, CustomerPartNumber = CustomerPartNumber, BranchPlant = BranchPlant, ItemNumber = PartNumber, CustomerNumber = CustomerNumber, StockQuantity = 0 }

                        //  };
                        ////string result = string.Format("Succesfully uploaded: {0}", data);
                        //return new[]
                        //       {
                        //      new GetPricingAPI {}

                        //  };



                        
                        returnPriceList.pricingList.Add(obj);
                    
                    

                }
                
            }


            return returnPriceList;
        }
    }
}





    

    

