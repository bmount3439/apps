//dynamic off;

using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.WebControls;
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.Mvc;
using Microsoft.SharePoint;
using System.Net;
using System.IO;
using System.Xml;
using System.Web.UI;
using Microsoft.IdentityModel.S2S.Tokens;
using RequestForQuoteSharePointWeb;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Security;
using RequestForQuoteSharePointWeb.Models;
using System.ComponentModel.DataAnnotations;
using Microsoft.SharePoint.Client.Utilities;
using Microsoft.VisualBasic;
using System.Runtime.InteropServices;
using System.Data.Odbc;
using RequestForQuoteSharePointWeb.Services;
using System.Web.Http.WebHost;
//using System.Web.Ex

//using Microsoft.VisualBasic.Interaction;
//using Microsoft.IdentityModel.S2S.Protocols.OAuth2;
//using Microsoft.IdentityModel.Tokens;
//using System.Web.Routing;

namespace RequestForQuoteSharePointWeb.Controllers

{
    [Authorize]
    public class HomeController : BaseController
    {
        SharePointConnectionUpdated db = new SharePointConnectionUpdated();

        public ActionResult Index(UserModel users)
        {
            ViewBag.Message = "Your home page.";

            users.username = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1);

            if (string.IsNullOrEmpty(users.username) == true)
            {
                users.username = Interaction.InputBox("No username found. Please enter a username", "Username Not Found", "", -1, -1);
                if (string.IsNullOrEmpty(users.username) == true || users.username == "")
                {
                    MessageBox.Show("Username not found. Exiting...");
                    Response.Write("<script>window.close();</script>");
                }
            }

            
            
            return View("~/Views/Home/Index.cshtml");
        }

        public ActionResult DataManagementConsole()
        {
            ViewBag.Message = "Your data management page.";
            return View();

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult TestPage()
        {
            ViewBag.Message = "Test page.";

            return View();
        }

        [HttpGet]
        public ActionResult CurrentQueue()
        {
            ViewBag.Message = "Your queue.";

            int user_id;
            int org_id;
            user_id = 0;
            org_id = 0;
            //string username;
            string org_name_var;
            //org_name_var = "CAM";
            // string connectionString;
            string sqlScript;

            if (string.IsNullOrEmpty(Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)) == true)
            {
                MessageBox.Show("Username not found. Exiting...");
                Response.Write("<script>window.close();</script>");
                return View("~/Views/Home/CreateQuote.cshtml");
            }

            SqlConnection cnn;
            
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            cnn.Open();

            //get the last_update_by
            sqlScript = "select u.user_id, u.org_id, o.org_name from users u, organization o where u.org_id=o.org_id and active_flag=1";
            SqlCommand command = new SqlCommand(sqlScript, cnn)
            {
                CommandText = sqlScript
            };
            SqlParameter parameter = new SqlParameter
            {
                ParameterName = "@username",
                Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
            };
            command.Parameters.Add(parameter);
            SqlDataReader reader;
            reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    user_id = reader.GetInt32(0);
                    org_id = reader.GetInt32(1);
                    org_name_var = reader.GetString(2);
                }
            }
            else
            {
                MessageBox.Show("No users were found matching that criteria.");
            }

            reader.Close();
            cnn.Close();

            SharePointConnectionUpdated dc = new SharePointConnectionUpdated();
            var requests = (from c in db.vCheck_Your_Requests
                                //where (c.org_name==org_name_var || c.org_name=="CAM")
                            select new ViewQueue
                            {
                                Request_Id = c.request_id,
                                Series = c.assigned_series,
                                Priority = c.highest_priority,
                                DueDate = c.closest_due_date,
                                Status = c.status,
                                CreatedBy = c.created_by,
                                CustomerName = c.CUSTOMER_NAME,
                                OpportunityType = c.opportunity_type_desc,
                                ProductType = c.product_type,
                                Comments = c.comments,
                                CurrentAssignment = c.current_assignment,
                                //OptionalAssignment = c.optional_assignment,
                                Organization = c.org_name

                            }).ToList();

            return View(requests);
        }

        [HttpGet]
        public ActionResult ManageUsers()
        {
            ViewBag.Message = "User roles.";

            //int user_id;
            //string username;
            //string email;
            //string organization;
            //string role;
            //string active_flag;
            //string multi_org;

            //ManageUser m = new ManageUser();
            UserList users = new UserList();

            using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                dbConnection.Open();

                users = this.DbReader<UserList>("select * From vManage_Users;", dbConnection, (reader) =>
                {

                    var userList = new UserList();

                    while (reader.Read())
                    {
                        userList.Users.Add(new ManageUser()
                        {
                            UserId = reader.GetInt32(0),
                            Username = reader.GetString(1),
                            Email = reader.GetString(2),
                            Organization = reader.GetString(3),
                            Role = reader.GetString(4),
                            ActiveFlag = reader.GetString(5),
                            MultiOrg = reader.GetString(6)
                        });
                    }
                    return userList;
                });
            }
            return View(users.Users);
        }

        public ActionResult Reporting()
        {
            ViewBag.Message = "Reports.";
            if (string.IsNullOrEmpty(Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)) == true)
            {

                MessageBox.Show("Username not found. Exiting...");
                Response.Write("<script>window.close();</script>");
                return View("~/Views/Home/CreateQuote.cshtml");
            }
            return View();
        }

        //[SharePointContextFilter]
        //public static void SendEmail(List<SelectListItem> Emails, string type_of_email, Int32 request_num, string attach_blueprint, string attach_other, string optional_assignment)
        //{


        //using (SmtpClient client = new SmtpClient())
        //{
        //var basicCredential = new NetworkCredential("arcbackup", "Nn5uau19US}){"+]");
        //using (MailMessage mail = new MailMessage())
        //{
        //    //MailMessage mail = new MailMessage("no_reply@RequestForQuote.com", "bmount@conaeromfg.com");
        //    MailAddress fromAddress = new MailAddress("QuoteRequest@RFQ.com");
        //    client.Port = 587;
        //    client.DeliveryMethod = SmtpDeliveryMethod.Network;
        //    client.UseDefaultCredentials = false;
        //    //client.EnableSsl = true;
        //    client.Credentials = basicCredential; //new System.Net.NetworkCredential("bmount", "Darcy3439;'");
        //    client.Host = "smtp.office365.com";

        //    mail.From = fromAddress;
        //    //mail.To.Add(email_chosen);
        //    if (optional_assignment != null)
        //    {
        //        mail.CC.Add(optional_assignment);
        //    }
        //    foreach (var email in Emails)
        //    {
        //        MailAddress to = new MailAddress(email.Text);
        //        mail.To.Add(to);
        //    }


        //    if (attach_blueprint != null && attach_other == null)
        //    {
        //        System.Net.Mail.Attachment attachment;
        //        attachment = new System.Net.Mail.Attachment(attach_blueprint);
        //        mail.Attachments.Add(attachment);
        //    }
        //    else if (attach_blueprint == null && attach_other != null)
        //    {
        //        System.Net.Mail.Attachment attachment_other;
        //        attachment_other = new System.Net.Mail.Attachment(attach_other);
        //        mail.Attachments.Add(attachment_other);
        //    }
        //    else if (attach_blueprint != null && attach_other != null)
        //    {
        //        System.Net.Mail.Attachment attachment_other;
        //        attachment_other = new System.Net.Mail.Attachment(attach_other);
        //        mail.Attachments.Add(attachment_other);

        //        System.Net.Mail.Attachment attachment;
        //        attachment = new System.Net.Mail.Attachment(attach_blueprint);
        //        mail.Attachments.Add(attachment);
        //    }

        //    if (type_of_email == "NEW")
        //    {
        //        mail.Subject = "Request To Review " + request_num.ToString() + "";
        //        mail.Body = "Quote request " + request_num.ToString() + " has been created and assigned to you for review, possible qualification, and evaluation.";
        //    }
        //    else if (type_of_email == "QUOTE")
        //    {
        //        mail.Subject = "Quote Request Approved " + request_num.ToString() + "";
        //        mail.Body = "Quote request " + request_num.ToString() + " has been closed with a recommendation to quote the respective part.";
        //    }
        //    else if (type_of_email == "NO QUOTE")
        //    {
        //        mail.Subject = "Quote Request Denied " + request_num.ToString() + "";
        //        mail.Body = "Quote request " + request_num.ToString() + " has been closed with a recommendation not to quote the respective part.";
        //    }
        //    else
        //    {
        //        mail.Subject = "Request To Review " + request_num.ToString() + "";
        //        mail.Body = "You have been assigned request for quote " + request_num.ToString() + " to review for possible qualification and evaluation.";
        //    }

        //    try
        //    {
        //        client.Send(mail);
        //    }
        //    catch (Exception ex)
        //    {
        //        //Response.Write(ex.Message);
        //        MessageBox.Show("Failed to send email. Reason: " + ex.Message);
        //    }
        //}
        //}
        //}

        [HttpGet]
        //      [SharePointContextFilter]
        public ActionResult CreateQuote()
        {
            if (string.IsNullOrEmpty(Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)) == true)
            {

                MessageBox.Show("Username not found. Exiting...");
                Response.Write("<script>window.close();</script>");
                return View("~/Views/Home/CreateQuote.cshtml");
            }

            ViewBag.Message = "Your quote page.";


            SqlConnection cnn;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            MakeRequestData model = new MakeRequestData();
            List<SelectListItem> Materials1 = new List<SelectListItem>();
            using (SharePointConnectionUpdated db = new SharePointConnectionUpdated())
            {
                string query = " SELECT Material_Type_Desc, Material_Type_id FROM Material_Type";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = cnn;
                    cnn.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Materials1.Add(new SelectListItem
                            {
                                Text = sdr["Material_Type_Desc"].ToString(),
                                Value = sdr["Material_Type_ID"].ToString()
                            });
                        }
                    }
                    cnn.Close();
                }
            }
            model.MaterialTypeList1 = Materials1;
            //line 2
            List<SelectListItem> Materials2 = new List<SelectListItem>();
            using (SharePointConnectionUpdated db = new SharePointConnectionUpdated())
            {
                string query = " SELECT Material_Type_Desc, Material_Type_id FROM Material_Type";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = cnn;
                    cnn.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Materials2.Add(new SelectListItem
                            {
                                Text = sdr["Material_Type_Desc"].ToString(),
                                Value = sdr["Material_Type_ID"].ToString()
                            });
                        }
                    }
                    cnn.Close();
                }
            }
            model.MaterialTypeList2 = Materials1;
            //line 3
            List<SelectListItem> Materials3 = new List<SelectListItem>();
            using (SharePointConnectionUpdated db = new SharePointConnectionUpdated())
            {
                string query = " SELECT Material_Type_Desc, Material_Type_id FROM Material_Type";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = cnn;
                    cnn.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Materials3.Add(new SelectListItem
                            {
                                Text = sdr["Material_Type_Desc"].ToString(),
                                Value = sdr["Material_Type_ID"].ToString()
                            });
                        }
                    }
                    cnn.Close();
                }
            }
            model.MaterialTypeList3 = Materials1;
            //line 4
            List<SelectListItem> Materials4 = new List<SelectListItem>();
            using (SharePointConnectionUpdated db = new SharePointConnectionUpdated())
            {
                string query = " SELECT Material_Type_Desc, Material_Type_id FROM Material_Type";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = cnn;
                    cnn.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Materials4.Add(new SelectListItem
                            {
                                Text = sdr["Material_Type_Desc"].ToString(),
                                Value = sdr["Material_Type_ID"].ToString()
                            });
                        }
                    }
                    cnn.Close();
                }
            }
            model.MaterialTypeList4 = Materials4;
            //line 5
            List<SelectListItem> Materials5 = new List<SelectListItem>();
            using (SharePointConnectionUpdated db = new SharePointConnectionUpdated())
            {
                string query = " SELECT Material_Type_Desc, Material_Type_id FROM Material_Type";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = cnn;
                    cnn.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Materials5.Add(new SelectListItem
                            {
                                Text = sdr["Material_Type_Desc"].ToString(),
                                Value = sdr["Material_Type_ID"].ToString()
                            });
                        }
                    }
                    cnn.Close();
                }
            }
            model.MaterialTypeList5 = Materials5;

            if (ModelState.IsValid)
            {
                List<SelectListItem> Priorities1 = new List<SelectListItem>();
                using (SharePointConnectionUpdated db = new SharePointConnectionUpdated())
                {
                    string query = " SELECT Priority_Desc, Priority_ID FROM Priority";
                    using (SqlCommand cmd = new SqlCommand(query))
                    {
                        cmd.Connection = cnn;
                        cnn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                Priorities1.Add(new SelectListItem
                                {
                                    Text = sdr["Priority_Desc"].ToString(),
                                    Value = sdr["Priority_ID"].ToString()
                                });
                            }
                        }
                        cnn.Close();
                    }
                }
                model.PriorityTypeDescList1 = Priorities1;
            }
            //line 2
            if (ModelState.IsValid)
            {
                List<SelectListItem> Priorities2 = new List<SelectListItem>();
                using (SharePointConnectionUpdated db = new SharePointConnectionUpdated())
                {
                    string query = " SELECT Priority_Desc, Priority_ID FROM Priority";
                    using (SqlCommand cmd = new SqlCommand(query))
                    {
                        cmd.Connection = cnn;
                        cnn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                Priorities2.Add(new SelectListItem
                                {
                                    Text = sdr["Priority_Desc"].ToString(),
                                    Value = sdr["Priority_ID"].ToString()
                                });
                            }
                        }
                        cnn.Close();
                    }
                }
                model.PriorityTypeDescList2 = Priorities2;
            }
            //line 3
            if (ModelState.IsValid)
            {
                List<SelectListItem> Priorities3 = new List<SelectListItem>();
                using (SharePointConnectionUpdated db = new SharePointConnectionUpdated())
                {
                    string query = " SELECT Priority_Desc, Priority_ID FROM Priority";
                    using (SqlCommand cmd = new SqlCommand(query))
                    {
                        cmd.Connection = cnn;
                        cnn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                Priorities3.Add(new SelectListItem
                                {
                                    Text = sdr["Priority_Desc"].ToString(),
                                    Value = sdr["Priority_ID"].ToString()
                                });
                            }
                        }
                        cnn.Close();
                    }
                }
                model.PriorityTypeDescList3 = Priorities3;
            }
            //line 4
            if (ModelState.IsValid)
            {
                List<SelectListItem> Priorities4 = new List<SelectListItem>();
                using (SharePointConnectionUpdated db = new SharePointConnectionUpdated())
                {
                    string query = " SELECT Priority_Desc, Priority_ID FROM Priority";
                    using (SqlCommand cmd = new SqlCommand(query))
                    {
                        cmd.Connection = cnn;
                        cnn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                Priorities4.Add(new SelectListItem
                                {
                                    Text = sdr["Priority_Desc"].ToString(),
                                    Value = sdr["Priority_ID"].ToString()
                                });
                            }
                        }
                        cnn.Close();
                    }
                }
                model.PriorityTypeDescList4 = Priorities4;
            }
            //line 5
            if (ModelState.IsValid)
            {
                List<SelectListItem> Priorities5 = new List<SelectListItem>();
                using (SharePointConnectionUpdated db = new SharePointConnectionUpdated())
                {
                    string query = " SELECT Priority_Desc, Priority_ID FROM Priority";
                    using (SqlCommand cmd = new SqlCommand(query))
                    {
                        cmd.Connection = cnn;
                        cnn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                Priorities5.Add(new SelectListItem
                                {
                                    Text = sdr["Priority_Desc"].ToString(),
                                    Value = sdr["Priority_ID"].ToString()
                                });
                            }
                        }
                        cnn.Close();
                    }
                }
                model.PriorityTypeDescList5 = Priorities5;
            }

            if (ModelState.IsValid)
            {
                model.OpportunityTypesList = PopulateOpportunities();
            }
            if (ModelState.IsValid)
            {
                model.ProductTypecList = PopulateProductTypes();
            }
            if (ModelState.IsValid)
            {
                model.RoleTypeList = PopulateRoles();
            }

            return View(model);
        }



        private static List<SelectListItem> PopulateOpportunities()
        {
            List<SelectListItem> Opportunities = new List<SelectListItem>();
            SqlConnection cnn;
            //string connectionString;
            
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            using (SharePointConnectionUpdated dc = new SharePointConnectionUpdated())
            {
                string query = " SELECT Opportunity_Type_Desc, Opportunity_Type_ID FROM Opportunity_Type";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = cnn;
                    cnn.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Opportunities.Add(new SelectListItem
                            {
                                Text = sdr["Opportunity_Type_Desc"].ToString(),
                                Value = sdr["Opportunity_Type_ID"].ToString()
                            });
                        }
                    }
                    cnn.Close();
                }
            }

            return Opportunities;
        }
        private static List<SelectListItem> PopulateRoles()
        {
            List<SelectListItem> RoleList = new List<SelectListItem>();
            SqlConnection cnn;
            // string connectionString;
            
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            using (SharePointConnectionUpdated dc = new SharePointConnectionUpdated())
            {
                string query = " SELECT Role_Desc, Role_ID FROM vCreate_Quote_Routing";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = cnn;
                    cnn.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            RoleList.Add(new SelectListItem
                            {
                                Text = sdr["Role_Desc"].ToString(),
                                Value = sdr["Role_ID"].ToString()
                            });
                        }
                    }
                    cnn.Close();
                }
            }

            return RoleList;
        }
        private static List<SelectListItem> PopulateProductTypes()
        {
            List<SelectListItem> ProductTypes = new List<SelectListItem>();
            SqlConnection cnn;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            using (SharePointConnectionUpdated dc = new SharePointConnectionUpdated())
            {
                string query = " SELECT Product_Type_Desc, Product_Type_ID FROM Product_Type";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = cnn;
                    cnn.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            ProductTypes.Add(new SelectListItem
                            {
                                Text = sdr["Product_Type_Desc"].ToString(),
                                Value = sdr["Product_Type_ID"].ToString()
                            });
                        }
                    }
                    cnn.Close();
                }
            }

            return ProductTypes;
        }

    [HttpGet]
    public ActionResult ViewQuoteModel()
    {

      ViewBag.Message = "Your quote PDF page.";
      ViewQuoteModel v_model = new ViewQuoteModel();
      List<QuoteLineItems> line_model = new List<QuoteLineItems>();

      SqlConnection cnn;
      cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
     
      int quote_id_selected;
      var quote_val = Request.Params["QuoteID"];
      quote_id_selected = Convert.ToInt32(quote_val);
      v_model.QuoteID = quote_id_selected;

      cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

      cnn.Open();
      using (cnn)
      {
        string sqlScript = "select * from vQuote_View_Status where quoteid=@quote_id;";
        SqlCommand command = new SqlCommand(sqlScript, cnn)
        {
          CommandText = sqlScript
        };
        SqlParameter quote_id_val = new SqlParameter
        {
          ParameterName = "@quote_id",
          Value = v_model.QuoteID
        };
        command.Parameters.Add(quote_id_val);
        SqlDataReader reader = command.ExecuteReader();
        try
        {
          while (reader.Read())
          {
            v_model.QuoteNum = reader.GetString(1);
            v_model.CreatedBy = reader.GetString(2);
            v_model.CreationDate = reader.GetDateTime(3);
            v_model.ShipTooName = reader.GetString(4);
            v_model.AssignedRoleDesc = reader.GetString(5);
            v_model.WorkflowStatus = reader.GetString(7);
            v_model.Status_Desc = reader.GetString(8);
          }

        }
        catch (Exception)
        {
          MessageBox.Show("No quote found matching that criteria. Contact your system administrator.");

          return View("Index", "Index");
        }
        cnn.Close();
      }

      cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

      cnn.Open();
      using (cnn)
      {
        string sqlScript = "select sold_to_num, sold_to_name, ship_to_num, ship_to_name, customer_po, LEAD_TIME_COMMENT, CONVERT(varchar, due_date, 23) due_date from quote_header where quote_id=@quote_id;";
        SqlCommand command = new SqlCommand(sqlScript, cnn)
        {
          CommandText = sqlScript
        };
        SqlParameter quote_id_val = new SqlParameter
        {
          ParameterName = "@quote_id",
          Value = v_model.QuoteID
        };
        command.Parameters.Add(quote_id_val);
        SqlDataReader reader = command.ExecuteReader();
        try
        {
          while (reader.Read())
          {
            v_model.SoldTooNum = reader.GetString(0);
            v_model.SoldTooName = reader.GetString(1);
            v_model.ShipTooNum = reader.GetString(2);
            v_model.ShipTooName = reader.GetString(3);
            v_model.CustPO = reader.IsDBNull(4) ? "" : (string)reader.GetValue(4);
            v_model.LeadTimeComments = reader.IsDBNull(5) ? "" : (string)reader.GetValue(5);
            v_model.DueDate = reader.GetString(6);
          }

        }
        catch (Exception)
        {
          MessageBox.Show("Unable to find the quote of interest. Please contact your sys admin.");

          return View("Index", "Index");
        }
        cnn.Close();
      }

      int line_num = 0;

      //if (line_model.QCPartNumber != null && string.IsNullOrEmpty(line_model.QCPartNumber) == true)
      //{
      //    line_num = 1;
      //}

      cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

      cnn.Open();
      using (cnn)
      {

        int i = 1;
        QuoteLineItems line = new QuoteLineItems();
        string sqlScript = @"select l.PART_NUMBER, l.QUANTITY, l.UNIT_PRICE, (l.quantity * l.UNIT_PRICE) ext_price, l.comment , l.quote_line_num, l.unit_cost, l.cost_ref
                             From quote_lines l join QUOTE_HEADER h 
                             on h.quote_id = l.quote_header_id 
                             where h.quote_id = @quote_id; ";
        SqlCommand command = new SqlCommand(sqlScript, cnn)
        {
          CommandText = sqlScript
        };
        SqlParameter quote_id_val = new SqlParameter
        {
          ParameterName = "@quote_id",
          Value = v_model.QuoteID
        };
        command.Parameters.Add(quote_id_val);
        SqlDataReader reader = command.ExecuteReader();
        try
        {
          while (reader.Read())
          {
            //if (i == 1)
            QuoteLineItems item = new QuoteLineItems()
            {
              QCLineNum = reader.GetInt32(5),
              QCPartNumber = reader.GetString(0),
              QCQuoteQty = reader.GetInt32(1),
              QCQuoteUnitPrice = reader.GetDecimal(2),
              QCLineComments = reader.GetString(4),
              QCExtPrice = reader.GetDecimal(2) * reader.GetInt32(1),
              QCQuoteUnitCost = reader.GetDecimal(6),
              QCExtCost = reader.GetDecimal(6) * reader.GetInt32(1),
              QCMargin = (reader.GetDecimal(2) * reader.GetInt32(1)) - (reader.GetDecimal(6) * reader.GetInt32(1)),
              QCCostReference=reader.GetString(7)
            };
            v_model.TotalCost = v_model.TotalCost + item.QCExtCost;
            v_model.TotalPrice = v_model.TotalPrice + item.QCExtPrice;
            line_model.Add(item);


          }

        }

        catch (Exception ex)
        {
          MessageBox.Show("Error loading quote. Please consult your system administrator.");

          return View("Index", "Index");
        }
        cnn.Close();

      }

      var ourModel = new QuoteOverview
      {
        ViewQuoteModel = v_model,
        QuoteDisplayLines = line_model
      };
      //overallTime.Stop(this.HttpContext);
      return View(ourModel);
    }



        [HttpGet]
        public ActionResult ViewRequest()
        {

            ViewBag.Message = "Your review quote page.";


            Int32 workflow_id;
            Int32 prototype_required_flag;
            string qualification_testing_type;

            string requested_by;
            DateTime? creation_date;
            string part_series;
            string customer_name;
            string buyers_name;
            string contact_information;
            string opportunity_type;
            //Int32 product_type_id;
            string product_type;
            string comments;
            //Int32 status_id;
            string status;
            string optional_assignment;

            requested_by = "";
            prototype_required_flag = 0;
            workflow_id = 0;

            status = "";
            product_type = "";
            part_series = "";
            customer_name = "";
            buyers_name = "";
            contact_information = "";
            opportunity_type = "";
            comments = "";
            // username = "";
            qualification_testing_type = "Awaiting Assignment";
            creation_date = Convert.ToDateTime(DateTime.Now);

            ViewRequestModel model = new ViewRequestModel
            {
                RequestID = Convert.ToInt32(Request.Params["Request_Id"])
            };

            using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                dbConnection.Open(); // open once .. do what we need ... then close

                model.RoleTypeList = this.GetRolesList(dbConnection);

                model.UpdateStatusList = this.GetUpdateStatusList(dbConnection);

                try
                {
                    this.DbReaderWithParam<bool>("select h.REQUEST_ID "
                        + " , isnull(u.full_name,u.USER_NAME) "
                        + " , H.CREATION_DATE "
                        + " , H.WORKFLOW_ID "
                        + " , H.PART_SERIES "
                        + " , H.CUSTOMER_NAME "
                        + " , H.BUYERS_NAME "
                        + " , H.CONTACT_INFORMATION "
                        + " , O.OPPORTUNITY_TYPE_DESC "
                        + " , H.PROTOTYPE_REQUIRED_FLAG "
                        + " , H.QUALIFICATION_TESTING_TYPE "
                        + " , P.PRODUCT_TYPE_DESC "
                        + " , H.COMMENTS "
                        + " , S.STATUS_DESC "
                        + " , H.OPTIONAL_ASSIGNMENT "
                        + " from REQUEST_QUOTE_HEADER h "
                        + " left join OPPORTUNITY_TYPE o on "
                        + " h.OPPORTUNITY_TYPE = o.OPPORTUNITY_TYPE_ID "
                        + " left join PRODUCT_TYPE p on "
                        + " h.PRODUCT_TYPE_ID = P.PRODUCT_TYPE_ID "
                        + " left join status s on "
                        + " h.STATUS_ID = s.STATUS_ID "
                        + " join USERS u on "
                        + " h.requested_by_id = u.user_id "
                        + " where 1 = 1 "
                        + " and h.request_id = @request_id",
                        dbConnection,
                        new SqlParameter("@request_id", model.RequestID),
                        (reader) =>
                        {
                            while (reader.Read())
                            {
                                //   request_id = reader.GetInt32(0);
                                requested_by = reader.GetString(1);
                                creation_date = reader.GetDateTime(2);
                                workflow_id = reader.GetInt32(3);
                                part_series = reader.GetString(4);
                                customer_name = reader.GetString(5);
                                buyers_name = reader.GetString(6);
                                contact_information = reader.GetString(7);
                                opportunity_type = reader.GetString(8);
                                prototype_required_flag = reader.GetInt32(9);
                                qualification_testing_type = reader.GetString(10);
                                product_type = reader.GetString(11);
                                comments = reader.GetString(12);
                                status = reader.GetString(13);
                                optional_assignment = reader.GetString(14);
                            }
                            return true;
                        });
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find the request specified.");
                    return View();
                }

                int count_lines = this.DbReaderWithParam<int>(@"select count(l.REQUEST_LINE_ID) 
                    from Request_quote_lines l
                     where 1 = 1 
                     and l.request_id = @request_id",
                    dbConnection,
                    new SqlParameter("@request_id", model.RequestID),
                    (reader) =>
                    {
                        if (reader.Read())
                            return reader.GetInt32(0);
                        return 0;
                    });

                int check_blank_lines = count_lines;

                //for (int i = 1; i <= check_blank_lines; i++)
                //{
                //find the lines now
                string sqlScript = "select l.Request_ID "
                    + " , l.REQUEST_LINE_ID "
                    + ", l.SIZE_NUM "
                    + ", l.OTHER_SOURCE "
                    + ", l.PLATFORM "
                    + ", m.MATERIAL_TYPE_DESC "
                    + ", L.EST_REVENUE_Y1 "
                    + ", L.MARKET_PRICE "
                    + ", L.EST_REVENUE_Q_Y1 "
                    + ", L.EST_REVENUE_Y2 "
                    + ", L.EST_REVENUE_Q_Y2 "
                    + ", L.EST_REVENUE_Y3 "
                    + ", L.EST_REVENUE_Q_Y3 "
                    + ", L.EST_REVENUE_Y4 "
                    + ", L.EST_REVENUE_Q_Y4 "
                    + ", L.EST_REVENUE_Y5 "
                    + ", L.EST_REVENUE_Q_Y5 "
                    + ", L.DUE_DATE "
                    + ", P.PRIORITY_DESC "
                    + "from REQUEST_QUOTE_LINES l "
                    + "left outer join material_type m on "
                    + "l.MATERIAL_TYPE_ID = m.MATERIAL_TYPE_ID "
                    + "LEFT OUTER JOIN PRIORITY P ON "
                    + "L.PRIORITY_ID = P.PRIORITY_ID "
                    + " where l.request_id = @request_id "
                    + " and l.request_line_id > 0 AND l.request_line_id <= @request_line_id ";
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@request_id", model.RequestID),
                    new SqlParameter("@request_line_id", check_blank_lines)
                };

                var lineRequests = this.DbReaderWithParams<List<RequestLineViewModel>>(sqlScript, dbConnection, parameters, (reader) =>
                {
                    var results = new List<RequestLineViewModel>();
                    while (reader.Read())
                    {
                        var line = new RequestLineViewModel();
                        line.ReadFrom(reader);
                        results.Add(line);
                    }
                    return results;
                });

                //find product type id
                int product_type_id = 0;
                try
                {
                    product_type_id = this.DbReaderWithParam<int>("select product_type_id from product_type where product_type_desc=@product_type_desc",
                    dbConnection, new SqlParameter("@product_type_desc", product_type), (rdr) =>
                    {
                        if (rdr.Read())
                            return rdr.GetInt32(0);
                        return 0;
                    });
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find the product type id. Contact your system admin with this error.");
                    return View();
                }

                try
                {
                    foreach (var line in lineRequests)
                    {
                        line.material_type_id = this.DbReaderWithParam<int>(
                            "select material_type_id from material_type where material_type_desc=@material_type_desc ",
                            dbConnection, new SqlParameter("@material_type_desc", line.material_type_desc), (rdr) =>
                            {
                                if (rdr.Read())
                                    return rdr.GetInt32(0);
                                return 0;
                            });
                    }

                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find the materal type id. Contact your system admin with this error.");
                    return View();
                }

                //find opportunity type id

                int opportunity_type_id;
                opportunity_type_id = 0;

                try
                {
                    opportunity_type_id = this.DbReaderWithParam<int>(
                        "select opportunity_type_id from opportunity_type where opportunity_type_desc=@opportunity_type_desc",
                        dbConnection, new SqlParameter("@opportunity_type_desc", product_type), (rdr) =>
                        {
                            if (rdr.Read())
                                return rdr.GetInt32(0);
                            return 0;
                        });
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find the opportunity type id. Contact your system admin with this error.");
                    return View();
                }

                try
                {
                    foreach (var line in lineRequests)
                    {
                        line.priority_id = this.DbReaderWithParam<int>(
                             "select priority_id from priority where priority_desc=@priority_desc",
                             dbConnection, new SqlParameter("@priority_desc", line.priority_desc), (rdr) =>
                             {
                                 if (rdr.Read())
                                     return rdr.GetInt32(0);
                                 return 0;
                             });
                    }

                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find the priority type id. Contact your system admin with this error.");
                    return View();
                }

                int qualification_testing_type_id;
                qualification_testing_type_id = 0;

                try
                {

                    qualification_testing_type_id = this.DbReaderWithParam<int>(
                        "select qualification_testing_type_id from qualification_Testing_type where qualification_testing_type_desc=@qualification_testing_type_desc",
                        dbConnection, new SqlParameter("@qualification_testing_type_desc", qualification_testing_type), (rdr) =>
                        {
                            if (rdr.Read())
                                return rdr.GetInt32(0);
                            return 0;
                        });

                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find the qualification testing type id. Contact your system admin with this error.");
                    return View();
                }



                //lookup the opportunity type desc based on the id
                bool prototype_required_bool;
                if (prototype_required_flag == 1)
                {
                    prototype_required_bool = true;
                }
                else
                {
                    prototype_required_bool = false;
                }

                //header
                model.RequestedBy = requested_by;
                model.CreationDate = creation_date;
                model.PartSeries = part_series;
                model.CustomerName = customer_name;
                model.BuyersName = buyers_name;
                model.ContactInfo = contact_information;
                model.Opportunity_Type_ID = opportunity_type_id;
                model.Opportunity_Type_Desc = opportunity_type;
                model.Product_Type_ID = product_type_id;
                model.Product_Type_Desc = product_type;
                model.Comments = comments;
                model.Status_Desc = status;
                model.PrototypeRequired = prototype_required_bool;
                model.Qualification_Testing_Type_Desc = qualification_testing_type;
                model.Qualification_Testing_Type_ID = qualification_testing_type_id;


                model.Size1 = lineRequests[0].size_num;
                model.OtherSource1 = lineRequests[0].other_source;
                model.Platform1 = lineRequests[0].platform;
                model.Material_Type_Desc1 = lineRequests[0].material_type_desc;
                model.Material_Type_ID1 = lineRequests[0].material_type_id;
                model.EstRevenueYr11 = lineRequests[0].est_revenue_y1;
                model.MarketPrice1 = lineRequests[0].market_price;
                if (lineRequests[0].market_price == 0)
                {
                    lineRequests[0].market_price = 1;
                }
                model.EstQtyYr11 = Convert.ToInt32(lineRequests[0].est_revenue_y1 / lineRequests[0].market_price);
                model.EstRevenueYr21 = lineRequests[0].est_revenue_y2;
                model.EstRevenueYr31 = lineRequests[0].est_revenue_y3;
                model.EstRevenueYr41 = lineRequests[0].est_revenue_y4;
                model.EstRevenueYr51 = lineRequests[0].est_revenue_y5;
                model.EstQtyYr21 = lineRequests[0].est_qty_y2;
                model.EstQtyYr31 = lineRequests[0].est_qty_y3;
                model.EstQtyYr41 = lineRequests[0].est_qty_y4;
                model.EstQtyYr51 = lineRequests[0].est_qty_y5;
                model.DueDate1 = lineRequests[0].due_date;
                //model.Priority_Type_ID1 = prio;
                model.Priority_Type_Desc1 = lineRequests[0].priority_desc;

                if (ModelState.IsValid)
                {
                    model.QualificationTestingList = GetQualificationList(dbConnection);
                    var qual_types = model.QualificationTestingList.Find(m => m.Text == model.Qualification_Testing_Type_Desc.ToString());
                }


                if (ModelState.IsValid)
                {
                    var PriorityList = this.GetPrioritiesList(dbConnection);

                    model.PriorityTypeDescList1 = PriorityList;
                    model.PriorityTypeDescList2 = PriorityList;
                    model.PriorityTypeDescList3 = PriorityList;
                    model.PriorityTypeDescList4 = PriorityList;
                    model.PriorityTypeDescList5 = PriorityList;

                }

                dbConnection.Close();
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult ViewRequest(ViewRequestModel model)
        {

            Int32 assigned_to_role_id;
            int assigned_to_org_id;
            int org_id;

            Int32 status_id_val;
            Int32 workflow_id;
            Int32 prototype_required_flag;
            string qualification_testing_type;
            qualification_testing_type = "";
            string sqlScript;
            Int32 updated_by_user_id;
            int request_id_val;

            int product_type;
            product_type = 0;
            int opportunity_type_val;
            opportunity_type_val = 0;

            // request_id_val = 0;
            prototype_required_flag = 0;
            workflow_id = 0;
            assigned_to_role_id = 0;
            // priority_id1 = 0;
            //sql_value = "";
            //Int32 active_flag_val = 1;
            updated_by_user_id = 0;
            org_id = 1;
            assigned_to_org_id = 1;

            // SqlConnecion wrapper for entire function
            using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                System.Diagnostics.Debug.WriteLine("connecting to database...");
                dbConnection.Open(); // open once .. do what we need ... then close
                System.Diagnostics.Debug.WriteLine("connected to database.");

                request_id_val = model.RequestID;

                if (!this.DbReaderWithParam<bool>("select user_id, org_id from users where user_name=@username and active_flag=1",
                     dbConnection, new SqlParameter("@username", Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)),
                     (reader) =>
                     {
                         if (reader.HasRows)
                         {
                             if (reader.Read())
                             {
                                 updated_by_user_id = reader.GetInt32(0);
                                 assigned_to_org_id = reader.GetInt32(1);
                                 org_id = reader.GetInt32(1);
                             }
                             return true;
                         }
                         return false;
                     }))
                {
                    MessageBox.Show("No users were found matching that criteria.");
                }


                model.RoleTypeList = this.GetRolesListFromCreate(dbConnection);

                model.UpdateStatusList = this.GetStatusList(dbConnection);

                var status_types = model.UpdateStatusList.Find(m => m.Value == model.Status_ID.ToString());
                status_id_val = model.Status_ID; //status that was chosen

                //qualification selection
                int qualification_testing_type_id;
                qualification_testing_type_id = 0;
                if (ModelState.IsValid)
                {
                    model.QualificationTestingList = this.GetQualificationList(dbConnection);
                }
                var qual_types = model.QualificationTestingList.Find(m => m.Value == model.Qualification_Testing_Type_ID.ToString());
                qualification_testing_type_id = Convert.ToInt32(model.Qualification_Testing_Type_ID);
                qualification_testing_type = qual_types.Text;

                model.ProductTypecList = this.GetProductTypesList(dbConnection);

                var product_types = model.ProductTypecList.Find(m => m.Value == model.Product_Type_ID.ToString());
                product_type = Convert.ToInt32(model.Product_Type_ID);

                model.OpportunityTypesList = this.GetOppurtunitiesList(dbConnection);

                var opportunity_type = model.OpportunityTypesList.Find(m => m.Text == model.Opportunity_Type_ID.ToString());
                opportunity_type_val = Convert.ToInt32(model.Opportunity_Type_ID);

                assigned_to_role_id = Convert.ToInt32(model.Role_ID);

                status_id_val = 0;
                //engineering
                if (assigned_to_role_id == 5) //role is engineering
                {
                    status_id_val = 2; //assign to engineeer
                }
                else if (assigned_to_role_id == 6) //role is engineering manager
                {
                    status_id_val = 3; //assign to engineering manager
                }
                else if (assigned_to_role_id == 2) //assigned to sales manager
                {
                    status_id_val = 1; //assign back to sales
                }
                else if (assigned_to_role_id == 3) //assigned to sales
                {
                    status_id_val = 1; //assign back to sales
                }
                else
                //sales manager
                {
                    status_id_val = 2; //assign to sales manager
                }

                workflow_id = this.DbReaderWithParam("select workflow_id From request_quote_header where 1=1 and request_id=@request_id",
                        dbConnection, new SqlParameter("@request_id", request_id_val),
                        (reader) =>
                        {

                            if (reader.HasRows)
                            {
                                if (reader.Read())
                                {
                                    return reader.GetInt32(0);
                                }
                            }
                            return 0;
                        });

                //create workflow
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = dbConnection;
                    sqlScript = "update workflow set status_id=@status_id, LAST_UPDATED=convert(date,getdate()), LAST_UPDATED_BY=@updated_by_user_id, ASSIGNED_TO_ROLE_ID=@assigned_to_role_id where workflow_id=@workflow_id; "; //fill the sql value
                    command.CommandText = sqlScript;
                    SqlParameter status_id = new SqlParameter
                    {
                        ParameterName = "@status_id",
                        Value = status_id_val
                    };
                    command.Parameters.Add(status_id);
                    //
                    SqlParameter last_updated_by = new SqlParameter
                    {
                        ParameterName = "@updated_by_user_id",
                        Value = updated_by_user_id
                    };
                    command.Parameters.Add(last_updated_by);
                    //
                    SqlParameter assigned_to_role_id_val = new SqlParameter
                    {
                        ParameterName = "@assigned_to_role_id",
                        Value = assigned_to_role_id
                    };
                    command.Parameters.Add(assigned_to_role_id_val);
                    //
                    SqlParameter workflow_id_param = new SqlParameter
                    {
                        ParameterName = "@workflow_id",
                        Value = workflow_id
                    };
                    command.Parameters.Add(workflow_id_param);
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Failed to update workflow information. Exiting. Please contact your system administrator.");
                        return View("~/Views/Home/CreateQuote.cshtml");
                    }
                }


                sqlScript = @"insert into workflow_history (WORKFLOW_ID,STATUS_ID, LAST_UPDATED, LAST_UPDATED_BY, ASSIGNED_TO_ROLE_ID) 
                     values(@workflow_id,@status_id 
                     , CONVERT(DATE, GETDATE()) 
                     , @last_updated_by, @assigned_to_role_id
                     ); "; //fill the sql value
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = dbConnection;
                    command.CommandText = sqlScript;
                    SqlParameter status_id = new SqlParameter
                    {
                        ParameterName = "@status_id",
                        Value = status_id_val
                    };
                    command.Parameters.Add(status_id);
                    //
                    SqlParameter last_updated_by = new SqlParameter
                    {
                        ParameterName = "@last_updated_by",
                        Value = updated_by_user_id
                    };
                    command.Parameters.Add(last_updated_by);
                    //
                    SqlParameter assigned_to_role_id_val = new SqlParameter
                    {
                        ParameterName = "@assigned_to_role_id",
                        Value = assigned_to_role_id
                    };
                    command.Parameters.Add(assigned_to_role_id_val);
                    //
                    SqlParameter workflow_id_param = new SqlParameter
                    {
                        ParameterName = "@workflow_id",
                        Value = workflow_id
                    };
                    command.Parameters.Add(workflow_id_param);

                    try
                    {
                        command.ExecuteNonQuery();


                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("RFQ error #1.01 - Failed to update workflow history information.");
                        //return View("~/Views/Home/CreateQuote.cshtml");
                    }
                    finally
                    {
                        //cnn.close();
                    }

                }

                sqlScript = "update h set h.created_by_user_id=w.created_by_user_id, " +
                         " h.creation_date = w.creation_date " +
                         "from workflow_history h " +
                         " right join workflow w on h.workflow_id = w.workflow_id " +
                         " where h.workflow_hist_id = (select max(s.WORKFLOW_HIST_ID) " +
                           "     from WORKFLOW_HISTORY s " +
                            "    where w.WORKFLOW_ID = s.WORKFLOW_ID) " +
                         " and h.WORKFLOW_ID = @workflow_id ";
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = dbConnection;
                    command.CommandText = sqlScript;
                    SqlParameter workflow_id_param = new SqlParameter
                    {
                        ParameterName = "@workflow_id",
                        Value = workflow_id
                    };
                    command.Parameters.Add(workflow_id_param);
                    try
                    {
                        command.ExecuteNonQuery();


                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Failed to insert workflow history information.");
                        //return View("~/Views/Home/CreateQuote.cshtml");
                    }
                }

                string part_series;
                part_series = "";
                string customer_name;
                customer_name = "";
                string comments;
                bool prototype_req_bool;
                //int prototype_req_flag;
                comments = "";
                part_series = model.PartSeries;
                customer_name = model.CustomerName;
                comments = model.Comments;


                string optional_assignment;

                prototype_req_bool = model.PrototypeRequired; //shows true or false
                prototype_required_flag = Convert.ToInt32(prototype_req_bool);
                //qualification_testing_type = model.Qualification_Testing_Type_Desc;
                optional_assignment = model.OptionalAssignment;

                sqlScript = @"update REQUEST_QUOTE_HEADER set QUALIFICATION_TESTING_TYPE=@qualification_testing_type, PROTOTYPE_REQUIRED_FLAG=@PROTOTYPE_REQUIRED_FLAG,COMMENTS=@COMMENTS 
                         ,STATUS_ID=@STATUS_ID, QUOTE_STATUS_ID=@QUOTE_STATUS_ID, OPTIONAL_ASSIGNMENT=@OPTIONAL_ASSIGNMENT WHERE 1=1 
                         AND REQUEST_ID=@REQUEST_ID;  ";
                using (SqlCommand command = new SqlCommand(sqlScript))
                {
                    command.Connection = dbConnection;
                    //request id
                    SqlParameter request_id_param = new SqlParameter
                    {
                        ParameterName = "@request_id",
                        Value = request_id_val
                    };
                    command.Parameters.Add(request_id_param);

                    //comments
                    SqlParameter comments_val = new SqlParameter
                    {
                        ParameterName = "@COMMENTS",
                        Value = String.IsNullOrEmpty(comments) ? "" : comments.ToString()
                    };
                    command.Parameters.Add(comments_val);
                    // status id
                    SqlParameter quote_status_id = new SqlParameter
                    {
                        ParameterName = "@status_id",
                        Value = model.Status_ID //value from the 
                    };
                    command.Parameters.Add(quote_status_id);
                    //quote status / PROCES STATUS
                    SqlParameter process_status_id = new SqlParameter
                    {
                        ParameterName = "@quote_status_id",
                        Value = status_id_val //who is it assigned too?status_id_val
                    };
                    command.Parameters.Add(process_status_id);
                    //optional assignment
                    SqlParameter optional_assignment_val = new SqlParameter
                    {
                        ParameterName = "@optional_assignment",
                        Value = String.IsNullOrEmpty(optional_assignment) ? "" : optional_assignment.ToString()
                    };
                    command.Parameters.Add(optional_assignment_val);
                    //prototype required flag
                    SqlParameter prototype = new SqlParameter
                    {
                        ParameterName = "@prototype_required_flag",
                        Value = prototype_required_flag
                    };
                    command.Parameters.Add(prototype);
                    //qualification testing
                    SqlParameter qualification = new SqlParameter
                    {
                        ParameterName = "@qualification_testing_type",
                        Value = qualification_testing_type
                    };
                    command.Parameters.Add(qualification);

                    try
                    {
                        command.ExecuteNonQuery();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Failed to insert quote header information.");
                        return View("~/Views/Home/CreateQuote.cshtml");
                    }
                    finally
                    {
                        dbConnection.Close();
                    }
                }


            }

            return View("~/Views/Home/Index.cshtml");

        }


        //    [HttpGet]
        //    public ActionResult CurrentQueue()
        //    {
        //        ViewBag.Message = "Your queue.";

        //        int user_id;
        //        int org_id;
        //        user_id = 0;
        //        org_id = 0;
        //        //string username;
        //        string org_name_var;
        //        org_name_var = "CAM";
        //        // string connectionString;
        //        string sqlScript;

        //        if (string.IsNullOrEmpty(Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)) == true)
        //        {

        //            MessageBox.Show("Username not found. Exiting...");
        //            Response.Write("<script>window.close();</script>");
        //            return View("~/Views/Home/CreateQuote.cshtml");
        //        }

        //        // SqlConnecion wrapper for entire function
        //        using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
        //        {
        

        //            System.Diagnostics.Debug.WriteLine("connecting to database...");
        //            dbConnection.Open();
        //            System.Diagnostics.Debug.WriteLine("connected to database.");

        //            //get the last_update_by
        //            sqlScript = "select u.user_id, u.org_id, o.org_name from users u, organization o where u.org_id=o.org_id and user_name='bmount' and active_flag=1";

        //            using (SqlCommand command = new SqlCommand(sqlScript, dbConnection))
        //            {

        //                SqlParameter parameter = new SqlParameter
        //                {
        //                    ParameterName = "@username",
        //                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
        //                };
        //                command.Parameters.Add(parameter);
        //                using (SqlDataReader reader = command.ExecuteReader())
        //                {
        //                    try
        //                    {
        //                        if (reader.HasRows)
        //                        {
        //                            while (reader.Read())
        //                            {
        //                                user_id = reader.GetInt32(0);
        //                                org_id = reader.GetInt32(1);
        //                                org_name_var = reader.GetString(2);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            MessageBox.Show("No users were found matching that criteria.");
        //                            //return;
        //                        }
        //                    }
        //                    finally
        //                    {

        //                        reader.Close();
        //                    }
        //                }

        //            }
        //        }
        //        //username = User.Identity.Name;
        //        //username = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1);

        //        using (SharePointConnectionUpdated dc = new SharePointConnectionUpdated())
        //        {
        //            var requests = (from c in dc.vCheck_Your_Requests
        //                                //where (c.org_name==org_name_var || c.org_name=="CAM")
        //                            select new ViewQueue
        //                            {
        //                                Request_Id = c.request_id,
        //                                Series = c.assigned_series,
        //                                Priority = c.highest_priority,
        //                                DueDate = c.closest_due_date,
        //                                Status = c.status,
        //                                CreatedBy = c.created_by,
        //                                CustomerName = c.CUSTOMER_NAME,
        //                                OpportunityType = c.opportunity_type_desc,
        //                                ProductType = c.product_type,
        //                                Comments = c.comments,
        //                                CurrentAssignment = c.current_assignment,
        //                                //OptionalAssignment = c.optional_assignment,
        //                                Organization = c.org_name

        //                            }).ToList();

        //            return View(requests);
        //        }
        //    }

        //    [HttpGet]
        //    public ActionResult ManageUsers(ManageUser model)
        //    {
        //        ViewBag.Message = "User roles.";

        //        using (SharePointConnectionUpdated dc = new SharePointConnectionUpdated())
        //        {
        //            var manage_users = (from m in dc.vManage_Users
        //                                select new ManageUser
        //                                {
        //                                    UserId = m.user_id,
        //                                    Username = m.username,
        //                                    Email = m.email,
        //                                    Organization = m.organization,
        //                                    Role = m.role,
        //                                    ActiveFlag = m.active_flag,
        //                                    MultiOrg = m.multi_org
        //                                }).ToList();

        //            return View(manage_users);
        //        }
        //    }
        //    public ActionResult Reporting()
        //    {
        //        ViewBag.Message = "Reports.";
        //        if (string.IsNullOrEmpty(Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)) == true)
        //        {

        //            MessageBox.Show("Username not found. Exiting...");
        //            Response.Write("<script>window.close();</script>");
        //            return View("~/Views/Home/CreateQuote.cshtml");
        //        }
        //        return View();
        //    }

        //    //[SharePointContextFilter]
        //    public static void SendEmail(List<SelectListItem> Emails, string type_of_email, Int32 request_num, string attach_blueprint, string attach_other, string optional_assignment)
        //    {
        //        using (SmtpClient client = new SmtpClient())
        //        {
        //            var basicCredential = new NetworkCredential("CAM_SVC_RFQ", "Str0ngPassw0rdz");
        //            using (MailMessage mail = new MailMessage())
        //            {
        //                //MailMessage mail = new MailMessage("no_reply@RequestForQuote.com", "bmount@conaeromfg.com");
        //                MailAddress fromAddress = new MailAddress("QuoteRequest@RFQ.com");
        //                client.Port = 25;
        //                client.DeliveryMethod = SmtpDeliveryMethod.Network;
        //                client.UseDefaultCredentials = false;
        //                //client.EnableSsl = true;
        //                client.Credentials = basicCredential; //new System.Net.NetworkCredential("bmount", "Darcy3439;'");
        //                client.Host = "mobile.conaeromfg.com";

        //                mail.From = fromAddress;
        //                //mail.To.Add(email_chosen);
        //                if (optional_assignment != null)
        //                {
        //                    mail.CC.Add(optional_assignment);
        //                }
        //                foreach (var email in Emails)
        //                {
        //                    MailAddress to = new MailAddress(email.Text);
        //                    mail.To.Add(to);
        //                }


        //                if (attach_blueprint != null && attach_other == null)
        //                {
        //                    System.Net.Mail.Attachment attachment;
        //                    attachment = new System.Net.Mail.Attachment(attach_blueprint);
        //                    mail.Attachments.Add(attachment);
        //                }
        //                else if (attach_blueprint == null && attach_other != null)
        //                {
        //                    System.Net.Mail.Attachment attachment_other;
        //                    attachment_other = new System.Net.Mail.Attachment(attach_other);
        //                    mail.Attachments.Add(attachment_other);
        //                }
        //                else if (attach_blueprint != null && attach_other != null)
        //                {
        //                    System.Net.Mail.Attachment attachment_other;
        //                    attachment_other = new System.Net.Mail.Attachment(attach_other);
        //                    mail.Attachments.Add(attachment_other);

        //                    System.Net.Mail.Attachment attachment;
        //                    attachment = new System.Net.Mail.Attachment(attach_blueprint);
        //                    mail.Attachments.Add(attachment);
        //                }

        //                if (type_of_email == "NEW")
        //                {
        //                    mail.Subject = "Request To Review " + request_num.ToString() + "";
        //                    mail.Body = "Quote request " + request_num.ToString() + " has been created and assigned to you for review, possible qualification, and evaluation.";
        //                }
        //                else if (type_of_email == "QUOTE")
        //                {
        //                    mail.Subject = "Quote Request Approved " + request_num.ToString() + "";
        //                    mail.Body = "Quote request " + request_num.ToString() + " has been closed with a recommendation to quote the respective part.";
        //                }
        //                else if (type_of_email == "NO QUOTE")
        //                {
        //                    mail.Subject = "Quote Request Denied " + request_num.ToString() + "";
        //                    mail.Body = "Quote request " + request_num.ToString() + " has been closed with a recommendation not to quote the respective part.";
        //                }
        //                else
        //                {
        //                    mail.Subject = "Request To Review " + request_num.ToString() + "";
        //                    mail.Body = "You have been assigned request for quote " + request_num.ToString() + " to review for possible qualification and evaluation.";
        //                }

        //                try
        //                {
        //                    client.Send(mail);
        //                }
        //                catch (Exception ex)
        //                {
        //                    //Response.Write(ex.Message);
        //                    MessageBox.Show("Failed to send email. Reason: " + ex.Message);
        //                }
        //            }
        //        }
        //    }



        [HttpPost]
        public ActionResult CreateQuote(MakeRequestData model)
        {
            // string connectionString;

            if (string.IsNullOrEmpty(Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)) == true)
            {

                MessageBox.Show("Username not found. Unable to submit. Exiting...");
                Response.Write("<script>window.close();</script>");
                return View("~/Views/Home/CreateQuote.cshtml");
            }

            string sql_value;
            int org_id;
            int assigned_to_org_id;
            //Int32 priority_id;
            Int32 assigned_to_role_id;
            //string route_desc;// = Request["routing"];
            Int32 created_by_user_id;
            //string username;
            Int32 status_id_val;
            Int32 workflow_id;
            Int32 prototype_required_flag;
            //string qualification_testing_type;

            prototype_required_flag = 0;
            workflow_id = 0;
            assigned_to_role_id = 0;
            //priority_id = 0;
            sql_value = "";
            created_by_user_id = 0;
            //Int32 active_flag_val = 1;
            org_id = 1;
            assigned_to_org_id = 1;

            Int32 priority;
            priority = 0;
            Int32 priority2;
            priority2 = 0;
            Int32 priority3;
            priority3 = 0;
            Int32 priority4;
            priority4 = 0;
            Int32 priority5;
            priority5 = 0;

            //in opportunity_type;
            //int product_type_val;
            string optional_assignment;
            string buyers_name;
            string contact_info;
            int material_type;
            material_type = 0;
            int material_type2;
            material_type2 = 0;
            int material_type3;
            material_type3 = 0;
            int material_type4;
            material_type4 = 0;
            int material_type5;
            material_type5 = 0;
            int product_type;
            product_type = 0;
            int opportunity_type_val;
            opportunity_type_val = 0;


            using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                System.Diagnostics.Debug.WriteLine("connecting to database...");
                dbConnection.Open();
                System.Diagnostics.Debug.WriteLine("connected to database.");


                //sql_value = this.DbReader<string>("select sql_statement from sql_statements where sql_id = 1", dbConnection, (reader) =>
                //{
                //    if (reader.Read())
                //        return reader.GetString(0);
                //    return string.Empty;
                //});

                //get the last_update_by

                using (SqlCommand command = new SqlCommand("select user_id, org_id from users where user_name=@username and active_flag=1", dbConnection))
                {
                    SqlParameter parameter = new SqlParameter
                    {
                        ParameterName = "@username",
                        Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                    };
                    command.Parameters.Add(parameter);

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                created_by_user_id = reader.GetInt32(0);
                                org_id = reader.GetInt32(1);
                                assigned_to_org_id = reader.GetInt32(1);
                            }
                        }
                        else
                        {
                            MessageBox.Show("No users were found matching that criteria.");
                            //return;
                        }
                        reader.Close();
                    }
                    //cnn.Close();
                }

                model.RoleTypeList = this.GetRolesListFromCreate(dbConnection);


                model.MaterialTypeList1 = this.GetMaterialTypesList(dbConnection);
                var material_type_selected = model.MaterialTypeList1.Find(m => m.Value == model.Material_Type_ID1.ToString());
                material_type = Convert.ToInt32(model.Material_Type_ID1);


                model.MaterialTypeList2 = this.GetMaterialTypesList(dbConnection);
                var material_type_selected2 = model.MaterialTypeList2.Find(m => m.Value == model.Material_Type_ID2.ToString());
                material_type2 = Convert.ToInt32(model.Material_Type_ID2);


                model.MaterialTypeList3 = this.GetMaterialTypesList(dbConnection);
                var material_type_selected3 = model.MaterialTypeList3.Find(m => m.Value == model.Material_Type_ID3.ToString());
                material_type3 = Convert.ToInt32(model.Material_Type_ID3);

                model.MaterialTypeList4 = this.GetMaterialTypesList(dbConnection);
                var material_type_selected4 = model.MaterialTypeList4.Find(m => m.Value == model.Material_Type_ID4.ToString());
                material_type4 = Convert.ToInt32(model.Material_Type_ID4);

                model.MaterialTypeList5 = this.GetMaterialTypesList(dbConnection);
                var material_type_selected5 = model.MaterialTypeList5.Find(m => m.Value == model.Material_Type_ID5.ToString());
                material_type5 = Convert.ToInt32(model.Material_Type_ID5);

                model.ProductTypecList = this.GetProductTypesList(dbConnection);
                var product_types = model.ProductTypecList.Find(m => m.Value == model.Product_Type_ID.ToString());
                product_type = Convert.ToInt32(model.Product_Type_ID);

                model.OpportunityTypesList = this.GetOppurtunitiesList(dbConnection);

                var opportunity_type = model.OpportunityTypesList.Find(m => m.Text == model.Opportunity_Type_ID.ToString());
                opportunity_type_val = Convert.ToInt32(model.Opportunity_Type_ID);

                model.PriorityTypeDescList1 = this.GetPrioritiesList(dbConnection);

                var priorities = model.PriorityTypeDescList1.Find(m => m.Value == model.Priority_ID1.ToString());
                priority = Convert.ToInt32(model.Priority_ID1);

                model.PriorityTypeDescList2 = this.GetPrioritiesList(dbConnection);

                var priorities2 = model.PriorityTypeDescList2.Find(m => m.Value == model.Priority_ID2.ToString());
                priority2 = Convert.ToInt32(model.Priority_ID2);

                model.PriorityTypeDescList3 = this.GetPrioritiesList(dbConnection);

                var priorities3 = model.PriorityTypeDescList3.Find(m => m.Value == model.Priority_ID3.ToString());
                priority3 = Convert.ToInt32(model.Priority_ID3);


                model.PriorityTypeDescList4 = this.GetPrioritiesList(dbConnection);

                var priorities4 = model.PriorityTypeDescList4.Find(m => m.Value == model.Priority_ID4.ToString());
                priority4 = Convert.ToInt32(model.Priority_ID4);

                model.PriorityTypeDescList5 = this.GetPrioritiesList(dbConnection);

                var priorities5 = model.PriorityTypeDescList5.Find(m => m.Value == model.Priority_ID5.ToString());
                priority5 = Convert.ToInt32(model.Priority_ID5);


                var role_types = model.RoleTypeList.Find(m => m.Value == model.Role_ID.ToString());

                assigned_to_role_id = Convert.ToInt32(model.Role_ID);

                status_id_val = 0;
                //engineering
                if (assigned_to_role_id == 5)
                {
                    status_id_val = 2;
                }
                //engineering manager
                else if (assigned_to_role_id == 6)
                {
                    status_id_val = 3;
                }
                else
                //sales manager
                {
                    status_id_val = 2;
                }

                //cnn.Open();
                //create workflow
                string sqlScriptInsert = @"insert into workflow (STATUS_ID, LAST_UPDATED, LAST_UPDATED_BY, ASSIGNED_TO_ROLE_ID, CREATED_BY_USER_ID, CREATION_DATE) 
                 values(@status_id 
                 , CONVERT(DATE, GETDATE()) 
                 , @last_updated_by, @assigned_to_role_id 
                 , @created_by_user_id, convert(date, GETDATE())); "; //fill the sql value
                                                                         //command.CommandText = sqlScript;

                using (var command = new SqlCommand(sqlScriptInsert, dbConnection))
                {
                    SqlParameter status_id = new SqlParameter
                    {
                        ParameterName = "@status_id",
                        Value = status_id_val
                    };
                    command.Parameters.Add(status_id);
                    //
                    SqlParameter last_updated_by = new SqlParameter
                    {
                        ParameterName = "@last_updated_by",
                        Value = created_by_user_id
                    };
                    command.Parameters.Add(last_updated_by);
                    //
                    SqlParameter assigned_to_role_id_val = new SqlParameter
                    {
                        ParameterName = "@assigned_to_role_id",
                        Value = assigned_to_role_id
                    };
                    command.Parameters.Add(assigned_to_role_id_val);
                    //
                    SqlParameter created_by_user_id_val = new SqlParameter
                    {
                        ParameterName = "@created_by_user_id",
                        Value = created_by_user_id
                    };
                    command.Parameters.Add(created_by_user_id_val);

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.Write(ex.Message, "Create Quote");
                        //MessageBox.Show("Failed to insert workflow information.");
                        ViewBag.ErrorMsg = "Failed to insert workflow information.";
                        return View("~/Views/Home/CreateQuote.cshtml", model);
                    }

                }

                int find_new_workflow = this.DbReaderWithParam<int>("select isnull(max(workflow_id),1) From workflow where 1=1 and last_updated_by=@last_updated_by ",
                    dbConnection,
                    new SqlParameter("@last_updated_by", created_by_user_id),
                    (reader) =>
                    {
                        if (reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                return reader.GetInt32(0);
                            }
                        }
                        else
                        {
                            Console.WriteLine("No records found.");
                        }
                        return 0;
                    });

                string sqlScriptWorkflow = @"insert into workflow_history (WORKFLOW_ID,STATUS_ID, LAST_UPDATED, LAST_UPDATED_BY, ASSIGNED_TO_ROLE_ID, CREATED_BY_USER_ID, CREATION_DATE) 
                     values(@workflow_id,@status_id 
                     , CONVERT(DATE, GETDATE()) 
                     , @last_updated_by, @assigned_to_role_id 
                     , @created_by_user_id, convert(date, GETDATE())); "; //fill the sql value
                                                                          //command.CommandText = sqlScript;
                using (var command = new SqlCommand(sqlScriptWorkflow, dbConnection))
                {

                    command.Parameters.Add(new SqlParameter("@workflow_id", find_new_workflow));
                    command.Parameters.Add(new SqlParameter("@status_id", status_id_val));
                    command.Parameters.Add(new SqlParameter("@assigned_to_role_id", assigned_to_role_id));
                    command.Parameters.Add(new SqlParameter("@last_updated_by", created_by_user_id));
                    command.Parameters.Add(new SqlParameter("@created_by_user_id", created_by_user_id));


                    try
                    {
                        command.ExecuteNonQuery();
                        command.Dispose();

                    }
                    catch (Exception ex)
                    {
                        ViewBag.ErrorMsg = "Failed to insert workflow information.";
                        return View("~/Views/Home/CreateQuote.cshtml", model);
                    }

                }

                //command.Parameters.Clear();

                System.Diagnostics.Debug.WriteLine(created_by_user_id.ToString());
                //find workflow detail that was just assigned
                string sqlScriptFindDetail = "select max(workflow_id) workflow_id From workflow where created_by_user_id=@created_by_user_id ";
                //command.CommandText = sqlScript;
                using (var command = new SqlCommand(sqlScriptFindDetail, dbConnection))
                {
                    command.Parameters.Add(new SqlParameter("@created_by_user_id", created_by_user_id));

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                workflow_id = reader.GetInt32(0);
                            }
                        }
                        else
                        {
                            Console.WriteLine("No records found.");
                        }
                        reader.Close();
                    }
                }

                string part_series;
                part_series = "";
                string customer_name;
                customer_name = "";
                string comments;
                bool prototype_req_bool;
                //int prototype_req_flag;
                comments = "";
                part_series = model.PartSeries;
                customer_name = model.CustomerName;
                comments = model.Comments;

                //MakeRequestData data = new MakeRequestData();


                //end of changes
                buyers_name = "";
                contact_info = "";
                buyers_name = model.BuyersName;
                contact_info = model.ContactInfo;


                prototype_req_bool = model.PrototypeRequired; //shows true or false
                prototype_required_flag = Convert.ToInt32(prototype_req_bool);
                optional_assignment = model.OptionalAssignment;

                string attach_blueprint;
                attach_blueprint = "";
                string attach_other;
                attach_other = "";
                attach_blueprint = model.BluePrintAttachment;
                attach_other = model.OtherAttachment;

                //cnn.Open();

                string sqlScriptQuote = "insert into REQUEST_QUOTE_HEADER (REQUESTED_BY_ID, CREATION_DATE,WORKFLOW_ID   , PART_SERIES   "
                    + ", CUSTOMER_NAME   , BUYERS_NAME, CONTACT_INFORMATION   , OPPORTUNITY_TYPE   , PROTOTYPE_REQUIRED_FLAG "
                    + ", QUALIFICATION_TESTING_TYPE,PRODUCT_TYPE_ID   , COMMENTS "
                    + ", STATUS_ID   , OPTIONAL_ASSIGNMENT, QUOTE_STATUS_ID, ATTACH_BLUEPRINT, ATTACH_OTHER)    values(@requested_by_id "
                    + ", convert(date, getdate()), @workflow_id, @part_series, @customer_name,@buyers_name,@contact_information "
                    + ", @opportunity_type, @prototype_required_flag,@qualification_testing_type,@product_type_id, @comments, @process_status_id "
                    + ", @optional_assignment, @quote_status_id, @attach_blueprint, @attach_other);";
                //command.CommandText = sqlScript;
                using (SqlCommand command = new SqlCommand(sqlScriptQuote, dbConnection))
                {
                    //requested_by_id
                    SqlParameter requested_by_id = new SqlParameter
                    {
                        ParameterName = "@requested_by_id",
                        Value = created_by_user_id
                    };
                    command.Parameters.Add(requested_by_id);
                    //workflow
                    SqlParameter workflow_id_val = new SqlParameter
                    {
                        ParameterName = "@workflow_id",
                        Value = workflow_id
                    };
                    command.Parameters.Add(workflow_id_val);
                    //part series
                    SqlParameter part_series_val = new SqlParameter
                    {
                        ParameterName = "@part_series",
                        Value = part_series
                    };
                    command.Parameters.Add(part_series_val);
                    //customer
                    SqlParameter customer_name_val = new SqlParameter
                    {
                        ParameterName = "@customer_name",
                        Value = String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString()
                    };
                    command.Parameters.Add(customer_name_val);
                    //opportunity type
                    SqlParameter opportunity_type_param = new SqlParameter
                    {
                        ParameterName = "@opportunity_type",
                        Value = opportunity_type_val
                    };
                    command.Parameters.Add(opportunity_type_param);
                    //product type id
                    SqlParameter product_type_id_val = new SqlParameter
                    {
                        ParameterName = "@product_type_id",
                        Value = product_type
                    };
                    command.Parameters.Add(product_type_id_val);
                    //comments
                    SqlParameter comments_val = new SqlParameter
                    {
                        ParameterName = "@comments",
                        Value = String.IsNullOrEmpty(comments) ? "" : comments.ToString()
                    };
                    command.Parameters.Add(comments_val);
                    //quote status id
                    SqlParameter quote_status_id = new SqlParameter
                    {
                        ParameterName = "@quote_status_id",
                        Value = status_id_val
                    };
                    command.Parameters.Add(quote_status_id);
                    //PROCES STATUS
                    SqlParameter process_status_id = new SqlParameter
                    {
                        ParameterName = "@process_status_id",
                        Value = 8 // more information requested //status 6
                    };
                    command.Parameters.Add(process_status_id);
                    //optional assignment
                    SqlParameter optional_assignment_val = new SqlParameter
                    {
                        ParameterName = "@optional_assignment",
                        Value = String.IsNullOrEmpty(optional_assignment) ? "" : optional_assignment.ToString()
                    };
                    command.Parameters.Add(optional_assignment_val);
                    //prototype required flag
                    SqlParameter prototype = new SqlParameter
                    {
                        ParameterName = "@prototype_required_flag",
                        Value = prototype_required_flag
                    };
                    command.Parameters.Add(prototype);
                    //qualification testing flag
                    SqlParameter qualification = new SqlParameter
                    {
                        ParameterName = "@qualification_testing_type",
                        Value = "Awaiting Clarification"
                    };
                    command.Parameters.Add(qualification);
                    //buyers name
                    SqlParameter buyers_name_val = new SqlParameter
                    {
                        ParameterName = "@buyers_name",
                        Value = String.IsNullOrEmpty(buyers_name) ? "" : buyers_name.ToString()
                    };
                    command.Parameters.Add(buyers_name_val);
                    //contact info
                    SqlParameter contact_info_val = new SqlParameter
                    {
                        ParameterName = "@contact_information",
                        Value = String.IsNullOrEmpty(contact_info) ? "" : contact_info.ToString()
                    };
                    command.Parameters.Add(contact_info_val);
                    //blueprint
                    SqlParameter attach_blueprint_val = new SqlParameter
                    {
                        ParameterName = "@attach_blueprint",
                        Value = String.IsNullOrEmpty(attach_blueprint) ? "" : attach_blueprint.ToString()
                    };
                    command.Parameters.Add(attach_blueprint_val);
                    //other attachment
                    SqlParameter attach_other_val = new SqlParameter
                    {
                        ParameterName = "@attach_other",
                        Value = String.IsNullOrEmpty(attach_other) ? "" : attach_other.ToString()
                    };
                    command.Parameters.Add(attach_other_val);
                    try
                    {
                        command.ExecuteNonQuery();
                        //command.Dispose();
                        //MessageBox.Show("Submitted part 1!");
                    }
                    catch (Exception)
                    {
                        //MessageBox.Show("Failed to insert quote header information.");
                        ViewBag.ErrorMsg = "Failed to insert quote header information.";
                        return View("~/Views/Home/CreateQuote.cshtml", model);
                    }

                }
                //find the new request that was created
                Int32 request_id_val;
                request_id_val = 0;


                System.Diagnostics.Debug.WriteLine(created_by_user_id.ToString());
                //find workflow detail that was just assigned
                string sqlScriptQuoteFind = "select h.request_id request_id from Request_Quote_Header h "
                        + " , workflow w "
                        + " where h.workflow_id = w.workflow_id and w.workflow_id=@workflow_id";
                //command.CommandText = sqlScript;
                using (SqlCommand command = new SqlCommand(sqlScriptQuoteFind, dbConnection))
                {

                    command.Parameters.Add(new SqlParameter("@workflow_id", workflow_id));

                    //cnn.Open();
                    using (var reader = command.ExecuteReader())
                    {

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                request_id_val = reader.GetInt32(0);
                            }
                        }
                        else
                        {

                            ViewBag.ErrorMsg = "No workflow ID assigned. Exiting.";
                            return View("~/Views/Home/CreateQuote.cshtml");
                        }
                        reader.Close();
                    }
                }

                //define variables to be used in lines inserstion // loop should start here

                //dyanmic parameters
                string size_num;
                size_num = "";
                string platform;
                platform = "";
                decimal market_price;
                market_price = 0;
                DateTime? due_date;
                due_date = DateTime.Now;
                string other_sources;
                other_sources = "";
                decimal est_revenue_y1;
                est_revenue_y1 = 0;
                decimal est_revenue_y2;
                est_revenue_y2 = 0;
                decimal est_revenue_y3;
                est_revenue_y3 = 0;
                decimal est_revenue_y4;
                est_revenue_y4 = 0;
                decimal est_revenue_y5;
                est_revenue_y5 = 0;
                decimal est_qty_y1;
                est_qty_y1 = 0;
                decimal est_qty_y2;
                est_qty_y2 = 0;
                decimal est_qty_y3;
                est_qty_y3 = 0;
                decimal est_qty_y4;
                est_qty_y4 = 0;
                decimal est_qty_y5;
                est_qty_y5 = 0;

                //line1
                string size_num1;
                size_num1 = "";
                string platform1;
                platform1 = "";
                decimal market_price1;
                market_price1 = 0;
                DateTime? due_date1;
                string other_sources1;
                other_sources1 = "";
                decimal est_revenue_y11;
                est_revenue_y11 = 0;
                decimal est_revenue_y21;
                est_revenue_y21 = 0;
                decimal est_revenue_y31;
                est_revenue_y31 = 0;
                decimal est_revenue_y41;
                est_revenue_y41 = 0;
                decimal est_revenue_y51;
                est_revenue_y51 = 0;
                decimal est_qty_y11;
                est_qty_y11 = 0;
                decimal est_qty_y21;
                est_qty_y21 = 0;
                decimal est_qty_y31;
                est_qty_y31 = 0;
                decimal est_qty_y41;
                est_qty_y41 = 0;
                decimal est_qty_y51;
                est_qty_y51 = 0;
                //line 2
                string size_num2;
                size_num2 = "";
                string platform2;
                platform2 = "";
                decimal market_price2;
                market_price2 = 0;
                DateTime? due_date2;
                string other_sources2;
                other_sources2 = "";
                decimal est_revenue_y12;
                est_revenue_y12 = 0;
                decimal est_revenue_y22;
                est_revenue_y22 = 0;
                decimal est_revenue_y32;
                est_revenue_y32 = 0;
                decimal est_revenue_y42;
                est_revenue_y42 = 0;
                decimal est_revenue_y52;
                est_revenue_y52 = 0;
                decimal est_qty_y12;
                est_qty_y12 = 0;
                decimal est_qty_y22;
                est_qty_y22 = 0;
                decimal est_qty_y32;
                est_qty_y32 = 0;
                decimal est_qty_y42;
                est_qty_y42 = 0;
                decimal est_qty_y52;
                est_qty_y52 = 0;
                //line3
                string size_num3;
                size_num3 = "";
                string platform3;
                platform3 = "";
                decimal market_price3;
                market_price3 = 0;
                DateTime? due_date3;
                string other_sources3;
                other_sources3 = "";
                decimal est_revenue_y13;
                est_revenue_y13 = 0;
                decimal est_revenue_y23;
                est_revenue_y23 = 0;
                decimal est_revenue_y33;
                est_revenue_y33 = 0;
                decimal est_revenue_y43;
                est_revenue_y43 = 0;
                decimal est_revenue_y53;
                est_revenue_y53 = 0;
                decimal est_qty_y13;
                est_qty_y13 = 0;
                decimal est_qty_y23;
                est_qty_y23 = 0;
                decimal est_qty_y33;
                est_qty_y33 = 0;
                decimal est_qty_y43;
                est_qty_y43 = 0;
                decimal est_qty_y53;
                est_qty_y53 = 0;
                //line4
                string size_num4;
                size_num4 = "";
                string platform4;
                platform4 = "";
                decimal market_price4;
                market_price4 = 0;
                DateTime? due_date4;
                string other_sources4;
                other_sources4 = "";
                decimal est_revenue_y14;
                est_revenue_y14 = 0;
                decimal est_revenue_y24;
                est_revenue_y24 = 0;
                decimal est_revenue_y34;
                est_revenue_y34 = 0;
                decimal est_revenue_y44;
                est_revenue_y44 = 0;
                decimal est_revenue_y54;
                est_revenue_y54 = 0;
                decimal est_qty_y14;
                est_qty_y14 = 0;
                decimal est_qty_y24;
                est_qty_y24 = 0;
                decimal est_qty_y34;
                est_qty_y34 = 0;
                decimal est_qty_y44;
                est_qty_y44 = 0;
                decimal est_qty_y54;
                est_qty_y54 = 0;
                //line5
                string size_num5;
                size_num5 = "";
                string platform5;
                platform5 = "";
                decimal market_price5;
                market_price5 = 0;
                DateTime? due_date5;
                string other_sources5;
                other_sources5 = "";
                decimal est_revenue_y15;
                est_revenue_y15 = 0;
                decimal est_revenue_y25;
                est_revenue_y25 = 0;
                decimal est_revenue_y35;
                est_revenue_y35 = 0;
                decimal est_revenue_y45;
                est_revenue_y45 = 0;
                decimal est_revenue_y55;
                est_revenue_y55 = 0;
                decimal est_qty_y15;
                est_qty_y15 = 0;
                decimal est_qty_y25;
                est_qty_y25 = 0;
                decimal est_qty_y35;
                est_qty_y35 = 0;
                decimal est_qty_y45;
                est_qty_y45 = 0;
                decimal est_qty_y55;
                est_qty_y55 = 0;

                int material_type_dyn;
                material_type_dyn = 0;
                Int32 priority_dyn;
                priority_dyn = 0;

                size_num1 = model.Size1;
                platform1 = model.Platform1;
                est_revenue_y11 = model.EstRevenueYr11;
                est_revenue_y21 = model.EstRevenueYr21;
                est_revenue_y31 = model.EstRevenueYr31;
                est_revenue_y41 = model.EstRevenueYr41;
                est_revenue_y51 = model.EstRevenueYr51;
                est_qty_y11 = model.EstQtyYr11;
                est_qty_y21 = model.EstQtyYr21;
                est_qty_y31 = model.EstQtyYr31;
                est_qty_y41 = model.EstQtyYr41;
                est_qty_y51 = model.EstQtyYr51;
                market_price1 = model.MarketPrice1;
                due_date1 = model.DueDate1;
                other_sources1 = model.OtherSource1;
                //line 2
                size_num2 = model.Size2;
                platform2 = model.Platform2;
                est_revenue_y12 = model.EstRevenueYr12;
                est_revenue_y22 = model.EstRevenueYr22;
                est_revenue_y32 = model.EstRevenueYr32;
                est_revenue_y42 = model.EstRevenueYr42;
                est_revenue_y52 = model.EstRevenueYr52;
                est_qty_y12 = model.EstQtyYr12;
                est_qty_y22 = model.EstQtyYr22;
                est_qty_y32 = model.EstQtyYr32;
                est_qty_y42 = model.EstQtyYr42;
                est_qty_y52 = model.EstQtyYr52;
                market_price2 = model.MarketPrice2;
                due_date2 = model.DueDate2;
                other_sources2 = model.OtherSource2;
                //line 3
                size_num3 = model.Size3;
                platform3 = model.Platform3;
                est_revenue_y13 = model.EstRevenueYr13;
                est_revenue_y23 = model.EstRevenueYr23;
                est_revenue_y33 = model.EstRevenueYr33;
                est_revenue_y43 = model.EstRevenueYr43;
                est_revenue_y53 = model.EstRevenueYr53;
                est_qty_y13 = model.EstQtyYr13;
                est_qty_y23 = model.EstQtyYr23;
                est_qty_y33 = model.EstQtyYr33;
                est_qty_y43 = model.EstQtyYr43;
                est_qty_y53 = model.EstQtyYr53;
                market_price3 = model.MarketPrice3;
                due_date3 = model.DueDate3;
                other_sources3 = model.OtherSource3;
                //line 4
                size_num4 = model.Size4;
                platform4 = model.Platform4;
                est_revenue_y14 = model.EstRevenueYr14;
                est_revenue_y24 = model.EstRevenueYr24;
                est_revenue_y34 = model.EstRevenueYr34;
                est_revenue_y44 = model.EstRevenueYr44;
                est_revenue_y54 = model.EstRevenueYr54;
                est_qty_y14 = model.EstQtyYr14;
                est_qty_y24 = model.EstQtyYr24;
                est_qty_y34 = model.EstQtyYr34;
                est_qty_y44 = model.EstQtyYr44;
                est_qty_y54 = model.EstQtyYr54;
                market_price4 = model.MarketPrice4;
                due_date4 = model.DueDate4;
                other_sources4 = model.OtherSource4;
                //line 5
                size_num5 = model.Size5;
                platform5 = model.Platform5;
                est_revenue_y15 = model.EstRevenueYr15;
                est_revenue_y25 = model.EstRevenueYr25;
                est_revenue_y35 = model.EstRevenueYr35;
                est_revenue_y45 = model.EstRevenueYr45;
                est_revenue_y55 = model.EstRevenueYr55;
                est_qty_y15 = model.EstQtyYr15;
                est_qty_y25 = model.EstQtyYr25;
                est_qty_y35 = model.EstQtyYr35;
                est_qty_y45 = model.EstQtyYr45;
                est_qty_y55 = model.EstQtyYr55;
                market_price5 = model.MarketPrice5;
                due_date5 = model.DueDate5;
                other_sources5 = model.OtherSource5;

                //cnn.Open();
                int check_blank_lines;
                check_blank_lines = 0;
                //i = 0;
                if (string.IsNullOrEmpty(model.Size2) == true && string.IsNullOrEmpty(model.Size3) == true && string.IsNullOrEmpty(model.Size4) == true && string.IsNullOrEmpty(model.Size5) == true)
                {
                    check_blank_lines = 1;
                }
                else if (model.Size2 != null && string.IsNullOrEmpty(model.Size3) == true && string.IsNullOrEmpty(model.Size4) == true && string.IsNullOrEmpty(model.Size5) == true)
                {
                    check_blank_lines = 2;
                }
                else if (model.Size2 != null && model.Size3 != null && string.IsNullOrEmpty(model.Size4) == true && string.IsNullOrEmpty(model.Size5) == true)
                {
                    check_blank_lines = 3;
                }
                else if (model.Size2 != null && model.Size3 != null && model.Size4 != null && string.IsNullOrEmpty(model.Size5) == true)
                {

                    check_blank_lines = 4;
                }
                else if (model.Size2 != null && model.Size3 != null && model.Size4 != null && model.Size5 != null)
                {
                    check_blank_lines = 5;
                }
                else check_blank_lines = 0;



                for (int i = 1; i <= check_blank_lines; i++)
                {
                    //string test = model.Size3;
                    //MessageBox.Show(Convert.ToString(model.Size3));

                    size_num = "";
                    platform = "";
                    est_revenue_y1 = 0;
                    est_revenue_y2 = 0;
                    est_revenue_y3 = 0;
                    est_revenue_y4 = 0;
                    est_revenue_y5 = 0;
                    est_qty_y1 = 0;
                    est_qty_y2 = 0;
                    est_qty_y3 = 0;
                    est_qty_y4 = 0;
                    est_qty_y5 = 0;
                    market_price = 0;
                    due_date = DateTime.Now;
                    other_sources = "";
                    material_type_dyn = 0;
                    priority_dyn = 0;


                    if (i == 1 && check_blank_lines != 0)
                    {
                        size_num = size_num1;
                        platform = platform1;
                        est_revenue_y1 = est_revenue_y11;
                        est_revenue_y2 = est_revenue_y21;
                        est_revenue_y3 = est_revenue_y31;
                        est_revenue_y4 = est_revenue_y41;
                        est_revenue_y5 = est_revenue_y51;
                        est_qty_y1 = est_qty_y11;
                        est_qty_y2 = est_qty_y21;
                        est_qty_y3 = est_qty_y31;
                        est_qty_y4 = est_qty_y41;
                        est_qty_y5 = est_qty_y51;
                        market_price = market_price1;
                        due_date = due_date1;
                        other_sources = other_sources1;
                        priority_dyn = priority;
                        material_type_dyn = material_type;
                    }
                    else if (i == 2 && check_blank_lines != 0)
                    {
                        size_num = size_num2;
                        platform = platform2;
                        est_revenue_y1 = est_revenue_y12;
                        est_revenue_y2 = est_revenue_y22;
                        est_revenue_y3 = est_revenue_y32;
                        est_revenue_y4 = est_revenue_y42;
                        est_revenue_y5 = est_revenue_y52;
                        est_qty_y1 = est_qty_y12;
                        est_qty_y2 = est_qty_y22;
                        est_qty_y3 = est_qty_y32;
                        est_qty_y4 = est_qty_y42;
                        est_qty_y5 = est_qty_y52;
                        market_price = market_price2;
                        due_date = due_date2;
                        other_sources = other_sources2;
                        priority_dyn = priority2;
                        material_type_dyn = material_type2;
                    }
                    else if (i == 3 && check_blank_lines != 0)
                    {
                        size_num = size_num3;
                        platform = platform3;
                        est_revenue_y1 = est_revenue_y13;
                        est_revenue_y2 = est_revenue_y23;
                        est_revenue_y3 = est_revenue_y33;
                        est_revenue_y4 = est_revenue_y43;
                        est_revenue_y5 = est_revenue_y53;
                        est_qty_y1 = est_qty_y13;
                        est_qty_y2 = est_qty_y23;
                        est_qty_y3 = est_qty_y33;
                        est_qty_y4 = est_qty_y43;
                        est_qty_y5 = est_qty_y53;
                        market_price = market_price3;
                        due_date = due_date3;
                        other_sources = other_sources3;
                        priority_dyn = priority3;
                        material_type_dyn = material_type3;
                    }
                    else if (i == 4 && check_blank_lines != 0)
                    {
                        size_num = size_num4;
                        platform = platform4;
                        est_revenue_y1 = est_revenue_y14;
                        est_revenue_y2 = est_revenue_y24;
                        est_revenue_y3 = est_revenue_y34;
                        est_revenue_y4 = est_revenue_y44;
                        est_revenue_y5 = est_revenue_y54;
                        est_qty_y1 = est_qty_y14;
                        est_qty_y2 = est_qty_y24;
                        est_qty_y3 = est_qty_y34;
                        est_qty_y4 = est_qty_y44;
                        est_qty_y5 = est_qty_y54;
                        market_price = market_price4;
                        due_date = due_date4;
                        other_sources = other_sources4;
                        priority_dyn = priority4;
                        material_type_dyn = material_type4;
                    }
                    else if (i == 5 && check_blank_lines != 0)
                    {
                        size_num = size_num5;
                        platform = platform5;
                        est_revenue_y1 = est_revenue_y15;
                        est_revenue_y2 = est_revenue_y25;
                        est_revenue_y3 = est_revenue_y35;
                        est_revenue_y4 = est_revenue_y45;
                        est_revenue_y5 = est_revenue_y55;
                        est_qty_y1 = est_qty_y15;
                        est_qty_y2 = est_qty_y25;
                        est_qty_y3 = est_qty_y35;
                        est_qty_y4 = est_qty_y45;
                        est_qty_y5 = est_qty_y55;
                        market_price = market_price5;
                        due_date = due_date5;
                        other_sources = other_sources5;
                        priority_dyn = priority5;
                        material_type_dyn = material_type5;
                    }
                    //else
                    //{
                    //    i = 1;
                    //    check_blank_lines = 1;
                    //}

                    //starting insert into line
                    string sqlScriptInsertQuote = "insert into REQUEST_QUOTE_LINES (REQUEST_LINE_ID,CREATION_DATE,REQUEST_ID,SIZE_NUM,OTHER_SOURCE,PLATFORM,MATERIAL_TYPE_ID "
                    + " , EST_REVENUE_Y1, MARKET_PRICE, EST_REVENUE_Y2, EST_REVENUE_Y3, EST_REVENUE_Y4, EST_REVENUE_Y5, EST_REVENUE_Q_Y1, "
                    + " EST_REVENUE_Q_Y2, EST_REVENUE_Q_Y3, EST_REVENUE_Q_Y4, EST_REVENUE_Q_Y5, DUE_DATE,PRIORITY_ID "
                    + " ) VALUES (@request_line_id,convert(date,getdate()),@request_id,@size_num,@other_source,@platform,@material_type_id "
                    + ", @est_revenue_y1, @market_price, @est_revenue_y2, @est_revenue_y3, @est_revenue_y4, @est_revenue_y5, @est_qty_y1 "
                    + " , @est_qty_y2, @est_qty_y3, @est_qty_y4, @est_qty_y5, @due_date,@priority_id);";
                    //command.CommandText = sqlScript;
                    //requested_by_id
                    //command.Parameters.Clear();
                    using (SqlCommand command = new SqlCommand(sqlScriptInsertQuote, dbConnection))
                    {
                        SqlParameter request_id = new SqlParameter();
                        SqlParameter request_line_id_param = new SqlParameter();
                        SqlParameter size_num_val = new SqlParameter();
                        SqlParameter other_source_val = new SqlParameter();
                        SqlParameter platform_val = new SqlParameter();
                        SqlParameter material_type_val = new SqlParameter();
                        SqlParameter market_price_val = new SqlParameter();
                        SqlParameter est_revenue_y1_val = new SqlParameter();
                        SqlParameter est_revenue_y2_val = new SqlParameter();
                        SqlParameter est_revenue_y3_val = new SqlParameter();
                        SqlParameter est_revenue_y4_val = new SqlParameter();
                        SqlParameter est_revenue_y5_val = new SqlParameter();
                        SqlParameter est_revenue_q_y1_val = new SqlParameter();
                        SqlParameter est_revenue_q_y2_val = new SqlParameter();
                        SqlParameter est_revenue_q_y3_val = new SqlParameter();
                        SqlParameter est_revenue_q_y4_val = new SqlParameter();
                        SqlParameter est_revenue_q_y5_val = new SqlParameter();
                        SqlParameter due_date_val = new SqlParameter();
                        SqlParameter priority_val = new SqlParameter();
                        request_id.ParameterName = "@request_id";
                        request_id.Value = request_id_val;
                        command.Parameters.Add(request_id);
                        //request_line_id
                        request_line_id_param.ParameterName = "@request_line_id";
                        request_line_id_param.Value = i;
                        command.Parameters.Add(request_line_id_param);
                        //size num
                        size_num_val.ParameterName = "@size_num";
                        size_num_val.Value = size_num; //String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString();
                        command.Parameters.Add(size_num_val);
                        //other source
                        other_source_val.ParameterName = "@other_source";
                        other_source_val.Value = String.IsNullOrEmpty(other_sources) ? "" : other_sources.ToString();
                        command.Parameters.Add(other_source_val);
                        //platform
                        platform_val.ParameterName = "@platform";
                        platform_val.Value = platform; //String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString();
                        command.Parameters.Add(platform_val);
                        //material type
                        material_type_val.ParameterName = "@material_type_id";
                        material_type_val.Value = material_type_dyn; //String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString();
                        command.Parameters.Add(material_type_val);
                        //market price
                        market_price_val.ParameterName = "@market_price";
                        market_price_val.Value = market_price; //String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString();
                        command.Parameters.Add(market_price_val);
                        //est revenue y1
                        est_revenue_y1_val.ParameterName = "@est_revenue_y1";
                        est_revenue_y1_val.Value = est_revenue_y1; //String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString();
                        command.Parameters.Add(est_revenue_y1_val);
                        //est revenue y2
                        est_revenue_y2_val.ParameterName = "@est_revenue_y2";
                        est_revenue_y2_val.Value = est_revenue_y2; //String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString();
                        command.Parameters.Add(est_revenue_y2_val);
                        //market price
                        est_revenue_y3_val.ParameterName = "@est_revenue_y3";
                        est_revenue_y3_val.Value = est_revenue_y3; //String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString();
                        command.Parameters.Add(est_revenue_y3_val);
                        //market price
                        est_revenue_y4_val.ParameterName = "@est_revenue_y4";
                        est_revenue_y4_val.Value = est_revenue_y4; //String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString();
                        command.Parameters.Add(est_revenue_y4_val);
                        //market price
                        est_revenue_y5_val.ParameterName = "@est_revenue_y5";
                        est_revenue_y5_val.Value = est_revenue_y5; //String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString();
                        command.Parameters.Add(est_revenue_y5_val);
                        //est qty
                        est_revenue_q_y1_val.ParameterName = "@est_qty_y1";
                        est_revenue_q_y1_val.Value = est_qty_y1; //String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString();
                        command.Parameters.Add(est_revenue_q_y1_val);
                        //est qty
                        est_revenue_q_y2_val.ParameterName = "@est_qty_y2";
                        est_revenue_q_y2_val.Value = est_qty_y2; //String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString();
                        command.Parameters.Add(est_revenue_q_y2_val);
                        //est qty
                        est_revenue_q_y3_val.ParameterName = "@est_qty_y3";
                        est_revenue_q_y3_val.Value = est_qty_y3;  //String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString();
                        command.Parameters.Add(est_revenue_q_y3_val);
                        //est qty
                        est_revenue_q_y4_val.ParameterName = "@est_qty_y4";
                        est_revenue_q_y4_val.Value = est_qty_y4; //String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString();
                        command.Parameters.Add(est_revenue_q_y4_val);
                        //est qty
                        est_revenue_q_y5_val.ParameterName = "@est_qty_y5";
                        est_revenue_q_y5_val.Value = est_qty_y5; //String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString();
                        command.Parameters.Add(est_revenue_q_y5_val);
                        //due date
                        due_date_val.ParameterName = "@due_date";
                        due_date_val.Value = due_date; //String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString();
                        command.Parameters.Add(due_date_val);
                        //priority
                        priority_val.ParameterName = "@priority_id";
                        priority_val.Value = priority_dyn; //String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString();
                        command.Parameters.Add(priority_val);


                        try
                        {
                            command.ExecuteNonQuery();
                            //command.Dispose();
                            //   MessageBox.Show("Submitted!");
                        }
                        catch (Exception)
                        {
                            //MessageBox.Show("Failed to submit quote. Please consult your system administrator.");
                            ViewBag.ErrorMsg = "Failed to submit quote. Please consult your system administrator.";
                        }
                    }
                    //cnn.Close();
                }

            }

            return View("~/Views/Home/Index.cshtml");


        }
        [HttpGet]
        public ActionResult PricingTool()
        {

            if (string.IsNullOrEmpty(Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)) == true)
            {
                Response.Write("<script>window.close();</script>");
                return View("~/Views/Home/CreateQuote.cshtml");
            }

            GenerateQuoteHeader gqh = new GenerateQuoteHeader();
            SqlConnection cnn;
            //gqh.LoadingPricingValue = 0;

            gqh.UnitCost = 0;
            gqh.UnitPrice = 0;

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            cnn.Open();
            using (cnn)
            {
                string sqlScript = "select ISNULL(max(quote_id),0)+1 from quote_header ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlDataReader reader;
                reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        gqh.QuoteNum = Convert.ToString(reader.GetInt32(0));
                        gqh.QuoteID = reader.GetInt32(0);
                    }
                    reader.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find the Quote ID.");
                    return View();
                }
                reader.Close();
            }
            cnn.Close();

            OdbcConnection cnn_odbc;
            cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString);
            List<SelectListItem> CostReference = new List<SelectListItem>();
            using (cnn_odbc)
            {
                string query = @"SELECT c.ieledg COST_ID,case when c.IELEDG = 'P1' then 'Frozen Cost' 
                                 when c.IELEDG = '07' then 'Simulated Cost' 
                                 end Cost_Description 
                                 FROM(SELECT DISTINCT(IELEDG) from PRODDTA.F30026 WHERE IELEDG IN('07', 'P1') 
                                 ) c order by c.ieledg desc ";
                OdbcCommand cmd = new OdbcCommand(query, cnn_odbc);
                cnn_odbc.Open();
                using (OdbcDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        CostReference.Add(new SelectListItem
                        {
                            Text = reader["Cost_Description"].ToString(),
                            Value = reader["COST_ID"].ToString(),
                        });
                    }
                }
                //cnn_odbc.Close();
                gqh.Costing = CostReference;


                cnn_odbc.Close();
            }

            var ourModel = new GetPricingInfo
            {
                GenerateQuoteHeader = gqh,
                SalesOrders = null,
                Competitors = null,
                Components = null,
                DemandList = null,
                Comments = null,
                LTAs = null,
                ActualCosts = null,
                QuoteWorkbench = null
            };
            //this is the get of main pricing tool form
            return View(ourModel);

        }


        [HttpGet]
        public ActionResult OriginalPricingTool()
        {
            var Part_Searched = Request.Params["PartNumber"];

            GenerateQuoteHeader gqh = new GenerateQuoteHeader();
            List<Competitor> comps = new List<Competitor>();
            List<LTAPart> lta = new List<LTAPart>();
            List<CustomerPart> cps = new List<CustomerPart>();
            SqlConnection cnn;

            if (string.IsNullOrEmpty(Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)) == true)
            {
                Response.Write("<script>window.close();</script>");
                return View("~/Views/Home/Index.cshtml");
            }
            //GenerateQuoteHeader gqh = new GenerateQuoteHeader();
            //SqlConnection cnn;
            //gqh.LoadingPricingValue = 0;
            gqh.UnitCost = 0;
            gqh.UnitPrice = 0;
      
            
      
            if (Part_Searched != "" || string.IsNullOrEmpty(Part_Searched) == false)
            {
                gqh.PartNumber =Part_Searched;
            }

            try
            {
                OdbcConnection cnn_odbc;
                cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString);
                cnn_odbc.Open();
                cnn_odbc.Dispose();
                cnn_odbc.Close();
            }
            catch
            {
                MessageBox.Show("Could not connect to JDE. Please work with your system administrator.");
                return View();
            }
            //SqlConnection cnn;
            if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
            {
                using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                {
                    cnn.Open();
                    gqh.Costing = this.GetCostYearList(cnn);
                }
            }
            var ourModel = new OriginalPricingTool
            {
                GenerateQuoteHeader = gqh,
                SalesOrders = null,
                Competitors = comps,
                Components = null,
                DemandList = null,
                Comments = null,
                LTAs = null,
              LTAParts = lta,
              CustParts=cps
            };
            return View(ourModel);
        }

       

        [HttpPost]
        public ActionResult _CommentAdd(GetPricingInfo model, UserModel users)
        {

            GenerateQuoteHeader header_model = new GenerateQuoteHeader();

            SqlConnection cnn;
            //string conString = ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            cnn.Open();
            using (cnn)
            {
                //cnn.Open();
                string sqlScript = "select ISNULL(max(quote_id),0)+1 from quote_header ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlDataReader reader;
                reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        header_model.QuoteNum = Convert.ToString(reader.GetInt32(0));
                        header_model.QuoteID = reader.GetInt32(0);
                    }
                    reader.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find the Quote ID.");
                    cnn.Close();
                    cnn.Dispose();
                    return View();
                
            }
                }
            cnn.Close();

            //find the user's id
            int user_id = 0;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            cnn.Open();
            using (cnn)
            {

                string sqlScript = "select user_id from users where user_name=@user_name ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter user_name = new SqlParameter
                {
                    ParameterName = "@user_name",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(user_name);
                SqlDataReader reader;
                reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        user_id = reader.GetInt32(0);
                    }
                    reader.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find the user's ID.");
                    return View();
                }
                reader.Close();
            }
            cnn.Close();

            //submit part comment
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            cnn.Open();
            using (cnn)
            {

                string sqlScript = "insert into comment_header (created_by_user_id, creation_date,last_updated_date,quote_part_link,active_flag) VALUES (@user_id,convert(date,getdate()),convert(date,getdate()),@item_num,1); " +
                    "insert into comment_lines (comment_header_id,comment,created_by_user_id,creation_date,active_flag) values (SCOPE_IDENTITY(),@comment,@user_id,convert(date,getdate()),1);";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter user_id_val = new SqlParameter();
                SqlParameter item_num_val = new SqlParameter();
                SqlParameter comment = new SqlParameter();
                user_id_val.ParameterName = "@user_id";
                user_id_val.Value = user_id; //String.IsNullOrEmpty(customer_name) ? "" : customer_name.ToString();
                command.Parameters.Add(user_id_val);
                //next param
                item_num_val.ParameterName = "@item_num";
                item_num_val.Value = users.PartNumber;
                command.Parameters.Add(item_num_val);
                //next param
                comment.ParameterName = "@comment";
                comment.Value = model.NewComment.NewPartComment;
                command.Parameters.Add(comment);
                try
                {
                    command.ExecuteNonQuery();
                    command.Dispose();
                }

                catch (Exception)
                {
                    MessageBox.Show("Could not insert the new comment.");
                    return View();
                }
            }
            cnn.Close();

            OdbcConnection cnn_odbc;
            cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString);
            List<SelectListItem> CostReference = new List<SelectListItem>();
            using (cnn_odbc)
            {
                string query = @"SELECT c.ieledg COST_ID,case when c.IELEDG = '07' then 'Simulated Cost' 
                                 when c.IELEDG = 'P1' then 'Frozen Cost' 
                                 end Cost_Description 
                                 FROM(SELECT DISTINCT(IELEDG) from PRODDTA.F30026 WHERE IELEDG IN('07', 'P1') 
                                 ) c";
                OdbcCommand cmd = new OdbcCommand(query, cnn_odbc);
                cnn_odbc.Open();
                using (OdbcDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        CostReference.Add(new SelectListItem
                        {
                            Text = reader["Cost_Description"].ToString(),
                            Value = reader["COST_ID"].ToString(),
                        });
                    }
                }
                //cnn_odbc.Close();
                header_model.Costing = CostReference;
                cnn_odbc.Close();
            }

            var ourModel = new GetPricingInfo
            {
                GenerateQuoteHeader = header_model,
                SalesOrders = null,
                Competitors = null,
                Components = null,
                Comments = null,
                LTAs = null,
                ActualCosts = null,
                QuoteWorkbench = null,
                DemandList = null
            };


            return View("~/Views/Home/PricingTool.cshtml", ourModel);

        }


        [HttpGet]
        public ActionResult ConfigureNotification()
        {
            return View("~/Views/Shared/ComingSoon.cshtml");
        }

        [HttpGet]
        public ActionResult AddUser()
        {
            return View();

        }

        [HttpGet]
        public ActionResult AddStrategicPricing()
        {
            return View();

        }

        [HttpPost]
        public ActionResult AddUser(ManageUser model)
        {

            
            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                cnn.Open();

                //find who is loggged in
                int created_by = 0;
                string sqlScript = "select user_id from users where user_name=@username ";
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    command.Parameters.Add(new SqlParameter("@username", Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)));

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                created_by = reader.GetInt32(0);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Could not find who is logged in.");
                            cnn.Close();
                            cnn.Dispose();
                        }
                    }
                }


                sqlScript = @"insert into USERS ([User_name],org_id,[MULTI_LOCATION_FLAG],EMAIL,ROLE_ID
                    ,ACTIVE_FLAG,CREATION_DATE,CREATED_BY,FULL_NAME)
                    values (@username,@org_id,0,@email,@role,1,convert(date,getdate()),@created_by,@username)";
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    SqlParameter user_name = new SqlParameter
                    {
                        ParameterName = "@username",
                        Value = model.Username
                    };
                    command.Parameters.Add(user_name);
                    SqlParameter org_id = new SqlParameter
                    {
                        ParameterName = "@org_id",
                        Value = model.Organization
                    };
                    command.Parameters.Add(org_id);
                    SqlParameter email = new SqlParameter
                    {
                        ParameterName = "@email",
                        Value = model.Email
                    };
                    command.Parameters.Add(email);
                    SqlParameter role = new SqlParameter
                    {
                        ParameterName = "@role",
                        Value = model.Role
                    };
                    command.Parameters.Add(role);
                    SqlParameter created_by_id = new SqlParameter
                    {
                        ParameterName = "@created_by",
                        Value = created_by
                    };
                    command.Parameters.Add(created_by_id);
                    //SqlDataReader reader;
                    //reader = command.ExecuteReader();
                    try
                    {
                        command.ExecuteNonQuery();
                        command.Dispose();
                        MessageBox.Show("User Added!");
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show("Failed to create user.");
                        //return View();
                    }
                }
                //reader.Close();

                //cnn.Close();
                //}

                //return model
                UserList users = new UserList();



                users = this.DbReader<UserList>("select * From vManage_Users;", cnn, (reader) =>
                {

                    var userList = new UserList();

                    while (reader.Read())
                    {
                        userList.Users.Add(new ManageUser()
                        {
                            UserId = reader.GetInt32(0),
                            Username = reader.GetString(1),
                            Email = reader.GetString(2),
                            Organization = reader.GetString(3),
                            Role = reader.GetString(4),
                            ActiveFlag = reader.GetString(5),
                            MultiOrg = reader.GetString(6)
                        });
                    }
                    return userList;
                });

                return View("~/Views/Home/ManageUsers.cshtml", users.Users);

            }

        }

        [HttpPost]
        public ActionResult AddStrategicPricing(StrategicPricingAdjustments model)
        {


            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                cnn.Open();

                //find who is loggged in
                int created_by = 0;
                string sqlScript = "select user_id from users where user_name=@username ";
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    command.Parameters.Add(new SqlParameter("@username", Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)));

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                created_by = reader.GetInt32(0);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Could not find who is logged in.");
                            cnn.Close();
                            cnn.Dispose();
                        }
                    }
                }

                int apply_adj_flag = 0;

                sqlScript = @"insert into [STRATEGIC_PRICING_ADJUSTMENTS] (DIVISION,VALUE_STREAM,PRODUCT_GROUP,PRODUCT_CODE,CUSTOMER_NUMBER
                            ,INTERNAL_PART_NUMBER,CUSTOMER_PART_NUMBER,OTHER,[STATUS],COMMENT,ADJUSTMENT,APPLY_VARIANCE_ADJUSTMENT, CREATED_BY_ID,CREATION_DATE,LAST_EDIT_ID,LAST_EDIT_DATE
                                , quantity_break_range_start, quantity_break_range_end, CUSTOMER_RECENT_PRICE_FLAG
                              , BASE_PRICE_FLAG, BASE_PRICE, RESERVE_FLAG, LEAD_TIME_FLAG,BLACKLIST_FLAG,BLACKLIST_REASONING,MINIMUM_ORDER_QTY_FLAG,MINIMUM_ORDER_QUANTITY)
                            values (@division,@value_Stream,@product_group,@product_code,@customer_number,@internal_part_number,@customer_part_number,@other,@status
                            , @comment, @adjustment, @apply_adj_flag, @created_By_id, getdate(),@created_By_id,getdate(),@quantity_break_range_start, @quantity_break_range_end
                        , @CUSTOMER_RECENT_PRICE_FLAG, @BASE_PRICE_FLAG, @BASE_PRICE, @RESERVE_FLAG, @LEAD_TIME_FLAG,@BLACKLIST_FLAG,@BLACKLIST_REASONING,@moq_flag, @moq)";
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    SqlParameter user_name = new SqlParameter
                    {
                        ParameterName = "@division",
                        Value = model.DIVISION
                    };
                    command.Parameters.Add(user_name);
                    SqlParameter value_stream_val = new SqlParameter
                    {
                        ParameterName = "@value_stream",
                        //Value = model.VALUE_STREAM
                        Value = String.IsNullOrEmpty(model.VALUE_STREAM) ? "" : model.VALUE_STREAM.ToString()
                    };
                    command.Parameters.Add(value_stream_val);
                    SqlParameter product_group_val = new SqlParameter
                    {
                        ParameterName = "@product_group",
                        //Value = model.PRODUCT_GROUP
                        Value = String.IsNullOrEmpty(model.PRODUCT_GROUP) ? "" : model.PRODUCT_GROUP.ToString()
                    };
                    command.Parameters.Add(product_group_val);
                    SqlParameter product_code_val = new SqlParameter
                    {
                        ParameterName = "@product_code",
                        //Value = model.PRODUCT_CODE
                        Value = String.IsNullOrEmpty(model.PRODUCT_CODE) ? "" : model.PRODUCT_CODE.ToString()
                    };
                    command.Parameters.Add(product_code_val);
                    SqlParameter customer_number_val = new SqlParameter
                    {
                        ParameterName = "@customer_number",
                        //Value = model.CUSTOMER_NUMBER
                        Value = String.IsNullOrEmpty(model.CUSTOMER_NUMBER) ? "" : model.CUSTOMER_NUMBER.ToString()
                    };
                    command.Parameters.Add(customer_number_val);
                    SqlParameter internal_part_number_val = new SqlParameter
                    {
                        ParameterName = "@internal_part_number",
                        //Value = model.INTERNAL_PART_NUMBER
                        Value = String.IsNullOrEmpty(model.INTERNAL_PART_NUMBER) ? "" : model.INTERNAL_PART_NUMBER.ToString()
                    };
                    command.Parameters.Add(internal_part_number_val);
                    SqlParameter customer_part_number_val = new SqlParameter
                    {
                        ParameterName = "@customer_part_number",
                        //Value = model.CUSTOMER_PART_NUMBER
                        Value = String.IsNullOrEmpty(model.CUSTOMER_PART_NUMBER) ? "" : model.CUSTOMER_PART_NUMBER.ToString()
                    };
                    command.Parameters.Add(customer_part_number_val);
                    SqlParameter other_val = new SqlParameter
                    {
                        ParameterName = "@other",
                        //Value = model.OTHER
                        Value = String.IsNullOrEmpty(model.OTHER) ? "" : model.OTHER.ToString()
                    };
                    command.Parameters.Add(other_val);
                    SqlParameter status_val = new SqlParameter
                    {
                        ParameterName = "@status",
                        //Value = model.STATUS'
                        Value = String.IsNullOrEmpty(model.STATUS) ? "" : model.STATUS.ToString()
                    };
                    command.Parameters.Add(status_val);
                    SqlParameter comment_Val = new SqlParameter
                    {
                        ParameterName = "@comment",
                        //Value = model.COMMENT
                        Value = String.IsNullOrEmpty(model.COMMENT) ? "" : model.COMMENT.ToString()
                    };
                    command.Parameters.Add(comment_Val);
                    SqlParameter adjustment_val = new SqlParameter
                    {
                        ParameterName = "@adjustment",
                        Value = model.ADJUSTMENT
                        //Value = decimal.IsNullOrEmpty(model.ADJUSTMENT) ? 0 : model.ADJUSTMENT.ToDecimal()
                    };
                    command.Parameters.Add(adjustment_val);
                    SqlParameter apply_variance_adj_val = new SqlParameter
                    {
                        ParameterName = "@apply_adj_flag",
                        //Value = model.APPLY_VARIANCE_ADJUSTMENT
                        Value = apply_adj_flag
                    };
                    command.Parameters.Add(apply_variance_adj_val);
          
          SqlParameter created_by_val = new SqlParameter
                    {
                        ParameterName = "@created_by_id",
                        Value = created_by
                    };
                    command.Parameters.Add(created_by_val);
                    //SqlParameter quantity_adjustment_flag_param = new SqlParameter
                    //{
                    //    ParameterName = "@APPLY_QUANTITY_BREAK_RANGE",
                    //    Value = model.APPLY_QUANTITY_BREAK_RANGE
                    //};
                    //command.Parameters.Add(quantity_adjustment_flag_param);
                    SqlParameter QUANTITY_BREAK_RANGE_START_PARAM = new SqlParameter
                    {
                        ParameterName = "@QUANTITY_BREAK_RANGE_START",
                        Value = model.QUANTITY_BREAK_RANGE_START
                    };
                    command.Parameters.Add(QUANTITY_BREAK_RANGE_START_PARAM);
                    SqlParameter QUANTITY_BREAK_RANGE_END_PARAM = new SqlParameter
                    {
                        ParameterName = "@QUANTITY_BREAK_RANGE_END",
                        Value = model.QUANTITY_BREAK_RANGE_END
                    };
                    command.Parameters.Add(QUANTITY_BREAK_RANGE_END_PARAM);
                    SqlParameter CUSTOMER_RECENT_PRICE_FLAG_param = new SqlParameter
                    {
                        ParameterName = "@CUSTOMER_RECENT_PRICE_FLAG",
                        Value = model.CUSTOMER_RECENT_PRICE_FLAG
                        //decimal.IsNullOrEmpty(model.ADJUSTMENT) ? 0 : model.ADJUSTMENT.ToDecimal()
                    };
                    command.Parameters.Add(CUSTOMER_RECENT_PRICE_FLAG_param);
                    SqlParameter BASE_PRICE_FLAG_param = new SqlParameter
                    {
                        ParameterName = "@BASE_PRICE_FLAG",
                        Value = model.BASE_PRICE_FLAG
                    };
                    command.Parameters.Add(BASE_PRICE_FLAG_param);
                    SqlParameter BASE_PRICE_param = new SqlParameter
                    {
                        ParameterName = "@BASE_PRICE",
                        Value = model.BASE_PRICE
                        //decimal.IsNullOrEmpty(model.ADJUSTMENT) ? 0 : model.ADJUSTMENT.ToDecimal()
                    };
                    command.Parameters.Add(BASE_PRICE_param);
                    SqlParameter reserve_param = new SqlParameter
                    {
                        ParameterName = "@RESERVE_FLAG",
                        Value = model.RESERVE_FLAG
                    };
                    command.Parameters.Add(reserve_param);
          SqlParameter lead_time_flag_param = new SqlParameter
          {
            ParameterName = "@LEAD_TIME_fLAG",
            Value = model.LEAD_TIME_FLAG
          };
          command.Parameters.Add(lead_time_flag_param);
          SqlParameter apply_blacklist_flag_val = new SqlParameter
          {
            ParameterName = "@blacklist_flag",

            Value = model.BLACKLIST_FLAG
          };
          command.Parameters.Add(apply_blacklist_flag_val);
          SqlParameter apply_blacklist_comment_val = new SqlParameter
          {
            ParameterName = "@blacklist_reasoning",

            Value = model.BLACKLIST_REASONING
          };
          command.Parameters.Add(apply_blacklist_comment_val);
          SqlParameter apply_moq_flag_val = new SqlParameter
          {
            ParameterName = "@moq_flag",
            Value = model.MINIMUM_ORDER_QUANTITY_FLAG
          };
          command.Parameters.Add(apply_moq_flag_val);
          SqlParameter apply_moq_val = new SqlParameter
          {
            ParameterName = "@moq",
            Value = model.MINIMUM_ORDER_QUANTITY
          };
          command.Parameters.Add(apply_moq_val);
          try
                    {
                        command.ExecuteNonQuery();
                        command.Dispose();
                        MessageBox.Show("Adjustment added!");
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show("Failed to apply adjustment.");
                        //return View();
                    }
                }

                SpList sp = new SpList();
                sp = this.DbReader<SpList>("select * From tV_Strategic_Pricing_Adjustments;", cnn, (reader) =>
                {

                    var spList = new SpList();

                    while (reader.Read())
                    {
                        spList.StrategicAdjustments.Add(new StrategicPricingAdjustments()
                        {
                            ADJUSTMENT_ID = reader.GetInt32(0),
                            DIVISION = reader.GetString(1),
                            VALUE_STREAM = reader.IsDBNull(2) ? "" : (string)reader.GetValue(2),
                            PRODUCT_GROUP = reader.IsDBNull(3) ? "" : (string)reader.GetValue(3),
                          PRODUCT_CODE = reader.IsDBNull(4) ? "" : (string)reader.GetValue(4),
                          CUSTOMER_NUMBER = reader.IsDBNull(5) ? "" : (string)reader.GetValue(5),
                          INTERNAL_PART_NUMBER = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6),
                            CUSTOMER_PART_NUMBER=reader.IsDBNull(7) ? "" : (string)reader.GetValue(7),
                            QUANTITY_BREAK_RANGE_START = reader.GetInt32(8),
                            QUANTITY_BREAK_RANGE_END = reader.GetInt32(9),
                            OTHER =reader.IsDBNull(10) ? "" : (string)reader.GetValue(10),
                            STATUS =reader.GetString(11),
                            COMMENT=reader.GetString(12),
                            ADJUSTMENT=reader.GetDecimal(13),
                            CreationDate=reader.GetDateTime(14),
                            LastEditDate=reader.GetDateTime(15)
                        });
                    }
                    return spList;
                });

                    return View("~/Views/Home/StrategicPricingAdjustments.cshtml", sp.StrategicAdjustments);

            }

        }

        [HttpPost]
        public ActionResult AddStrategicAdjustment(StrategicPricingAdjustments model)
        {

            SqlConnection cnn;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            string factor_selected;
            var factor_selected_val = Request.Params["ADJUSTMENT_ID"];
            factor_selected = factor_selected_val;

            int charLocation = factor_selected.IndexOf(",", StringComparison.Ordinal);
            if (charLocation > 0)
            {
                factor_selected = factor_selected.Substring(0, charLocation);
            }
            model.ADJUSTMENT_ID = Convert.ToInt32(factor_selected);

            //part_selected =left(part_selected,)

            cnn.Open();
            int user_id = 0;
            using (cnn)
            {
                string sqlScript = "select user_id from users where user_name=@user_name ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter user_name = new SqlParameter
                {
                    ParameterName = "@user_name",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(user_name);
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        user_id = reader.GetInt32(0);
                    }
                    reader.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find the user's ID.");
                    return View();
                }
                reader.Close();
            }
            cnn.Close();

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                //create workflow
                string sqlScript = @"update m SET division=@division,
                                value_stream=@value_stream, product_group=@product_group, customer_number=@customer_number
                                    , internal_part_number=@internal_part_number, customer_part_number=@customer_part_number, other=@other
                                    , status=@status, comment=@comment, adjustment=@adjustment, apply_variance_adjustment=@apply_variance_adjustment
                                    , last_edit_id=@last_edit_id, last_edit_date=@last_edit_date, lead_time_flag=@lead_time_flag, blacklist_flag=@blacklist_flag, blacklist_reasoning=@blacklist_reasoning
                                 from strategic_adjustment_factors m where adjustment_id=@adjustment_id; ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter adjustment_param = new SqlParameter
                {
                    ParameterName = "@adjustment_id",
                    Value = model.ADJUSTMENT_ID
                };
                command.Parameters.Add(adjustment_param);
                SqlParameter division_param = new SqlParameter
                {
                    ParameterName = "@division",
                    Value = model.DIVISION
                };
                command.Parameters.Add(division_param);
                SqlParameter product_group_param = new SqlParameter
                {
                    ParameterName = "@product_group",
                    Value = model.PRODUCT_GROUP
                };
                command.Parameters.Add(product_group_param);
                SqlParameter customer_num_param = new SqlParameter
                {
                    ParameterName = "@customer_number",
                    Value = model.CUSTOMER_NUMBER   
                };
                command.Parameters.Add(customer_num_param);
                SqlParameter internal_pn_param = new SqlParameter
                {
                    ParameterName = "@internal_part_number",
                    Value = model.INTERNAL_PART_NUMBER
                };
                command.Parameters.Add(internal_pn_param);
                SqlParameter customer_pn = new SqlParameter
                {
                    ParameterName = "@customer_part_number",
                    Value = model.CUSTOMER_PART_NUMBER
                };
                command.Parameters.Add(customer_pn);
                SqlParameter other_param = new SqlParameter
                {
                    ParameterName = "@lead_time_flag",
                    Value = model.LEAD_TIME_FLAG
                };
                command.Parameters.Add(other_param);
        SqlParameter blacklist_flag_param = new SqlParameter
        {
          ParameterName = "@blacklist_flag",
          Value = model.BLACKLIST_FLAG
        };
        command.Parameters.Add(blacklist_flag_param);
        SqlParameter blacklist_reasoning_param = new SqlParameter
        {
          ParameterName = "@blacklist_reasoning",
          Value = model.BLACKLIST_REASONING
        };
        command.Parameters.Add(blacklist_reasoning_param);
        SqlParameter status_param = new SqlParameter
                {
                    ParameterName = "@status",
                    Value = model.STATUS
                };
                command.Parameters.Add(status_param);
                SqlParameter comment_param = new SqlParameter
                {
                    ParameterName = "@comment",
                    Value = model.COMMENT
                };
                command.Parameters.Add(comment_param);
                SqlParameter adjustment_amount_param = new SqlParameter
                {
                    ParameterName = "@adjustment",
                    Value = model.ADJUSTMENT
                };
                command.Parameters.Add(adjustment_amount_param);
                SqlParameter apply_variance_param = new SqlParameter
                {
                    ParameterName = "@apply_variance_adjustment",
                    Value = model.APPLY_VARIANCE_ADJUSTMENT
                };
                command.Parameters.Add(apply_variance_param);
                //SqlParameter created_by_param = new SqlParameter
                //{
                //    ParameterName = "@created_by_user_id",
                //    Value = model.CreatedBy
                //};
                //command.Parameters.Add(created_by_param);
                //SqlParameter creation_date_param = new SqlParameter
                //{
                //    ParameterName = "@creation_date",
                //    Value = model.
                //};
                //command.Parameters.Add(creation_date_param);
                SqlParameter last_edit_id = new SqlParameter
                {
                    ParameterName = "@last_edit_id",
                    Value = user_id
                };
                command.Parameters.Add(last_edit_id);
                SqlParameter last_edit_date_param = new SqlParameter
                {
                    ParameterName = "@last_edit_param",
                    Value = model.LastEditDate
                };
                command.Parameters.Add(last_edit_date_param);
                try
                {
                    command.ExecuteNonQuery();
                    command.Dispose();
                    MessageBox.Show("Updated!");

                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to update the cost factor of the part selected.");
                    return View(model);
                }
            }
            cnn.Close();
            return View(model);

        }

        [HttpGet]
        public ActionResult EditUser()
        {
            var user_id_being_edited = Request.Params["UserId"];
            ManageUser mguser = new ManageUser();
            //

            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                cnn.Open();

                //find who is loggged in

                string sqlScript = @"select user_name, email,o.ORG_NAME,r.ROLE_DESC,active_flag,case when multi_location_flag=1 then 'Yes' else 'No' end multi_flag
                                    from users u
                                    left outer join ROLES r on u.ROLE_ID = r.ROLE_ID
                                    left outer join ORGANIZATION o on u.ORG_ID = o.ORG_ID
                                    where[user_id] = @user_id ";
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    command.Parameters.Add(new SqlParameter("@user_id", user_id_being_edited));

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                mguser.Username = reader.GetString(0);
                                mguser.Email = reader.GetString(1);
                                mguser.Organization = reader.GetString(2);
                                mguser.Role = reader.GetString(3);
                                mguser.ActiveFlag = reader.GetString(4);
                                mguser.MultiOrg = reader.GetString(5);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Could not find who is logged in.");
                            cnn.Close();
                            cnn.Dispose();
                        }
                    }
                }
            }


                return View(mguser);
        }

        [HttpGet]
        public ActionResult EditStrategicPricing()
        {
            var adjustment_id = Request.Params["ADJUSTMENT_ID"];
            StrategicPricingAdjustments mguser = new Models.StrategicPricingAdjustments();
            //

            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                cnn.Open();

                //find who is loggged in

                string sqlScript = @"select  division, value_stream, product_group, product_code, customer_number
                                    , internal_part_number, quantity_break_range_start, quantity_Break_range_end, customer_part_number
                                    , other, status, comment, adjustment, apply_variance_adjustment, u.user_name, a.creation_date, u2.user_name, a.last_edit_date, adjustment_id
                                    , customer_recent_price_flag, base_price_flag, base_price, reserve_flag, lead_time_flag, blacklist_flag, blacklist_reasoning, minimum_order_qty_flag, minimum_order_quantity
                                    from STRATEGIC_PRICING_ADJUSTMENTS a left outer join [users] u on u.user_id=a.created_by_id
                                        left outer join [users] u2 on u2.user_id=a.last_edit_id
                                    where adjustment_id=@adjustment_id ";
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    command.Parameters.Add(new SqlParameter("@adjustment_id", adjustment_id));

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                //reader.IsDBNull(4) ? "" : (string)reader.GetValue(4);
                                mguser.DIVISION = reader.GetString(0);
                                mguser.VALUE_STREAM = reader.IsDBNull(1) ? "" : (string)reader.GetValue(1);
                                mguser.PRODUCT_GROUP = reader.IsDBNull(2) ? "" : (string)reader.GetValue(2);
                                mguser.PRODUCT_CODE = reader.IsDBNull(3) ? "" : (string)reader.GetValue(3);
                                mguser.CUSTOMER_NUMBER = reader.IsDBNull(4) ? "" : (string)reader.GetValue(4);
                                mguser.INTERNAL_PART_NUMBER = reader.IsDBNull(5) ? "" : (string)reader.GetValue(5);
                                //mguser.APPLY_QUANTITY_BREAK_RANGE = reader.IsDBNull(6) ? 0 : (int)reader.GetValue(6);
                                mguser.QUANTITY_BREAK_RANGE_START = reader.IsDBNull(6) ? 0 : (int)reader.GetValue(6);
                                mguser.QUANTITY_BREAK_RANGE_END = reader.IsDBNull(7) ? 0 : (int)reader.GetValue(7);
                                mguser.CUSTOMER_PART_NUMBER = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                                mguser.OTHER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                                mguser.STATUS = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                                mguser.COMMENT = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                                mguser.ADJUSTMENT = reader.IsDBNull(12) ? 0 : (decimal)reader.GetValue(12);
                                mguser.APPLY_VARIANCE_ADJUSTMENT = Convert.ToBoolean(reader.GetInt32(13));
                                mguser.CreatedBy = reader.GetString(14);
                                mguser.CreationDate = reader.GetDateTime(15);
                                mguser.LastEditedBy = reader.GetString(16);
                                mguser.LastEditDate = reader.GetDateTime(17);
                                mguser.ADJUSTMENT_ID = reader.GetInt32(18);
                                mguser.CUSTOMER_RECENT_PRICE_FLAG = Convert.ToBoolean(reader.GetInt32(19));
                                mguser.BASE_PRICE_FLAG = Convert.ToBoolean(reader.GetInt32(20));
                                mguser.BASE_PRICE = reader.GetDecimal(21);
                                mguser.RESERVE_FLAG = Convert.ToBoolean(reader.GetInt32(22));
                                mguser.LEAD_TIME_FLAG = Convert.ToBoolean(reader.GetInt32(23));
                                mguser.BLACKLIST_FLAG = Convert.ToBoolean(reader.GetInt32(24));
                                mguser.BLACKLIST_REASONING = reader.GetString(25);
                                mguser.MINIMUM_ORDER_QUANTITY_FLAG = Convert.ToBoolean(reader.GetInt32(26));
                                mguser.MINIMUM_ORDER_QUANTITY = reader.IsDBNull(27) ? 0 : (int)reader.GetValue(27);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Could not find adjustment.");
                            cnn.Close();
                            cnn.Dispose();
                        }
                    }
                }
            }


            return View(mguser);
        }

        [HttpPost]
        public ActionResult InactivateUser(ManageUser model)
        {
            //var user_id_being_edited = Request.Params["UserId"];
            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                cnn.Open();

                string sqlScript = @"update users set active_flag=0 where user_name=@username";
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    SqlParameter username = new SqlParameter
                    {
                        ParameterName = "@username",
                        Value = model.Username
                    };
                    command.Parameters.Add(username);
                    
                    //SqlDataReader reader;
                    //reader = command.ExecuteReader();
                    try
                    {
                        command.ExecuteNonQuery();
                        command.Dispose();
                        //MessageBox.Show("User inactivated!");
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show("Failed to disable user.");
                        //return View();
                    }
                }


                UserList users = new UserList();

            users = this.DbReader<UserList>("select * From vManage_Users;", cnn, (reader) =>
            {

                var userList = new UserList();

                while (reader.Read())
                {
                    userList.Users.Add(new ManageUser()
                    {
                        UserId = reader.GetInt32(0),
                        Username = reader.GetString(1),
                        Email = reader.GetString(2),
                        Organization = reader.GetString(3),
                        Role = reader.GetString(4),
                        ActiveFlag = reader.GetString(5),
                        MultiOrg = reader.GetString(6)
                    });
                }
                return userList;
            });

                //return View("~/Views/Home/ManageUsers.cshtml", users.Users);
                return new HttpStatusCodeResult(HttpStatusCode.OK);

            }
    }

        [HttpPost]
        public ActionResult EditUser(ManageUser model)
        {

            var user_id_being_edited = Request.Params["UserId"];
            int role_id_val;
            int org_id_val;
            int multi_org_val;


            //var user_id_being_edited = Request.Params["UserId"];
            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                cnn.Open();
                
                string sqlScript;

                //find the org, role, etc.
                sqlScript = @"select top 1 role_id, org_id, case when @multi_org='YES' then 1 else 0 end multi_location_flag
from ORGANIZATION, ROLES r
where ORG_NAME=@org_name and r.ROLE_DESC=@role_desc";
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    SqlParameter multi_org = new SqlParameter
                    {
                        ParameterName = "@multi_org",
                        Value = model.MultiOrg
                    };
                    command.Parameters.Add(multi_org);
                    SqlParameter role_desc = new SqlParameter
                    {
                        ParameterName = "@role_desc",
                        Value = model.Role
                    };
                    command.Parameters.Add(role_desc);
                    SqlParameter org_name = new SqlParameter
                    {
                        ParameterName = "@org_name",
                        Value = model.Organization
                    };
                    command.Parameters.Add(org_name);

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                role_id_val = reader.GetInt32(0);
                                org_id_val = reader.GetInt32(1);
                                multi_org_val = reader.GetInt32(2);
                                //
                                model.Org_ID = org_id_val;
                                model.Role_ID = role_id_val;
                                model.multi_org_id = multi_org_val;
                            }
                            
                        }

                        else
                        {
                            MessageBox.Show("Could not identify save values for this user. Please try again or contact your system administrator.");
                            cnn.Close();
                            cnn.Dispose();
                        }
                    }
                }


                // perform update of user

                sqlScript = @"update users set email=@email, org_id=@org_id, role_id=@role_id, multi_location_flag=@multi_org where user_name=@username";
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    SqlParameter username = new SqlParameter
                    {
                        ParameterName = "@username",
                        Value = model.Username
                    };
                    command.Parameters.Add(username);
                    SqlParameter email = new SqlParameter
                    {
                        ParameterName = "@email",
                        Value = model.Email
                    };
                    command.Parameters.Add(email);
                    SqlParameter org_id = new SqlParameter
                    {
                        ParameterName = "@org_id",
                        Value = model.Org_ID
                    };
                    command.Parameters.Add(org_id);
                    SqlParameter role_id = new SqlParameter
                    {
                        ParameterName = "@role_id",
                        Value = model.Role_ID
                    };
                    command.Parameters.Add(role_id);
                    SqlParameter multi_org = new SqlParameter
                    {
                        ParameterName = "@multi_org",
                        Value = model.multi_org_id
                    };
                    command.Parameters.Add(multi_org);
                    //SqlDataReader reader;
                    //reader = command.ExecuteReader();
                    try
                    {
                        command.ExecuteNonQuery();
                        command.Dispose();
                        //MessageBox.Show("User inactivated!");
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show("Failed to disable user.");
                        //return View();
                    }
                }


                UserList users = new UserList();

                users = this.DbReader<UserList>("select * From vManage_Users;", cnn, (reader) =>
                {

                    var userList = new UserList();

                    while (reader.Read())
                    {
                        userList.Users.Add(new ManageUser()
                        {
                            UserId = reader.GetInt32(0),
                            Username = reader.GetString(1),
                            Email = reader.GetString(2),
                            Organization = reader.GetString(3),
                            Role = reader.GetString(4),
                            ActiveFlag = reader.GetString(5),
                            MultiOrg = reader.GetString(6)
                        });
                    }
                    return userList;
                });
                return View("~/Views/Home/ManageUsers.cshtml", users.Users);
            }
        }

        [HttpGet]
        public ActionResult Reports()
        {
            return View("~/Views/Home/Reports.cshtml");
        }

        [HttpGet]
        public ActionResult Configure()
        {

            // MessageBox.Show(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            string username;
            if (string.IsNullOrEmpty(Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)) == true)
            {
                username = Interaction.InputBox("No username found. Please enter a username", "Username Not Found", "", -1, -1);
                if (string.IsNullOrEmpty(Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)) == true)
                {
                    MessageBox.Show("Username not found. Exiting...");
                    Response.Write("<script>window.close();</script>");
                }
            }
            //string what_is_my_username = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1);
            SqlConnection cnn;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            cnn.Open();

            ConfigureMargins config = new ConfigureMargins();

            //List<SelectListItem> Division = new List<SelectListItem>();
            
            using (cnn)
            {
                string sqlScript = @"select convert(int,d.DIVISION) division, u.user_id, r.role_id, r.role_Desc From USERS u 
                                     left outer join organization d on u.org_id = d.org_id 
									 left outer join ROLES r on u.ROLE_ID=r.ROLE_ID
                                     where u.USER_NAME = @username
									 and r.ROLE_ID in ('1','4','7') ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter username_val = new SqlParameter
                {
                    ParameterName = "@username",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(username_val);

                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            config.division_id = reader.GetInt32(0);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Cannot find division name or you are not listed as an administrator and do not have access to modify these settings.");
                        return View("~/Views/Home/Index.cshtml");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cannot find division name or you are not listed as an administrator and do not have access to modify these settings.");
                    return View("~/Views/Home/Index.cshtml");
                }
                finally
                {
                    reader.Close();
                }
            }

            SharePointConnectionUpdated dc = new SharePointConnectionUpdated();
            var config_var = (from m in dc.config_MARGIN
                         select new ConfigureMargins
                         {
                             margin_id = m.MARGIN_ID,
                             division_id=m.DIVISION_ID,
                             division_id_select=m.APPLY_DIVISION_ID,
                             division_id_val=m.DIVISION_ID_APPLIED,
                             value_stream_select=m.APPLY_VALUE_STREAM,
                             value_stream_val=m.VALUE_STREAM,
                             product_group_select=m.APPLY_PRODUCT_GROUP,
                             product_group_val=m.PRODUCT_GROUP,
                             product_code_select=m.APPLY_PRODUCT_CODE,
                             product_code_val=m.PRODUCT_CODE,
                             customer_number_select=m.APPLY_CUSTOMER_NUMBER,
                             customer_number_val=m.CUSTOMER_NUMBER,
                             internal_part_number_select=m.APPLY_INTERNAL_PART_NUMBER,
                             internal_part_number_val=m.INTERNAL_PART_NUMBER,
                             customer_part_number_select=m.APPLY_CUSTOMER_PART_NUMBER,
                             customer_part_number_val=m.CUSTOMER_PART_NUMBER,
                             other_select=m.APPLY_OTHER,
                             other_val=m.OTHER,
                             margin_value=m.MARGIN_VALUE,
                             quantity_break_range_start=m.QUANTITY_BREAK_RANGE_START,
                             quantity_break_range_finish=m.QUANTITY_BREAK_RANGE_END
                         }).ToList();

            cnn.Close();
            return View(config_var);

        }

        [HttpGet]
        public ActionResult AddProductCode()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddMargin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddMargin(ConfigureMargins model)
        {
            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                cnn.Open();

                string org_val;
                string org_name_val = "";
                int org_id=1;
                //find who is loggged in
                int created_by = 0;
                string sqlScript = "select u.user_id, o.org_name, o.org_id from users u left outer join organization o on u.org_id=o.org_id where u.user_name=@username ";
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    command.Parameters.Add(new SqlParameter("@username", Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)));

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                created_by = reader.GetInt32(0);
                                org_val = reader.GetString(1);
                                org_id = reader.GetInt32(2);
                                org_name_val = org_val;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Could not find who is logged in.");
                            cnn.Close();
                            cnn.Dispose();
                        }
                    }
                }

                if (org_id !=0)
        {
          model.division_id_select = 1;
        }
                else
        {
          model.division_id_select = 0;
        }
                if (model.value_stream_val!=null)
        {
          model.value_stream_select = 1;
        }
                else
        {
          model.value_stream_select = 0;
        }
                if (model.product_code_val!=null)
        {
          model.product_group_select = 1;
        }
        else { model.product_group_select = 0; }
                if (model.product_group_val!=null)
        {
          model.product_group_select = 1;
        }
                else
        {
          model.product_group_select = 0;
        }
                
                if (model.customer_number_val!=null)
        {
          model.customer_number_select = 1;
        }
                else
        {
          model.customer_number_select = 0;
        }
                if (model.other_val!=null)
        {
          model.other_select = 1;
        }
                else
        {
          model.other_select = 0;
        }
                if (model.internal_part_number_val!=null)
        {
          model.internal_part_number_select = 1;
        }
                else
        {
          model.internal_part_number_select = 0;
        }
        if (model.customer_part_number_val != null)
        {
          model.customer_part_number_select = 1;
        }
        else
        {
          model.customer_part_number_select = 0;
        }

        sqlScript = @"insert into config_MARGIN (LAST_UPDATED_BY, LAST_UPDATED_DATE, DIVISION_ID, APPLY_DIVISION_ID, DIVISION_ID_APPLIED, APPLY_VALUE_STREAM, VALUE_STREAM, APPLY_PRODUCT_GROUP, PRODUCT_GROUP
                                , APPLY_PRODUCT_CODE, PRODUCT_CODE, APPLY_CUSTOMER_NUMBER, CUSTOMER_NUMBER, APPLY_INTERNAL_PART_NUMBER, INTERNAL_PART_NUMBER, APPLY_CUSTOMER_PART_NUMBER, CUSTOMER_PART_NUMBER, 
                            APPLY_OTHER, OTHER, MARGIN_VALUE, QUANTITY_BREAK_RANGE_START, QUANTITY_BREAK_RANGE_END) 
                                VALUES (@LAST_UPDATED_BY, getdate(),@DIVISION_ID,@APPLY_DIVISION_ID, @DIVISION_ID_APPLIED, @APPLY_VALUE_STREAM, @VALUE_STREAM, @APPLY_PRODUCT_GROUP, @PRODUCT_GROUP
                                , @APPLY_PRODUCT_CODE, @PRODUCT_CODE, @APPLY_CUSTOMER_NUMBER, @CUSTOMER_NUMBER, @APPLY_INTERNAL_PART_NUMBER, @INTERNAL_PART_NUMBER, @APPLY_CUSTOMER_PART_NUMBER, @CUSTOMER_PART_NUMBER, 
                            @APPLY_OTHER, @OTHER, @MARGIN_VALUE, @QUANTITY_BREAK_RANGE_START, @QUANTITY_BREAK_RANGE_END);";
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    SqlParameter created_by_Val = new SqlParameter
                    {
                        ParameterName = "@last_updated_by",
                        //Value = model.SpecificationStandard
                        Value = created_by
                    };
                    command.Parameters.Add(created_by_Val);
                    SqlParameter division_id_val = new SqlParameter
                    {
                        ParameterName = "@division_id",
                        //Value = model.Custodian
                        Value = org_id
                    };
                    command.Parameters.Add(division_id_val);
                    SqlParameter apply_division_select_val = new SqlParameter
                    {
                        ParameterName = "@APPLY_DIVISION_ID",
                      //Value = model.Manufacturer
                      Value =model.division_id_select
                     
                    };
                    command.Parameters.Add(apply_division_select_val);
                    SqlParameter division_id_param = new SqlParameter
                    {
                        ParameterName = "@DIVISION_ID_APPLIED",
                        //Value = model.Manufacturer
                        Value = model.division_id_val
                        
                    };
                    command.Parameters.Add(division_id_param);
                    SqlParameter apply_value_stream_param = new SqlParameter
                    {
                        ParameterName = "@apply_value_stream",
                        //Value = model.Size
                        Value = model.value_stream_select
                    };
                    command.Parameters.Add(apply_value_stream_param);
          SqlParameter apply_value_stream_val_param = new SqlParameter
          {
            ParameterName = "@value_stream",
            //Value = model.Size
            //Value = model.value_stream_val
            Value = String.IsNullOrEmpty(model.value_stream_val) ? "" : model.value_stream_val.ToString()
          };
          command.Parameters.Add(apply_value_stream_val_param);
          SqlParameter apply_product_code_param = new SqlParameter
          {
            ParameterName = "@apply_product_code",
            //Value = model.Size
            Value = model.product_code_select
          };
          command.Parameters.Add(apply_product_code_param);
          SqlParameter apply_product_code_val_param = new SqlParameter
          {
            ParameterName = "@product_code",
            //Value = model.Size
            //Value = model.product_code_val
            Value = String.IsNullOrEmpty(model.product_code_val) ? "" : model.product_code_val.ToString()
          };
          command.Parameters.Add(apply_product_code_val_param);
          SqlParameter apply_product_group_param = new SqlParameter
          {
            ParameterName = "@apply_product_group",
            //Value = model.Size
            Value = model.product_code_select
          };
          command.Parameters.Add(apply_product_group_param);
          SqlParameter apply_product_group_val_param = new SqlParameter
          {
            ParameterName = "@product_group",
            //Value = model.Size
            //Value = model.product_group_val
            Value = String.IsNullOrEmpty(model.product_group_val) ? "" : model.product_group_val.ToString()
          };
          command.Parameters.Add(apply_product_group_val_param);
          SqlParameter apply_customer_number = new SqlParameter
          {
            ParameterName = "@apply_customer_number",
            //Value = model.Size
            Value = model.customer_number_select
          };
          command.Parameters.Add(apply_customer_number);
          SqlParameter customer_number_val = new SqlParameter
          {
            ParameterName = "@customer_number",
            Value = String.IsNullOrEmpty(model.customer_number_val) ? "" : model.customer_number_val.ToString()
          };
          command.Parameters.Add(customer_number_val);
          SqlParameter apply_internal_part_number = new SqlParameter
          {
            ParameterName = "@apply_internal_part_number",
            //Value = model.Size
            Value = model.internal_part_number_select
          };
          command.Parameters.Add(apply_internal_part_number);
          SqlParameter apply_internal_part_num_val = new SqlParameter
          {
            ParameterName = "@internal_part_number",
            Value = String.IsNullOrEmpty(model.internal_part_number_val) ? "" : model.internal_part_number_val.ToString()
          };
          command.Parameters.Add(apply_internal_part_num_val);
          SqlParameter apply_customer_part_number = new SqlParameter
          {
            ParameterName = "@apply_customer_part_number",
            //Value = model.Size
            Value = model.customer_part_number_select
          };
          command.Parameters.Add(apply_customer_part_number);
          SqlParameter customer_part_number = new SqlParameter
          {
            ParameterName = "@customer_part_number",
            //Value = model.Size
            Value = String.IsNullOrEmpty(model.customer_part_number_val) ? "" : model.customer_part_number_val.ToString()//model.customer_part_number_val
          };
          command.Parameters.Add(customer_part_number);
          SqlParameter apply_other = new SqlParameter
          {
            ParameterName = "@apply_other",
            Value = model.other_select
            
          };
          command.Parameters.Add(apply_other);
          SqlParameter other_param = new SqlParameter
          {
            ParameterName = "@other",
            Value = String.IsNullOrEmpty(model.other_val) ? "" : model.other_val.ToString()
          };
          command.Parameters.Add(other_param);
          SqlParameter quantity_break_start = new SqlParameter
          {
            ParameterName = "@quantity_break_range_start",
            Value = model.quantity_break_range_start
          };
          command.Parameters.Add(quantity_break_start);
          SqlParameter quantity_break_end = new SqlParameter
          {
            ParameterName = "@quantity_break_range_end",
            Value = model.quantity_break_range_finish
          };
          command.Parameters.Add(quantity_break_end);
          SqlParameter margin_value_param = new SqlParameter
          {
            ParameterName = "@margin_value",
            Value = model.margin_value
          };
          command.Parameters.Add(margin_value_param);
                    try
                    {
                        command.ExecuteNonQuery();
                        command.Dispose();
                        MessageBox.Show("Configuration added!");
                    }

                    catch (Exception ex)
                    {
                        cnn.Close();
                        cnn.Dispose();

                        MessageBox.Show("Failed to configure margin. Please see your system administrator.");

                        //return View();
                    }
                    //cnn.close();
                }

            }

            SharePointConnectionUpdated dc = new SharePointConnectionUpdated();
            var config = (from m in dc.config_MARGIN
                                  select new ConfigureMargins
                                  {
                                      margin_id = m.MARGIN_ID,
                                      division_id_val = m.APPLY_DIVISION_ID,
                                      value_stream_val = m.VALUE_STREAM,
                                      product_group_val = m.PRODUCT_GROUP,
                                      product_code_val = m.PRODUCT_CODE,
                                      customer_number_val = m.CUSTOMER_NUMBER,
                                      internal_part_number_val = m.INTERNAL_PART_NUMBER,
                                      customer_part_number_val = m.CUSTOMER_PART_NUMBER,
                                      other_val = m.OTHER,
                                      margin_value = m.MARGIN_VALUE,
                                      quantity_break_range_start = m.QUANTITY_BREAK_RANGE_START,
                                      quantity_break_range_finish = m.QUANTITY_BREAK_RANGE_END
                                  }).ToList();
            return View("Configure", config);
        }

        [HttpPost]
        public ActionResult AddProductCode(QualifiedParts model)
        {

            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                cnn.Open();

                string org_val;
                string org_name_val = "";
                //find who is loggged in
                int created_by = 0;
                string sqlScript = "select u.user_id, o.org_name from users u left outer join organization o on u.org_id=o.org_id where u.user_name=@username ";
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    command.Parameters.Add(new SqlParameter("@username", Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)));

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                created_by = reader.GetInt32(0);
                                org_val = reader.GetString(1);
                                org_name_val = org_val;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Could not find who is logged in.");
                            cnn.Close();
                            cnn.Dispose();
                        }
                    }
                }


                sqlScript = @"insert into APPROVED_PARTS (SPECIFICATION_STANDARD, CUSTODIAN_PART, CUSTODIAN, MANUFACTURER, SIZE, RANGE, FINISH, STATUS, ORGANIZATION
                                , DATE_CREATED, ENTERED_BY, MODIFIED_BY, COMPETITOR_PRICE) VALUES (@specification_standard, @custodian_part, @custodian, @manufacturer, @size, @range, @finish, @status
                                , @organization, getdate(), @entered_by, @entered_by, @competitor_pricing);";
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    SqlParameter spec_std = new SqlParameter
                    {
                        ParameterName = "@specification_standard",
                        //Value = model.SpecificationStandard
                        Value = String.IsNullOrEmpty(model.SpecificationStandard) ? "" : model.SpecificationStandard.ToString()
                    };
                    command.Parameters.Add(spec_std);
                    SqlParameter custodian_part = new SqlParameter
                    {
                        ParameterName = "@custodian_part",
                        //Value = model.CustodianPart
                        Value=String.IsNullOrEmpty(model.CustodianPart) ? "" : model.CustodianPart.ToString() 
                    };
                    command.Parameters.Add(custodian_part);
                    SqlParameter custodian = new SqlParameter
                    {
                        ParameterName = "@custodian",
                        //Value = model.Custodian
                        Value = String.IsNullOrEmpty(model.Custodian) ? "" : model.Custodian.ToString()
                    };
                    command.Parameters.Add(custodian);
                    SqlParameter mfg = new SqlParameter
                    {
                        ParameterName = "@manufacturer",
                        //Value = model.Manufacturer
                        Value = String.IsNullOrEmpty(model.Manufacturer) ? "" : model.Manufacturer.ToString()
                    };
                    command.Parameters.Add(mfg);
                    SqlParameter size = new SqlParameter
                    {
                        ParameterName = "@size",
                        //Value = model.Size
                        Value = String.IsNullOrEmpty(model.Size) ? "" : model.Size.ToString()
                    };
                    command.Parameters.Add(size);
                    SqlParameter range = new SqlParameter
                    {
                        ParameterName = "@range",
                        //Value = model.Range
                        Value = String.IsNullOrEmpty(model.Range) ? "" : model.Range.ToString()
                    };
                    command.Parameters.Add(range);
                    SqlParameter finish = new SqlParameter
                    {
                        ParameterName = "@finish",
                        //Value = model.Finish
                        Value = String.IsNullOrEmpty(model.Finish) ? "" : model.Finish.ToString()
                    };
                    command.Parameters.Add(finish);
                    SqlParameter status = new SqlParameter
                    {
                        ParameterName = "@status",
                        //Value = model.Status
                        Value = String.IsNullOrEmpty(model.Status) ? "" : model.Status.ToString()
                    };
                    command.Parameters.Add(status);
                    SqlParameter entered_by = new SqlParameter
                    {
                        ParameterName = "@entered_by",
                        Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                    };
                    command.Parameters.Add(entered_by);
                    SqlParameter org = new SqlParameter
                    {
                        ParameterName = "@organization",
                        Value = org_name_val
                    };
                    command.Parameters.Add(org);
                    SqlParameter comp_price = new SqlParameter
                    {
                        ParameterName = "@competitor_pricing",
                        Value=model.CompetitorPrice
                        //Value = Int32.IsNullOrEmpty(model.CompetitorPrice) ? 0 : model.Status.val()
                        //Value = decimal.IsNullOrEmpty(model.CompetitorPrice) ? 0 : model.CompetitorPrice.ToDecimal()

                    };
                    command.Parameters.Add(comp_price);
                    //SqlDataReader reader;
                    //reader = command.ExecuteReader();
                    try
                    {
                        command.ExecuteNonQuery();
                        command.Dispose();
                        MessageBox.Show("Qualification added!");
                    }

                    catch (Exception ex)
                    {
                        cnn.Close();
                        cnn.Dispose();

                        MessageBox.Show("Failed to add qualification.");
                        
                        //return View();
                    }
                    //cnn.close();
                }
                
            }

            SharePointConnectionUpdated dc = new SharePointConnectionUpdated();
            var approved_parts = (from m in dc.APPROVED_PARTS
                                  select new QualifiedParts
                                  {
                                      Product_Code_ID = m.PRODUCT_CODE_ID,
                                      SpecificationStandard = m.SPECIFICATION_STANDARD,
                                      CustodianPart = m.CUSTODIAN_PART,
                                      Custodian = m.CUSTODIAN,
                                      Manufacturer = m.MANUFACTURER,
                                      MaterialType = m.MATERIAL_TYPE,
                                      Size = m.SIZE,
                                      Range = m.RANGE,
                                      Finish = m.FINISH,
                                      Status = m.STATUS,
                                      Organization = m.ORGANIZATION,
                                      DateCreated = m.DATE_CREATED.Value,
                                      EnteredBy = m.ENTERED_BY,
                                      ModifiedBy = m.MODIFIED_BY,
                                      CompetitorPrice = m.COMPETITOR_PRICE
                                  }).ToList();
            return View("QualifiedPartsListing",approved_parts);

            //return View("QualifiedPartsListing","QualifiedPartsListing");

        }

        //[HttpPost]
        //public ActionResult Configure(ConfigureMargins model)
        //{

        //    SqlConnection cnn;
        //    cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

        //    //fill the role option dropdown
        //    //Configure config = new Configure();

        //    Configure config = new Configure();

        //    List<SelectListItem> Division = new List<SelectListItem>();
        //    using (cnn)
        //    {
        //        string query = "SELECT Division, ORG_ID FROM organization";
        //        using (SqlCommand cmd = new SqlCommand(query, cnn))
        //        {
        //            cmd.Connection = cnn;
        //            cnn.Open();
        //            using (SqlDataReader sdr = cmd.ExecuteReader())
        //            {
        //                try
        //                {
        //                    while (sdr.Read())
        //                    {
        //                        Division.Add(new SelectListItem
        //                        {
        //                            Text = sdr["Division"].ToString(),
        //                            Value = sdr["ORG_ID"].ToString()
        //                        });
        //                    }
        //                }
        //                finally
        //                {
        //                    sdr.Close();
        //                }
        //            }
        //            //cnn.Close();
        //        }
        //    }
        //    //cnn.Close();
        //    config.ConfigureDivisionList = Division;

        //    // decimal margin_changed;
        //    int qty_break;

        //    //margin_changed = config.margin_500;
        //    //update data based on division selected
        //    cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
        //    cnn.Open();
        //    using (cnn)
        //    {
        //        for (int i = 1; i <= 10; i++)
        //        {
        //            if (i == 1)
        //            {
        //                qty_break = 500;
        //            }
        //            else if (i == 2)
        //            {
        //                qty_break = 1000;
        //            }
        //            else if (i == 3)
        //            {
        //                qty_break = 2000;
        //            }
        //            else if (i == 4)
        //            {
        //                qty_break = 2500;
        //            }
        //            else if (i == 5)
        //            {
        //                qty_break = 5000;
        //            }
        //            else if (i == 6)
        //            {
        //                qty_break = 10000;
        //            }
        //            else if (i == 7)
        //            {
        //                qty_break = 20000;
        //            }
        //            else if (i == 8)
        //            {
        //                qty_break = 25000;
        //            }
        //            else if (i == 9)
        //            {
        //                qty_break = 50000;
        //            }
        //            else if (i == 10)
        //            {
        //                qty_break = 100000;
        //            }
        //            else
        //            {
        //                MessageBox.Show("Error finding the quantity break to update. Exiting.");
        //                return View();
        //            }

        //            string sqlScript = @"update m set m.last_updated_by = @user_id, m.last_updated_date = convert(date, getdate()), 
        //              m.division_id = o.org_id, m.margin_value = @margin 
        //              from config_MARGIN m left outer join users u 
        //             on m.last_updated_by = u.user_id 
        //              left outer join organization o on o.org_id = m.division_id 
        //              where 1 = 1 
        //              and m.quantity_break = @qtybreak 
        //              and m.DIVISION_ID = @division_id ";
        //            SqlCommand command = new SqlCommand(sqlScript, cnn);
        //            SqlParameter username_val = new SqlParameter
        //            {
        //                ParameterName = "@username",
        //                Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
        //            };
        //            command.Parameters.Add(username_val);
        //            SqlParameter division_val = new SqlParameter
        //            {
        //                ParameterName = "@division_id",
        //                Value = model.ORG_ID
        //            };
        //            command.Parameters.Add(division_val);
        //            SqlParameter margin_val = new SqlParameter
        //            {
        //                ParameterName = "@margin"
        //            };

        //            if (i == 1)
        //            {
        //                margin_val.Value = model.margin_500;
        //            }
        //            else if (i == 2)
        //            {
        //                margin_val.Value = model.margin_value;
        //            }
        //            else if (i == 3)
        //            {
        //                margin_val.Value = model.margin_2000;
        //            }
        //            else if (i == 4)
        //            {
        //                margin_val.Value = model.margin_2500;
        //            }
        //            else if (i == 5)
        //            {
        //                margin_val.Value = model.margin_5000;
        //            }
        //            else if (i == 6)
        //            {
        //                margin_val.Value = model.margin_10000;
        //            }
        //            else if (i == 7)
        //            {
        //                margin_val.Value = model.margin_20000;
        //            }
        //            else if (i == 8)
        //            {
        //                margin_val.Value = model.margin_25000;
        //            }
        //            else if (i == 9)
        //            {
        //                margin_val.Value = model.margin_50000;
        //            }
        //            else if (i == 10)
        //            {
        //                margin_val.Value = model.margin_100000;
        //            }

        //            command.Parameters.Add(margin_val);
        //            SqlParameter qtybreak_val = new SqlParameter
        //            {
        //                ParameterName = "@qtybreak",
        //                Value = qty_break
        //            };
        //            command.Parameters.Add(qtybreak_val);

        //            SqlParameter user_id_val = new SqlParameter
        //            {
        //                ParameterName = "@user_id",
        //                Value = 1
        //            };
        //            command.Parameters.Add(user_id_val);
        //            try
        //            {
        //                command.ExecuteNonQuery();
        //                command.Dispose();
        //            }
        //            catch (Exception)
        //            {
        //                MessageBox.Show("Failed to update margin information. Please see your system administrator. Your site might not be setup for default margins, for which require a first-time load.");
        //                return View();
        //            }
        //        }
        //        cnn.Close();
        //    }
        //    return View("Index", config);
        //}

        [HttpPost]
        public ActionResult ApprovalGenerate(ApprovalGenerate subm_model, QuoteWorkBench quotewkbc, UserModel users)
        {

            //MessageBox.Show("Preparing to submit quote.");
            SqlConnection cnn;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

      //what is the quote number that is possibly being processed
      //string quote_num = subm_model.QuoteNum;

      //prepare submission
      //int line_num = 0;

      //if (quotewkbc.QCPartNumber != null)
      //{
      //    line_num = 1;
      //}


      int workbench_id = 0;
            //int quote_id = 0;
      string division_id = "";

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            cnn.Open();
            using (cnn)
            {
        string sqlScript = @"select u.user_id, h.workbench_id, 
                          o.DIVISION
                          from users u left outer join workbench_header h 
                          on u.user_id=h.created_by_user_id
                          left outer join ORGANIZATION o on u.org_id=o.ORG_ID
                           where u.user_name=@username;";
                //, qh.quote_id
                // left outer join quote_header qh on h.workbench_id=qh.workflow_id
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter username_val = new SqlParameter
                {
                    ParameterName = "@username",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(username_val);
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        users.user_id = reader.GetInt32(0);
                        workbench_id = reader.GetInt32(1);
            division_id = reader.GetString(2);
                        //quote_id = reader.GetInt32(3);
          }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Could not find user. Exiting...");
                    return View();
                }
                reader.Close();
            }
            cnn.Close();


            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            cnn.Open();

            using (cnn)
            {

        string sqlScript = "";
        if (division_id == "720000")
        {
          sqlScript = @"DECLARE @wkbchID int insert into workflow(status_id, last_updated, last_updated_by, assigned_to_role_id, created_by_user_id, creation_date, workflow_type_id) 
                                     values (15, convert(date,getdate()),@user_id,2,@user_id,convert(date,getdate()),2); 
                 insert into QUOTE_HEADER (QUOTE_NUM, REQUESTED_BY_ID, CREATION_DATE, WORKFLOW_ID, SHIP_TO_NUM, SHIP_TO_NAME, SOLD_TO_NUM, SOLD_TO_NAME, COMMENT,LEAD_TIME_COMMENT
                 ,STATUS_ID, QUOTE_STATUS_ID, CUSTOMER_PO, EXTENDED_PRICE, EXTENDED_COST, DUE_DATE) 
                 select (convert(varchar,format(getdate(),'yyMMddHHmmss'))), wh.CREATED_BY_USER_ID, wh.CREATION_DATE, SCOPE_IDENTITY(), wh.ship_to, wh.ship_to_name, wh.sold_to, wh.sold_to_name, wh.COMMENT,wh.lta_Comment, 1,15,wh.customer_po 
                , sum(l.unit_PRICE*l.quantity) extended_price ,sum(l.unit_cost*l.quantity) extended_cost, wh.DUE_DATE
                From workbench_header wh  join WORKBENCH_LINES l on wh.workbench_id=l.WORKBENCH_ID
                 where 1 = 1 
                 and wh.created_by_user_id = @user_id
              group by wh.CREATED_BY_USER_ID, wh.CREATION_DATE, wh.ship_to, wh.ship_to_name, wh.sold_to, wh.sold_to_name, wh.COMMENT,wh.lta_Comment,wh.customer_po, wh.due_date
                SET @wkbchID = SCOPE_IDENTITY() ; 
                 insert into quote_lines (QUOTE_HEADER_ID, QUOTE_LINE_NUM, PART_NUMBER, CUST_PART_NUMBER,LEAD_TIME, QUANTITY, UNIT_PRICE, UNIT_PRICE_CALCULATOR, UNIT_COST, PRICING_TOOL_COST, COMMENT, STATUS_ID, QUOTE_STATUS_ID, COST_OVERRIDE, COST_REF) 
                 select SCOPE_IDENTITY(), wl.workbench_line_num, wl.PART_NUMBER, wl.CUST_PART_NUMBER, wl.LEAD_TIME, wl.quantity, wl.unit_price, wl.unit_price_calculator, WL.unit_cost, wl.pricing_tool_cost, wl.comment, 1,15, wl.COST_OVERRIDE, wl.COST_REF
                 FROM WORKBENCH_LINES wl 
                 left outer join WORKBENCH_HEADER wh on wh.WORKBENCH_ID = wl.WORKBENCH_ID 
                 where 1 = 1 
                 and wh.CREATED_BY_USER_ID = @user_id 
                 and wl.Part_number is not null; 
                 delete from workbench_header where created_by_user_id=@user_id;
                 delete from workbench_lines where workbench_id=@workbench_id; ";
        }
        else
        {
          sqlScript = @"DECLARE @wkbchID int insert into workflow(status_id, last_updated, last_updated_by, assigned_to_role_id, created_by_user_id, creation_date, workflow_type_id) 
                                     values (18, convert(date,getdate()),@user_id,2,@user_id,convert(date,getdate()),2); 
                 insert into QUOTE_HEADER (QUOTE_NUM, REQUESTED_BY_ID, CREATION_DATE, WORKFLOW_ID, SHIP_TO_NUM, SHIP_TO_NAME, SOLD_TO_NUM, SOLD_TO_NAME, COMMENT,LEAD_TIME_COMMENT
                 ,STATUS_ID, QUOTE_STATUS_ID, CUSTOMER_PO, EXTENDED_PRICE, EXTENDED_COST, DUE_DATE) 
                 select (convert(varchar,format(getdate(),'yyMMddHHmmss'))), wh.CREATED_BY_USER_ID, wh.CREATION_DATE, SCOPE_IDENTITY(), wh.ship_to, wh.ship_to_name, wh.sold_to, wh.sold_to_name, wh.COMMENT,wh.lta_Comment, 1,17,wh.customer_po 
                , sum(l.unit_PRICE*l.quantity) extended_price ,sum(l.unit_cost*l.quantity) extended_cost, wh.DUE_DATE
                From workbench_header wh  join WORKBENCH_LINES l on wh.workbench_id=l.WORKBENCH_ID
                 where 1 = 1 
                 and wh.created_by_user_id = @user_id
              group by wh.CREATED_BY_USER_ID, wh.CREATION_DATE, wh.ship_to, wh.ship_to_name, wh.sold_to, wh.sold_to_name, wh.COMMENT,wh.lta_Comment,wh.customer_po, wh.due_date
                SET @wkbchID = SCOPE_IDENTITY() ; 
                 insert into quote_lines (QUOTE_HEADER_ID, QUOTE_LINE_NUM, PART_NUMBER, CUST_PART_NUMBER,LEAD_TIME, QUANTITY, UNIT_PRICE,UNIT_PRICE_CALCULATOR, UNIT_COST, PRICING_TOOL_COST, COMMENT, STATUS_ID, QUOTE_STATUS_ID, COST_OVERRIDE, COST_REF) 
                 select SCOPE_IDENTITY(), wl.workbench_line_num, wl.PART_NUMBER, wl.CUST_PART_NUMBER, wl.LEAD_TIME, wl.quantity, wl.unit_price, wl.unit_price_calculator, WL.unit_cost,WL.pricing_tool_cost, wl.comment, 1,17, wl.COST_OVERRIDE, WL.COST_REF
                 FROM WORKBENCH_LINES wl 
                 left outer join WORKBENCH_HEADER wh on wh.WORKBENCH_ID = wl.WORKBENCH_ID 
                 where 1 = 1 
                 and wh.CREATED_BY_USER_ID = @user_id 
                 and wl.Part_number is not null; 
                 delete from workbench_header where created_by_user_id=@user_id;
                 delete from workbench_lines where workbench_id=@workbench_id; ";
        }
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter user_id_val = new SqlParameter
                {
                    ParameterName = "@user_id",
                    Value = users.user_id
                };
                command.Parameters.Add(user_id_val);
                //SqlParameter quote_num_val = new SqlParameter
                //{
                //    ParameterName = "@quote_num",
                //    Value = quote_num
                //};
                //command.Parameters.Add(quote_num_val);
                SqlParameter workbench_id_val = new SqlParameter
                {
                    ParameterName = "@workbench_id",
                    Value = workbench_id
                };
                command.Parameters.Add(workbench_id_val);

                try
                {
                    command.ExecuteNonQuery();
                    command.Dispose();
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Failed to route quote for approval. Does data exist in your workbench? Please contact your system administrator for help.");
                }
                cnn.Close();
            }


            //find the workflow id that was just created
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            Int32 workflow_id;
            workflow_id = 0;
            cnn.Open();
            using (cnn)
            {
                string sqlScript = "select convert(int,max(workflow_id)) workflow_id from workflow where last_updated_by=@user_id;";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter user_id_val = new SqlParameter
                {
                    ParameterName = "@user_id",
                    Value = users.user_id
                };
                command.Parameters.Add(user_id_val);
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        workflow_id = reader.GetInt32(0);
                    }
                    reader.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find user. Exiting...");
                    return View();
                }
                reader.Close();
            }
            cnn.Close();

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            //UPDATE WORKFLOW HISTORY
            cnn.Open();
      //create workflow
      //string sqlScript = "";
            using (cnn)
            {
        
          string sqlScript = @"insert into workflow_history (WORKFLOW_ID,STATUS_ID, LAST_UPDATED, LAST_UPDATED_BY, ASSIGNED_TO_ROLE_ID, CREATED_BY_USER_ID, CREATION_DATE, WORKFLOW_TYPE_ID) 
                     values(@workflow_id,1 
                     , CONVERT(DATE, GETDATE()) 
                     , @user_id, 2, @user_id, convert(date,getdate()), 2 
                     ); ";
        
        
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter user_id_val = new SqlParameter
                {
                    ParameterName = "@user_id",
                    Value = users.user_id
                };
                command.Parameters.Add(user_id_val);
                SqlParameter workflow_id_val = new SqlParameter
                {
                    ParameterName = "@workflow_id",
                    Value = workflow_id
                };
                command.Parameters.Add(workflow_id_val);

                try
                {
                    command.ExecuteNonQuery();
                    command.Dispose();

                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to update workflow history information.");
                }
            }

            //go get the approval date on the quote_header

            //using (cnn)
            //{
            //    DateTime approval_date;

            //    string sqlScript = @"select isnull(approval_date,convert(date,getdate())) from quote_header where quote_id=@quote_id ; ";


            //    SqlCommand command = new SqlCommand(sqlScript, cnn);
            //    //SqlParameter user_id_val = new SqlParameter
            //    //{
            //    //    ParameterName = "@user_id",
            //    //    Value = users.user_id
            //    //};
            //    //command.Parameters.Add(user_id_val);
            //    SqlParameter workflow_id_val = new SqlParameter
            //    {
            //        ParameterName = "@quote_id",
            //        Value = quote_id
            //    };
            //    command.Parameters.Add(workflow_id_val);

            //    SqlDataReader reader = command.ExecuteReader();
            //    try
            //    {
            //        while (reader.Read())
            //        {
            //            approval_date = reader.GetDateTime(0);
            //        }
            //        reader.Close();
                
            //    }
            //    catch (Exception)
            //    {
            //        MessageBox.Show("Failed to update workflow history information.");
            //        return View("~/Home/Index.cshtml");
            //    }
            //    finally
            //    {
            //        cnn.Close();
            //        cnn.Dispose();

            //    }
            //}

            //cnn.Close();

            List<QuoteListItem> wf = new List<QuoteListItem>();

            if (division_id == "730000")
            {
                var dir = @"\\10.100.0.248\StrategicPricing\Bristol\SQORDERS.csv";

                if (!System.IO.File.Exists(dir))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Sold To,Ship To,Qty Ordered,Part Number,Request Date,Sched Pick Date,Customer PO,Unit Price");
                    foreach (QuoteListItem li in wf)
                        //sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + li.approval_date.ToString("MM/dd/yyyy") + "," + li.approval_date.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
                        sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
                    {
                        var string_with_your_data = sb.ToString();
                        var byteArray = Encoding.ASCII.GetBytes(string_with_your_data);
                        var stream = new MemoryStream(byteArray);
                        System.IO.File.WriteAllBytes(@"\\10.100.0.248\StrategicPricing\Bristol\SQORDERS.csv", stream.ToArray());
                    }
                }
                    else
                {

                    using (var fs = new FileStream(@"\\10.100.0.248\StrategicPricing\Bristol\SQORDERS.csv", FileMode.Append, FileAccess.Write))
                    {
                        TextWriter tw = new StreamWriter(fs);
                        //tw.Write("blabla");
                        tw.Flush();
                        StringBuilder sb = new StringBuilder();
                        foreach (QuoteListItem li in wf)
                            sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
                        //sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + li.approval_date.ToString("MM/dd/yyyy") + "," + li.approval_date.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
                        {
                            var string_with_your_data = sb.ToString();
                            tw.Write(string_with_your_data);
                        }
                        tw.Flush();
                    }
                }

            }

      

      else if (division_id == "720000")
      {
                      cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
      //UPDATE WORKFLOW HISTORY
      cnn.Open();

        //List<QuoteListItem> wf = new List<QuoteListItem>();
        using (cnn)
        {
          string sqlScript = @"select o.DIVISION, h.workflow_id, h.customer_po, l.QUOTE_LINE_NUM, l.quantity, l.part_number, l.unit_price, h.SOLD_TO_NUM, h.SHIP_TO_NUM
                                        From quote_header h
										left outer join QUOTE_LINES l on h.quote_id=l.QUOTE_HEADER_ID
										left outer join WORKFLOW w on h.WORKFLOW_ID=w.WORKFLOW_ID
										left outer join [users] u on u.[USER_ID]=w.CREATED_BY_USER_ID
										left outer join ORGANIZATION o on u.ORG_ID=o.ORG_ID
                                        where 1 = 1 and h.requested_by_id=@created_by_user_id and quote_id=(select max(quote_id) quote_id from QUOTE_HEADER where requested_by_id=@created_by_user_id); ";
                    //, h.approval_date
                    SqlCommand command = new SqlCommand(sqlScript, cnn);
          SqlParameter created_by_user_id_val = new SqlParameter
          {
            ParameterName = "@created_by_user_id",
            Value = users.user_id
          };
          command.Parameters.Add(created_by_user_id_val);

          SqlDataReader reader = command.ExecuteReader();
          if (reader.HasRows)
          {
            while (reader.Read())
            {
              QuoteListItem item = new QuoteListItem()
              {
                division_id = reader.GetString(0),
                workflow_id = reader.GetInt32(1),
                customer_po = reader.GetString(2),
                line_num = reader.GetInt32(3),
                quantity = reader.GetInt32(4),
                part_number = reader.GetString(5),
                unit_price = reader.GetDecimal(6),
                sold_to_num = reader.GetString(7),
                ship_to_num = reader.GetString(8)
                //approval_date=reader.GetDateTime(9)
              };
              wf.Add(item);
            }
            reader.Close();
          }

          else
          {
            MessageBox.Show("Failed to approve quote and integrate with JDE. Please see your system administrator.");
            return View("~/Home/Index.cshtml");
          }

        }
        cnn.Close();


        var dir = @"\\10.100.0.248\StrategicPricing\QRP\SQORDERS.csv";

        if (!System.IO.File.Exists(dir))
        {
          StringBuilder sb = new StringBuilder();
          sb.AppendLine("Sold To,Ship To,Qty Ordered,Part Number,Request Date,Sched Pick Date,Customer PO,Unit Price");
          foreach (QuoteListItem li in wf)
            sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
          {
            var string_with_your_data = sb.ToString();
            var byteArray = Encoding.ASCII.GetBytes(string_with_your_data);
            var stream = new MemoryStream(byteArray);
            System.IO.File.WriteAllBytes(@"\\10.100.0.248\StrategicPricing\QRP\SQORDERS.csv", stream.ToArray());
          }
        }

        else
        {

          using (var fs = new FileStream(@"\\10.100.0.248\StrategicPricing\QRP\SQORDERS.csv", FileMode.Append, FileAccess.Write))
          {
            TextWriter tw = new StreamWriter(fs);
            //tw.Write("blabla");
            tw.Flush();
            StringBuilder sb = new StringBuilder();
            foreach (QuoteListItem li in wf)
              sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
            {
              var string_with_your_data = sb.ToString();
              tw.Write(string_with_your_data);
            }
            tw.Flush();
          }
        }


      if (division_id == "710000")
                {
                    cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
                    //UPDATE WORKFLOW HISTORY
                    cnn.Open();

                    dir = @"\\10.100.0.248\StrategicPricing\3V\SQORDERS.csv";

                    if (!System.IO.File.Exists(dir))
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine("Sold To,Ship To,Qty Ordered,Part Number,Request Date,Sched Pick Date,Customer PO,Unit Price");
                        foreach (QuoteListItem li in wf)
                            sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
                        {
                            var string_with_your_data = sb.ToString();
                            var byteArray = Encoding.ASCII.GetBytes(string_with_your_data);
                            var stream = new MemoryStream(byteArray);
                            System.IO.File.WriteAllBytes(@"\\10.100.0.248\StrategicPricing\3V\SQORDERS.csv", stream.ToArray());
                        }
                    }

                    else
                    {

                        using (var fs = new FileStream(@"\\10.100.0.248\StrategicPricing\3V\SQORDERS.csv", FileMode.Append, FileAccess.Write))
                        {
                            TextWriter tw = new StreamWriter(fs);
                            //tw.Write("blabla");
                            tw.Flush();
                            StringBuilder sb = new StringBuilder();
                            foreach (QuoteListItem li in wf)
                                sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
                            {
                                var string_with_your_data = sb.ToString();
                                tw.Write(string_with_your_data);
                            }
                            tw.Flush();
                        }
                    }
                }
                //else if (division_id == "750000")
                //{
                //  var dir = @"\\10.100.0.248\StrategicPricing\Voss\SQORDERS.csv";

                //  if (!System.IO.File.Exists(dir))
                //  {
                //    StringBuilder sb = new StringBuilder();
                //    sb.AppendLine("Sold To,Ship To,Qty Ordered,Part Number,Request Date,Sched Pick Date,Customer PO,Unit Price");
                //    foreach (QuoteListItem li in wf)
                //      sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
                //    {
                //      var string_with_your_data = sb.ToString();
                //      var byteArray = Encoding.ASCII.GetBytes(string_with_your_data);
                //      var stream = new MemoryStream(byteArray);
                //      System.IO.File.WriteAllBytes(@"\\10.100.0.248\StrategicPricing\Voss\SQORDERS.csv", stream.ToArray());
                //    }
                //  }

                //  else
                //  {

                //    using (var fs = new FileStream(@"\\10.100.0.248\StrategicPricing\Voss\SQORDERS.csv", FileMode.Append, FileAccess.Write))
                //    {
                //      TextWriter tw = new StreamWriter(fs);
                //      //tw.Write("blabla");
                //      tw.Flush();
                //      StringBuilder sb = new StringBuilder();
                //      foreach (QuoteListItem li in wf)
                //        sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
                //      {
                //        var string_with_your_data = sb.ToString();
                //        tw.Write(string_with_your_data);
                //      }
                //      tw.Flush();
                //    }
                //  }
            }

      //return View("~/Views/Home/Index.cshtml", subm_model);
      return new HttpStatusCodeResult(HttpStatusCode.OK);

        }

    [HttpPost]
    public ActionResult ClearWorkbench(QuoteWorkBench quotewkbc, UserModel users)
    {

      SqlConnection cnn;
      cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
      
      int workbench_id;
      workbench_id = 0;

      cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
      cnn.Open();
      using (cnn)
      {
        string sqlScript = "select u.user_id, h.workbench_id from users u left outer join workbench_header h on u.user_id=h.created_by_user_id where u.user_name=@username";
        SqlCommand command = new SqlCommand(sqlScript, cnn);
        SqlParameter username_val = new SqlParameter
        {
          ParameterName = "@username",
          Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
        };
        command.Parameters.Add(username_val);
        SqlDataReader reader = command.ExecuteReader();
        try
        {
          while (reader.Read())
          {
            users.user_id = reader.GetInt32(0);
            workbench_id = reader.GetInt32(1);
          }
          reader.Close();
        }
        catch (Exception)
        {
          MessageBox.Show("Could not find user. Exiting...");
          return View();
        }
        reader.Close();
      }
      cnn.Close();


      cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
      cnn.Open();

      using (cnn)
      {
        string sqlScript = @"delete from workbench_header where created_by_user_id=@user_id;
                 delete from workbench_lines where workbench_id=@workbench_id; ";
        SqlCommand command = new SqlCommand(sqlScript, cnn);
        SqlParameter user_id_val = new SqlParameter
        {
          ParameterName = "@user_id",
          Value = users.user_id
        };
        command.Parameters.Add(user_id_val);
        //SqlParameter quote_num_val = new SqlParameter
        //{
        //    ParameterName = "@quote_num",
        //    Value = quote_num
        //};
        //command.Parameters.Add(quote_num_val);
        SqlParameter workbench_id_val = new SqlParameter
        {
          ParameterName = "@workbench_id",
          Value = workbench_id
        };
        command.Parameters.Add(workbench_id_val);

        try
        {
          command.ExecuteNonQuery();
          command.Dispose();
          return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        catch (Exception ex)
        {
          return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
        }
        //finally
        //{
        //  cnn.Close();
          
        //}
      }
      cnn.Close();
      return new HttpStatusCodeResult(HttpStatusCode.OK);



    }

    [HttpPost]
        public ActionResult Parts(GenerateQuoteHeader gqh_model)
        {
            //int material_type_id2;
            //material_type_id2 = 0;
            string division = string.Empty;
            var partList = new List<string>();

            if (gqh_model?.PartNumber == null)
            {
                return Json(partList);
            }

            SqlConnection cnn;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            //cnn.Open();
            //using (cnn)
            //{
            //    string sqlScript = @"select d.division from users u
            //                        left outer join ORGANIZATION d on u.org_id = d.org_id
            //                        where user_name = @username ";
            //    SqlCommand command = new SqlCommand(sqlScript, cnn);
            //    SqlParameter user_param = new SqlParameter
            //    {
            //        ParameterName = "@username",
            //        Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
            //    };
            //    command.Parameters.Add(user_param);

            //    SqlDataReader reader;
            //    reader = command.ExecuteReader();
            //    try
            //    {
            //        while (reader.Read())
            //        {
            //            division = reader.GetString(0);
            //        }
            //        reader.Close();
            //    }
            //    catch (Exception)
            //    {
            //        MessageBox.Show("Could not find the user's information.");
            //        return View();
            //    }
            //    reader.Close();

            //}
            //cnn.Close();

            ////get part listing from JDE
            //OdbcConnection cnn_odbc;
            //cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString);



      //if (division == "CAM")
      //{
      //  division = "('710000','730000','750000','720000','780000')";
      //}

      if (cnn == null || cnn.State == ConnectionState.Closed)
      {
        cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
        cnn.Open();
      }

      //i = 1;
      using (cnn)
            {

        //string sqlScript = @"select top 1 DIVISION, CustPart, Part
        //                                from iT_Sales_PartMaster where CustPart like @CustPart and customernum like @customer_num
        //                                order by CustPart ";
        //SqlCommand command = new SqlCommand(sqlScript, cnn);
        //SqlParameter cust_part = new SqlParameter
        //{
        //  ParameterName = "@CustPart",
        //  Value = gqh_model.CustomerPartNum + '%'
        //};
        //SqlParameter customer_num = new SqlParameter
        //{
        //  ParameterName = "@customer_num",
        //  Value = String.IsNullOrEmpty(gqh_model.CUSTOMER_NUMBER) ? "" : gqh_model.CUSTOMER_NUMBER.ToString() //quotewkbc.QCLineComments; + '%'
        //};



        string sqlScript = @"select distinct Part
                                        from iT_Sales_PartMaster where Part like @Part 
                                        order by Part ";
        SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter part = new SqlParameter
                {
                    ParameterName = "@Part",
                    Value = gqh_model.PartNumber + '%'
                };
        command.Parameters.Add(part);
        //SqlParameter customer_num = new SqlParameter
        //{
        //  ParameterName = "@customer_num",
        //  Value = gqh_model.CUSTOMER_NUMBER + '%'
        //};
        //command.Parameters.Add(part);
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            partList.Add(reader.GetString(0));
                        }
                    }

                }

                catch (Exception ex)
                {
                  //  MessageBox.Show("Could not find parts that matched that criteria.");
                }
                cnn.Close();
                cnn.Dispose();
            }
                return Json(partList);
        }


        [HttpPost]
        public ActionResult CustParts(GenerateQuoteHeader gqh_model)
        {
            //int material_type_id2;
            //material_type_id2 = 0;
            string division = string.Empty;
            var custPartList = new List<string>();

            if (gqh_model?.CustomerPartNum == null)
            {
                return Json(custPartList);
            }

            SqlConnection cnn;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            cnn.Open();
            //using (cnn)
            //{
            //    string sqlScript = @"select d.division from users u
            //                        left outer join ORGANIZATION d on u.org_id = d.org_id
            //                        where user_name = @username ";
            //    SqlCommand command = new SqlCommand(sqlScript, cnn);
            //    SqlParameter user_param = new SqlParameter
            //    {
            //        ParameterName = "@username",
            //        Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
            //    };
            //    command.Parameters.Add(user_param);

            //    SqlDataReader reader;
            //    reader = command.ExecuteReader();
            //    try
            //    {
            //        while (reader.Read())
            //        {
            //            division = reader.GetString(0);
            //        }
            //        reader.Close();
            //    }
            //    catch (Exception)
            //    {
            //        MessageBox.Show("Could not find the user's information.");
            //        return View();
            //    }
            //    reader.Close();

            //}
            ////cnn.Close();

            //if (division == "CAM")
            //{
            //    division = ("'710000','730000','750000','720000'");
            //}

            ////get part listing from JDE
            ////OdbcConnection cnn_odbc;
            ////cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString);

            //if (cnn == null || cnn.State == ConnectionState.Closed)
            //{
            //    cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            //    cnn.Open();
            //}

            //i = 1;
            using (cnn)
            {
                //cnn.Open();
                //string sqlScript = @"select	TRIM(ibmcu) DIVISION, (ibaitm) CustPart, iblitm Part 
                //                        from PRODDTA.f4102 join PRODDTA.f4101 on ibitm = imitm
                //                        where  trim(ibaitm) like ? 
                //                        and iblitm not like '%*OP%'
                //                        and ibstkt<> 'X'
                //                        and iblitm<>''
                //                        and trim(ibmcu) in ('710000','730000','750000','720000')
                //                        order by    iblitm";


                string sqlScript = @"select distinct CustPart
                                        from iT_Sales_PartMaster where CustPart like @CustPart 
                                        order by CustPart ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter cust_part = new SqlParameter
                {
                    ParameterName = "@CustPart",
                    Value = '%' + gqh_model.CustomerPartNum + '%'
            };
        //command.Parameters.Add(cust_part);
        //SqlParameter customer_num = new SqlParameter
        //    {
        //      ParameterName = "@customer_num",
        //      Value = gqh_model.CUSTOMER_NUMBER + '%'
        //};
        command.Parameters.Add(cust_part);
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            custPartList.Add(reader.GetString(0));
                        }
                    }    
                    
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Could not find customer parts that matched that criteria.");
                }
                cnn.Close();
                cnn.Dispose();


            }
            return Json(custPartList);
        }

        [HttpPost]
        public ActionResult CustomerNames(GenerateQuoteHeader gqh_model)
        {
            string division = string.Empty;
            var customerList = new List<string>();
            if (gqh_model?.CustomerName == null)
            {
                return Json(customerList);
            }

            OdbcConnection cnn_odbc;
            cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString);
            cnn_odbc.Open();
            using (cnn_odbc)
            //, so.sdshan ship_to, ca2.abalph ship_to_name 
            {
              string sqlScript = @"SELECT distinct trim(so.sdshan) customer
                                        FROM PRODDTA.f4211 so
                                        left outer join proddta.f0101 ca2 on so.sdshan=ca2.aban8
                                        where trim(so.sdshan) like ? order by TRIM(SO.SDSHAN)";
                OdbcCommand command = new OdbcCommand(sqlScript, cnn_odbc);
                command.Parameters.Add("@customer_number", OdbcType.VarChar).Value = gqh_model.CustomerName + '%';
                try
                {
                    using (OdbcDataReader reader = command.ExecuteReader())
                    {
                          try
                          {
                            while (reader.Read())
                            {
                              //customerList.Add(reader.GetString(0));
                              customerList.Add(Convert.ToString(reader.GetInt32(0)));
                            }
                          }
                          catch (Exception ex)
                          {
                            //not sure what happened
                          }
                          finally
                          {

                            cnn_odbc.Close();
                          }
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find customers that matched that criteria.");
                }


            }
            return Json(customerList);

        }

        [HttpPost]
        public ActionResult CustomerSearchNames(GenerateQuoteHeader gqh_model)
        {
            string division = string.Empty;
            var customerList = new List<string>();
            if (gqh_model?.CustomerName == null)
            {
                return Json(customerList);
            }

            OdbcConnection cnn_odbc;
            cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString);
            cnn_odbc.Open();
            using (cnn_odbc)
            //, so.sdshan ship_to, ca2.abalph ship_to_name 
            {
                string sqlScript = @"SELECT distinct trim(ca2.abalph)||' - '||trim(so.sdan8) customer
                                        FROM PRODDTA.f4211 so
                                        full outer join proddta.f0101 ca2 on so.sdan8=ca2.aban8
                                        where 1=1 and ca2.abalph LIKE trim(?)
                                        AND ca2.abalph is not null and so.sdan8 is not null
                                        order by trim(ca2.abalph)||' - '||trim(so.sdan8)";
                OdbcCommand command = new OdbcCommand(sqlScript, cnn_odbc);
                command.Parameters.Add("@customer_name", OdbcType.VarChar).Value = gqh_model.CustomerName + '%';
                try
                {
                    using (OdbcDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            //customerList.Add(reader.GetString(0));
                            customerList.Add(reader.GetString(0));
                        }

                        cnn_odbc.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Could not find customers that matched that criteria.");
                }


            }
            return Json(customerList);

        }

        [HttpPost]
        public ActionResult QuoteWorkbenchHeaderAdd(QuoteWorkBench QuoteWorkBench, UserModel users, OriginalPricingTool model, GenerateQuoteHeader GenerateQuoteHeader)
        {

            SqlConnection cnn;
            string sqlScript;

            string cust_po_var = GenerateQuoteHeader.CustPO;
            string header_comment_var = GenerateQuoteHeader.HeaderComment;
            string lead_Time_var = GenerateQuoteHeader.LeadTimeComment;
      string due_date_var = GenerateQuoteHeader.DueDate;
            decimal UNIT_COST = 0;
            int cost_override = 0;

            if (model.GenerateQuoteHeader.UnitCost==0)
            {
                UNIT_COST = QuoteWorkBench.QCQuoteUnitCost;
            }
            else if (model.QuoteWorkbench.QCQuoteUnitCost!=0)
            {
                UNIT_COST = QuoteWorkBench.QCQuoteUnitCost;
                cost_override = 1;
            }
            else
            {
                UNIT_COST = model.GenerateQuoteHeader.UnitCost;
            }

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            cnn.Open();
            using (cnn)
            {
                sqlScript = "select user_id from users where user_name=@username ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter username_val = new SqlParameter
                {
                    ParameterName = "@username",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(username_val);
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        users.user_id = reader.GetInt32(0);
                    }
                    reader.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("No user found. Exiting.");
                    return View();
                }
                reader.Close();
            }
            cnn.Close();

            int workbench_exist;
            int i = 0;
            workbench_exist = 0;
      int workbench_line_count = 0;


            //does workbench exist?
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            cnn.Open();
            using (cnn)
            {
                i = 1;
                sqlScript = @"select w.workbench_id, isnull((select count(workbench_id) from workbench_lines l
				            where l.WORKBENCH_ID=w.WORKBENCH_ID),0) workbench_lines
				            From WORKBENCH_HEADER w left outer join users u ON w.created_by_user_id=u.user_id 
			              WHERE u.user_name=@username;";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter username_val = new SqlParameter
                {
                    ParameterName = "@username",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(username_val);
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        workbench_exist = reader.GetInt32(0);
                        workbench_line_count = reader.GetInt32(1);
                    }
                    reader.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("No workbench exists. Creating...");
                    return View();
                }
                reader.Close();
            }
            cnn.Close();


            //how many lines need to be updated?
            int line_num = 0;

            if (QuoteWorkBench.QCPartNumber != null)
            {
                line_num = 1;
            }

      //check value of customer number 
      //      string customer_name_convert = GenerateQuoteHeader.WBCustomerName;
      //      //string customer_number_convert;
      //      string customer_name;
      //      string customer_num;
      //      int customer_name_length = customer_name_convert.Length - 9;
      //      customer_name = customer_name_convert.Substring(0, customer_name_length);
      //GenerateQuoteHeader.ShipTooName = customer_name;
      //      int customer_num_length_start;
      //      customer_num_length_start = customer_name_length + 3;
      //      customer_num = customer_name_convert.Substring(customer_num_length_start, 6);

      //added line because we removed the name of the customer as part of the lookup
      string customer_num = GenerateQuoteHeader.WBCustomerName;

      GenerateQuoteHeader.ShipTooNum = Convert.ToInt32(customer_num);

            string SoldTooNum;
            string SoldTooName;
            OdbcConnection cnn_odbc;
            cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString);
            cnn_odbc.Open();
            using (cnn_odbc)
            //, so.sdshan ship_to, ca2.abalph ship_to_name 
            {
                sqlScript = @"SELECT DISTINCT so.sdan8 sold_to
                        , (ca.abalph) sold_to_name
                        , so.sdshan ship_too
                        , (ca2.abalph) ship_to_name
                    from proddta.f4211 so
                    join proddta.f0101 ca on so.sdan8 = ca.aban8
                    join proddta.f0101 ca2 on so.sdshan = ca2.aban8
                    WHERE 1 = 1
                    and so.sdshan = ? and ca.abalph<>'DO NOT USE' fetch first row only";
                OdbcCommand command = new OdbcCommand(sqlScript, cnn_odbc);
                command.Parameters.Add("@ship_too_num", OdbcType.VarChar).Value = customer_num;
                try
                {
                    using (OdbcDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            SoldTooNum = reader.GetString(0);
                            SoldTooName = reader.GetString(1);
              GenerateQuoteHeader.ShipTooNum = reader.GetInt32(2);
              GenerateQuoteHeader.ShipTooName = reader.GetString(3);
              GenerateQuoteHeader.SoldTooNum = Convert.ToInt32(SoldTooNum);
              GenerateQuoteHeader.SoldTooName = SoldTooName;
                        }

                        cnn_odbc.Close();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find customers that matched that criteria.");
                }
            }
            if (workbench_exist < 1 && line_num > 0)
            {
                cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
                cnn.Open();
                //for (i = 1; i <= line_num; i++)
                //{
                using (cnn)
                {
                    i = 1;
                    sqlScript = @"DECLARE @wkbchID int insert into WORKBENCH_HEADER (created_by_user_id, creation_date, last_updated_date, SOLD_TO, SOLD_TO_NAME, comment, lta_comment, customer_po, SHIP_TO, SHIP_TO_NAME, DUE_DATE) 
                                        values(@user_id, convert(date, getdate()), convert(date, getdate()), @soldto, @soldtoname, @hcomment, @lta_comment, @custpo, @shipto, @shiptoname,convert(date,@due_date)) 
                                     SET @wkbchID = SCOPE_IDENTITY() 
                                        insert into WORKBENCH_LINES (WORKBENCH_ID, WORKBENCH_LINE_NUM, CREATION_dATE, LAST_UPDATED_DATE,  PART_NUMBER, CUST_PART_NUMBER, LEAD_TIME,COMMENT, QUANTITY, UNIT_PRICE, UNIT_PRICE_CALCULATOR ,UNIT_COST,PRICING_TOOL_COST, COST_OVERRIDE) 
                                     VALUES(@wkbchID, 1, CONVERT(DATE, GETDATE()), CONVERT(DATE, GETDATE()), @PART_NUMBER, @CUST_PART_NUMBER,@LEAD_TIME, @COMMENT, @QUANTITY, @UNIT_PRICE,@UNIT_PRICE, @UNIT_COST,@UNIT_COST, @COST_OVERRIDE);";
                    SqlCommand command = new SqlCommand(sqlScript, cnn);
                    SqlParameter user_id_val = new SqlParameter
                    {
                        ParameterName = "@user_id",
                        Value = users.user_id
                    };
                    command.Parameters.Add(user_id_val);
                    SqlParameter cust_no_val = new SqlParameter
                    {
                        ParameterName = "@soldto",
                        Value = GenerateQuoteHeader.SoldTooNum
                    };
                    command.Parameters.Add(cust_no_val);
                    SqlParameter cust_name_val = new SqlParameter
                    {
                        ParameterName = "@soldtoname",
                        Value = (GenerateQuoteHeader.SoldTooName).Trim()
                    };
                    command.Parameters.Add(cust_name_val);
                    SqlParameter ship_to_param = new SqlParameter
                    {
                        ParameterName = "@shipto",
                        Value = GenerateQuoteHeader.ShipTooNum
                    };
                    command.Parameters.Add(ship_to_param);
                    SqlParameter ship_to_name_param = new SqlParameter
                    {
                        ParameterName = "@shiptoname",
                        Value = GenerateQuoteHeader.ShipTooName.Trim()
                    };
                    command.Parameters.Add(ship_to_name_param);
                    SqlParameter hcomment_val = new SqlParameter
                    {
                        ParameterName = "@hcomment",
                      Value = String.IsNullOrEmpty(GenerateQuoteHeader.HeaderComment) ? "" : GenerateQuoteHeader.HeaderComment.ToString() 
                    };
                    command.Parameters.Add(hcomment_val);
                    SqlParameter lta_parm = new SqlParameter
                    {
                        ParameterName = "@lta_comment",
                      Value = String.IsNullOrEmpty(GenerateQuoteHeader.LeadTimeComment) ? "" : GenerateQuoteHeader.LeadTimeComment.ToString()
                    };
                    command.Parameters.Add(lta_parm);
                    SqlParameter cust_po_val = new SqlParameter
                    {
                        ParameterName = "@custpo",
                      Value = String.IsNullOrEmpty(GenerateQuoteHeader.CustPO) ? "" : GenerateQuoteHeader.CustPO.ToString()
                    };
                    command.Parameters.Add(cust_po_val);
          SqlParameter due_date_val = new SqlParameter
          {
            ParameterName = "@due_date",
            Value = GenerateQuoteHeader.DueDate
          };
          command.Parameters.Add(due_date_val);
          //end of header
          //*********************
          SqlParameter part_num_val = new SqlParameter
                    {
                      ParameterName = "@PART_NUMBER",
                      Value = QuoteWorkBench.QCPartNumber
                    };
                    command.Parameters.Add(part_num_val);
                    SqlParameter cust_part_num_val = new SqlParameter
                    {
                      ParameterName = "@CUST_PART_NUMBER",
                      Value = QuoteWorkBench.QCCustPartNumber
                    };
                    command.Parameters.Add(cust_part_num_val);
                    SqlParameter lead_time_val = new SqlParameter
                    {
                      ParameterName = "@LEAD_TIME",
                      Value = String.IsNullOrEmpty(QuoteWorkBench.QCLeadTime) ? "" : QuoteWorkBench.QCLeadTime.ToString() //model.QCLineComments;
                    };
                    command.Parameters.Add(lead_time_val);
                    SqlParameter comment_val = new SqlParameter
                    {
                      ParameterName = "@COMMENT",
                      Value = String.IsNullOrEmpty(QuoteWorkBench.QCLineComments) ? "" : QuoteWorkBench.QCLineComments.ToString() //model.QCLineComments;
                    };
                    command.Parameters.Add(comment_val);
                    SqlParameter quantity_val = new SqlParameter
                    {
                      ParameterName = "@QUANTITY",
                      Value = QuoteWorkBench.QCQuoteQty
                    };
                    command.Parameters.Add(quantity_val);
                    SqlParameter unit_price_val = new SqlParameter
                    {
                      ParameterName = "@UNIT_PRICE",
                      Value = QuoteWorkBench.QCQuoteUnitPrice
                    };
                    command.Parameters.Add(unit_price_val);
                    SqlParameter unit_cost_val = new SqlParameter
                    {
                        ParameterName = "@UNIT_COST",
                        Value = UNIT_COST
          };
                    command.Parameters.Add(unit_cost_val);
                    SqlParameter cost_override_val = new SqlParameter
                    {
                        ParameterName = "@COST_OVERRIDE",
                        Value = cost_override
                    };
                    command.Parameters.Add(cost_override_val);
          try
                    {
                        command.ExecuteNonQuery();
                        command.Dispose();
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show("Could not create workbench as the customer number or customer name may have been left blank. Please try agian or contact your sys admin." + ex.ToString());
                        //return View();
                    }
                    cnn.Close();
                }

            }
           else if (workbench_exist!=0 && line_num > 0)
      {
        cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
        cnn.Open();
        workbench_line_count = workbench_line_count + 1;
        using (cnn)
        {
          i = 1;
          sqlScript = @"  insert into WORKBENCH_LINES (WORKBENCH_ID, WORKBENCH_LINE_NUM, CREATION_dATE, LAST_UPDATED_DATE,  PART_NUMBER, CUST_PART_NUMBER, LEAD_TIME, COMMENT, QUANTITY, UNIT_PRICE,UNIT_PRICE_CALCULATOR,  UNIT_COST, PRICING_TOOL_COST,COST_OVERRIDE) 
                                     VALUES(@workbench_id, @workbench_line_count, CONVERT(DATE, GETDATE()), CONVERT(DATE, GETDATE()), @PART_NUMBER, @CUST_PART_NUMBER,@LEAD_TIME, @COMMENT, @QUANTITY, @UNIT_PRICE, @UNIT_PRICE,@UNIT_COST, @UNIT_COST,@COST_OVERRIDE);";
          SqlCommand command = new SqlCommand(sqlScript, cnn);
          //workbench_exist
          //workbench_line_count
          SqlParameter user_id_val = new SqlParameter
          {
            ParameterName = "@user_id",
            Value = users.user_id
          };
          command.Parameters.Add(user_id_val);
          SqlParameter workbench_line_count_param = new SqlParameter
          {
            ParameterName = "@workbench_line_count",
            Value = workbench_line_count
          };
          command.Parameters.Add(workbench_line_count_param);
          SqlParameter workbench_header_id = new SqlParameter
          {
            ParameterName = "@workbench_id",
            Value = workbench_exist
          };
          command.Parameters.Add(workbench_header_id);
          SqlParameter cust_no_val = new SqlParameter
          {
            ParameterName = "@soldto",
            Value = GenerateQuoteHeader.SoldTooNum
          };
          command.Parameters.Add(cust_no_val);
          SqlParameter cust_name_val = new SqlParameter
          {
            ParameterName = "@soldtoname",
            Value = (GenerateQuoteHeader.SoldTooName).Trim()
          };
          command.Parameters.Add(cust_name_val);
          SqlParameter ship_to_param = new SqlParameter
          {
            ParameterName = "@shipto",
            Value = GenerateQuoteHeader.ShipTooNum
          };
          command.Parameters.Add(ship_to_param);
          SqlParameter ship_to_name_param = new SqlParameter
          {
            ParameterName = "@shiptoname",
            Value = GenerateQuoteHeader.ShipTooName.Trim()
          };
          command.Parameters.Add(ship_to_name_param);
          SqlParameter hcomment_val = new SqlParameter
          {
            ParameterName = "@hcomment",
            Value = String.IsNullOrEmpty(GenerateQuoteHeader.HeaderComment) ? "" : GenerateQuoteHeader.HeaderComment.ToString()
          };
          command.Parameters.Add(hcomment_val);
          SqlParameter lta_parm = new SqlParameter
          {
            ParameterName = "@lta_comment",
            Value = String.IsNullOrEmpty(GenerateQuoteHeader.LeadTimeComment) ? "" : GenerateQuoteHeader.LeadTimeComment.ToString()
          };
          command.Parameters.Add(lta_parm);
          SqlParameter cust_po_val = new SqlParameter
          {
            ParameterName = "@custpo",
            Value = String.IsNullOrEmpty(GenerateQuoteHeader.CustPO) ? "" : GenerateQuoteHeader.CustPO.ToString()
          };
          command.Parameters.Add(cust_po_val);
          SqlParameter due_date_param = new SqlParameter
          {
            ParameterName = "@due_date",
            Value = GenerateQuoteHeader.DueDate
          };
          command.Parameters.Add(due_date_param);
          //end of header
          //*********************
          SqlParameter part_num_val = new SqlParameter
          {
            ParameterName = "@PART_NUMBER",
            Value = QuoteWorkBench.QCPartNumber
          };
          command.Parameters.Add(part_num_val);
          SqlParameter cust_part_num_val = new SqlParameter
          {
            ParameterName = "@CUST_PART_NUMBER",
            Value = QuoteWorkBench.QCCustPartNumber
          };
          command.Parameters.Add(cust_part_num_val);
          SqlParameter line_lead_time_val = new SqlParameter
          {
            ParameterName = "@LEAD_TIME",
            Value = String.IsNullOrEmpty(QuoteWorkBench.QCLeadTime) ? "" : QuoteWorkBench.QCLeadTime.ToString()
          };
          command.Parameters.Add(line_lead_time_val);
          SqlParameter comment_val = new SqlParameter
          {
            ParameterName = "@COMMENT",
            Value = String.IsNullOrEmpty(QuoteWorkBench.QCLineComments) ? "" : QuoteWorkBench.QCLineComments.ToString() //model.QCLineComments;
          };
          command.Parameters.Add(comment_val);
          SqlParameter quantity_val = new SqlParameter
          {
            ParameterName = "@QUANTITY",
            Value = QuoteWorkBench.QCQuoteQty
          };
          command.Parameters.Add(quantity_val);
          SqlParameter unit_price_val = new SqlParameter
          {
            ParameterName = "@UNIT_PRICE",
            Value = QuoteWorkBench.QCQuoteUnitPrice
          };
          command.Parameters.Add(unit_price_val);
          SqlParameter unit_cost_param = new SqlParameter
          {
            ParameterName = "@UNIT_COST",
            Value = UNIT_COST
          };
          command.Parameters.Add(unit_cost_param);
                    SqlParameter cost_override_val = new SqlParameter
                    {
                        ParameterName = "@COST_OVERRIDE",
                        Value = cost_override
                    };
                    command.Parameters.Add(cost_override_val);
                    try
          {
            command.ExecuteNonQuery();
            command.Dispose();
          }

          catch (Exception ex)
          {
            MessageBox.Show("Could not create workbench as the customer number or customer name may have been left blank. Please try agian or contact your sys admin." + ex.ToString());
            //return View();
          }
          cnn.Close();
        }

      }
            else //update the workbench
            {
                //find the workbench that was last updated
                int workbench_id = 0;
                cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
                cnn.Open();
                using (cnn)
                {
                    i = 1;
                    sqlScript = "select workbench_id from workbench_header where created_by_user_id=@user_id; ";
                    SqlCommand command = new SqlCommand(sqlScript, cnn);
                    SqlParameter user_id_val = new SqlParameter
                    {
                        ParameterName = "@user_id",
                        Value = users.user_id
                    };
                    command.Parameters.Add(user_id_val);
                    SqlDataReader reader = command.ExecuteReader();

                    try
                    {
                        while (reader.Read())
                        {
                            workbench_id = reader.GetInt32(0);
                        }
                        reader.Close();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("No workbench exist or query failure. Contact your system administrator.");
                        return View();
                    }
                    reader.Close();
                }
                cnn.Close();
        //updates the existing workbench to retain the new values

        string QCPartNumber;
        string QCCustPartNumber = "";
        string QCLeadTime = "";
        string QCLineComments;
        int QCQuoteQty;
        decimal QCQuoteUnitPrice;
                decimal QCQuoteUnitCost;
        decimal QCExtPrice;
        QCPartNumber = "";
        QCLineComments = "";
        QCQuoteQty = 0;
        QCQuoteUnitPrice = 0;
        QCExtPrice = 0;

        //i = 1;
        cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
        cnn.Open();
        //MessageBox.Show(line_num.ToString());
        if (workbench_exist >= 1)
        {
          using (cnn)
          {
            for (i = 1; i <= line_num; i++)
            {
              if (i == 1)
              {
                QCPartNumber = QuoteWorkBench.QCPartNumber;
                QCCustPartNumber = QuoteWorkBench.QCCustPartNumber;
                QCLeadTime = QuoteWorkBench.QCLeadTime;
                QCLineComments = QuoteWorkBench.QCLineComments;
                QCQuoteQty = QuoteWorkBench.QCQuoteQty;
                QCQuoteUnitPrice = QuoteWorkBench.QCQuoteUnitPrice;
                               QCQuoteUnitCost = QuoteWorkBench.QCQuoteUnitCost;
                                 //= QuoteWorkBench.QCCalcCost;
                 QCExtPrice = QuoteWorkBench.QCQuoteUnitPrice * QuoteWorkBench.QCQuoteQty;

              }
              sqlScript = @"update h set LAST_UPDATED_DATE=convert(date,Getdate()),SOLD_TO=@soldto, SOLD_TO_NAME=@soldtoname, COMMENT=@hcomment 
                             , lta_comment=@lta_comment, customer_po=@custpo, SHIP_TO=@shipto, SHIP_TO_NAME=@shiptoname, DUE_DATE=convert(date,@DUE_DATE) from WORKBENCH_HEADER h 
                             where workbench_id = @workbench_id; 
                             update l set LAST_UPDATED_DATE = convert(date, getdate()), PART_NUMBER = @PART_NUMBER, CUST_PART_NUMBER=@CUST_PART_NUMBER,LEAD_TIME=@LEAD_TIME, COMMENT = @COMMENT, QUANTITY = @QUANTITY, UNIT_PRICE = @UNIT_PRICE 
                                     from WORKBENCH_LINES l 
                                     where workbench_id = @workbench_id and workbench_line_num=@workbench_line_num; ";
              SqlCommand command = new SqlCommand(sqlScript, cnn);
              SqlParameter workbench_id_val = new SqlParameter
              {
                ParameterName = "@workbench_id",
                Value = workbench_id
              };
              command.Parameters.Add(workbench_id_val);
              SqlParameter workbench_line_id_val = new SqlParameter
              {
                ParameterName = "@workbench_line_num",
                Value = i
              };
              command.Parameters.Add(workbench_line_id_val);
              //updatable values
              SqlParameter cust_no_val = new SqlParameter
              {
                ParameterName = "@soldto",
                Value = GenerateQuoteHeader.SoldTooNum
              };
              command.Parameters.Add(cust_no_val);
              SqlParameter cust_name_val = new SqlParameter
              {
                ParameterName = "@soldtoname",
                Value = GenerateQuoteHeader.SoldTooName.Trim()
              };
              command.Parameters.Add(cust_name_val);
              SqlParameter hcomment_val = new SqlParameter
              {
                ParameterName = "@hcomment",
                Value = String.IsNullOrEmpty(GenerateQuoteHeader.HeaderComment) ? "" : GenerateQuoteHeader.HeaderComment.ToString()
              };
              command.Parameters.Add(hcomment_val);
              SqlParameter lta_parm = new SqlParameter
              {
                ParameterName = "@lta_comment",
                Value = String.IsNullOrEmpty(GenerateQuoteHeader.LeadTimeComment) ? "" : GenerateQuoteHeader.LeadTimeComment.ToString()
              };
              command.Parameters.Add(lta_parm);
              SqlParameter cust_po_val = new SqlParameter
              {
                ParameterName = "@custpo",
                Value = String.IsNullOrEmpty(GenerateQuoteHeader.CustPO) ? "" : GenerateQuoteHeader.CustPO.ToString()
              };
              command.Parameters.Add(cust_po_val);
              SqlParameter ship_to_param = new SqlParameter
              {
                ParameterName = "@shipto",
                Value = GenerateQuoteHeader.ShipTooNum
              };
              command.Parameters.Add(ship_to_param);
              SqlParameter ship_to_name_param = new SqlParameter
              {
                ParameterName = "@shiptoname",
                Value = GenerateQuoteHeader.ShipTooName.Trim()
              };
              command.Parameters.Add(ship_to_name_param);
              SqlParameter due_date_param = new SqlParameter
              {
                ParameterName = "@due_date",
                Value = GenerateQuoteHeader.DueDate
              };
              command.Parameters.Add(due_date_param);
              //end of header
              //*********************
              SqlParameter part_num_val = new SqlParameter
              {
                ParameterName = "@PART_NUMBER",
                Value = QCPartNumber
              };
              command.Parameters.Add(part_num_val);
              SqlParameter cust_part_num_val = new SqlParameter
              {
                ParameterName = "@CUST_PART_NUMBER",
                Value = QCCustPartNumber
              };
              command.Parameters.Add(cust_part_num_val);
              SqlParameter lead_time_val = new SqlParameter
              {
                ParameterName = "@LEAD_TIME",
                Value = String.IsNullOrEmpty(QCLeadTime) ? "" : QCLeadTime.ToString()
              };
              command.Parameters.Add(lead_time_val);
              SqlParameter comment_val = new SqlParameter
              {
                ParameterName = "@COMMENT",
                Value = String.IsNullOrEmpty(QCLineComments) ? "" : QCLineComments.ToString() //QuoteWorkBench.QCLineComments;
              };
              command.Parameters.Add(comment_val);
              SqlParameter quantity_val = new SqlParameter
              {
                ParameterName = "@QUANTITY",
                Value = QCQuoteQty
              };
              command.Parameters.Add(quantity_val);
              SqlParameter unit_price_val = new SqlParameter
              {
                ParameterName = "@UNIT_PRICE",
                Value = QCQuoteUnitPrice
              };
              command.Parameters.Add(unit_price_val);
              try
              {
                command.ExecuteNonQuery();
                command.Dispose();
              }
              catch (Exception)
              {
                MessageBox.Show("Failed to find and update the applicable quote workbench.");
                return View();
              }

            }
            cnn.Close();

          }

        }
      }


            return new HttpStatusCodeResult(HttpStatusCode.OK);

        }

        [HttpGet]
        public ActionResult CurrentQuoteQueue()
        {
            ViewBag.Message = "Your queue.";

            int user_id;
            int org_id;
            user_id = 0;
            org_id = 0;
            //string username;
            string org_name_var;
            org_name_var = "CAM";
            // string connectionString;
            string sqlScript;

            if (string.IsNullOrEmpty(Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)) == true)
            {

                MessageBox.Show("Username not found. Exiting...");
                Response.Write("<script>window.close();</script>");
                return View("~/Views/Home/CreateQuote.cshtml");
            }


            SqlConnection cnn;

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            System.Diagnostics.Debug.WriteLine("connecting to database...");
            cnn.Open();
            System.Diagnostics.Debug.WriteLine("connected to database.");

            //get the last_update_by
            sqlScript = "select u.user_id, u.org_id, o.org_name from users u, organization o where u.org_id=o.org_id and user_name='bmount' and active_flag=1";
            SqlCommand command = new SqlCommand(sqlScript, cnn)
            {
                CommandText = sqlScript
            };
            SqlParameter parameter = new SqlParameter
            {
                ParameterName = "@username",
                Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
            };
            command.Parameters.Add(parameter);
            SqlDataReader reader;
            reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    user_id = reader.GetInt32(0);
                    org_id = reader.GetInt32(1);
                    org_name_var = reader.GetString(2);
                }
            }
            else
            {
                MessageBox.Show("No users were found matching that criteria.");
                //return;
            }

            reader.Close();

            cnn.Close();

            //username = User.Identity.Name;
            //username = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1);

            SharePointConnectionUpdated dc = new SharePointConnectionUpdated();
      var quotes = (from c in dc.vQuote_View_status
                    select new ViewQuoteQueue
                    {
                      QuoteID = c.QuoteID,
                      QuoteNum = c.QuoteNum,
                      CreatedBy = c.CreatedBy,
                      CreationDate = c.CreationDate,
                      DueDate = c.Due_Date,
                              CustomerName = c.CustomerName,
                              AssignedRoleDesc = c.AssignedRoleDesc,
                              StatusDesc = c.StatusDesc,
                              Division = c.Division
                          }).ToList();

            return View(quotes);
        }

        [HttpGet]
        public ActionResult ViewQuote()
        {

            ViewQuoteModel v_model = new ViewQuoteModel();
            List<QuoteLineItems> line_model = new List<QuoteLineItems>();

            SqlConnection cnn;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            //fill the status dropdown box
            
                List<SelectListItem> StatusUpdates = new List<SelectListItem>();
                using (SharePointConnectionUpdated db = new SharePointConnectionUpdated())
                {
                    string query = " select Status_Desc, Status_ID from STATUS where PROCESS_TYPE_ID=3 and status_id IN ('15','16')";
                    using (SqlCommand cmd = new SqlCommand(query))
                    {
                        cmd.Connection = cnn;
                        cnn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                StatusUpdates.Add(new SelectListItem
                                {
                                    Text = sdr["Status_Desc"].ToString(),
                                    Value = sdr["Status_ID"].ToString()
                                });
                            }
                        }
                        cnn.Close();
                    }
                }
                v_model.AssignedStatusList = StatusUpdates;

            int quote_id_selected;
            var quote_val = Request.Params["QuoteID"];
            quote_id_selected = Convert.ToInt32(quote_val);
            v_model.QuoteID = quote_id_selected;


            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                string sqlScript = "select * from vQuote_View_Status where quoteid=@quote_id;";
                SqlCommand command = new SqlCommand(sqlScript, cnn)
                {
                    CommandText = sqlScript
                };
                SqlParameter quote_id_val = new SqlParameter
                {
                    ParameterName = "@quote_id",
                    Value = v_model.QuoteID
                };
                command.Parameters.Add(quote_id_val);
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        v_model.QuoteNum = reader.GetString(1);
                        v_model.CreatedBy = reader.GetString(2);
                        v_model.CreationDate = reader.GetDateTime(3);
                        v_model.ShipTooName = reader.GetString(4);
                        v_model.AssignedRoleDesc = reader.GetString(5);
                        v_model.WorkflowStatus = reader.GetString(7);
                        v_model.Status_Desc = reader.GetString(8);
                    }

                }
                catch (Exception)
                {
                    MessageBox.Show("No quote found matching that criteria. Contact your system administrator.");

                    return View("Index", "Index");
                }
                cnn.Close();
            }

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                string sqlScript = "select sold_to_num, sold_to_name, ship_to_num, ship_to_name, customer_po, LEAD_TIME_COMMENT, CONVERT(varchar, due_date, 23) due_date from quote_header where quote_id=@quote_id;";
                SqlCommand command = new SqlCommand(sqlScript, cnn)
                {
                    CommandText = sqlScript
                };
                SqlParameter quote_id_val = new SqlParameter
                {
                    ParameterName = "@quote_id",
                    Value = v_model.QuoteID
                };
                command.Parameters.Add(quote_id_val);
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        v_model.SoldTooNum = reader.GetString(0);
                        v_model.SoldTooName = reader.GetString(1);
                        v_model.ShipTooNum = reader.GetString(2);
                        v_model.ShipTooName = reader.GetString(3);
                        v_model.CustPO = reader.IsDBNull(4) ? "" : (string)reader.GetValue(4);
                        v_model.LeadTimeComments = reader.IsDBNull(5) ? "" : (string)reader.GetValue(5);
                        v_model.DueDate = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);

                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to find the quote of interest. Please contact your sys admin.");

                    return View("Index", "Index");
                }
                cnn.Close();
            }

            int line_num = 0;

            //if (line_model.QCPartNumber != null && string.IsNullOrEmpty(line_model.QCPartNumber) == true)
            //{
            //    line_num = 1;
            //}

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                
                int i = 1;
        QuoteLineItems line = new QuoteLineItems();
        string sqlScript = @"select l.PART_NUMBER, l.QUANTITY, l.UNIT_PRICE, (l.quantity * l.UNIT_PRICE) ext_price, l.comment , l.quote_line_num, l.unit_cost, l.lead_time, l.cust_part_number
                                , l.cost_ref, l.pricing_tool_cost
                             From quote_lines l join QUOTE_HEADER h 
                             on h.quote_id = l.quote_header_id 
                             where h.quote_id = @quote_id; ";
                SqlCommand command = new SqlCommand(sqlScript, cnn)
                {
                    CommandText = sqlScript
                };
                SqlParameter quote_id_val = new SqlParameter
                {
                    ParameterName = "@quote_id",
                    Value = v_model.QuoteID
                };
                command.Parameters.Add(quote_id_val);
                SqlDataReader reader = command.ExecuteReader();
        try
        {
          while (reader.Read())
          {
                        //if (i == 1)
                        QuoteLineItems item = new QuoteLineItems()
                        {
                            QCLineNum = reader.GetInt32(5),
                            QCPartNumber = reader.GetString(0),
                            QCCustPartNumber = reader.GetString(8),
                            QCQuoteQty = reader.GetInt32(1),
                            QCQuoteUnitPrice = reader.GetDecimal(2),
                            QCLineComments = reader.GetString(4),
                            QCExtPrice = reader.GetDecimal(2) * reader.GetInt32(1),
                            QCQuoteUnitCost = reader.GetDecimal(6),
                            QCExtCost = reader.GetDecimal(6) * reader.GetInt32(1),
                            QCMargin = (reader.GetDecimal(2) * reader.GetInt32(1)) - (reader.GetDecimal(6) * reader.GetInt32(1)),
                            QCLeadTime = reader.GetString(7),
                            QCCostReference = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9),
                            QCQuoteCalcUnitCost=reader.GetDecimal(10)
                        //QCMarginPerc = (reader.GetDecimal(2) * reader.GetInt32(1)) / (reader.GetDecimal(6) * reader.GetInt32(1)) * 100
                    };
            v_model.TotalCost = v_model.TotalCost + item.QCExtCost;
            v_model.TotalPrice = v_model.TotalPrice+item.QCExtPrice;
            line_model.Add(item);

            
          }
          
        }






        catch (Exception ex)
        {
          MessageBox.Show("No quote found matching that criteria. Contact your system administrator.");

          return View("Index", "Index");
        }
                cnn.Close();

                //fill the lines associated with the request
                //cnn.Open();
                //using (cnn)
                //{
                //    string sqlScript = "select * from quote_lines where ;";
                //    SqlCommand command = new SqlCommand(sqlScript, cnn);
                //    command.CommandText = sqlScript;
                //    SqlParameter quote_id_val = new SqlParameter();
                //    quote_id_val.ParameterName = "@quote_id";
                //    quote_id_val.Value = v_model.QuoteID;
                //    command.Parameters.Add(quote_id_val);
                //    SqlDataReader reader = command.ExecuteReader();
                //    try
                //    {
                //        while (reader.Read())
                //        {
                //            v_model.QuoteNum = reader.GetString(1);
                //            v_model.CreatedBy = reader.GetString(2);
                //            v_model.CreationDate = reader.GetDateTime(3);
                //            v_model.CustomerName = reader.GetString(4);
                //            v_model.AssignedRoleDesc = reader.GetString(5);
                //            v_model.WorkflowStatus = reader.GetString(7);
                //            v_model.Status_Desc = reader.GetString(8);
                //        }

                //    }
                //    catch (Exception)
                //    {
                //        MessageBox.Show("No quote found matching that criteria. Contact your system administrator.");

                //        return View("Index", "Index");
                //    }
                //    cnn.Close();

                //}
            }

      var ourModel = new QuoteOverview
      {
        ViewQuoteModel = v_model,
        QuoteDisplayLines = line_model
      };
      //overallTime.Stop(this.HttpContext);
      return View(ourModel);
    }

        [HttpGet]
        public ActionResult QualifiedPartsListing()
        {
            ViewBag.Message = "Your Qualified Parts Listing.";

            int user_id;
            int org_id;
            user_id = 0;
            org_id = 0;
            //string username;
            string org_name_var;
            org_name_var = "CAM";
            // string connectionString;
            string sqlScript;

            if (string.IsNullOrEmpty(Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)) == true)
            {

                MessageBox.Show("Username not found. Exiting...");
                Response.Write("<script>window.close();</script>");
                return View("~/Views/Home/CreateQuote.cshtml");
            }


            SqlConnection cnn;

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            System.Diagnostics.Debug.WriteLine("connecting to database...");
            cnn.Open();
            System.Diagnostics.Debug.WriteLine("connected to database.");

            //get the last_update_by
            sqlScript = "select u.user_id, u.org_id, o.org_name from users u, organization o where u.org_id=o.org_id and user_name=@username and active_flag=1";
            SqlCommand command = new SqlCommand(sqlScript, cnn)
            {
                CommandText = sqlScript
            };
            SqlParameter parameter = new SqlParameter
            {
                ParameterName = "@username",
                Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
            };
            command.Parameters.Add(parameter);
            SqlDataReader reader;
            reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    user_id = reader.GetInt32(0);
                    org_id = reader.GetInt32(1);
                    org_name_var = reader.GetString(2);
                }
            }
            else
            {
                MessageBox.Show("No users were found matching that criteria.");
                //return;
            }

            reader.Close();

            cnn.Close();


            SharePointConnectionUpdated dc = new SharePointConnectionUpdated();
            var approved_parts = (from m in dc.APPROVED_PARTS
                                  select new QualifiedParts
                                  {
                                      Product_Code_ID = m.PRODUCT_CODE_ID,
                                      SpecificationStandard = m.SPECIFICATION_STANDARD,
                                      CustodianPart = m.CUSTODIAN_PART,
                                      Custodian = m.CUSTODIAN,
                                      Manufacturer = m.MANUFACTURER,
                                      MaterialType = m.MATERIAL_TYPE,
                                      Size = m.SIZE,
                                      Range = m.RANGE,
                                      Finish = m.FINISH,
                                      Status = m.STATUS,
                                      CompetitorPrice=m.COMPETITOR_PRICE,
                                      Organization = m.ORGANIZATION,
                                      DateCreated = m.DATE_CREATED.Value,
                                      EnteredBy = m.ENTERED_BY,
                                      ModifiedBy = m.MODIFIED_BY
                                  }).ToList();
            return View(approved_parts);

        }

        [HttpGet]
        public ActionResult CostFactors()
        {
            ViewBag.Message = "Your Cost Factors.";

            int user_id;
            int org_id;
            user_id = 0;
            org_id = 0;
            //string username;
            string org_name_var;
            org_name_var = "CAM";
            // string connectionString;
            string sqlScript;

            if (string.IsNullOrEmpty(Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)) == true)
            {

                MessageBox.Show("Username not found. Exiting...");
                Response.Write("<script>window.close();</script>");
                return View("~/Views/Home/CreateQuote.cshtml");
            }


            SqlConnection cnn;

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            System.Diagnostics.Debug.WriteLine("connecting to database...");
            cnn.Open();
            System.Diagnostics.Debug.WriteLine("connected to database.");

            //get the last_update_by
            sqlScript = "select u.user_id, u.org_id, o.org_name from users u, organization o where u.org_id=o.org_id and user_name=@username and active_flag=1";
            SqlCommand command = new SqlCommand(sqlScript, cnn)
            {
                CommandText = sqlScript
            };
            SqlParameter parameter = new SqlParameter
            {
                ParameterName = "@username",
                Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
            };
            command.Parameters.Add(parameter);
            SqlDataReader reader;
            reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    user_id = reader.GetInt32(0);
                    org_id = reader.GetInt32(1);
                    org_name_var = reader.GetString(2);
                }
            }
            else
            {
                MessageBox.Show("No users were found matching that criteria.");
                //return;
            }

            reader.Close();

            cnn.Close();


            SharePointConnectionUpdated dc = new SharePointConnectionUpdated();
            var costs = (from m in dc.MASTER_COST
                         select new CostFactors
                         {
                             Factor_ID = m.FACTOR_ID,
                             PartNumber = m.ITEM_NUMBER,
                             Description = m.DESCRIPTION,
                             Factor_500 = m.C500_FACTOR,
                             Factor_1000 = m.C1000_FACTOR,
                             Factor_2500 = m.C2500_FACTOR,
                             Factor_5000 = m.C5000_FACTOR,
                             Factor_10000 = m.C10000_FACTOR,
                             Factor_25000 = m.C25000_FACTOR,
                             Factor_50000 = m.C50000_FACTOR,
                             Factor_100000 = m.C100000_FACTOR,
                             LastEditBy = m.LAST_EDIT_INITIALS,
                             Comments = m.COMMENTS,
                             LastEditDate = m.LAST_EDIT_DATE

                         }).ToList();


            return View(costs);
        }

        [HttpGet]
        public ActionResult StrategicPricingAdjustments()
        {
            ViewBag.Message = "Your Strategic Adjustments.";

            int user_id;
            int org_id;
            user_id = 0;
            org_id = 0;
            //string username;
            string org_name_var;
            org_name_var = "CAM";
            // string connectionString;
            string sqlScript;

            if (string.IsNullOrEmpty(Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)) == true)
            {

                MessageBox.Show("Username not found. Exiting...");
                Response.Write("<script>window.close();</script>");
                return View("~/Views/Home/CreateQuote.cshtml");
            }


            SqlConnection cnn;

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            System.Diagnostics.Debug.WriteLine("connecting to database...");
            cnn.Open();
            System.Diagnostics.Debug.WriteLine("connected to database.");

            //get the last_update_by
            sqlScript = @"select u.user_id, u.org_id, o.org_name from users u left outer join organization o on u.org_id=o.org_id
                    left outer join ROLES r on u.ROLE_ID=r.ROLE_ID
                                     where u.USER_NAME = @username
                                     and user_name=@username and active_flag=1 and r.ROLE_ID in ('1','4','7') ";
            SqlCommand command = new SqlCommand(sqlScript, cnn)
            {
                CommandText = sqlScript
            };
            SqlParameter parameter = new SqlParameter
            {
                ParameterName = "@username",
                Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
            };
            command.Parameters.Add(parameter);
            SqlDataReader reader;
            reader = command.ExecuteReader();
            try
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        user_id = reader.GetInt32(0);
                        org_id = reader.GetInt32(1);
                        org_name_var = reader.GetString(2);
                    }
                }
                else
                {
                MessageBox.Show("Cannot find division name or you are not listed as an administrator and do not have access to modify these settings.");
                    return View("~/Views/Home/DataManagementConsole.cshtml");
              }
              reader.Close();
            }
            catch (Exception)
            {
                //do nothing
            }

     
         //sqlScript = @"select convert(int,d.DIVISION) division, u.user_id, r.role_id, r.role_Desc From USERS u 
         //                            left outer join organization d on u.org_id = d.org_id 
									// left outer join ROLES r on u.ROLE_ID=r.ROLE_ID
         //                            where u.USER_NAME = @username
									// and r.ROLE_ID in ('1','4','7') ";
        
        //  else
        //  {
        //    MessageBox.Show("Cannot find division name or you are not listed as an administrator and do not have access to modify these settings.");
        //    return View("~/Views/Home/Index.cshtml");
        //  }
        //}
        //catch (Exception ex)
        //{
        //  MessageBox.Show("Cannot find division name or you are not listed as an administrator and do not have access to modify these settings.");
        //  return View("~/Views/Home/Index.cshtml");
        //}
        //finally
        //{
        //  reader.Close();
        //}
      


      cnn.Close();

            string created_by="";
            string last_edited_by="";

            SharePointConnectionUpdated dc = new SharePointConnectionUpdated();
            var factors = (from m in dc.STRATEGIC_PRICING_ADJUSTMENTS
                         select new StrategicPricingAdjustments
                         {
                             ADJUSTMENT_ID = m.ADJUSTMENT_ID,
                             DIVISION = m.DIVISION,
                             VALUE_STREAM = m.VALUE_STREAM,
                             PRODUCT_GROUP= m.PRODUCT_GROUP,
                             PRODUCT_CODE = m.PRODUCT_CODE,
                             CUSTOMER_NUMBER = m.CUSTOMER_NUMBER,
                             INTERNAL_PART_NUMBER = m.INTERNAL_PART_NUMBER,
                             CUSTOMER_PART_NUMBER = m.CUSTOMER_PART_NUMBER,
                             QUANTITY_BREAK_RANGE_START=m.QUANTITY_BREAK_RANGE_START,
                             QUANTITY_BREAK_RANGE_END=m.QUANTITY_BREAK_RANGE_END,
                             OTHER = m.OTHER,
                             STATUS = m.STATUS,
                             COMMENT = m.COMMENT,
                             ADJUSTMENT = m.ADJUSTMENT,
                             //APPLY_VARIANCE_ADJUSTMENT = m.APPLY_VARIANCE_ADJUSTMENT,
                             //CreatedBy = created_by,
                             CreationDate = m.CREATION_DATE,
                             //LastEditedBy = last_edited_by,
                             LastEditDate = m.LAST_EDIT_DATE
                             //LEAD_TIME_FLAG=m.LEAD_TIME_FLAG

                         }).ToList();


            return View(factors);
        }

        public ActionResult Notifications()
        {
            ViewBag.Message = "Your notifications.";
            return View();

        }

        [HttpPost]
        public ActionResult ViewQuote(ViewQuoteModel model, QuoteListItem model2)
        {
            SqlConnection cnn;

            int quote_selected=0;
      //var quote_selected_val = Request.Params["quote_id"];
      quote_selected = model.QuoteID;
          cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            cnn.Open();
            int user_id = 0;
            int workflow_id=0;
            DateTime approval_date;
            string division_id = "";
            using (cnn)
            {
                string sqlScript = "select user_id from users where user_name=@user_name ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter user_name = new SqlParameter
                {
                    ParameterName = "@user_name",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(user_name);
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                  while (reader.Read())
                  {
                    user_id = reader.GetInt32(0);
                  }
                  //reader.Close();
                }
                catch (Exception)
                {
                  MessageBox.Show("Could not find the user's ID.");
                  return View();
                }
                finally
                {
                  //reader.Close();
                }
            }
      cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
      cnn.Open();
      using (cnn)
      {
        //cnn.Open();
        string sqlScript = @"select top 1 o.DIVISION, h.workflow_id, isnull(h.approval_date,convert(date,getdate()))
                                        From quote_header h
                                        left outer join QUOTE_LINES l on h.quote_id = l.QUOTE_HEADER_ID
                                        left outer join WORKFLOW w on h.WORKFLOW_ID = w.WORKFLOW_ID
                                        left outer join[users] u on u.[USER_ID] = w.CREATED_BY_USER_ID
                                        left outer join ORGANIZATION o on u.ORG_ID = o.ORG_ID
                                        where 1 = 1 and quote_id = @quote_id; ";
        SqlCommand command = new SqlCommand(sqlScript, cnn);
        SqlParameter user_name = new SqlParameter
        {
          ParameterName = "@quote_id",
          Value = model.QuoteID
        };
        command.Parameters.Add(user_name);
        SqlDataReader reader = command.ExecuteReader();
        try
        {
          while (reader.Read())
          {
            division_id = reader.GetString(0);
            workflow_id = reader.GetInt32(1);
                        approval_date = reader.GetDateTime(2);
          }
          //reader.Close();
        }
        catch (Exception ex)
        {
          MessageBox.Show("Could not find the quote. Please contact your system administrator.");
          return View();
        }
        finally
        {
          //reader.Close();
        }
      }


      cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
      cnn.Open();
      //find workflow detail that was just assigned

      List<QuoteListItem> wf = new List<QuoteListItem>();
      using (cnn)
      {
        string sqlScript = @"select o.DIVISION, h.workflow_id, h.customer_po, l.QUOTE_LINE_NUM, l.quantity, l.part_number, l.unit_price, h.SOLD_TO_NUM, h.SHIP_TO_NUM, isnull(h.approval_date,convert(date,getdate())) approval_date
                                        From quote_header h
										left outer join QUOTE_LINES l on h.quote_id=l.QUOTE_HEADER_ID
										left outer join WORKFLOW w on h.WORKFLOW_ID=w.WORKFLOW_ID
										left outer join [users] u on u.[USER_ID]=w.CREATED_BY_USER_ID
										left outer join ORGANIZATION o on u.ORG_ID=o.ORG_ID
                                        where 1 = 1 and quote_id = @quote_id; ";
        SqlCommand command = new SqlCommand(sqlScript, cnn);
        SqlParameter quote_id_val = new SqlParameter
        {
          ParameterName = "@quote_id",
          Value = model.QuoteID
        };
        command.Parameters.Add(quote_id_val);

        SqlDataReader reader = command.ExecuteReader();
        if (reader.HasRows)
        {
          while (reader.Read())
          {
                        QuoteListItem item = new QuoteListItem()
                        {
                            division_id = reader.GetString(0),
                            workflow_id = reader.GetInt32(1),
                            customer_po = reader.GetString(2),
                            line_num = reader.GetInt32(3),
                            quantity = reader.GetInt32(4),
                            part_number = reader.GetString(5),
                            unit_price = reader.GetDecimal(6),
                            sold_to_num = reader.GetString(7),
                            ship_to_num = reader.GetString(8),
                            approval_date = reader.GetDateTime(9)
            };
            wf.Add(item);
          }
          reader.Close();
        }
      
                else
                {
                    MessageBox.Show("Failed to approve quote and integrate with JDE. Please see your system administrator.");
                    return View("~/Home/Index.cshtml");
                }

            }

            int status_id;

            status_id = model.Status_ID; //status that was chosen

      cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
      cnn.Open();
      using (cnn)
            {
                //create workflow
                string sqlScript = @"update workflow set status_id=@status_id, LAST_UPDATED=convert(date,getdate()), LAST_UPDATED_BY=@updated_by_user_id, ASSIGNED_TO_ROLE_ID=3 where workflow_id=@workflow_id; 
                    update quote_header set quote_status_id=@status_id where workflow_id=@workflow_id;"; 
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter status_id_val = new SqlParameter
                {
                    ParameterName = "@status_id",
                    Value = status_id //update to status 15 if approved, not approved is 16
                };
                command.Parameters.Add(status_id_val);
                //
                SqlParameter last_updated_by = new SqlParameter
                {
                    ParameterName = "@updated_by_user_id",
                    Value = user_id
                };
                command.Parameters.Add(last_updated_by);
                //
                SqlParameter workflow_id_param = new SqlParameter
                {
                    ParameterName = "@workflow_id",
                    Value = workflow_id
                };
                command.Parameters.Add(workflow_id_param);
                try
                {
                    command.ExecuteNonQuery();
                    command.Dispose();

                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to update workflow information. Exiting. Please contact your system administrator.");
                    return View("~/Views/Home/CreateQuote.cshtml");
                }
            }
            cnn.Close();

            //workflow history update

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                //create workflow
                string sqlScript = @"insert into workflow_history (WORKFLOW_ID,STATUS_ID, LAST_UPDATED, LAST_UPDATED_BY, ASSIGNED_TO_ROLE_ID, WORKFLOW_TYPE_ID) 
                     values(@workflow_id,@status_id 
                     , CONVERT(DATE, GETDATE()) 
                     , @last_updated_by, 3,2 
                     ); "; //fill the sql value
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                //SqlParameter workflow_id_hist = new SqlParameter();
                SqlParameter workflow_id_param = new SqlParameter
                {
                    ParameterName = "@workflow_id",
                    Value = workflow_id
                };
                command.Parameters.Add(workflow_id_param);
                SqlParameter status_id_val = new SqlParameter
                {
                    ParameterName = "@status_id",
                    Value = status_id //update to status 15 if approved, not approved is 16
                };
                command.Parameters.Add(status_id_val);
                SqlParameter last_updated_by = new SqlParameter
                {
                    ParameterName = "@last_updated_by",
                    Value = user_id
                };
                command.Parameters.Add(last_updated_by);
                //assigned_to_role_id_val.ParameterName = "@assigned_to_role_id";

                try
                {
                    command.ExecuteNonQuery();
                    command.Dispose();

                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to update workflow history information.");
                    //return View("~/Views/Home/CreateQuote.cshtml");
                }
            }
            cnn.Close();

            //find original 

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            cnn.Open();
            //create workflow
            using (cnn)
            {
                string sqlScript = @"update h set h.created_by_user_id=w.created_by_user_id, 
                           h.creation_date = w.creation_date 
                          from workflow_history h 
                           right join workflow w on h.workflow_id = w.workflow_id 
                           where h.workflow_hist_id = (select max(s.WORKFLOW_HIST_ID) 
                                 from WORKFLOW_HISTORY s 
                                 where w.WORKFLOW_ID = s.WORKFLOW_ID) 
                           and h.WORKFLOW_ID = @workflow_id ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter workflow_id_param = new SqlParameter
                {
                    ParameterName = "@workflow_id",
                    Value = workflow_id
                };
                command.Parameters.Add(workflow_id_param);
                try
                {
                    command.ExecuteNonQuery();
                    command.Dispose();

                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to insert workflow history information.");
                    //return View("~/Views/Home/CreateQuote.cshtml");
                }
            }
            cnn.Close();

            //find the EDI document number
            //OdbcConnection cnn_odbc;
            //cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString);
            //cnn_odbc.Open();
            //int next_order_num;
            //next_order_num = 0;
            //using (cnn_odbc)
            //{
            //    string sqlScript = "select(max(syedoc) + 1) EdiDocumentNumber From testdta.f47011";
            //    OdbcCommand command = new OdbcCommand(sqlScript, cnn_odbc);
            //    try
            //    {
            //        using (OdbcDataReader reader = command.ExecuteReader())
            //        {
            //            while (reader.Read())
            //            {
            //                next_order_num = reader.GetInt32(0);
            //            }
            //        }
            //    }
            //    catch (Exception)
            //    {
            //        // no action
            //    }
            //}
            //cnn_odbc.Close();

            //now that I know the next EDI number, prepare the insertion

            //if (status_id == 15)
            //{
            //    MessageBox.Show("Starting loop to write too JDE tables.");
            //    cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString);
            //    cnn_odbc.Open();
            //    using (cnn_odbc)
            //    {
            //        string sqlScript = @"insert into PRODDTA.F47011 (SYEDSQ,SYEKCO,SYEDOC,SYEDCT,SYEDLN,SYEDST,SYEDFT,SYEDDT,SYEDER
            //                            ,SYEDDL,SYEDSP,SYEDBT,SYPNID,SYOFRQ,SYNXDJ,SYSSDJ,SYTPUR,SYKCOO,SYDOCO,SYDCTO,SYSFXO,SYMCU,SYCO
            //                            ,SYAN8,SYSHAN,SYPA8,SYDRQJ,SYTRDJ,SYTRDDJ,SYOPDJ,SYADDJ,SYCNDJ,SYPEFJ,SYPPDJ,SYPSDJ
            //                            ,SYVR01,SYTRDC,SYPCRT,SYANBY,SYCARS,SYOTOT,SYTOTC,SYCEXP,SYCRR,SYFAP,SYFCST,SYURDT,SYURAT,SYURAB
            //                            ,SYTORG,SYUSER,SYPID,SYJOBN,SYUPMJ,SYTDAY
            //                            , SYSOOR, SYPMDT, SYRSDT, SYRQSJ, SYPSTM, SYPDTT,SYOPTT,SYDRQT,SYADTM,SYADLJ,SYPBAN,SYITAN,SYFTAN,SYDVAN,SYDOC1,SYDCT4,SYCORD,SYRSHT,SYOPTC,SYOPLD
            //                            ,SYOPBK,SYOPSB,SYPRAN8,SYPRCIDLN,SYOPPID,SYCCIDLN,SYSHCCIDLN,SYEXNM0,SYEXNM1,SYEXNM2,SYEXNMP) 
            //                            VALUES (0,'00710',?,'SQ',0,850,'',0,'R',0,'',) ";
            //        OdbcCommand command = new OdbcCommand(sqlScript, cnn_odbc);
            //        try
            //        {
            //            command.Parameters.Add("@sydoc", OdbcType.BigInt).Value = next_order_num;
            //            command.ExecuteNonQuery();
            //        }
            //        catch (Exception)
            //        {
            //            // no action
            //        }
            //    }



            if (division_id == "730000")
            {
                var dir = @"\\10.100.0.248\StrategicPricing\Bristol\SQORDERS.csv";

                if (!System.IO.File.Exists(dir))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Sold To,Ship To,Qty Ordered,Part Number,Request Date,Sched Pick Date,Customer PO,Unit Price");
                    foreach (QuoteListItem li in wf)
                        sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + li.approval_date.ToString("MM/dd/yyyy") + "," + li.approval_date.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
                    {
                        var string_with_your_data = sb.ToString();
                        var byteArray = Encoding.ASCII.GetBytes(string_with_your_data);
                        var stream = new MemoryStream(byteArray);
                        System.IO.File.WriteAllBytes(@"\\10.100.0.248\StrategicPricing\Bristol\SQORDERS.csv", stream.ToArray());
                    }
                }

                else
                {

                    using (var fs = new FileStream(@"\\10.100.0.248\StrategicPricing\Bristol\SQORDERS.csv", FileMode.Append, FileAccess.Write))
                    {
                        TextWriter tw = new StreamWriter(fs);
                        //tw.Write("blabla");
                        tw.Flush();
                        StringBuilder sb = new StringBuilder();
                        foreach (QuoteListItem li in wf)
                            sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + li.approval_date.ToString("MM/dd/yyyy") + "," + li.approval_date.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
                        {
                            var string_with_your_data = sb.ToString();
                            tw.Write(string_with_your_data);
                        }
                        tw.Flush();
                    }
                }
            }

            //else if (division_id == "720000")
            //{
            //  var dir = @"\\10.100.0.248\StrategicPricing\QRP\SQORDERS.csv";

            //  if (!System.IO.File.Exists(dir))
            //  {
            //    StringBuilder sb = new StringBuilder();
            //    sb.AppendLine("Sold To,Ship To,Qty Ordered,Part Number,Request Date,Sched Pick Date,Customer PO,Unit Price");
            //    foreach (QuoteListItem li in wf)
            //      sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
            //    {
            //      var string_with_your_data = sb.ToString();
            //      var byteArray = Encoding.ASCII.GetBytes(string_with_your_data);
            //      var stream = new MemoryStream(byteArray);
            //      System.IO.File.WriteAllBytes(@"\\10.100.0.248\StrategicPricing\QRP\SQORDERS.csv", stream.ToArray());
            //    }
            //  }

            //  else
            //  {

            //    using (var fs = new FileStream(@"\\10.100.0.248\StrategicPricing\QRP\SQORDERS.csv", FileMode.Append, FileAccess.Write))
            //    {
            //      TextWriter tw = new StreamWriter(fs);
            //      //tw.Write("blabla");
            //      tw.Flush();
            //      StringBuilder sb = new StringBuilder();
            //      foreach (QuoteListItem li in wf)
            //        sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
            //      {
            //        var string_with_your_data = sb.ToString();
            //        tw.Write(string_with_your_data);
            //      }
            //      tw.Flush();
            //    }
            //  }
            //}
            else if (division_id == "710000")
            {
                var dir = @"\\10.100.0.248\StrategicPricing\3V\SQORDERS.csv";

                if (!System.IO.File.Exists(dir))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Sold To,Ship To,Qty Ordered,Part Number,Request Date,Sched Pick Date,Customer PO,Unit Price");
                    foreach (QuoteListItem li in wf)
                        sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + li.approval_date.ToString("MM/dd/yyyy") + "," + li.approval_date.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
                    {
                        var string_with_your_data = sb.ToString();
                        var byteArray = Encoding.ASCII.GetBytes(string_with_your_data);
                        var stream = new MemoryStream(byteArray);
                        System.IO.File.WriteAllBytes(@"\\10.100.0.248\StrategicPricing\3V\SQORDERS.csv", stream.ToArray());
                    }
                }

                else
                {

                    using (var fs = new FileStream(@"\\10.100.0.248\StrategicPricing\3V\SQORDERS.csv", FileMode.Append, FileAccess.Write))
                    {
                        TextWriter tw = new StreamWriter(fs);
                        //tw.Write("blabla");
                        tw.Flush();
                        StringBuilder sb = new StringBuilder();
                        foreach (QuoteListItem li in wf)
                            sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + li.approval_date.ToString("MM/dd/yyyy") + "," + li.approval_date.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
                        {
                            var string_with_your_data = sb.ToString();
                            tw.Write(string_with_your_data);
                        }
                        tw.Flush();
                    }
                }
            }
            //else if (division_id == "750000")
            //{
            //  var dir = @"\\10.100.0.248\StrategicPricing\Voss\SQORDERS.csv";

            //  if (!System.IO.File.Exists(dir))
            //  {
            //    StringBuilder sb = new StringBuilder();
            //    sb.AppendLine("Sold To,Ship To,Qty Ordered,Part Number,Request Date,Sched Pick Date,Customer PO,Unit Price");
            //    foreach (QuoteListItem li in wf)
            //      sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
            //    {
            //      var string_with_your_data = sb.ToString();
            //      var byteArray = Encoding.ASCII.GetBytes(string_with_your_data);
            //      var stream = new MemoryStream(byteArray);
            //      System.IO.File.WriteAllBytes(@"\\10.100.0.248\StrategicPricing\Voss\SQORDERS.csv", stream.ToArray());
            //    }
            //  }

            //  else
            //  {

            //    using (var fs = new FileStream(@"\\10.100.0.248\StrategicPricing\Voss\SQORDERS.csv", FileMode.Append, FileAccess.Write))
            //    {
            //      TextWriter tw = new StreamWriter(fs);
            //      //tw.Write("blabla");
            //      tw.Flush();
            //      StringBuilder sb = new StringBuilder();
            //      foreach (QuoteListItem li in wf)
            //        sb.AppendLine(li.sold_to_num + "," + li.ship_to_num + "," + li.quantity + "," + li.part_number + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + DateTime.Now.ToString("MM/dd/yyyy") + "," + li.customer_po + "," + li.unit_price);
            //      {
            //        var string_with_your_data = sb.ToString();
            //        tw.Write(string_with_your_data);
            //      }
            //      tw.Flush();
            //    }





            //MessageBox.Show("Approved!")
            return View("~/Views/Home/Index.cshtml");
        }

        [HttpGet]
        public ActionResult EditQualifiedPart()
        {

            //pass the qualified parts to their respective text boxes prior to updating
            QualifiedParts qp = new QualifiedParts();

            //which one am I editing?

            string part_selected;
            var part_selected_val = Request.Params["Product_Code_ID"];
            part_selected = part_selected_val;

            SqlConnection cnn;

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                //create workflow
                string sqlScript = @"select specification_standard, custodian_part, custodian, manufacturer, material_type, size, range, finish, status, 
                organization, convert(date, date_created) date_created, entered_by, modified_by, competitor_price, product_code_id
                     from approved_parts where product_code_id=@product_code_id; ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                //SqlParameter workflow_id_hist = new SqlParameter();
                SqlParameter part_selected_param = new SqlParameter
                {
                    ParameterName = "@product_code_id",
                    Value = part_selected
                };
                command.Parameters.Add(part_selected_param);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    try
                    {
                        qp.SpecificationStandard = reader.GetString(0);
                        qp.CustodianPart = reader.GetString(1);
                        qp.Custodian = reader.GetString(2);
                        qp.Manufacturer = reader.GetString(3);
                        qp.MaterialType = reader.IsDBNull(4) ? "" : (string)reader.GetValue(4);
                        qp.Size = reader.IsDBNull(5) ? "" : (string)reader.GetValue(5);
                        qp.Range = reader.GetString(6);
                        qp.Finish = reader.GetString(7);
                        qp.Status = reader.GetString(8);
                        qp.Status = reader.GetString(8);
                        qp.Organization = reader.GetString(9);
                        qp.DateCreated = reader.GetDateTime(10);
                        qp.EnteredBy = reader.GetString(11);
                        qp.ModifiedBy = reader.GetString(12);
                        //qp.CompetitorPrice = reader.GetString(13);
                        qp.CompetitorPrice=reader.IsDBNull(13) ? 0 : (decimal)reader.GetValue(13);
                        qp.Product_Code_ID = reader.GetInt32(14);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Failed to load approved part that was selected.");
                        //return View("~/Views/Home/CreateQuote.cshtml");
                    }
            }
            cnn.Close();

            return View(qp);
        }

        [HttpPost]
        public ActionResult EditQualifiedPart(QualifiedParts qp, UserModel users)
        {

            SqlConnection cnn;

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            string part_selected;
            var part_selected_val = Request.Params["Product_Code_ID"];
            part_selected = part_selected_val;

            int charLocation = part_selected.IndexOf(",", StringComparison.Ordinal);
            if (charLocation > 0)
            {
                part_selected = part_selected.Substring(0, charLocation);
            }


            //part_selected =left(part_selected,)

            cnn.Open();
            int user_id = 0;
            using (cnn)
            {
                string sqlScript = "select user_id from users where user_name=@user_name ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter user_name = new SqlParameter
                {
                    ParameterName = "@user_name",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(user_name);
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        user_id = reader.GetInt32(0);
                    }
                    reader.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find the user's ID.");
                    return View();
                }
                reader.Close();
            }
            cnn.Close();



            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                //create workflow
                string sqlScript = @"update t set specification_standard=@specification_standard
                , custodian_part=@custodian_part, custodian=@custodian, manufacturer=@manufacturer, material_type=@material_type
                , size=@size, range=@range, finish=@finish, competitor_price=@competitor_price, status=@status, modified_by=@username
                     from approved_parts t where product_code_id=@product_code_id ;";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                //SqlParameter workflow_id_hist = new SqlParameter();
                SqlParameter part_selected_param = new SqlParameter
                {
                    ParameterName = "@product_code_id",
                    Value = part_selected
                };
                command.Parameters.Add(part_selected_param);
                SqlParameter specification_standard_param = new SqlParameter
                {
                    ParameterName = "@specification_standard",
                    Value = qp.SpecificationStandard
                };
                command.Parameters.Add(specification_standard_param);
                SqlParameter custodian_part_param = new SqlParameter
                {
                    ParameterName = "@custodian_part",
                    Value = qp.CustodianPart
                };
                command.Parameters.Add(custodian_part_param);
                SqlParameter material_type_param = new SqlParameter
                {
                    ParameterName = "@material_type",
                    Value = String.IsNullOrEmpty(qp.MaterialType) ? "" : qp.MaterialType.ToString()
                };
                command.Parameters.Add(material_type_param);
                SqlParameter manufacturer_parma = new SqlParameter
                {
                    ParameterName = "@manufacturer",
                    Value = qp.Manufacturer
                };
                command.Parameters.Add(manufacturer_parma);
                SqlParameter size_param = new SqlParameter
                {
                    ParameterName = "@size",
                    Value = String.IsNullOrEmpty(qp.Size) ? "" : qp.Size.ToString()
                };
                command.Parameters.Add(size_param);
                SqlParameter range_param = new SqlParameter
                {
                    ParameterName = "@range",
                    Value = String.IsNullOrEmpty(qp.Range) ? "" : qp.Range.ToString()
                };
                command.Parameters.Add(range_param);
                SqlParameter finish_param = new SqlParameter
                {
                    ParameterName = "@finish",
                    Value = String.IsNullOrEmpty(qp.Finish) ? "" : qp.Finish.ToString()
                };
                command.Parameters.Add(finish_param);
                SqlParameter custodian_parma = new SqlParameter
                {
                    ParameterName = "@custodian",
                    Value = qp.Custodian
                };
                command.Parameters.Add(custodian_parma);
                SqlParameter status_param = new SqlParameter
                {
                  ParameterName = "@status",
                  Value = qp.Status
                };
                command.Parameters.Add(status_param);
                SqlParameter competitor_price_param = new SqlParameter
                {
                    ParameterName = "@competitor_price",
                    Value = qp.CompetitorPrice
                };
                command.Parameters.Add(competitor_price_param);
                SqlParameter username_param = new SqlParameter
                {
                    ParameterName = "@username",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(username_param);
                try
                {
                    command.ExecuteNonQuery();
                    command.Dispose();
                    MessageBox.Show("Updated!");

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to update approved part.");
                    return View(qp);
                }

            }
            cnn.Close();

            return View(qp);
        }

        [HttpGet]
        public ActionResult EditCostFactor(CostFactors cf, UserModel users)
        {

            string factor_selected;
            var factor_selected_val = Request.Params["Factor_ID"];
            factor_selected = factor_selected_val;

            SqlConnection cnn;

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                //create workflow
                string sqlScript = @"select factor_id,item_number, description, [500_FACTOR], [1000_FACTOR], [2500_FACTOR], [5000_FACTOR]
                                    , [10000_FACTOR], [25000_FACTOR], [50000_FACTOR], [100000_FACTOR], COMMENTS, LAST_EDIT_INITIALS, LAST_EDIT_DATE 
                                 from master_cost where Factor_id=@factor_id; ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                //SqlParameter workflow_id_hist = new SqlParameter();
                SqlParameter factor_id_param = new SqlParameter
                {
                    ParameterName = "@factor_id",
                    Value = factor_selected
                };
                command.Parameters.Add(factor_id_param);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    try
                    {
                        cf.Factor_ID = reader.GetInt32(0);
                        cf.PartNumber = reader.GetString(1);
                        cf.Description = reader.GetString(2); //reader.IsDBNull(5) ? "" : (string)reader.GetValue(5);
                        cf.Factor_500 = reader.GetDecimal(3);
                        cf.Factor_1000 = reader.IsDBNull(4) ? 0 : (decimal)reader.GetValue(4);
                        cf.Factor_2500 = reader.GetDecimal(5);
                        cf.Factor_5000 = reader.GetDecimal(6);
                        cf.Factor_10000 = reader.GetDecimal(7);
                        cf.Factor_25000 = reader.GetDecimal(8);
                        cf.Factor_50000 = reader.GetDecimal(9);
                        cf.Factor_100000 = reader.GetDecimal(10);
                        cf.Comments = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                        cf.LastEditBy = reader.IsDBNull(12) ? "" : (string)reader.GetValue(12);
                        cf.LastEditDate = reader.GetDateTime(13);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Failed to find factors for your respective selection.");
                        //return View("~/Views/Home/CreateQuote.cshtml");
                    }
            }
            cnn.Close();

            return View(cf);
        }

        [HttpGet]
        public ActionResult EditMargin(ConfigureMargins cm, UserModel users)
        {

            string margin_selected;
            var margin_selected_val = Request.Params["MARGIN_ID"];
            margin_selected = margin_selected_val;

            SqlConnection cnn;

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                //create workflow
                string sqlScript = @"select margin_id, last_updated_by, last_updated_date, division_id, apply_division_id, division_id_applied, apply_value_stream, value_stream, 
                                apply_product_group, product_group, apply_product_code, product_code, apply_customer_number, customer_number, apply_internal_part_number
                                , internal_part_number, apply_customer_part_number, customer_part_number, apply_other, other, margin_value, quantity_break_range_start, quantity_break_range_end
                                 from config_margin where margin_id=@margin_id; ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                //SqlParameter workflow_id_hist = new SqlParameter();
                SqlParameter margin_id_param = new SqlParameter
                {
                    ParameterName = "@margin_id",
                    Value = margin_selected
                };
                command.Parameters.Add(margin_id_param);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    try
                    {
                        cm.margin_id = reader.GetInt32(0);
                        cm.division_id = reader.GetInt32(3);
                        cm.division_id_select = reader.GetInt32(4); //reader.IsDBNull(5) ? "" : (string)reader.GetValue(5);
                        cm.division_id_val = reader.GetInt32(5);
                        cm.value_stream_select = reader.GetInt32(6);
                        cm.value_stream_val = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                        cm.product_group_select = reader.GetInt32(8);
                        cm.product_group_val = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                        cm.product_code_select = reader.GetInt32(10);
                        cm.product_code_val = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                        cm.customer_number_select = reader.GetInt32(12);
                        cm.customer_number_val = reader.IsDBNull(13) ? "" : (string)reader.GetValue(13);
                        cm.internal_part_number_select = reader.GetInt32(14);
                        cm.internal_part_number_val = reader.IsDBNull(15) ? "" : (string)reader.GetValue(15);
                        cm.customer_part_number_select = reader.GetInt32(16);
                        cm.customer_part_number_val = reader.IsDBNull(17) ? "" : (string)reader.GetValue(17);
                        cm.other_select = reader.GetInt32(18);
                        cm.other_val = reader.IsDBNull(19) ? "" : (string)reader.GetValue(19);
                        cm.margin_value = reader.GetDecimal(20);
                        cm.quantity_break_range_start = reader.GetInt32(21);
                        cm.quantity_break_range_finish = reader.GetInt32(22);

                        //cm.Comments = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                        //cm.LastEditBy = reader.IsDBNull(12) ? "" : (string)reader.GetValue(12);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Failed to find margin selected.");
                        //return View("~/Views/Home/CreateQuote.cshtml");
                    }
            }
            cnn.Close();

            return View(cm);
        }

        [HttpPost]
        public ActionResult EditMargin(ConfigureMargins model)
        {

            SqlConnection cnn;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            int user_id = 0;
            using (cnn)
            {
                string sqlScript = "select user_id from users where user_name=@user_name ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter user_name = new SqlParameter
                {
                    ParameterName = "@user_name",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(user_name);
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        user_id = reader.GetInt32(0);
                    }
                    reader.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find the user's ID.");
                    return View();
                }
                reader.Close();
            }
            cnn.Close();

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                //create workflow
                string sqlScript = @"update m set LAST_UPDATED_BY=@LAST_UPDATED_BY, APPLY_VALUE_STREAM=@APPLY_VALUE_STREAM; ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                //SqlParameter factor_selected_param = new SqlParameter
                //{
                //    ParameterName = "@factor_id",
                //    Value = model.Va
                //};
                //command.Parameters.Add(factor_selected_param);
                SqlParameter username_param = new SqlParameter
                {
                    ParameterName = "@username",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(username_param);
                try
                {
                    command.ExecuteNonQuery();
                    command.Dispose();
                    MessageBox.Show("Updated!");

                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to update the configurable margin.");
                    return View(model);
                }
            }
            cnn.Close();
            return View("~/Views/Home/Configure.cshtml");

        }

        [HttpPost]
        public ActionResult EditStrategicPricing(StrategicPricingAdjustments model)
        {

            SqlConnection cnn;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            int user_id = 0;
            using (cnn)
            {
                string sqlScript = "select user_id from users where user_name=@user_name ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter user_name = new SqlParameter
                {
                    ParameterName = "@user_name",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(user_name);
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        user_id = reader.GetInt32(0);
                    }
                    reader.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find the user's ID.");
                    return View();
                }
                reader.Close();
            }
            cnn.Close();

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                //create workflow
                string sqlScript = @"update m set division=@division, APPLY_VALUE_STREAM=@APPLY_VALUE_STREAM from strategic_pricing_adjustments ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter username_param = new SqlParameter
                {
                    ParameterName = "@username",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(username_param);
                try
                {
                    command.ExecuteNonQuery();
                    command.Dispose();
                    MessageBox.Show("Updated!");

                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to update the strategic pricing adjustment.");
                    return View(model);
                }
            }
            cnn.Close();
            return View("~/Views/Home/Configure.cshtml");

        }

        [HttpPost]
        public ActionResult DeleteMargin(ConfigureMargins model)
        {

            SqlConnection cnn;

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                //create workflow
                string sqlScript = @"delete from config_margin where margin_id=@margin_id; ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                //SqlParameter workflow_id_hist = new SqlParameter();
                SqlParameter margin_id_param = new SqlParameter
                {
                    ParameterName = "@margin_id",
                    Value = model.margin_id
                };
                command.Parameters.Add(margin_id_param);
                //SqlDataReader reader = command.ExecuteReader();
                //while (reader.Read())
                    try
                    {
                    command.ExecuteNonQuery();
                    command.Dispose();
                    MessageBox.Show("Updated!");
                }
                    catch (Exception)
                    {
                        MessageBox.Show("Failed to find margin selected.");
                        //return View("~/Views/Home/CreateQuote.cshtml");
                    }
            }
            cnn.Close();

            return View("~/Views/Home/Configure.cshtml");
        }

        [HttpPost]
        public ActionResult DeleteStrategy(StrategicPricingAdjustments StrategicPricingAdjustments)
        {

            //var adjustment_id = Request.Params["ADJUSTMENT_ID"];
            var adjustment_id = StrategicPricingAdjustments.ADJUSTMENT_ID;
            
            SqlConnection cnn;

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                string sqlScript = @"delete from strategic_Pricing_adjustments where adjustment_id=@adjustment_id; ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter strategy_id_param = new SqlParameter
                {
                    ParameterName = "@adjustment_id",
                    Value = adjustment_id
                };
                command.Parameters.Add(strategy_id_param);
                try
                {
                    command.ExecuteNonQuery();
                    command.Dispose();
                    //MessageBox.Show("Updated!");
                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to find and delete strategic adjustment.");
                    //return View("~/Views/Home/CreateQuote.cshtml");
                }
            }
            cnn.Close();

            //return View("~/Views/Home/StrategicPricingAdjustments.cshtml", StrategicPricingAdjustments);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult DeleteQualification(QualifiedParts QualifiedParts)
        {

            //var adjustment_id = Request.Params["ADJUSTMENT_ID"];
            var product_code_id = QualifiedParts.Product_Code_ID;

            SqlConnection cnn;

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                string sqlScript = @"delete from approved_parts where product_code_id=@product_code_id; ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter product_code_id_param = new SqlParameter
                {
                    ParameterName = "@product_code_id",
                    Value = product_code_id
                };
                command.Parameters.Add(product_code_id_param);
                try
                {
                    command.ExecuteNonQuery();
                    command.Dispose();
                    //MessageBox.Show("Updated!");
                }
                catch (Exception)
                {
                    MessageBox.Show("Failed.");
                    //return View("~/Views/Home/CreateQuote.cshtml");
                }
            }
            cnn.Close();

            //return View("~/Views/Home/StrategicPricingAdjustments.cshtml", StrategicPricingAdjustments);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }



        [HttpPost]
        public ActionResult EditCostFactor(CostFactors cf)
        {

            

            SqlConnection cnn;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            string factor_selected;
            var factor_selected_val = Request.Params["Factor_ID"];
            factor_selected = factor_selected_val;

            int charLocation = factor_selected.IndexOf(",", StringComparison.Ordinal);
            if (charLocation > 0)
            {
                factor_selected = factor_selected.Substring(0, charLocation);
            }
            cf.Factor_ID = Convert.ToInt32(factor_selected);

            //part_selected =left(part_selected,)

            cnn.Open();
            int user_id = 0;
            using (cnn)
            {
                string sqlScript = "select user_id from users where user_name=@user_name ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter user_name = new SqlParameter
                {
                    ParameterName = "@user_name",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(user_name);
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        user_id = reader.GetInt32(0);
                    }
                    reader.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find the user's ID.");
                    return View();
                }
                reader.Close();
            }
            cnn.Close();

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                //create workflow
                string sqlScript = @"update m SET [500_FACTOR]=@500_factor,
                                [1000_FACTOR]=@1000_factor, [2500_FACTOR]=@2500_factor, [5000_FACTOR]=@5000_factor
                                    , [10000_FACTOR]=@10000_factor, [25000_FACTOR]=@25000_factor, [50000_FACTOR]=@50000_factor
                                    , [100000_FACTOR]=@100000_factor, COMMENTS=@comment, LAST_EDIT_INITIALS=@username, LAST_EDIT_DATE=GetDate()
                                 from master_cost m where Factor_id=@factor_id; ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter factor_selected_param = new SqlParameter
                {
                    ParameterName = "@factor_id",
                    Value = factor_selected
                };
                command.Parameters.Add(factor_selected_param);
                SqlParameter factor_500_param = new SqlParameter
                {
                    ParameterName = "@500_factor",
                    Value = cf.Factor_500
                };
                command.Parameters.Add(factor_500_param);
                SqlParameter factor_1000_param = new SqlParameter
                {
                    ParameterName = "@1000_factor",
                    Value = cf.Factor_1000
                };
                command.Parameters.Add(factor_1000_param);
                SqlParameter factor_2500_param = new SqlParameter
                {
                    ParameterName = "@2500_factor",
                    Value = cf.Factor_2500
                };
                command.Parameters.Add(factor_2500_param);
                SqlParameter factor_5000_param = new SqlParameter
                {
                    ParameterName = "@5000_factor",
                    Value = cf.Factor_5000
                };
                command.Parameters.Add(factor_5000_param);
                SqlParameter factor_10000_param = new SqlParameter
                {
                    ParameterName = "@10000_factor",
                    Value = cf.Factor_10000
                };
                command.Parameters.Add(factor_10000_param);
                SqlParameter factor_25000_param = new SqlParameter
                {
                    ParameterName = "@25000_factor",
                    Value = cf.Factor_25000
                };
                command.Parameters.Add(factor_25000_param);
                SqlParameter factor_50000_param = new SqlParameter
                {
                    ParameterName = "@50000_factor",
                    Value = cf.Factor_50000
                };
                command.Parameters.Add(factor_50000_param);
                SqlParameter factor_100000_param = new SqlParameter
                {
                    ParameterName = "@100000_factor",
                    Value = cf.Factor_100000
                };
                command.Parameters.Add(factor_100000_param);
                SqlParameter comment_param = new SqlParameter
                {
                    ParameterName = "@comment",
                    Value = String.IsNullOrEmpty(cf.Comments) ? "" : cf.Comments.ToString()
                };
                command.Parameters.Add(comment_param);
                SqlParameter username_param = new SqlParameter
                {
                    ParameterName = "@username",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(username_param);
                try
                {
                    command.ExecuteNonQuery();
                    command.Dispose();
                    MessageBox.Show("Updated!");

                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to update the cost factor of the part selected.");
                    return View(cf);
                }
            }
            cnn.Close();
            return View(cf);

        }

        [HttpPost]
        public ActionResult UpdateCostFactorInPricingTool(GenerateQuoteHeader cf)
        {
            //update cost factor based on update button

            SqlConnection cnn;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                //create workflow
                string sqlScript = @"update m SET [500_FACTOR]=@500_factor,
                                [1000_FACTOR]=@1000_factor, [2500_FACTOR]=@2500_factor, [5000_FACTOR]=@5000_factor
                                    , [10000_FACTOR]=@10000_factor, [25000_FACTOR]=@25000_factor, [50000_FACTOR]=@50000_factor
                                    , [100000_FACTOR]=@100000_factor, LAST_EDIT_INITIALS=@username, LAST_EDIT_DATE=GetDate()
                                 from master_cost m where item_number=@part_num; ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);

                SqlParameter part_num_param = new SqlParameter
                {
                    ParameterName = "@part_num",
                    Value = cf.PartNumber
                };
                command.Parameters.Add(part_num_param);
                SqlParameter factor_500_param = new SqlParameter
                {
                    ParameterName = "@500_factor",
                    Value = cf.factor_adj_500
                };
                command.Parameters.Add(factor_500_param);
                SqlParameter factor_1000_param = new SqlParameter
                {
                    ParameterName = "@1000_factor",
                    Value = cf.factor_adj_1000
                };
                command.Parameters.Add(factor_1000_param);
                SqlParameter factor_2500_param = new SqlParameter
                {
                    ParameterName = "@2500_factor",
                    Value = cf.factor_adj_2500
                };
                command.Parameters.Add(factor_2500_param);
                SqlParameter factor_5000_param = new SqlParameter
                {
                    ParameterName = "@5000_factor",
                    Value = cf.factor_adj_5000
                };
                command.Parameters.Add(factor_5000_param);
                SqlParameter factor_10000_param = new SqlParameter
                {
                    ParameterName = "@10000_factor",
                    Value = cf.factor_adj_10000
                };
                command.Parameters.Add(factor_10000_param);
                SqlParameter factor_25000_param = new SqlParameter
                {
                    ParameterName = "@25000_factor",
                    Value = cf.factor_adj_25000
                };
                command.Parameters.Add(factor_25000_param);
                SqlParameter factor_50000_param = new SqlParameter
                {
                    ParameterName = "@50000_factor",
                    Value = cf.factor_adj_50000
                };
                command.Parameters.Add(factor_50000_param);
                SqlParameter factor_100000_param = new SqlParameter
                {
                    ParameterName = "@100000_factor",
                    Value = cf.factor_adj_100000
                };
                command.Parameters.Add(factor_100000_param);
                //SqlParameter comment_param = new SqlParameter();
                //comment_param.ParameterName = "@comment";
                //comment_param.Value = String.IsNullOrEmpty(cf.Comments) ? "" : cf.Comments.ToString();
                //command.Parameters.Add(comment_param);
                SqlParameter username_param = new SqlParameter
                {
                    ParameterName = "@username",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(username_param);
                try
                {
                    command.ExecuteNonQuery();
                    command.Dispose();
                    MessageBox.Show("Updated!");

                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to update the factors associated with this part.");
                    return View(cf);
                }
            }
            cnn.Close();
            return new HttpStatusCodeResult(HttpStatusCode.OK);

        }


        [HttpGet]
        public ActionResult LTAModification()
        {

            //ViewBag.Message = "Your LTA Information.";

            LTAModifcations lta = new LTAModifcations();
            SqlConnection cnn;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            
            SharePointConnectionUpdated dc = new SharePointConnectionUpdated();
  
            var costs = (from m in dc.vT_Sales_LTAModifications
                         select new LTAModifcations
                         {
                             LTA_ID = m.lta_id,
                             Division = lta.Division,
                             Customer_Name = m.abalph,
                             Customer_Part_Number = m.item_number_3,
                             Internal_Part_Number = m.item_number_2 ?? "",
                             //lta.Price= reader.IsDBNull(8) ? "" : (decimal)reader.GetValue(8);
                             Type = m.type,
                             StartDate = m.effective_start_Date.Value,
                             EndDate = m.effective_end_Date.Value,
                             Price = m.price,
                             UOM = m.uom,
                             AMU = m.customer_amu != null ? m.customer_amu : 0,
                             Open_Sales_Orders = m.Open_Quantity,
                             Total_Forecast = m.total_forecast != null ? m.total_forecast : 0
                         }).ToList();

            return View(costs);

        }

        [HttpGet]
        public ActionResult LTAEdit()
        {

            string lta_id_selected;
            var lta_id_selected_val = Request.Params["LTA_ID"];
            lta_id_selected = lta_id_selected_val;

            SqlConnection cnn;

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            LTAModifcations lta = new LTAModifcations();

            cnn.Open();
            using (cnn)
            {
                //create workflow
                string sqlScript = @"select u.lta_id, u.Customer_Number, u.Item_Number_3, u.Item_Number_2
                            , u.Type
                            , u.Effective_Start_Date
                            , u.Effective_End_Date
                            , u.Price
                            , u.UOM
                            , u.Customer_AMU
                            , u.Total_Forecast
                            , us.[USER_NAME] Username
                            , u.Last_Updated
                            from iT_Sales_LTAUpload u 
                            left outer join users us on u.last_updated_by=us.[USER_ID]
                            where LTA_ID=@LTA_ID; ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                //SqlParameter workflow_id_hist = new SqlParameter();
                SqlParameter lta_id_param = new SqlParameter
                {
                    ParameterName = "@LTA_ID",
                    Value = lta_id_selected
                };
                command.Parameters.Add(lta_id_param);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    try
                    {
                        lta.LTA_ID = reader.GetInt32(0);
                        lta.Customer_ID = reader.GetInt32(1);
                        lta.Customer_Part_Number = reader.GetString(2); //reader.IsDBNull(5) ? "" : (string)reader.GetValue(5);
                        lta.Internal_Part_Number = reader.GetString(3);
                        lta.Type = reader.GetString(4);
                        lta.StartDate = reader.GetDateTime(5);
                        lta.EndDate = reader.GetDateTime(6);
                        lta.Price = reader.GetDecimal(7);
                        lta.UOM = reader.GetString(8);
                        lta.AMU = reader.GetInt32(9);
                        lta.Total_Forecast = reader.GetInt32(10);
                        //lta.Total_Forecast=reader.IsDBNull(10) ? "" : (string)reader.GetInt32(10);
                        lta.Last_Updated_By_Name = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                        lta.Last_Updated_Date = reader.GetDateTime(12);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Failed load LTA inforamtion.");
                        //return View("~/Views/Home/CreateQuote.cshtml");
                    }
            }
            cnn.Close();

            return View(lta);
        }

        [HttpGet]
        public ActionResult AddLTA()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddLTA(LTAModifcations lta)
        {

            SqlConnection cnn;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            

            cnn.Open();
            int user_id = 0;
            using (cnn)
            {
                string sqlScript = "select user_id from users where user_name=@user_name ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter user_name = new SqlParameter
                {
                    ParameterName = "@user_name",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(user_name);
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        user_id = reader.GetInt32(0);
                    }
                    reader.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find the user's ID to add this contractural agreement.");
                    return View();
                }
                reader.Close();
            }
            cnn.Close();

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                //    //create workflow
                string sqlScript = @"insert into iT_Sales_LTAUpload (division_id, customer_number, item_number_3, item_number_2, type, effective_start_date, effective_end_date, price
                                ,UOM, customer_amu, total_forecast, last_updated,last_updated_by ) values (@division_id,@customer_ID,@customer_part_num, @internal_part_num, @type
                                    , @start_date,@end_date
                                        , @price
                                         , @uom
                                         , @amu, @total_forecast
                                         , getdate(), @user_id); ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter division_id = new SqlParameter
                {
                    ParameterName = "@division_id",
                    Value = lta.Division
                };
                command.Parameters.Add(division_id);
                SqlParameter customer_id_param = new SqlParameter
                {
                    ParameterName = "@customer_id",
                    Value = lta.Customer_ID
                };
                command.Parameters.Add(customer_id_param);
                SqlParameter customer_part_num_param = new SqlParameter
                {
                    ParameterName = "@customer_part_num",
                    Value = lta.Customer_Part_Number
                };
                command.Parameters.Add(customer_part_num_param);
                SqlParameter internal_part_num_param = new SqlParameter
                {
                    ParameterName = "@internal_part_num",
                    Value = lta.Internal_Part_Number
                };
                command.Parameters.Add(internal_part_num_param);
                SqlParameter type_param = new SqlParameter
                {
                    ParameterName = "@type",
                    Value = lta.Type
                };
                command.Parameters.Add(type_param);
                SqlParameter start_date_param = new SqlParameter
                {
                    ParameterName = "@start_date",
                    Value = lta.StartDate
                };
                command.Parameters.Add(start_date_param);
                SqlParameter end_date_param = new SqlParameter
                {
                    ParameterName = "@end_date",
                    Value = lta.EndDate
                };
                command.Parameters.Add(end_date_param);
                SqlParameter price_param = new SqlParameter
                {
                    ParameterName = "@price",
                    Value = lta.Price
                };
                command.Parameters.Add(price_param);
                SqlParameter uom_param = new SqlParameter
                {
                    ParameterName = "@uom",
                    Value = lta.UOM
                };
                command.Parameters.Add(uom_param);
                SqlParameter customer_amu_param = new SqlParameter
                {
                    ParameterName = "@AMU",
                    //Value = String.IsNullOrEmpty(lta.customer_amu_param) ? "" : lta.customer_amu_param.ToString()
                    Value = lta.AMU
                };
                command.Parameters.Add(customer_amu_param);
                SqlParameter total_forecast_param = new SqlParameter
                {
                    ParameterName = "@total_forecast",
                    Value = lta.Total_Forecast
                };
                command.Parameters.Add(total_forecast_param);
                SqlParameter user_id_param = new SqlParameter
                {
                    ParameterName = "@user_id",
                    Value = user_id
                };
                command.Parameters.Add(user_id_param);
                try
                {
                    command.ExecuteNonQuery();
                    command.Dispose();
                    MessageBox.Show("Added!");

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to add the contract. Please see your system administrator.");
                    return View(lta);
                }
            }
            cnn.Close();
            return View(lta);
        }

        [HttpPost]
        public ActionResult LTAEdit(LTAModifcations lta)
        {

            SqlConnection cnn;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            string lta_selected;
            var lta_selected_val = Request.Params["LTA_ID"];
            lta_selected = lta_selected_val;

            int charLocation = lta_selected.IndexOf(",", StringComparison.Ordinal);
            if (charLocation > 0)
            {
                lta_selected = lta_selected.Substring(0, charLocation);
            }
            lta.LTA_ID = Convert.ToInt32(lta_selected);

            cnn.Open();
            int user_id = 0;
            using (cnn)
            {
                string sqlScript = "select user_id from users where user_name=@user_name ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter user_name = new SqlParameter
                {
                    ParameterName = "@user_name",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(user_name);
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        user_id = reader.GetInt32(0);
                    }
                    reader.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not find the user's ID to update this contractural agreement.");
                    return View();
                }
                reader.Close();
            }
            cnn.Close();

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                //    //create workflow
                string sqlScript = @"update iT_Sales_LTAUpload set customer_number=@customer_ID, Item_Number_3=@customer_part_num, Item_Number_2=@internal_part_num, [type]=@type
                                    , Effective_Start_Date=@start_date, Effective_End_Date=@end_date
                                        , price=@price
                                         , uom=@uom
                                         , customer_amu=@amu
                                         , last_updated=getdate(), last_updated_by=@user_id from iT_Sales_LTAUpload where lta_id=@lta_id; ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter lta_selected_param = new SqlParameter
                {
                    ParameterName = "@lta_id",
                    Value = lta_selected
                };
                command.Parameters.Add(lta_selected_param);
                SqlParameter customer_id_param = new SqlParameter
                {
                    ParameterName = "@customer_id",
                    Value = lta.Customer_ID
                };
                command.Parameters.Add(customer_id_param);
                SqlParameter customer_part_num_param = new SqlParameter
                {
                    ParameterName = "@customer_part_num",
                    Value = lta.Customer_Part_Number
                };
                command.Parameters.Add(customer_part_num_param);
                SqlParameter internal_part_num_param = new SqlParameter
                {
                    ParameterName = "@internal_part_num",
                    Value = lta.Internal_Part_Number
                };
                command.Parameters.Add(internal_part_num_param);
                SqlParameter type_param = new SqlParameter
                {
                    ParameterName = "@type",
                    Value = lta.Type
                };
                command.Parameters.Add(type_param);
                SqlParameter start_date_param = new SqlParameter
                {
                    ParameterName = "@start_date",
                    Value = lta.StartDate
                };
                command.Parameters.Add(start_date_param);
                SqlParameter end_date_param = new SqlParameter
                {
                    ParameterName = "@end_date",
                    Value = lta.EndDate
                };
                command.Parameters.Add(end_date_param);
                SqlParameter price_param = new SqlParameter
                {
                    ParameterName = "@price",
                    Value = lta.Price
                };
                command.Parameters.Add(price_param);
                SqlParameter uom_param = new SqlParameter
                {
                    ParameterName = "@uom",
                    Value = lta.UOM
                };
                command.Parameters.Add(uom_param);
                SqlParameter customer_amu_param = new SqlParameter
                {
                    ParameterName = "@AMU",
                    //Value = String.IsNullOrEmpty(lta.customer_amu_param) ? "" : lta.customer_amu_param.ToString()
                    Value = lta.AMU
                };
                command.Parameters.Add(customer_amu_param);
                SqlParameter user_id_param = new SqlParameter
                {
                    ParameterName = "@user_id",
                    Value = user_id
                };
                command.Parameters.Add(user_id_param);
                try
                {
                    command.ExecuteNonQuery();
                    command.Dispose();
                    MessageBox.Show("Updated!");

                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to update the contract in question.");
                    return View(lta);
                }
            }
            cnn.Close();
            return View(lta);
        }



        //if (ModelState.IsValid)
        //{
        //    List<CostYears> Costings = new List<CostYears>();

        //    SqlConnection cnn;
        //    cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
        //    cnn.Open();
        //    using (SharePointConnectionUpdated db = new SharePointConnectionUpdated())
        //    {

        //        //dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString);
        //        //cnn.Open();
        //        //using (dbConnection)
        //        //{
        //        string sqlScript = "select cost_year_id, cost_year from dT_Finance_CostYearLookup ";
        //        SqlCommand command = new SqlCommand(sqlScript, cnn);
        //        SqlDataReader reader;
        //        reader = command.ExecuteReader();
        //        try
        //        {

        //            while (reader.Read())
        //            {

        [HttpPost]
        public ActionResult DeleteLTAs()
        {

            SqlConnection cnn;
            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);

            cnn.Open();
            using (cnn)
            {
                //create workflow
                string sqlScript = @"delete from iT_Sales_LTAUpload; ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                try
                {
                    command.ExecuteNonQuery();
                    command.Dispose();
                    MessageBox.Show("Deleted!");

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to update the factors associated with this part.");
                    return View("~/Home/LTAModification.cshtml");
                }
            }
            cnn.Close();

            return new HttpStatusCodeResult(HttpStatusCode.OK);

        }

        [HttpPost]
        public ActionResult OriginalPricingTool(OriginalPricingTool model, UserModel users)
        {
            string item_num;
            string cost_reference_id;
            string division;
            decimal adj_amount = 0;

            int cost_year_id;
            cost_year_id = 0;

            if (string.IsNullOrEmpty(Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)) == true)
            {
                Response.Write("<script>window.close();</script>");
                return View("~/Views/Home/Index.cshtml");
            }
            model.GenerateQuoteHeader.Username = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1);

            try
            {
                OdbcConnection cnn_odbc;
                cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString);
                cnn_odbc.Open();
                cnn_odbc.Dispose();
                cnn_odbc.Close();
            }
            catch
            {
                MessageBox.Show("Failed to connect to JDE. Please try again. If this problem persists, please check your connection or consult your system administrator.");
                return View();
            }

            //string qq_commas = model.GenerateQuoteHeader.MultipleQuoteQuantity.ToString();
            
            //if (qq_commas.IndexOf(',')>0)
            //{
            //    Int32 count = 1;
            //    String[] separator = { "," };
            //    String[] qty_list = qq_commas.Split(separator, count, StringSplitOptions.RemoveEmptyEntries);
            //    foreach(String qq in qty_list)
            //    {
            //        model.GenerateQuoteHeader.QuoteQuantity = Convert.ToInt32(qq);
            //    }
            //}
            //else
            //{
                model.GenerateQuoteHeader.QuoteQuantity = Convert.ToInt32(model.GenerateQuoteHeader.MultipleQuoteQuantity);
            //}

            

            if (model.GenerateQuoteHeader.PartNumber != null)
            {

                model.GenerateQuoteHeader.PartNumber = model.GenerateQuoteHeader.PartNumber.Trim();
                model.GenerateQuoteHeader.CustomerPartNum = null;


                if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                {
                    using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                    {
                        cnn.Open();

                        model.GenerateQuoteHeader.Costing = this.GetCostYearList(cnn);
                        var cost_vars = model.GenerateQuoteHeader.Costing.Find(m => m.Value == model.GenerateQuoteHeader.CostYearDesc.ToString());
                        cost_year_id = Convert.ToInt32(model.GenerateQuoteHeader.CostYearDesc);

                    }
                }
            }
            else
            {

                model.GenerateQuoteHeader.PartNumber = null;

                using (OdbcConnection cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString))
                {
                    cnn_odbc.Open();

                    string sqlScript;
                    sqlScript = @"select	distinct iblitm Part, ibitm item_id
                                        from PRODDTA.f4102 ib join PRODDTA.f4101 itm on ibitm = imitm
		                              left outer join proddta.F4104 xref on ibitm=xref.ivitm
                                        where  1=1
                                        and (xref.ivcitm=? Or ibaitm=? )  ";
                    using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                    {
                        command.Parameters.Add(new OdbcParameter("@item_num", model.GenerateQuoteHeader.CustomerPartNum));
                    command.Parameters.Add(new OdbcParameter("@item_num2", model.GenerateQuoteHeader.CustomerPartNum));
            //command.Parameters.Add(new OdbcParameter("@item_num", model.GenerateQuoteHeader.CustomerPartNum));
            string item_num_pass = model.GenerateQuoteHeader.CustomerPartNum;
                        try
                        {
                            using (var reader = command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        model.GenerateQuoteHeader.PartNumber = reader.IsDBNull(0) ? "" : (string)reader.GetValue(0);
                                        model.GenerateQuoteHeader.Item_ID = reader.GetInt32(1);
                  }
                                    reader.Close();
                                }
                                else if (model.GenerateQuoteHeader.CustomerPartNum == "")
                                {
                                    MessageBox.Show("No item selected. Please try again.");
                                    //MessageBox.Show("The connection is step 1: ");
                                    cnn_odbc.Close();
                                    cnn_odbc.Dispose();
                                    GenerateQuoteHeader gqh = new GenerateQuoteHeader();
                                    //gqh.LoadingPricingValue = 0;

                                    gqh.UnitCost = 0;
                                    gqh.UnitPrice = 0;

                                    //SqlConnection cnn;
                                    if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                                    {
                                        using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                                        {
                                            cnn.Open();
                                            gqh.Costing = this.GetCostYearList(cnn);
                                        }
                                    }
                                    var RefreshModel = new OriginalPricingTool
                                    {
                                        GenerateQuoteHeader = gqh,
                                        SalesOrders = null,
                                        Competitors = null,
                                        Components = null,
                                        DemandList = null,
                                        Comments = null,
                                        LTAs = null,
                                      LTAParts = null
                                    };
                                    MessageBox.Show("About to refresh step 1.");
                                    return View(RefreshModel);
                                }
                                else
                                {
                                    try
                                    {
                                        SqlConnection cnn;
                                        if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                                        {
                                            using (SqlConnection cnn_sql_con = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                                            {
                                                cnn_sql_con.Open();
                                                model.GenerateQuoteHeader.Costing = this.GetCostYearList(cnn_sql_con);
                                            }
                                        }

                                        SharePointConnectionUpdated dc = new SharePointConnectionUpdated();
                                        List<PartDescription> pd = new List<PartDescription>();

                                        pd = (from m in dc.sT_PartDetails_JDE
                                              where m.IBLITM.StartsWith(model.GenerateQuoteHeader.PartNumber)
                                              where m.IBLITM.Contains("*OP") == false
                                              orderby m.IBLITM
                                              select new PartDescription
                                              {
                                                  PartID = m.ID,
                                                  PartNumber = m.IBLITM,
                                                  PartDesc = m.IMDSC1
                                              }).ToList();

                                        MessageBox.Show("at the part description analysis step 1");
                                        return View("PartDescriptionAnalysis", pd);
                                        //mark me
                                    }
                                    catch
                                    {
                                        MessageBox.Show("failed to load part desc analysis step 1.1");
                                    }
                                }

                            }
                        }
                        catch (Exception ex)
                        {

                            MessageBox.Show("Item does not exist. Is this part an internal part nunber? Please try again.");
                            //MessageBox.Show("The connection is step 1: ");
                            cnn_odbc.Close();
                            cnn_odbc.Dispose();
                            GenerateQuoteHeader gqh = new GenerateQuoteHeader();
                            //gqh.LoadingPricingValue = 0;

                            gqh.UnitCost = 0;
                            gqh.UnitPrice = 0;

                            //SqlConnection cnn;
                            if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                            {
                                using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                                {
                                    cnn.Open();
                                    gqh.Costing = this.GetCostYearList(cnn);
                                }
                            }
                            var RefreshModel = new OriginalPricingTool
                            {
                                GenerateQuoteHeader = gqh,
                                SalesOrders = null,
                                Competitors = null,
                                Components = null,
                                DemandList = null,
                                Comments = null,
                                LTAs = null,
                              LTAParts = null
                            };
                            return View(RefreshModel);
                        }
                    }

                    //var cost_refs = model.GenerateQuoteHeader.CostReferenceList.Find(c => c.Value == model.GenerateQuoteHeader.Cost_ID.ToString());
                }

            }


            item_num = model.GenerateQuoteHeader.PartNumber;
      //int item_id = model.GenerateQuoteHeader.Item_ID;
            division = model.GenerateQuoteHeader.Division;

            users.PartNumber = item_num;

            model.GenerateQuoteHeader.PartNumber = model.GenerateQuoteHeader.PartNumber;
            model.GenerateQuoteHeader.Division = model.GenerateQuoteHeader.Division;

            List<LTAInfo> LTAs = new List<LTAInfo>();
      List<LTAPart> LTAParts = new List<LTAPart>();
      List<Quote> quotes = new List<Quote>();
            List<ComponentFound> Components = new List<ComponentFound>();
            List<CurrentLoadByWorkorder> cl = new List<CurrentLoadByWorkorder>();
            List<SalesOrder> model2 = new List<SalesOrder>();
            List<Comment> Comments = new List<Comment>();
            List<ActualCost> ActCost = new List<ActualCost>();
            List<Competitor> Comps = new List<Competitor>();
            List<CustomerPart> CustParts = new List<CustomerPart>();
            List<QuoteWorkBench> quoteworkbench = new List<QuoteWorkBench>();
            List<QuoteWorkbenchItems> bench = new List<QuoteWorkbenchItems>();


            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                if (cnn.ConnectionString != null)
                {
                    using (cnn)
                    {
                        cnn.Open();

                        model.GenerateQuoteHeader.Costing = this.GetCostYearList(cnn);
                        var cost_vars = model.GenerateQuoteHeader.Costing.Find(m => m.Value == model.GenerateQuoteHeader.CostYearDesc.ToString());
                        cost_year_id = Convert.ToInt32(model.GenerateQuoteHeader.CostYearDesc);
                    }

                }

                else
                {
                    //SqlConnection cnn = new SqlConnection();
                    model.GenerateQuoteHeader.Costing = this.GetCostYearList(cnn);
                    //model.GenerateQuoteHeader.Costing = this.GetCostYearList(cnn);
                    var cost_vars = model.GenerateQuoteHeader.Costing.Find(m => m.Value == model.GenerateQuoteHeader.CostYearDesc.ToString());
                    cost_year_id = Convert.ToInt32(model.GenerateQuoteHeader.CostYearDesc);

                }

                if (cost_year_id == 1)
                {
                    cost_reference_id = "07";
                }
                else
                {
                    cost_reference_id = "P1";
                }
                cnn.Dispose();
                cnn.Close();
            }

            using (OdbcConnection cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString))
            {
                cnn_odbc.Open();

                //check the division
                string sqlScript = @"select	distinct	trim(ibmcu), ibitm
			                from 		proddta.f4102 itm left outer join proddta.f3002 on ixitm = ibitm and ixmmcu = ibmcu and left(ibglpt,2) in ('FG','CP','RM') 
							                where 1=1 and trim(ibmcu) in ('710000','730000','720000','750000','780000','734000')
			                and 		iblitm = ?
			                 fetch first row only";
                using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                {
                    command.Parameters.Add(new OdbcParameter("@item_num", item_num));
                    //command.Parameters.Add(new OdbcParameter("@division", model.GenerateQuoteHeader.Division));
                    //command.Parameters.Add(new OdbcParameter("@cost_id", cost_reference_id));
                    using (var reader = command.ExecuteReader())
                    {
                        try
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    model.GenerateQuoteHeader.Division = reader.GetString(0);
                  model.GenerateQuoteHeader.Item_ID = reader.GetInt32(1);
                                }
                            }

                            else if (item_num.Replace(" ", String.Empty) is null)
                            {
                                try
                                {
                                    reader.Close();
                                    //cnn_odbc.Close();

                                    sqlScript = @"select CASE WHEN UPPER(trim(look2.drdl01))='BOLT' then '710000'  
	                                                    when upper(trim(look2.drdl01))='SCREWS' then '710000'
	                                                    when upper(trim(look2.drdl01))='SPECIALS' then '710000'
	                                                    when upper(trim(look2.drdl01))='SRB RING' then '710000'
	                                                    when upper(trim(look2.drdl01))='STUDS' 
	                                                    then '710000' ELSE '730000' END product_code
                                                                from proddta.f4102 join proddta.f4101 on ibitm = imitm
                                                             left outer join prodctl.f0005 look on trim(look.drky)= ibsrp3
                                                               left outer join prodctl.f0005 look2 on trim(look2.drky)= ibsrp2
                                                                where ibitm = ?
                                                            and look.drsy = '41'
                                                             and look.drrt = 'S3'
                                                            and look2.drsy = '41'
                                                         fetch first row only";
                                    using (var command_x = new OdbcCommand(sqlScript, cnn_odbc))
                                    {
                                        command_x.Parameters.Add(new OdbcParameter("@item_id", model.GenerateQuoteHeader.Item_ID));

                                        using (var reader_x = command_x.ExecuteReader())
                                        {
                                            if (reader_x.HasRows)
                                            {
                                                if (reader_x.Read())
                                                {
                                                    model.GenerateQuoteHeader.Division = reader_x.GetString(0);
                                                    //model.GenerateQuoteHeader.ProductGroup = reader_z.GetString(0);
                                                    //model.GenerateQuoteHeader.UserID = reader.GetInt32(2);
                                                }

                                                reader_x.Close();
                                            }
                                            else
                                            {
                                                MessageBox.Show("Your username and password do not match, this part doesn't exist, or this is a different type of part.");
                                                cnn_odbc.Close();
                                                cnn_odbc.Dispose();
                                                GenerateQuoteHeader gqh = new GenerateQuoteHeader();
                                                //gqh.LoadingPricingValue = 0;

                                                gqh.UnitCost = 0;
                                                gqh.UnitPrice = 0;

                                                //SqlConnection cnn;
                                                if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                                                {
                                                    using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                                                    {
                                                        cnn.Open();
                                                        gqh.Costing = this.GetCostYearList(cnn);
                                                    }
                                                }
                                                var RefreshModel = new OriginalPricingTool
                                                {
                                                    GenerateQuoteHeader = gqh,
                                                    SalesOrders = null,
                                                    Competitors = null,
                                                    Components = null,
                                                    DemandList = null,
                                                    Comments = null,
                                                    LTAs = null,
                                                  LTAParts = null
                                                };
                                                return View(RefreshModel);
                                            }
                                        }
                                        cnn_odbc.Close();
                                    }
                                }



                                catch (Exception ex)
                                {

                                    try
                                    {

                                        cnn_odbc.Close();
                                        using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                                        {
                                            cnn.Open();
                                            sqlScript = "select o.division, o.org_id, u.user_id from users u left outer join ORGANIZATION o on u.ORG_ID = o.ORG_ID where u.user_name = @username";
                                            using (var command_x = new SqlCommand(sqlScript, cnn))
                                            {
                                                command_x.Parameters.Add(new SqlParameter("@username", Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)));

                                                using (var reader_x = command_x.ExecuteReader())
                                                {
                                                    if (reader_x.HasRows)
                                                    {
                                                        while (reader_x.Read())
                                                        {
                                                            model.GenerateQuoteHeader.Division = reader_x.GetString(0);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        MessageBox.Show("No users found matching this criteria.");
                                                        return View();
                                                    }
                                                    reader_x.Close();
                                                }
                                            }
                                            cnn.Close();
                                        }

                                    }
                                    catch
                                    {
                                        MessageBox.Show("Please send this part to your system administrator.");
                                        cnn_odbc.Close();
                                        cnn_odbc.Dispose();
                                        GenerateQuoteHeader gqh = new GenerateQuoteHeader();
                                        //gqh.LoadingPricingValue = 0;

                                        gqh.UnitCost = 0;
                                        gqh.UnitPrice = 0;

                                        //SqlConnection cnn;
                                        if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                                        {
                                            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                                            {
                                                cnn.Open();
                                                gqh.Costing = this.GetCostYearList(cnn);
                                            }
                                        }
                                        var RefreshModel = new OriginalPricingTool
                                        {
                                            GenerateQuoteHeader = gqh,
                                            SalesOrders = null,
                                            Competitors = null,
                                            Components = null,
                                            DemandList = null,
                                            Comments = null,
                                            LTAs = null,
                                            LTAParts = null
                                        };
                                        return View(RefreshModel);

                                    }
                                }
                            }
                            else
                            {
                                try {
                                    //SqlConnection cnn_sql_con;
                                    if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                                    {
                                        using (SqlConnection cnn_sql_con = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                                        {
                                            cnn_sql_con.Open();
                                            model.GenerateQuoteHeader.Costing = this.GetCostYearList(cnn_sql_con);
                                        }
                                    }

                                    //    cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionUpdated"].ConnectionString);
                                    SharePointConnectionUpdated dc = new SharePointConnectionUpdated();
                                    List<PartDescription> pd = new List<PartDescription>();

                                    pd = (from m in dc.sT_PartDetails_JDE
                                          where m.IBLITM.StartsWith(model.GenerateQuoteHeader.PartNumber)
                                          where m.IBLITM.Contains("*OP") == false
                                          orderby m.IBLITM
                                          select new PartDescription
                                          {
                                              PartID = m.ID,
                                              PartNumber = m.IBLITM,
                                              PartDesc = m.IMDSC1
                                          }).ToList();
                                    return View("PartDescriptionAnalysis", pd);
                                }
                                catch (Exception ex)
                                {
                                    //MessageBox.Show(ex.ToString());
                                    MessageBox.Show("failed to load 2.1");
                                }

                            }
                            reader.Close();
                        }
                        catch
                        {
                            reader.Close();
                            cnn_odbc.Close();
                            cnn_odbc.Dispose();

                        }
                    }
                }
                cnn_odbc.Close();
                cnn_odbc.Dispose();
            }


      using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
      {
        cnn.Open();

        string sqlScript = @"select o.division, o.org_id, u.user_id, r.ROLE_DESC
                                    from users u
                                    left outer join ORGANIZATION o on u.ORG_ID = o.ORG_ID
                                    left outer join ROLES r on u.ROLE_ID = r.ROLE_ID
                                    where u.user_name =@username";
        using (var command = new SqlCommand(sqlScript, cnn))
        {
          command.Parameters.Add(new SqlParameter("@username", Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)));

          using (var reader = command.ExecuteReader())
          {
            if (reader.HasRows)
            {
              if (reader.Read())
              {
                model.GenerateQuoteHeader.UserID = reader.GetInt32(2);
                model.GenerateQuoteHeader.RoleDesc = reader.GetString(3);
              }
            }
            else
            {
              MessageBox.Show("Your username password don't match or you are not VPN'd into our network.");
              cnn.Close();
              cnn.Dispose();
              GenerateQuoteHeader gqh = new GenerateQuoteHeader();
              //gqh.LoadingPricingValue = 0;

              gqh.UnitCost = 0;
              gqh.UnitPrice = 0;

              //SqlConnection cnn;
              if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
              {
                using (SqlConnection cnn_sql = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                {
                  cnn_sql.Open();
                  gqh.Costing = this.GetCostYearList(cnn_sql);
                }
              }
              var RefreshModel = new OriginalPricingTool
              {
                GenerateQuoteHeader = gqh,
                SalesOrders = null,
                Competitors = null,
                Components = null,
                DemandList = null,
                Comments = null,
                LTAs = null,
                LTAParts = null
              };

              MessageBox.Show("User access rejected. Please consult your system administrator for access.");
              return View(RefreshModel);
            }
            reader.Close();
          }
        }



        //if (model.GenerateQuoteHeader.Division == "1")
        //{
        //    model.GenerateQuoteHeader.Division = ("'710000','730000','750000','720000'");
        //}

        //eo reserves
        sqlScript = "select glpt, extended_cost, pqoh, reserveqty from sT_Inventory_Reserves where litm=@item_num and ltrim(mcu) in ('710000','730000','750000','720000','780000','734000')";
        using (var command = new SqlCommand(sqlScript, cnn))
        {
          command.Parameters.Add(new SqlParameter("@item_num", item_num));
          //command.Parameters.Add(new SqlParameter("@division", model.GenerateQuoteHeader.Division));

          using (var reader = command.ExecuteReader())
          {
            if (reader.HasRows)
            {
              if (reader.Read())
              {
                model.GenerateQuoteHeader.GLClass = reader.GetString(0);
                model.GenerateQuoteHeader.EOExtCost = Convert.ToDecimal(reader.GetValue(1));
                //model.GenerateQuoteHeader.QuantityOnHand = Convert.ToInt32(reader.GetValue(2));
                model.GenerateQuoteHeader.ReserveQty = Convert.ToInt32(reader.GetValue(3));
              }
              //return true;
            }
            else
            {
              Console.WriteLine("Failed to obtain E/O inventory and costing information.");
              //return View();
            }
            reader.Close();
          }
        }

        sqlScript = "select blacklist_flag, blacklist_reasoning from strategic_pricing_adjustments pa where division in ('710000','730000','750000','720000','780000','734000') and internal_part_number=@item_num";
        using (var command = new SqlCommand(sqlScript, cnn))
        {
          command.Parameters.Add(new SqlParameter("@item_num", item_num));
          //command.Parameters.Add(new SqlParameter("@division", model.GenerateQuoteHeader.Division));

          using (var reader = command.ExecuteReader())
          {
            if (reader.HasRows)
            {
              if (reader.Read())
              {
                model.GenerateQuoteHeader.BlacklistFlag = Convert.ToBoolean(reader.GetInt32(0));
                model.GenerateQuoteHeader.BlacklistReasoning = reader.GetString(1);
                
              }
              //return true;
            }
            else
            {
              model.GenerateQuoteHeader.BlacklistFlag = false;
              //return View();
            }
            reader.Close();
          }
        }


        if (model.GenerateQuoteHeader.CustomerPartNum is null)
        {
          model.GenerateQuoteHeader.CustomerPartNum = "";
        }
        else if (item_num is null)
        {
          item_num = "";

        }

        string customer_name_convert_search = model.GenerateQuoteHeader.CustomerName;
        //string customer_number_convert;
        string customer_name = null;
        string customer_num = null;
        int customer_name_length = 0;

        if (customer_name_convert_search != null)
        {
          customer_name_length = customer_name_convert_search.Length - 9;
        }
        int customer_num_length_start;

        if (model.GenerateQuoteHeader.CustomerName != null)
        {
          customer_name = customer_name_convert_search.Substring(0, customer_name_length);
          customer_num_length_start = customer_name_length + 3;
          customer_num = customer_name_convert_search.Substring(customer_num_length_start, 6);
        }

        if (customer_name != null)
        {
          sqlScript = @"select count(*) from iT_Sales_LTAUpload where (item_number_2=ISNULL(@item_num,'') Or Item_Number_3=ISNULL(@cust_item_num,''))
                                and division_id in ('710000','730000','750000','720000','Q720000','780000','734000') and convert(date,effective_start_date)<=convert(date,getdate())
                                    and convert(date, Effective_End_Date)>= convert(date, getdate()) and customer_number=@customer_number";
        }
        else
        {
          sqlScript = @"select count(*) from iT_Sales_LTAUpload where (item_number_2=ISNULL(@item_num,'') Or Item_Number_3=ISNULL(@cust_item_num,''))
                                and division_id in ('710000','730000','750000','720000','Q720000','780000','734000') and convert(date,effective_start_date)<=convert(date,getdate())
                                    and convert(date, Effective_End_Date)>= convert(date, getdate())";
        }
        using (var command = new SqlCommand(sqlScript, cnn))
        {
          command.Parameters.Add(new SqlParameter("@item_num", item_num));
          command.Parameters.Add(new SqlParameter("@cust_item_num", model.GenerateQuoteHeader.CustomerPartNum));
          if (model.GenerateQuoteHeader.CustomerName != null)
          {
            command.Parameters.Add(new SqlParameter("@customer_number", customer_num));
          }
          // = reader.IsDBNull(2) ? "" : (string)reader.GetValue(2);
          //command.Parameters.Add(new SqlParameter("@division", model.GenerateQuoteHeader.Division));

          using (var reader = command.ExecuteReader())
          {
            if (reader.HasRows)
            {
              if (reader.Read())
              {
                model.GenerateQuoteHeader.LTACount = reader.GetInt32(0);
              }
            }
            else
            {
              Console.WriteLine("Failed to obtain LTA count information.");
            }
            reader.Close();
          }
        }

        if (model.GenerateQuoteHeader.CustomerPartNum is null)
        {
          model.GenerateQuoteHeader.CustomerPartNum = "";
        }
        else if (item_num is null)
        {
          item_num = "";

        }

        
      

        string customer_part_num;

        //go find the customer part number if not known
        if (model.GenerateQuoteHeader.PartNumber is null)
        {
          //string customer_part_num;
          customer_part_num = (model.GenerateQuoteHeader.CustomerPartNum).Replace(" ", string.Empty); ;

          //if we only have the customer part number, go find the internal part number
          using (OdbcConnection cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString))
          {
            cnn_odbc.Open();

            sqlScript = @"select im.imdsc1,cr.ivcitm, case when im.imstkt in ('I','O') then 'Obselete' else im.imstkt end as imstkt, TRIM(im.imaitm) customer_part_num, cr.ivitm
                            From PRODDTA.f4101 im 
                            left outer join(select cr_sub.ivan8, cr_sub.ivitm, cr_sub.ivcitm from PRODDTA.F4104 cr_sub where cr_sub.ivxrt = 'CR') cr on 
                            cr.ivitm = im.imitm 
                            where 1 = 1 
                        and TRIM(im.imaitm) =? ";
            using (var command = new OdbcCommand(sqlScript, cnn_odbc))
            {
              command.Parameters.Add(new OdbcParameter("@cust_item_num", customer_part_num));
              SqlParameter cust_item_param = new SqlParameter
              {
                ParameterName = "@cust_item_num",
                //Value = model.GenerateQuoteHeader.ProductGroup
                Value = String.IsNullOrEmpty(customer_part_num) ? "" : customer_part_num.ToString()
              };
              command.Parameters.Add(cust_item_param);
              //SqlParameter cust_item_param = new SqlParameter
              //{
              //  ParameterName = "@cust_item_num",
              //  //Value = model.GenerateQuoteHeader.ProductGroup
              //  Value = model.GenerateQuoteHeader.Item_ID
              //};
              //command.Parameters.Add(cust_item_param);
              using (var reader = command.ExecuteReader())
              {
                try
                {
                  if (reader.HasRows)
                  {
                    while (reader.Read())
                    {
                      model.GenerateQuoteHeader.Item_ID = reader.GetInt32(4);
                    }
                  }
                  else
                  {
                    MessageBox.Show("Item does not exist or a connection was not made. Please try again.");
                    //MessageBox.Show("The connection is step 1: ");
                    cnn_odbc.Close();
                    cnn_odbc.Dispose();
                    GenerateQuoteHeader gqh = new GenerateQuoteHeader();
                    //gqh.LoadingPricingValue = 0;

                    gqh.UnitCost = 0;
                    gqh.UnitPrice = 0;

                    //SqlConnection cnn;
                    if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                    {
                      using (cnn)
                      {
                        cnn.Open();
                        gqh.Costing = this.GetCostYearList(cnn);
                      }
                    }
                    var RefreshModel = new OriginalPricingTool
                    {
                      GenerateQuoteHeader = gqh,
                      SalesOrders = null,
                      Competitors = null,
                      Components = null,
                      DemandList = null,
                      Comments = null,
                      LTAs = null,
                      LTAParts = null
                    };
                    return View(RefreshModel);
                  }
                }
                catch
                {
                  MessageBox.Show("Item does not exist. Please try again.");
                  //MessageBox.Show("The connection is step 2: ");
                  cnn_odbc.Close();
                  cnn_odbc.Dispose();
                  GenerateQuoteHeader gqh = new GenerateQuoteHeader();
                  //gqh.LoadingPricingValue = 0;

                  gqh.UnitCost = 0;
                  gqh.UnitPrice = 0;

                  //SqlConnection cnn;
                  if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                  {
                    using (cnn)
                    {
                      cnn.Open();
                      gqh.Costing = this.GetCostYearList(cnn);
                    }
                  }
                  var RefreshModel = new OriginalPricingTool
                  {
                    GenerateQuoteHeader = gqh,
                    SalesOrders = null,
                    Competitors = null,
                    Components = null,
                    DemandList = null,
                    Comments = null,
                    LTAs = null,
                    LTAParts = null
                  };
                  //MessageBox.Show("just returned view 3.");
                  return View(RefreshModel);
                }
                //reader.Close();
              }
            }
            cnn_odbc.Close();
            cnn_odbc.Dispose();
          }
        }
        else
        {
          using (OdbcConnection cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString))
          {
            cnn_odbc.Open();

            if (model.GenerateQuoteHeader.CustomerName != null && (model.GenerateQuoteHeader.Division=="720000" || model.GenerateQuoteHeader.Division == "730000"|| model.GenerateQuoteHeader.Division == "750000" || model.GenerateQuoteHeader.Division == "780000" || model.GenerateQuoteHeader.Division=="734000"))
            {
              sqlScript = @"select	DISTINCT ifnull(xref.ivcitm,ibaitm) CustPart, xref.ivxrt ibmerl
                                        from PRODDTA.f4102 ib join PRODDTA.f4101 itm on ibitm = imitm
                              left outer join proddta.F4104 xref on ibitm = xref.ivitm
                                        where  1 = 1
                                        and ibitm=? and ivan8=?";
            }
            else if (model.GenerateQuoteHeader.CustomerName != null && model.GenerateQuoteHeader.Division == "710000")
            {
              sqlScript = @"select	DISTINCT ifnull(iblitm,ibaitm) CustPart
                                        from PRODDTA.f4102 ib join PRODDTA.f4101 itm on ibitm = imitm
                              left outer join proddta.F4104 xref on ibitm = xref.ivitm
                                        where  1 = 1
                                        and ibitm=? and ivan8=?";
            }
            else if (model.GenerateQuoteHeader.Division == "710000")
            {
              sqlScript = @"select	DISTINCT ifnull(iblitm,ibaitm) CustPart
                                        from PRODDTA.f4102 ib join PRODDTA.f4101 itm on ibitm = imitm
                              left outer join proddta.F4104 xref on ibitm = xref.ivitm
                                        where  1 = 1
                                        and ibitm=? and trim(ivan8)<>'710000'  ";
            }
            else if (model.GenerateQuoteHeader.Division == "730000")
            {
              sqlScript = @"select	DISTINCT ifnull(xref.ivcitm,ibaitm) CustPart, xref.ivxrt ibmerl
                                        from PRODDTA.f4102 ib join PRODDTA.f4101 itm on ibitm = imitm
                              left outer join proddta.F4104 xref on ibitm = xref.ivitm
                                        where  1 = 1
                                        and ibitm=? and trim(ivan8)<>'730000' ";
            }
            else if (model.GenerateQuoteHeader.Division == "720000")
            {
              sqlScript = @"select	DISTINCT ifnull(xref.ivcitm,ibaitm) CustPart, xref.ivxrt ibmerl
                                        from PRODDTA.f4102 ib join PRODDTA.f4101 itm on ibitm = imitm
                              left outer join proddta.F4104 xref on ibitm = xref.ivitm
                                        where  1 = 1
                                        and ibitm=? and trim(ivan8)<>'720000' ";
            }
            else
            {
              sqlScript = @"select	DISTINCT ifnull(xref.ivcitm,ibaitm) CustPart, xref.ivxrt ibmerl
                                        from PRODDTA.f4102 ib join PRODDTA.f4101 itm on ibitm = imitm
                              left outer join proddta.F4104 xref on ibitm = xref.ivitm
                                        where  1 = 1
                                        and ibitm=? ";
            }
            using (var command = new OdbcCommand(sqlScript, cnn_odbc))
            {
              command.Parameters.Add(new OdbcParameter("@item_id", model.GenerateQuoteHeader.Item_ID));
              if (model.GenerateQuoteHeader.CustomerName != null)
              {
                command.Parameters.Add(new OdbcParameter("@customer_number", customer_num));
              }
              using (var reader = command.ExecuteReader())
              {
                try
                {
                                    if (reader.HasRows)
                                    {
                                        if (model.GenerateQuoteHeader.Division != "710000")
                                            while (reader.Read())
                                            {
                                                CustomerPart item = new CustomerPart()
                                                {
                                                    CustomerPartNum = reader.IsDBNull(0) ? "" : (string)reader.GetValue(0),
                                                    CustomerPartNumRev = reader.IsDBNull(1) ? "" : (string)reader.GetValue(1)

                                                };
                                                CustParts.Add(item);

                                            }
                                        else
                                        {
                                            while (reader.Read())
                                            {
                                                CustomerPart item = new CustomerPart()
                                                {
                                                    CustomerPartNum = reader.IsDBNull(0) ? "" : (string)reader.GetValue(0)
                                                    //CustomerPartNumRev = reader.IsDBNull(1) ? "" : (string)reader.GetValue(1)

                                                };
                                                CustParts.Add(item);

                                            }
                                        }
                                    }
                                    else if (reader.HasRows == false)
                                    {

                                        while (reader.Read())
                                        {
                                            CustomerPart item = new CustomerPart()
                                            {
                                                CustomerPartNum = reader.IsDBNull(0) ? "" : (string)reader.GetValue(0),
                                                CustomerPartNumRev = reader.IsDBNull(1) ? "" : (string)reader.GetValue(1)

                                            };
                                            CustParts.Add(item);
                                        }
                                    }
                                    else if (item_num.Replace(" ", string.Empty) is null)
                                    {
                                        MessageBox.Show("Item does not exist. Please try again.");
                                        //MessageBox.Show("The connection is step 3: ");
                                        cnn_odbc.Close();
                                        cnn_odbc.Dispose();
                                        GenerateQuoteHeader gqh = new GenerateQuoteHeader();
                                        //gqh.LoadingPricingValue = 0;

                                        gqh.UnitCost = 0;
                                        gqh.UnitPrice = 0;

                                        //SqlConnection cnn;
                                        if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                                        {
                                            using (SqlConnection cnn_sql = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                                            {

                                                cnn_sql.Open();
                                                gqh.Costing = this.GetCostYearList(cnn);
                                            }
                                        }
                                        var RefreshModel = new OriginalPricingTool
                                        {
                                            GenerateQuoteHeader = gqh,
                                            SalesOrders = null,
                                            Competitors = null,
                                            Components = null,
                                            DemandList = null,
                                            Comments = null,
                                            LTAs = null,
                                            LTAParts = null
                                        };
                                        //MessageBox.Show("just returned view 5.");
                                        return View(RefreshModel);

                                    }


                                    else
                                    {
                                        try
                                        {

                                            if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                                            {
                                                using (SqlConnection cnn_sql_con = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                                                {
                                                    cnn_sql_con.Open();
                                                    model.GenerateQuoteHeader.Costing = this.GetCostYearList(cnn_sql_con);
                                                }
                                            }

                                            List<PartDescription> md = new List<PartDescription>();
                                            SharePointConnectionUpdated dc = new SharePointConnectionUpdated();

                                            md = (from m in dc.sT_PartDetails_JDE
                                                  where m.IBLITM.StartsWith(model.GenerateQuoteHeader.PartNumber)
                                                  where m.IBLITM.Contains("*OP") == false
                                                  orderby m.IBLITM
                                                  select new PartDescription
                                                  {
                                                      PartID = m.ID,
                                                      PartNumber = m.IBLITM,
                                                      PartDesc = m.IMDSC1
                                                  }).ToList();

                                            //MessageBox.Show("just returned view 6.");
                                            return View("PartDescriptionAnalysis", md);
                                        }
                                        catch
                                        {
                                            MessageBox.Show("failed to load part desc analysis step 3.1");
                                        }

                                    }

                }
                catch (Exception ex)
                {
                  MessageBox.Show(ex.Message);
                  MessageBox.Show("Item does not exist or the connection has failed. Please try again.");
                  //MessageBox.Show("The connection is step 4: "); //failing here
                  cnn_odbc.Close();
                  cnn_odbc.Dispose();
                  GenerateQuoteHeader gqh = new GenerateQuoteHeader();
                  //gqh.LoadingPricingValue = 0;

                  gqh.UnitCost = 0;
                  gqh.UnitPrice = 0;

                  //SqlConnection cnn;
                  if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                  {
                    using (SqlConnection cnn_sql = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                    {

                      cnn_sql.Open();
                      gqh.Costing = this.GetCostYearList(cnn);
                    }
                  }
                  var RefreshModel = new OriginalPricingTool
                  {
                    GenerateQuoteHeader = gqh,
                    SalesOrders = null,
                    Competitors = null,
                    Components = null,
                    DemandList = null,
                    Comments = null,
                    LTAs = null,
                    LTAParts = null
                  };
                  //MessageBox.Show("just returned view 6.");
                  return View(RefreshModel);

                }
              }
            }
          }
        }



        if (model.GenerateQuoteHeader.CustomerName != null)
        {
          sqlScript = @"select Item_Number_3, Price, Customer_Number , c.abalph as Customer_Name
                                            from iT_Sales_LTAUpload u
                                            left outer join  [PRODDTA].[Cam404].[PRODDTA].[F0101] c on 
	                                            c.aban8=u.Customer_Number
                                            where 1=1 and (item_number_3=isnull(@cust_item_num,''))
                                            and division_id in ('710000','730000','750000','720000','780000','734000') 
                                            and convert(date,effective_start_date)<=convert(date,getdate())
                                            and convert(date, Effective_End_Date)>= convert(date, getdate()) and customer_number=@customer_number";
        }
        else
        {
          sqlScript = @"select Item_Number_3, Price, Customer_Number , c.abalph as Customer_Name
                                            from iT_Sales_LTAUpload u
                                            left outer join  [PRODDTA].[Cam404].[PRODDTA].[F0101] c on 
	                                            c.aban8=u.Customer_Number
                                            where 1=1 and (item_number_3=isnull(@cust_item_num,''))
                                            and division_id in ('710000','730000','750000','720000','780000','734000') 
                                            and convert(date,effective_start_date)<=convert(date,getdate())
                                            and convert(date, Effective_End_Date)>= convert(date, getdate())";
        }

        List<string> scpArray = new List<string>();
        foreach (var y in CustParts)
        {
          scpArray.Add(y.CustomerPartNum.Trim());
        }


        int cp = 0;
        foreach (var y in scpArray.Distinct())
        {
          using (var command = new SqlCommand(sqlScript, cnn))
          {
            command.Parameters.Add(new SqlParameter("@item_num", item_num.Trim()));
            SqlParameter cust_item_param = new SqlParameter
            {
              ParameterName = "@cust_item_num",
              Value = String.IsNullOrEmpty(y) ? "" : y.ToString()
            };
            command.Parameters.Add(cust_item_param);
            if (model.GenerateQuoteHeader.CustomerName != null)
            {
              command.Parameters.Add(new SqlParameter("@customer_number", customer_num));
            }
     
            using (var reader = command.ExecuteReader())
            {
              try
              {
                if (reader.HasRows)
                {

                  while (reader.Read())
                  {
                    LTAPart lta = new LTAPart()
                    {
                      PartNumber = scpArray[cp],
                      Price = reader.GetDecimal(1),
                      CustomerNumber = reader.GetInt32(2),
                      CustomerName = reader.GetString(3),
                      //CompetitorPartNum = sArray[o]
                    };
                    LTAParts.Add(lta);
                  }
                  //customer_part_num = (model.GenerateQuoteHeader.CustomerPartNum).Replace(" ", string.Empty);
                }
                else
                {
                  Console.WriteLine("Failed to obtain LTA count information.");
                  //return View();
                }
              }
              catch (Exception ex)
              {
                //do nothing
              }
              reader.Close();
            }
          }
        }

                if (model.GenerateQuoteHeader.Division != "750000" || model.GenerateQuoteHeader.Division != "780000")
                {
                    sqlScript = @"select top 1 pd.ibmcu, pd.IBACQ ACQ
                        , pd.ibltmf MfgLT
                        , pd.IBLTCM CumLT
                        , pd.QOH
                        , pd.PRODUCT_CODE
                        , pd.PRODUCT_GROUP
                        , pd.IBSRP5 ValueStream
                        , pd.IBSRP1 ProductType   
                        , convert(int,ltrim(pd.ibitm)) ibitm
                        , pd.IBMERL PartRevision
                        , isnull(vs.throughput,0) CapacityThroughput
                        , IMIFLA ECFlag
                        From sT_PartDetails_JDE  pd left outer join [DataWarehouse].[dbo].[sT_ThroughputCapacityByValueStream] vs
							on pd.ibsrp2=vs.VALUE_STREAM and pd.ibmcu=vs.DIVISION
                            where pd.ibitm=@item and pd.ibmcu=@division";
                }
                else
                {
                    sqlScript = @"select top 1 ibmcu, IBACQ ACQ
                        , ibltmf MfgLT
                        , pd.IBLTCM CumLT
                        , pd.QOH
                        , pd.PRODUCT_CODE
                        , pd.PRODUCT_GROUP
                        , pd.IBSRP2 ValueStream
                        , pd.IBSRP1 ProductType   
                        , convert(int,ltrim(pd.ibitm)) ibitm
                        , pd.IBMERL PartRevision
						, isnull(vs.throughput,0) CapacityThroughput
                        , IMIFLA ECFlag
                        From sT_PartDetails_JDE pd left outer join [DataWarehouse].[dbo].[sT_ThroughputCapacityByValueStream] vs
							on pd.ibsrp2=vs.VALUE_STREAM and pd.ibmcu=vs.DIVISION
						where pd.ibitm=@item and pd.ibmcu=@division";
                }
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    SqlParameter item_val = new SqlParameter
                    {
                        ParameterName = "@item",
                        Value = model.GenerateQuoteHeader.Item_ID
                    };
                    command.Parameters.Add(item_val);
                    SqlParameter div = new SqlParameter
                    {
                        ParameterName = "@division",
                        Value = model.GenerateQuoteHeader.Division
                    };
                    command.Parameters.Add(div);

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                string division_check;
                                division_check = reader.GetString(0);

                                model.GenerateQuoteHeader.ACQ = Convert.ToInt32(reader.GetDecimal(1));
                                model.GenerateQuoteHeader.MfgLT = Convert.ToInt32(reader.GetDecimal(2));
                                model.GenerateQuoteHeader.CumLT = Convert.ToInt32(reader.GetDecimal(3));
                                model.GenerateQuoteHeader.QuantityOnHand = Convert.ToInt32(reader.GetDecimal(4));
                                model.GenerateQuoteHeader.ProductCode = reader.IsDBNull(5) ? "" : (string)reader.GetValue(5);

                                model.GenerateQuoteHeader.ProductGroup = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                                model.GenerateQuoteHeader.ValueStream = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                                model.GenerateQuoteHeader.ProductType = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                                model.GenerateQuoteHeader.ibitm = reader.GetInt32(9);
                                model.GenerateQuoteHeader.PartRevision = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                                model.GenerateQuoteHeader.CapacityThroughput = Convert.ToInt32(reader.GetDecimal(11));
                                model.GenerateQuoteHeader.ECFlag = reader.IsDBNull(12) ? "" : (string)reader.GetValue(12);

                            }
                        }
                        else
                        {

                            MessageBox.Show("Could not find Lead Time and Quantity On Hand. Does it exist?");
                        }
                        reader.Close();
                    }

                }


                int[] qty_breaks = { 10, 25, 50, 75, 100, 250, 500, 750, 1000, 2000, 2500, 5000, 10000, 20000, 25000, 50000, 100000, 25000 };
        foreach (int y in qty_breaks)
        {
          sqlScript = @"select top 1 m.MARGIN_VALUE, m.other from config_MARGIN m 
                                 left outer join organization o on m.DIVISION_ID = o.ORG_ID 
                                 WHERE 1 = 1 
                                 AND (o.DIVISION = isnull(@division_id,o.DIVISION) and m.VALUE_STREAM=isnull(@value_stream,m.Value_stream) and m.PRODUCT_GROUP=isnull(@product_group,m.product_group)
								)
								and @qty_of_interest between m.QUANTITY_BREAK_RANGE_START and m.QUANTITY_BREAK_RANGE_END order by m.margin_value; ";

                    //product code
                    //or m.CUSTOMER_NUMBER=isnull(@customer_number,m.customer_number)
                    //or m.INTERNAL_PART_NUMBER=isnull(@part_number,m.INTERNAL_PART_NUMBER)
                    //or m.CUSTOMER_PART_NUMBER = isnull(@customer_part_number, m.CUSTOMER_PART_NUMBER)
                        //or m.other = isnull(@other, m.OTHER)
                    using (var command = new SqlCommand(sqlScript, cnn))
          {
            SqlParameter division_val = new SqlParameter
            {
              ParameterName = "@division_id",
              Value = model.GenerateQuoteHeader.Division
            };
            command.Parameters.Add(division_val);

            SqlParameter value_stream_param = new SqlParameter
            {
              ParameterName = "@value_stream",
              //Value = model.GenerateQuoteHeader.ValueStream
              Value = String.IsNullOrEmpty(model.GenerateQuoteHeader.ValueStream) ? "" : model.GenerateQuoteHeader.ValueStream.ToString()
            };
            command.Parameters.Add(value_stream_param);
            SqlParameter product_group_param = new SqlParameter
            {
              ParameterName = "@product_group",
              //Value = model.GenerateQuoteHeader.ProductGroup
              Value = String.IsNullOrEmpty(model.GenerateQuoteHeader.ProductGroup) ? "" : model.GenerateQuoteHeader.ProductGroup.ToString()
            };
            command.Parameters.Add(product_group_param);
            SqlParameter product_code_param = new SqlParameter
            {
              ParameterName = "@product_code",
              //Value = model.GenerateQuoteHeader.ProductCode
              Value = String.IsNullOrEmpty(model.GenerateQuoteHeader.ProductCode) ? "" : model.GenerateQuoteHeader.ProductCode.ToString()
            };
            command.Parameters.Add(product_code_param);

            SqlParameter customer_number_param = new SqlParameter
            {
              ParameterName = "@customer_number",
              //Value = model.GenerateQuoteHeader.CustomerNum
              Value = String.IsNullOrEmpty(model.GenerateQuoteHeader.CustomerNum) ? "" : model.GenerateQuoteHeader.CustomerNum.ToString()
            };
            command.Parameters.Add(customer_number_param);
            SqlParameter internal_part_number_param = new SqlParameter
            {
              ParameterName = "@part_number",
              //Value = model.GenerateQuoteHeader.PartNumber
              Value = String.IsNullOrEmpty(model.GenerateQuoteHeader.PartNumber) ? "" : model.GenerateQuoteHeader.PartNumber.ToString()
            };
            command.Parameters.Add(internal_part_number_param);
            SqlParameter customer_part_number_param = new SqlParameter
            {
              ParameterName = "@customer_part_number",
              //Value = model.GenerateQuoteHeader.CustomerPartNum
              Value = String.IsNullOrEmpty(model.GenerateQuoteHeader.CustomerPartNum) ? "" : model.GenerateQuoteHeader.CustomerPartNum.ToString()
            };
            command.Parameters.Add(customer_part_number_param);
            SqlParameter other_param = new SqlParameter
            {
              ParameterName = "@other",
              Value = String.IsNullOrEmpty(model.GenerateQuoteHeader.Other) ? "" : model.GenerateQuoteHeader.Other.ToString()
              // Value = String.IsNullOrEmpty(model.CUSTOMER_NUMBER) ? "" : model.CUSTOMER_NUMBER.ToString()
            };
            command.Parameters.Add(other_param);
            SqlParameter qty_param = new SqlParameter
            {
              ParameterName = "@qty_of_interest",
              Value = y
            };
            command.Parameters.Add(qty_param);

            using (SqlDataReader reader = command.ExecuteReader())
            {
              try
              {

                while (reader.Read())
                {
                  if (reader.HasRows)
                  {
                    if (y == 500)
                    {
                      model.GenerateQuoteHeader.margin_500 = reader.GetDecimal(0);
                      //model.GenerateQuoteHeader.Other = reader.GetString(1);
                    }
                    else if (y == 10)
                    {
                      model.GenerateQuoteHeader.margin_10 = reader.GetDecimal(0);
                    }
                    else if (y == 25)
                    {
                      model.GenerateQuoteHeader.margin_25 = reader.GetDecimal(0);
                    }
                    else if (y == 50)
                    {
                      model.GenerateQuoteHeader.margin_50 = reader.GetDecimal(0);
                    }
                    else if (y == 75)
                    {
                      model.GenerateQuoteHeader.margin_75 = reader.GetDecimal(0);
                    }
                    else if (y == 100)
                    {
                      model.GenerateQuoteHeader.margin_100 = reader.GetDecimal(0);
                    }
                    else if (y == 250)
                    {
                      model.GenerateQuoteHeader.margin_250 = reader.GetDecimal(0);
                    }
                    else if (y == 500)
                    {
                      model.GenerateQuoteHeader.margin_500 = reader.GetDecimal(0);
                    }
                    else if (y == 750)
                    {
                      model.GenerateQuoteHeader.margin_750 = reader.GetDecimal(0);
                    }
                    else if (y == 1000)
                    {
                      model.GenerateQuoteHeader.margin_1000 = reader.GetDecimal(0);
                    }
                    else if (y == 2000)
                    {
                      model.GenerateQuoteHeader.margin_2000 = reader.GetDecimal(0);
                    }
                    else if (y == 2500)
                    {
                      model.GenerateQuoteHeader.margin_2500 = reader.GetDecimal(0);
                    }
                    else if (y == 5000)
                    {
                      model.GenerateQuoteHeader.margin_5000 = reader.GetDecimal(0);
                    }
                    else if (y == 10000)
                    {
                      model.GenerateQuoteHeader.margin_10000 = reader.GetDecimal(0);
                    }
                    else if (y == 20000)
                    {
                      model.GenerateQuoteHeader.margin_20000 = reader.GetDecimal(0);
                    }
                    else if (y == 25000)
                    {
                      model.GenerateQuoteHeader.margin_25000 = reader.GetDecimal(0);
                    }
                    else if (y == 50000)
                    {
                      model.GenerateQuoteHeader.margin_50000 = reader.GetDecimal(0);
                    }
                    else if (y == 100000)
                    {
                      model.GenerateQuoteHeader.margin_100000 = reader.GetDecimal(0);
                    }

                  }
                  //y++;
                }
                //reader.Close();
              }

              catch (Exception)
              {
                MessageBox.Show("Error loading the margins that have been configured.");
                return View();
              }
              //reader.Close();
              finally
              {
                reader.Close();
              }

            }

          }
        }




                sqlScript = @"select division, order_type, item_num, order_num, complete_date, qty_shipped
                                                                    , qty_scrapped, unit_cost, su_cost, lot_charge, variable_cost, acq, unit_fc, fixed_cost, unit_wo_fc
                                                                    , std_wo_qty, total_variance, unit_var, std_wo_yr, act_cost, adj_cost
                                                                    from sT_Costing_ActualsAdjFactor where item_num=@item_num order by complete_date desc, qty_shipped";
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    SqlParameter item_val = new SqlParameter
                    {
                        ParameterName = "@item_num",
                        Value = item_num
                    };
                    command.Parameters.Add(item_val);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                ActualCost acts = new ActualCost()
                                {
                                    OrderType = reader.GetString(1),
                                    OrderNum = reader.GetString(3),
                                    CompletionDate = reader.GetDateTime(4),
                                    QtyShipped = Convert.ToInt32(reader.GetValue(5)),
                                    StdQtyWOQty = Convert.ToDecimal(reader.GetValue(15)),
                                    ActCost = Convert.ToDecimal(reader.GetValue(19))
                                    //CostAdjFactor = Convert.ToDecimal(reader.GetValue(20))
                                };
                                ActCost.Add(acts);

                                model.GenerateQuoteHeader.ActualCostMin = ActCost.Select(x => x.ActCost).Min();
                                model.GenerateQuoteHeader.FifteenPercentCostMin = model.GenerateQuoteHeader.ActualCostMin * Convert.ToDecimal(1.15);
                                model.GenerateQuoteHeader.AverageCostAdjFactor = ActCost.Select(x => x.CostAdjFactor).Max();
                            }
                        }
                        catch
                        {
                            reader.Close();
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }
                }


                List<string> sArray = new List<string>();
        //sArray = CustParts;
        foreach (var cust in CustParts)
        {
          sArray.Add(cust.CustomerPartNum.Trim());
        }


        int o = 0;
        foreach (var cust in sArray.Distinct())
        {
          if (model.GenerateQuoteHeader.Division == "710000" || model.GenerateQuoteHeader.Division == "720000")
          {
            //qualified parts
            sqlScript = "select distinct manufacturer, status,COMPETITOR_PRICE from approved_parts where manufacturer <> organization and custodian_part like substring(@item_num,0,len(custodian_part) + 1) ";
          }

          else if (model.GenerateQuoteHeader.Division=="750000" || model.GenerateQuoteHeader.Division=="780000")
          {
            sqlScript = "select distinct manufacturer, status,COMPETITOR_PRICE from approved_parts where manufacturer <> organization and custodian_part=@item_num";
          }
          else
          {
            sqlScript = "select distinct manufacturer, status,COMPETITOR_PRICE from approved_parts where manufacturer <> organization and custodian_part=@item_num";
          }
          using (var command = new SqlCommand(sqlScript, cnn))
          {
            if (model.GenerateQuoteHeader.Division == "710000" || model.GenerateQuoteHeader.Division == "720000" || model.GenerateQuoteHeader.Division == "730000" || model.GenerateQuoteHeader.Division=="734000")
            {
              command.Parameters.Add(new SqlParameter("@item_num", item_num));
            }
            else
            {
              command.Parameters.Add(new SqlParameter("@item_num", (sArray[o]).Replace(" ", string.Empty)));
            }
            using (var reader = command.ExecuteReader())
            {
              
              if (reader.HasRows)
              {
                try
                {
                  while (reader.Read())
                  {
                    Competitor item = new Competitor()
                    {
                      CompetitorName = reader.GetString(reader.GetOrdinal("manufacturer")),
                      CompetitorStatus = reader.GetString(reader.GetOrdinal("status")),
                      CompetitorPrice = reader.IsDBNull(2) ? 0 : (decimal)reader.GetValue(2),
                      CompetitorPartNum = sArray[o]
                    };
                    Comps.Add(item);
                    
                  }
                }
                catch
                {
                  //
                }
              }
              else
              {
                //Console.WriteLine("Could not find any competitors.");
                //return View();
                //MessageBox.Show("just returned view 7.");
              }
              reader.Close();
              o++;
            }
          }
        }

        int comment_count = 0;
        sqlScript = @"select COUNT(COMMENT) comment_count From COMMENT_LINES L 
                                 join COMMENT_HEADER h on h.COMMENT_ID = l.COMMENT_HEADER_ID 
                                 where h.QUOTE_PART_LINK = @item_num;";
        using (var command = new SqlCommand(sqlScript, cnn))
        {
          command.Parameters.Add(new SqlParameter("@item_num", item_num));
          using (var reader = command.ExecuteReader())
          {
            if (reader.HasRows)
            {
              if (reader.Read())
              {
                comment_count = reader.GetInt32(reader.GetOrdinal("comment_count"));
                model.CommentCount = comment_count;
              }
              //return true;
            }
            else
            {
              Console.WriteLine("Could not find any comments.");
              //return View();
            }
            reader.Close();
          }
        }

        sqlScript = @"select l.COMMENT COMMENT, u.User_name USERNAME, l.CREATION_DATE CREATION_DATE
											, l.comment_line_id postid 
											, (select r.ROLE_DESC From users u 
													left outer join roles r 
													on u.ROLE_ID=r.ROLE_ID where u.USER_NAME=@username) ROLE_DESC, convert(varchar,SALES_HIST_HEAD_LINK) SO, convert(varchar,QUOTE_HIST_HEAD_LINK) quote
											From COMMENT_LINES L 
                                            join COMMENT_HEADER h on h.COMMENT_ID = l.COMMENT_HEADER_ID 
                                            left outer join USERS u on l.CREATED_BY_USER_ID = u.USER_ID 
                                            where 1 = 1 
                                            and h.QUOTE_PART_LINK= @item_num;";
        using (var command = new SqlCommand(sqlScript, cnn))
        {
          command.Parameters.Add(new SqlParameter("@item_num", item_num));
          command.Parameters.Add(new SqlParameter("@username", Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)));
          using (var reader = command.ExecuteReader())
          {
            if (reader.HasRows)
            {
              while (reader.Read())
              {
                Comments.Add(new Comment
                {
                  PartComment = reader.GetString(0),
                  UserName = reader.GetString(1),
                  PostDate = reader.GetDateTime(2),
                  PostID = reader.GetInt32(3),
                  RoleDesc = reader.GetString(4),
                  SalesOrderNum = reader.IsDBNull(5) ? "" : (string)reader.GetValue(5),
                  QuoteNum = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6)
                });
              }
            }
            else
            {
              Console.WriteLine("Could not find any comments.");
              //return View();
              //MessageBox.Show("just returned view 8.");
            }
            reader.Close();

          }
        }

        

        int workbench_id = 0;
       

        sqlScript = "select user_id from users where user_name=@username ";

        using (var command = new SqlCommand(sqlScript, cnn))
        {
          SqlParameter username_val = new SqlParameter
          {
            ParameterName = "@username",
            Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
          };
          command.Parameters.Add(username_val);
          try
          {
            using (var reader = command.ExecuteReader())
            {
              if (reader.HasRows)
              {
                while (reader.Read())
                {
                  users.user_id = reader.GetInt32(0);
                }
                reader.Close();
              }
            }
          }
          catch (Exception)
          {
            MessageBox.Show("No user found. Exiting.");
            //return View();
          }
        }
        


        int i = 1;
          sqlScript = "select workbench_id from workbench_header where created_by_user_id=@user_id; ";
        //SqlCommand command = new SqlCommand(sqlScript, cnn);
        //SqlDataReader reader = command.ExecuteReader();
        using (var command = new SqlCommand(sqlScript, cnn))
        {
          SqlParameter user_id_val = new SqlParameter
          {
            ParameterName = "@user_id",
            Value = users.user_id
          };
          command.Parameters.Add(user_id_val);
          try { 
          using (var reader = command.ExecuteReader())
          {
            if (reader.HasRows)
            {
              while (reader.Read())
              {
                workbench_id = reader.GetInt32(0);
              }
              reader.Close();
            }
          }
        }
          catch (Exception)
          {
            MessageBox.Show("No workbench exist or query failure. Contact your system administrator.");
            return View();
          }
          //reader.Close();
        }

        sqlScript = @"select h.WORKBENCH_ID, convert(int,h.SHIP_TO) ship_to, h.SHIP_TO_NAME, h.CUSTOMER_PO, h.COMMENT, h.LTA_comment
                                  , concat(h.ship_to_name,' - ',convert(text,h.SHIP_TO)) customer_name_concat, CONVERT(varchar, due_date, 23) due_date
                                            from workbench_header h where created_by_user_id =@user_id";
        using (var command = new SqlCommand(sqlScript, cnn))
        {
          SqlParameter user_id_val = new SqlParameter
          {
            ParameterName = "@user_id",
            Value = users.user_id
          };
          command.Parameters.Add(user_id_val);
          try
          {
            using (var reader = command.ExecuteReader())
            {
              if (reader.HasRows)
              {
                while (reader.Read())
                {
                  workbench_id = reader.GetInt32(0);
                  model.GenerateQuoteHeader.ShipTooNum = Convert.ToInt32(reader.GetInt32(1));
                  model.GenerateQuoteHeader.ShipTooName = reader.GetString(2);
                  model.GenerateQuoteHeader.CustPO = reader.GetString(3);
                  model.GenerateQuoteHeader.HeaderComment = reader.GetString(4);
                  model.GenerateQuoteHeader.LeadTimeComment = reader.GetString(5);
                  model.GenerateQuoteHeader.WBCustomerName = Convert.ToString(reader.GetInt32(1));
                  model.GenerateQuoteHeader.DueDate = reader.GetString(7);

                }

                reader.Close();
              }
            }
          }

          catch (Exception ex)
          {

            //create the workbench when workbench=0;
          }
          finally
          {
            //reader.Close();
          }
        }

        //List<QuoteWorkbenchItems> item = new List<QuoteWorkbenchItems>();

        
        sqlScript = @"select l.* from workbench_lines l join workbench_header h on h.workbench_id=l.workbench_id where h.workbench_id=@workbench_id 
                                                        and created_by_user_id=@user_id";
        using (var command = new SqlCommand(sqlScript, cnn))
        {
          SqlParameter workbench_id_val = new SqlParameter
          {
            ParameterName = "@workbench_id",
            Value = workbench_id
          };
          command.Parameters.Add(workbench_id_val);
          SqlParameter user_id_val = new SqlParameter
          {
            ParameterName = "@user_id",
            Value = users.user_id
          };
          command.Parameters.Add(user_id_val);
          try
          {
            using (var reader = command.ExecuteReader())
            {
              
              if (reader.HasRows)
              {
                while (reader.Read())
                {

                                    QuoteWorkbenchItems item = new QuoteWorkbenchItems()
                                    {
                                        QCLineNum = reader.GetInt32(1),
                                        QCPartNumber = reader.GetString(5),
                                        QCCustPartNumber = reader.GetString(6),
                                        QCLeadTime = reader.GetString(7),
                                        QCLineComments = reader.GetString(8),
                                        QCQuoteQty = reader.GetInt32(9),
                                        QCQuoteUnitPrice = reader.GetDecimal(10),
                                        QCUnitCost = reader.GetDecimal(11),
                                        QCCalcCost=reader.GetDecimal(15)
                    //QCExtPrice =QCQuoteUnitPrice * QCQuoteQty
                  };
                  bench.Add(item);

                }

              }
            }
          }

          catch (Exception ex)
          {

            //create the workbench when workbench=0;
          }
          finally
          {
            //reader.Close();
          }
        }
      





      cnn.Close();
            }

            //model.GenerateQuoteHeader.LoadingPricingValue = 50;

            using (OdbcConnection cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString))
            {
                cnn_odbc.Open();

                

               string sqlScript = @"select cal.date  
                                from proddta.F4105 uc left outer join zpemjde.calendar_dim cal on uc.coupmj=cal.juliandate 
                                where 1 = 1 
                                and trim(uc.colitm)=? 
                                and trim(uc.comcu) in ('710000','730000','750000','720000','780000','734000')
                                and uc.coledg ='P1' 
                                fetch first row only ";
                using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                {
                    command.Parameters.Add(new OdbcParameter("@item_num", item_num));
                    //command.Parameters.Add(new OdbcParameter("@division", model.GenerateQuoteHeader.Division));
                    command.Parameters.Add(new OdbcParameter("@cost_id", cost_reference_id));
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                model.GenerateQuoteHeader.LastChangedCostDate = reader.GetDate(0);
                            }
                        }
                        else
                        {
                            //MessageBox.Show("just returned view 10.");
                        }
                        reader.Close();
                    }
                }

        //check the division
        //sqlScript = @"select	distinct	trim(ibmcu)
        //   from 		proddta.f4102 itm left outer join proddta.f3002 on ixitm = ibitm and ixmmcu = ibmcu and left(ibglpt,2) in ('FG','CP','RM') 
        //       where 1=1 and trim(ibmcu) in ('710000','730000','720000','750000')
        //   and 		trim(iblitm) = ?
        //    fetch first row only ";
        sqlScript = @"select	distinct trim(ibmcu), trim(mast.imdsc1), case when mast.imstkt in ('I','O') then 'Obselete' else mast.imstkt end as imstkt
			                from 		proddta.f4102 itm left outer join proddta.f3002 on ixitm = ibitm and ixmmcu = ibmcu 
left outer join proddta.F4101 mast on mast.imitm=itm.ibitm
							                where 1=1 and trim(ibmcu) in ('710000','730000','720000','750000','780000','734000') and left(ibglpt,2) in ('FG','CP','RM') 
			                and 		ibitm = ?
			                 fetch first row only ";
        using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                {
                    command.Parameters.Add(new OdbcParameter("@item_id", model.GenerateQuoteHeader.Item_ID));
                    //command.Parameters.Add(new OdbcParameter("@division", model.GenerateQuoteHeader.Division));
                    //command.Parameters.Add(new OdbcParameter("@cost_id", cost_reference_id));
                    using (var reader = command.ExecuteReader())
                    {
                        try
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    model.GenerateQuoteHeader.Division = reader.GetString(0);
                  model.GenerateQuoteHeader.PartDescription = reader.GetString(1);
                }
                            }
                            else
                            {
                                try
                                {
                                    //cnn_odbc.Close();
                                    using (var command_z = new OdbcCommand(sqlScript, cnn_odbc))
                                    {
                                        sqlScript = @"select CASE WHEN UPPER(trim(look2.drdl01))='BOLT' then '710000'  
	                                                    when upper(trim(look2.drdl01))='SCREWS' then '710000'
                                                        when upper(trim(look2.drdl01))='SCREW' then '710000'
	                                                    when upper(trim(look2.drdl01))='SPECIALS' then '710000'
	                                                    when upper(trim(look2.drdl01))='SRB RING' then '710000'
	                                                    when upper(trim(look2.drdl01))='STUDS' 
	                                                    then '710000' ELSE '730000' END product_code
                                                                from proddta.f4102 join proddta.f4101 on ibitm = imitm
                                                             left outer join prodctl.f0005 look on trim(look.drky)= ibsrp3
                                                               left outer join prodctl.f0005 look2 on trim(look2.drky)= ibsrp2
                                                                where ibitm = ?
                                                            and look.drsy = '41'
                                                             and look.drrt = 'S3'
                                                            and look2.drsy = '41'
                                                         fetch first row only";
                                        command_z.Parameters.Add(new OdbcParameter("@item_id", model.GenerateQuoteHeader.Item_ID));

                                        using (var reader_z = command_z.ExecuteReader())
                                        {
                                            try
                                            {
                                                //if (reader_z.HasRows)
                                                //{
                                                while (reader_z.Read())
                                                {
                                                    model.GenerateQuoteHeader.Division = reader_z.GetString(0);
                                                    //model.GenerateQuoteHeader.PartDescription = reader_z.GetString(0);
                                                    //model.GenerateQuoteHeader.UserID = reader.GetInt32(2);
                                                }
                                                //}
                                                reader_z.Close();
                                            }
                                            catch (Exception ex)
                                            {
                                                //MessageBox.Show("just returned view 11.");
                                            }
                                            //else
                                            //{
                                            //    MessageBox.Show("No users found matching this criteria.");
                                            //    return View();
                                            //}
                                            reader_z.Close();
                                        }
                                    }
                                    cnn_odbc.Close();
                                }

                                catch
                                {

                                    try
                                    {

                                        cnn_odbc.Close();
                                        using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                                        {
                                            cnn.Open();
                                            sqlScript = "select o.division, o.org_id, u.user_id from users u left outer join ORGANIZATION o on u.ORG_ID = o.ORG_ID where u.user_name = @username";
                                            using (var command_x = new SqlCommand(sqlScript, cnn))
                                            {
                                                command_x.Parameters.Add(new SqlParameter("@username", Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)));

                                                using (var reader_x = command_x.ExecuteReader())
                                                {
                                                    if (reader_x.HasRows)
                                                    {
                                                        while (reader_x.Read())
                                                        {
                                                            //model.GenerateQuoteHeader.Division = reader.GetString(0);
                                                            model.GenerateQuoteHeader.Division = reader_x.GetString(0);
                                                            //model.GenerateQuoteHeader.UserID = reader.GetInt32(2);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        MessageBox.Show("No users found matching this criteria.");
                                                        return View();
                                                    }
                                                    reader_x.Close();
                                                }
                                            }
                                            cnn.Close();
                                        }

                                    }
                                    catch
                                    {
                                        MessageBox.Show("Please send this part to your system administrator.");
                                        cnn_odbc.Close();
                                        cnn_odbc.Dispose();
                                        GenerateQuoteHeader gqh = new GenerateQuoteHeader();
                                        //gqh.LoadingPricingValue = 0;

                                        gqh.UnitCost = 0;
                                        gqh.UnitPrice = 0;

                                        //SqlConnection cnn;
                                        if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                                        {
                                            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                                            {
                                                cnn.Open();
                                                gqh.Costing = this.GetCostYearList(cnn);
                                            }
                                        }
                                        var RefreshModel = new OriginalPricingTool
                                        {
                                            GenerateQuoteHeader = gqh,
                                            SalesOrders = null,
                                            Competitors = null,
                                            Components = null,
                                            DemandList = null,
                                            Comments = null,
                                            LTAs = null,
                                            QuoteWorkbench=null,
                                          LTAParts = null
                                        };
                                        return View(RefreshModel);

                                    }
                                }
                            }
                            reader.Close();
                        }
                        catch
                        {
                            reader.Close();
                            cnn_odbc.Close();
                            cnn_odbc.Dispose();

                        }
                    }
                    //cnn_odbc.Close();
                    //cnn_odbc.Dispose();
                }




                if (cnn_odbc == null || cnn_odbc.State == ConnectionState.Closed)
                {

                    cnn_odbc.Open();
                }
            }


            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                cnn.Open();
                string sqlScript;
        

                //go get capacity to load into model for capacity charts
                //come back to capacity

                sqlScript = @"select convert(date,min(starting_date)) start_date
, MAX(earliest_avail.ending_date) end_date
, @qty qty
                                        from (select * from
                                         (select cal_cartesian.[Date] starting_date
                                        , check_load.ending_date
                                        , check_load.VALUE_STREAM
                                        , vs.DIVISION
                                        , vs.Throughput
                                        From (select sum(qty) qty
										 , cap_date_check.ROW_DATE starting_date
											, cal2.ending_Date
	                                        , wo.VALUE_STREAM 
		                                        ,'CURRENT_LOAD'  CAP_TYPE
                                        from [DataWarehouse].[dbo].[sT_CurrentLoadbyWorkOrder] wo
										join (select * From 
												(SELECT *, SUM(NET_CAPACITY) OVER (PARTITION BY VALUE_STREAM ORDER BY ROW_DATE) AS ROLLING_NET
												FROM
												(
													   select 
															  ROW_DATE,
															  cl.VALUE_STREAM,
															  (SUM(QTY)+0) AS LOAD_VAL,
															  SUM(THROUGHPUT) AS THROUGHPUT,
															  SUM(THROUGHPUT) - (SUM(QTY)+25) AS NET_CAPACITY
													   from [DataWarehouse].[dbo].[sT_CurrentLoad] cl
													   inner join [DataWarehouse].[dbo].[sT_ThroughputCapacityByValueStream] cap
													   on cl.VALUE_STREAM = cap.VALUE_STREAM
													   group by cl.VALUE_STREAM, ROW_DATE
												) T
												where 1=1 
												and ROW_DATE > convert(date,getdate())
												and T.value_stream=@value_stream
												AND T.NET_CAPACITY>0
												) 
												earliest_date
												where earliest_date.ROLLING_NET>0) 
												cap_date_check on 
												cap_date_check.VALUE_STREAM=@value_stream
	                                        join (select convert(date,max(cal.[date])) ending_date
												From [DataWarehouse].[dbo].[sT_LeadTime] lt
												join  [DataWarehouse].[dbo].[Calendar_Dim] cal on cal.[Date] between 
												 convert(date,(select MIN(ROW_DATE) From (SELECT *, SUM(NET_CAPACITY) OVER (PARTITION BY VALUE_STREAM ORDER BY ROW_DATE) AS ROLLING_NET
												FROM
												(
													   select 
															  ROW_DATE,
															  cl.VALUE_STREAM,
															  (SUM(QTY)+@qty) AS LOAD_VAL,
															  SUM(THROUGHPUT) AS THROUGHPUT,
															  SUM(THROUGHPUT) - (SUM(QTY)+@qty) AS NET_CAPACITY
													   from [DataWarehouse].[dbo].[sT_CurrentLoad] cl
													   inner join [DataWarehouse].[dbo].[sT_ThroughputCapacityByValueStream] cap
													   on cl.VALUE_STREAM = cap.VALUE_STREAM
													   group by cl.VALUE_STREAM, ROW_DATE
												) T
												where 1=1 
												and ROW_DATE > convert(date,getdate()) 
												and T.value_stream=@value_stream
												) 
												earliest_date
												where earliest_date.ROLLING_NET>0))
												and
												(dateadd(day,lt.average_actual_span_time,convert(date,(select MIN(ROW_DATE) From 
												(SELECT *, SUM(NET_CAPACITY) OVER (PARTITION BY VALUE_STREAM ORDER BY ROW_DATE) AS ROLLING_NET
												FROM
												(
													   select 
															  ROW_DATE,
															  cl.VALUE_STREAM,
															  (SUM(QTY)+@qty) AS LOAD_VAL,
															  SUM(THROUGHPUT) AS THROUGHPUT,
															  SUM(THROUGHPUT) - (SUM(QTY)+@qty) AS NET_CAPACITY
													   from [DataWarehouse].[dbo].[sT_CurrentLoad] cl
													   inner join [DataWarehouse].[dbo].[sT_ThroughputCapacityByValueStream] cap
													   on cl.VALUE_STREAM = cap.VALUE_STREAM
													   group by cl.VALUE_STREAM, ROW_DATE
												) T
												where 1=1 
												and ROW_DATE > convert(date,getdate())
												and T.value_stream=@value_stream
												AND T.NET_CAPACITY>0
												) 
												earliest_date
												where earliest_date.ROLLING_NET>0))))
												where 1=1 and ITEM_ID=@item_id) cal2 on cal2.ending_date between cap_date_check.ROW_DATE and cal2.ending_date
                                         where 1=1
                                         group by cap_date_check.ROW_DATE
	                                        , cal2.[ending_date]
	                                        , wo.VALUE_STREAM
	                                        ) check_load 
	                                        join (Select distinct [Date] from [DataWarehouse].[dbo].[Calendar_Dim]) cal_cartesian on cal_cartesian.[Date] 
												between check_load.starting_date and check_load.ending_date
		                                        left outer join [DataWarehouse].[dbo].[sT_ThroughputCapacityByValueStream] vs on vs.VALUE_STREAM=check_load.VALUE_STREAM
		                                        where 1=1 
		                                        and check_load.VALUE_STREAM=vs.VALUE_STREAM
		                                        group by cal_cartesian.[Date], check_load.ending_date,check_load.VALUE_STREAM, vs.DIVISION, vs.Throughput
		                                        ) cap
		                                        where 1=1
												) earliest_avail
		                                        join sT_PartDetails_JDE pd on pd.ibsrp5=earliest_avail.value_stream
		                                        left outer join [DataWarehouse].[dbo].[sT_LeadTime] lt on lt.ITEM_ID=pd.IBITM 
		                                        where 1=1
		                                        and pd.ibitm=@item_id
												and earliest_avail.DIVISION=@division
												and VALUE_STREAM=@value_stream
											group by VALUE_STREAM, earliest_avail.DIVISION
											union
											select start_date, end_date, qty from [DataWarehouse].[dbo].[sT_CurrentLoadbyWorkOrder] where value_stream=@value_stream;";
            
            using (var command = new SqlCommand(sqlScript, cnn))
            {
                SqlParameter item_val = new SqlParameter
                {
                    ParameterName = "@item_id",
                    Value = model.GenerateQuoteHeader.Item_ID
                };
                command.Parameters.Add(item_val);
                SqlParameter div = new SqlParameter
                {
                    ParameterName = "@value_stream",
                    Value = model.GenerateQuoteHeader.ValueStream
                };
                command.Parameters.Add(div);
                SqlParameter division_param = new SqlParameter
                {
                    ParameterName = "@division",
                    Value = model.GenerateQuoteHeader.Division
                };
                command.Parameters.Add(division_param);
                    SqlParameter qty_param = new SqlParameter
                    {
                        ParameterName = "@qty",
                        Value = model.GenerateQuoteHeader.Division
                    };
                    command.Parameters.Add(qty_param);

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                CurrentLoadByWorkorder item = new CurrentLoadByWorkorder()
                                {
                                    //Capacity_id = i,
                                    StartDate = reader.GetDateTime(0),
                                    EndDate = reader.GetDateTime(1),
                                    Quantity = reader.GetInt32(2)
                                    //SOUnitCost = Convert.ToDecimal(string.Format("{0:0.00}", Math.Round(reader.GetDecimal(8), 2)))
                                };
                                cl.Add(item);
                                //i++;
                            }
                        }
                        reader.Close();

                    }

                }
                sqlScript = @"select min(starting_date) starting_date
                                    , MAX(earliest_avail.ending_date) ending_date
                            , datediff(DAY,convert(date,getdate()),convert(date,MAX(earliest_avail.ending_date))) total_days
                                , datediff(DAY, convert(date, getdate()), convert(date, MAX(earliest_avail.ending_date))) / 7 total_weeks
                                    , VALUE_STREAM
                                    , earliest_avail.DIVISION 
                                        from (select * from
                                         (select cal_cartesian.[Date] starting_date
                                        , check_load.ending_date
                                        , check_load.VALUE_STREAM
                                        , vs.DIVISION
                                        , vs.Throughput
                                        From (select sum(qty) qty
										 , cap_date_check.ROW_DATE starting_date
											, cal2.ending_Date
	                                        , wo.VALUE_STREAM 
		                                        ,'CURRENT_LOAD'  CAP_TYPE
                                        from [DataWarehouse].[dbo].[sT_CurrentLoadbyWorkOrder] wo
										join (select * From 
												(SELECT *, SUM(NET_CAPACITY) OVER (PARTITION BY VALUE_STREAM ORDER BY ROW_DATE) AS ROLLING_NET
												FROM
												(
													   select 
															  ROW_DATE,
															  cl.VALUE_STREAM,
															  (SUM(QTY)+@qty) AS LOAD,
                                                              SUM(THROUGHPUT) AS 'THROUGHPUT',
															  SUM(THROUGHPUT) - (SUM(QTY) + @qty) AS 'NET_CAPACITY'
                                                       from[DataWarehouse].[dbo].[sT_CurrentLoad] cl
                                                      inner join[DataWarehouse].[dbo].[sT_ThroughputCapacityByValueStream] cap
                                                     on cl.VALUE_STREAM = cap.VALUE_STREAM
                                                       group by cl.VALUE_STREAM, ROW_DATE
												) T
                                                where 1 = 1
                                                and ROW_DATE > convert(date, getdate())
                                                and T.value_stream =@value_stream
                                                AND T.NET_CAPACITY > 0
												) 
												earliest_date
                                                where earliest_date.ROLLING_NET > 0) 
												cap_date_check on
                                                cap_date_check.VALUE_STREAM = @value_stream
                                            join(select convert(date, max(cal.[date])) ending_date
                                                From[DataWarehouse].[dbo].[sT_LeadTime] lt
                                                join[DataWarehouse].[dbo].[Calendar_Dim] cal on cal.[Date] between
                                                convert(date,(select MIN(ROW_DATE) From (SELECT *, SUM(NET_CAPACITY) OVER (PARTITION BY VALUE_STREAM ORDER BY ROW_DATE) AS ROLLING_NET
												FROM
												(
													   select 
															  ROW_DATE,
															  cl.VALUE_STREAM,
															  (SUM(QTY)+@qty) AS LOAD_VAL,
															  SUM(THROUGHPUT) AS THROUGHPUT,
															  SUM(THROUGHPUT) - (SUM(QTY)+@qty) AS NET_CAPACITY
													   from [DataWarehouse].[dbo].[sT_CurrentLoad] cl
													   inner join [DataWarehouse].[dbo].[sT_ThroughputCapacityByValueStream] cap
													   on cl.VALUE_STREAM = cap.VALUE_STREAM
													   group by cl.VALUE_STREAM, ROW_DATE
												) T
												where 1=1 
												and ROW_DATE > convert(date,getdate()) 
												and T.value_stream=@value_stream
												) 
												earliest_date
												where earliest_date.ROLLING_NET>0))
												and
												(dateadd(day,lt.average_actual_span_time,convert(date,(select MIN(ROW_DATE) From 
												(SELECT *, SUM(NET_CAPACITY) OVER (PARTITION BY VALUE_STREAM ORDER BY ROW_DATE) AS ROLLING_NET
												FROM
												(
													   select 
															  ROW_DATE,
															  cl.VALUE_STREAM,
															  (SUM(QTY)+@qty) AS LOAD_VAL,
															  SUM(THROUGHPUT) AS THROUGHPUT,
															  SUM(THROUGHPUT) - (SUM(QTY)+@qty) AS NET_CAPACITY
													   from [DataWarehouse].[dbo].[sT_CurrentLoad] cl
													   inner join [DataWarehouse].[dbo].[sT_ThroughputCapacityByValueStream] cap
													   on cl.VALUE_STREAM = cap.VALUE_STREAM
													   group by cl.VALUE_STREAM, ROW_DATE
												) T
												where 1=1 
												and ROW_DATE > convert(date,getdate())
												and T.value_stream=@value_stream
												AND T.NET_CAPACITY>0
												) 
												earliest_date
												where earliest_date.ROLLING_NET>0))))
												where 1 = 1 and ITEM_ID = @item_id) cal2 on cal2.ending_date between cap_date_check.ROW_DATE and cal2.ending_date
                                             where 1 = 1
                                         group by cap_date_check.ROW_DATE
	                                        , cal2.[ending_date]
	                                        , wo.VALUE_STREAM
	                                        ) check_load
                                            join(Select distinct[Date] from[DataWarehouse].[dbo].[Calendar_Dim]) cal_cartesian on cal_cartesian.[Date]
                                                between check_load.starting_date and check_load.ending_date
                                                left outer join[DataWarehouse].[dbo].[sT_ThroughputCapacityByValueStream] vs on vs.VALUE_STREAM=check_load.VALUE_STREAM
                                                where 1=1 
                                                and check_load.VALUE_STREAM=vs.VALUE_STREAM
                                                group by cal_cartesian.[Date], check_load.ending_date, check_load.VALUE_STREAM, vs.DIVISION, vs.Throughput
                                                ) cap
                                                where 1=1
												) earliest_avail
                                                join sT_PartDetails_JDE pd on pd.ibsrp5=earliest_avail.value_stream
                                                left outer join [DataWarehouse].[dbo].[sT_LeadTime] lt on lt.ITEM_ID= pd.IBITM
                                                where 1=1
                                                and pd.ibitm= @item_id
                                                and earliest_avail.DIVISION= @division
                                                and VALUE_STREAM =@value_stream
                                            group by VALUE_STREAM, earliest_avail.DIVISION";

                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    SqlParameter item_val = new SqlParameter
                    {
                        ParameterName = "@item_id",
                        Value = model.GenerateQuoteHeader.Item_ID
                    };
                    command.Parameters.Add(item_val);
                    SqlParameter vs = new SqlParameter
                    {
                        ParameterName = "@value_stream",
                        Value = model.GenerateQuoteHeader.ValueStream
                    };
                    command.Parameters.Add(vs);
                    SqlParameter dv = new SqlParameter
                    {
                        ParameterName = "@division",
                        Value = model.GenerateQuoteHeader.Division
                    };
                    command.Parameters.Add(dv);
                    SqlParameter qty = new SqlParameter
                    {
                        ParameterName = "@qty",
                        Value = model.GenerateQuoteHeader.QuoteQuantity
                    };
                    command.Parameters.Add(qty);


                    using (var reader = command.ExecuteReader())
                    {
                        try
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    model.GenerateQuoteHeader.EarliestStartDate = reader.GetDateTime(0);
                                    model.GenerateQuoteHeader.EstimatedFinishDate = reader.GetDateTime(1);
                                    model.GenerateQuoteHeader.TotalEstimatedLTDays = reader.GetInt32(2);
                                    model.GenerateQuoteHeader.TotalEstimatedLTWeeks = reader.GetInt32(3);
                                }
                            }
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }

                }
                    //sqlScript= @"SELECT  *,
                    //                           SUM(NET_CAPACITY) OVER (PARTITION BY VALUE_STREAM ORDER BY ROW_DATE) AS ROLLING_NET
                    //                    FROM
                    //                    (
                    //                           select 
                    //                                  ROW_DATE,
                    //                                  cl.VALUE_STREAM,
                    //                                  (SUM(QTY)+15000000) AS "LOAD",
                    //                                  SUM(THROUGHPUT) AS "THROUGHPUT",
                    //                                  SUM(THROUGHPUT) - (SUM(QTY) + 15000000) AS "NET_CAPACITY"
                    //                           from sT_CurrentLoad cl
                    //                           inner
                    //                           join sT_ThroughputCapacityByValueStream cap

                    //                     on cl.VALUE_STREAM = cap.VALUE_STREAM
                    //                           group by cl.VALUE_STREAM, ROW_DATE
                    //                    ) T
                    //                    where ROW_DATE > '2020-06-01'-- This would be the starting date
                    //                    and value_stream = 'FF3'
                    //                    AND ROLLING_NET"


                                if (model.GenerateQuoteHeader.Division == "730000")
                {
                    sqlScript = @"select top 1	trim(irmcu) as irmcu, trim(irdsc1) as irdsc1
			                        from 		[DataWarehouse].[dbo].[iT_RoutingMasterFile_F3003]
			                        where 		trim(irmmcu) in ('730000','710000','750000','720000','780000','734000')
			                        and 		irkit = @item_num
			                        and			substring(ltrim(irmcu),4,1) in ('4','3')
			                        order by	iropsq ";
                }
                else
                {
                    sqlScript = @"select top 1		trim(irmcu) as irmcu, trim(irdsc1) as irdsc1
			                        from 		[DataWarehouse].[dbo].[iT_RoutingMasterFile_F3003]
			                        where 		trim(irmmcu) in ('730000','710000','750000','720000','780000','734000')
			                        and 		irkit = @item_num
			                        order by	iropsq ";
                }

                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    //command.Parameters.Add(new OdbcParameter("@division", model.GenerateQuoteHeader.Division));
                    command.Parameters.Add(new SqlParameter("@item_num", model.GenerateQuoteHeader.Item_ID));
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {

                            while (reader.Read())
                            {
                                model.GenerateQuoteHeader.GetFirstOp = reader.GetString(0).Substring(3, 3);
                            }
                        }
                        else
                        {
                            //MessageBox.Show("Could not find first operation given the business logic in place.");
                        }
                        reader.Close();
                    }
                }

                sqlScript = @"select isnull((sum(lipqoh)-sum(lipcom)-sum(lihcom)-sum(lifcom)-sum(liqowo)),0) qty_avail 
                        from [DataWarehouse].[dbo].[iT_ItemLocationFile_F41021] qty left outer join [DataWarehouse].[dbo].[iT_ItemBranchFile_F4102] ibm on qty.liitm=ibm.ibitm
                                     and qty.limcu=ibm.ibmcu
                                    where 1=1 
                                    and ltrim(limcu) in ('730000','710000','750000','720000','780000','734000')
                                    and ibm.ibitm = @item_num";
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    //command.Parameters.Add(new OdbcParameter("@division", model.GenerateQuoteHeader.Division));
                    command.Parameters.Add(new SqlParameter("@item_num", model.GenerateQuoteHeader.Item_ID));
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {

                            while (reader.Read())
                            {
                                model.GenerateQuoteHeader.QtyAvail = reader.GetInt32(0);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Could not find quantity available.");
                        }
                        reader.Close();
                    }
                }

                //get alternative lead time valuation for Bristol Industries 
                int bi_lead_time = 0;
                int bi_rm_lead_time = 0;

        //             sqlScript = @"select fin.lead_time, fin.rm_lead_time, fin.acq_multiple from [DataWarehouse].[dbo].[iT_Sales_ItemLeadTimes] fin
        //where 1=1
        //and fin.item_name=@item_num
        //and acq_multiple=(select top 1 isnull(acq,(select max(ag_mom.acq) from (select row_number() over (order by ag.item_num, ag.acq) as row_num, ag.acq, ag.lead_time from 
        //		(select check_lt.item_num, check_lt.item_name, check_lt.acq, check_lt.lead_time, check_lt.rm_lead_time 
        //		From (select distinct lt.item_num, lt.item_name, lt.acq, lt.lead_time, lt.rm_lead_time 
        //		From [DataWarehouse].[dbo].[iT_Sales_ItemLeadTimes] lt where 1=1 and item_name=@item_num
        //		and lt.acq_multiple=lt.acq
        //		union 
        //		select distinct lt_sub.item_num, lt_sub.item_name, lt_sub.acq_multiple, lt_sub.lead_time, lt_sub.rm_lead_time
        //		From [DataWarehouse].[dbo].[iT_Sales_ItemLeadTimes] lt_sub where 1=1 and item_name=@item_num and lt_sub.acq<>lt_Sub.acq_multiple) check_lt where 1=1
        //		) ag where 1=1) ag_mom)) from (select case when (Select def_sub1.acq_multiple acq from [DataWarehouse].[dbo].[iT_Sales_ItemLeadTimes] def_sub1 where acq_multiple=@acq and item_name=@item_num)=@acq then (Select def_sub.acq_multiple acq from [DataWarehouse].[dbo].[iT_Sales_ItemLeadTimes] def_sub where acq_multiple=@acq and item_name=@item_num)
        //else (select min(ag_mom.acq) from (select row_number() over (order by ag.item_num, ag.acq) as row_num, ag.acq, ag.lead_time from 
        //		(select check_lt.item_num, check_lt.item_name, check_lt.acq, check_lt.lead_time, check_lt.rm_lead_time 
        //		From (select distinct lt.item_num, lt.item_name, lt.acq, lt.lead_time, lt.rm_lead_time 
        //		From [DataWarehouse].[dbo].[iT_Sales_ItemLeadTimes] lt where 1=1 and item_name=@item_num
        //		and lt.acq_multiple=lt.acq
        //		union 
        //		select distinct lt_sub.item_num, lt_sub.item_name, lt_sub.acq_multiple, lt_sub.lead_time, lt_sub.rm_lead_time
        //		From [DataWarehouse].[dbo].[iT_Sales_ItemLeadTimes] lt_sub where 1=1 and item_name=@item_num and lt_sub.acq<>lt_Sub.acq_multiple) check_lt where 1=1
        //		) ag where @acq<ag.acq) ag_mom)
        //end acq from (select row_number() over (order by tb.item_num, tb.acq) as row_num, tb.acq, tb.lead_time from 
        //(select check_lt.item_num, check_lt.item_name, check_lt.acq, check_lt.lead_time, check_lt.rm_lead_time 
        //   From (select distinct lt.item_num, lt.item_name, lt.acq, lt.lead_time, lt.rm_lead_time 
        //   From [DataWarehouse].[dbo].[iT_Sales_ItemLeadTimes] lt where 1=1 and item_name=@item_num
        //and lt.acq_multiple=lt.acq
        //   union 
        //   select distinct lt_sub.item_num, lt_sub.item_name, lt_sub.acq_multiple, lt_sub.lead_time, lt_sub.rm_lead_time
        //   From [DataWarehouse].[dbo].[iT_Sales_ItemLeadTimes] lt_sub where 1=1 and item_name=@item_num and lt_sub.acq<>lt_Sub.acq_multiple) check_lt where 1=1
        //)  tb) def) tbl)";
        sqlScript = "select average_actual_span_time from [DataWarehouse].[dbo].[sT_LeadTime] where ltrim(division)=@division_id and item_id=@item_num";
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    //command.Parameters.Add(new OdbcParameter("@division", model.GenerateQuoteHeader.Division));
                    command.Parameters.Add(new SqlParameter("@item_num", model.GenerateQuoteHeader.ibitm));
          command.Parameters.Add(new SqlParameter("@division_id", model.GenerateQuoteHeader.Division));
          //if (model.GenerateQuoteHeader.QuoteQuantity > 0)
          //{
          //    command.Parameters.Add(new SqlParameter("@acq", model.GenerateQuoteHeader.QuoteQuantity));
          //}
          //else
          //{
          //    command.Parameters.Add(new SqlParameter("@acq", model.GenerateQuoteHeader.ACQ));
          //}
          using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                bi_lead_time = reader.GetInt32(0);
                                //bi_rm_lead_time = reader.GetInt32(1);
                            }
                        }
                        else
                        {
                            //MessageBox.Show("Could not find lead time for brist.");
                        }
                        reader.Close();
                    }
                }

                if (model.GenerateQuoteHeader.MfgLT != null)
                {
                    if (model.GenerateQuoteHeader.Division == "710000")
                    {
                        model.GenerateQuoteHeader.MfgLT = (model.GenerateQuoteHeader.MfgLT / 5);
                        model.GenerateQuoteHeader.CumLT = ((model.GenerateQuoteHeader.MfgLT) + ((model.GenerateQuoteHeader.CumLT - (model.GenerateQuoteHeader.MfgLT * 5)) / 7));
                        model.GenerateQuoteHeader.ActLT = bi_lead_time/5;
                    }
                    else if (model.GenerateQuoteHeader.Division == "730000")// || model.GenerateQuoteHeader.Division=="710000")
                    {
                        model.GenerateQuoteHeader.MfgLT = bi_lead_time/5;
                        model.GenerateQuoteHeader.CumLT = ((model.GenerateQuoteHeader.CumLT - bi_lead_time) / 7);
                        model.GenerateQuoteHeader.RmLT =  bi_rm_lead_time;
                    }
                    else
                    {
                        int mfg_lt_var = 0;
                        mfg_lt_var = model.GenerateQuoteHeader.MfgLT / 5;
                        model.GenerateQuoteHeader.ActLT = bi_lead_time/5;
                        int mfg_keep = model.GenerateQuoteHeader.MfgLT;
                        model.GenerateQuoteHeader.MfgLT = mfg_lt_var;
                        model.GenerateQuoteHeader.CumLT = ((mfg_lt_var) + ((model.GenerateQuoteHeader.CumLT - mfg_keep) / 7));
                    }
                }
                cnn.Close();
            }


            using (OdbcConnection cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString))
            {
                int int_num = 1;
                string sqlScript;
                cnn_odbc.Open();
                //moved components
                model.GenerateQuoteHeader.totalCPQtyCostVar = 0;
                model.GenerateQuoteHeader.totalCPQtyCostSU = 0;
                model.GenerateQuoteHeader.totalCPQtyCostFOH = 0;

                if (model.GenerateQuoteHeader.Division == "730000" || model.GenerateQuoteHeader.Division=="734000")
                {
                    sqlScript = @"select	ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ixum, ibacq, ibstkt, ibltmf, ibltcm, imdsc1,  
                  dec(ixqnty / 1000000, 15, 6) as ixqnty, ixcpnb,  
                  (select sum(lipqoh) from proddta.f41021 where limcu = ixmmcu and liitm = ixitm) as pqoh,  
                  (select sum(lipqoh) - sum(lipcom) - sum(lihcom) - sum(lifcom) - sum(liqowo) from proddta.f41021 
                  where limcu = ixmmcu and liitm = ixitm) as avail, 
                  sum(dec(iexscr / 1000000, 15, 7)) as simCost
                , sum(dec(iecsl / 1000000, 15, 7)) as fznCost ,
                        coalesce(sum(case when iecost = 'A1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznA1,
                        coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB2,
						coalesce(sum(case when iecost = 'B1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB1,
						coalesce(sum(case when iecost = 'C3' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC3,
						coalesce(sum(case when iecost = 'C1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC1,
                        coalesce(sum(case when iecost = 'C4' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC4,
						coalesce(sum(case when iecost = 'C2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC2,
						coalesce(sum(case when left(iecost,1) = 'D' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznD0,
                        coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000*ibacq as decimal(15,7)) else 0 end),0) as fznSUTotal
                  from proddta.f3002 join proddta.f4102 on ixitm = ibitm and ixmmcu = ibmcu and left(ibglpt,2) in ('FG', 'CP') 
                  join proddta.f4101 on imitm = ibitm 
                  left join proddta.f30026 on iemmcu = ibmcu and ieitm = ibitm and ieledg = ?
                  where ixtbm in ('M')
                and ixkit = ? 
                  and trim(ixmmcu) = ? 
                  group by    ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ibacq, imdsc1, ixqnty, ibstkt, ixcpnb, ibltmf, ibltcm, ixum";
                }
                else if (model.GenerateQuoteHeader.Division=="710000")
        {
          sqlScript = @"select	ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ixum, ibacq, ibstkt, ibltmf, ibltcm, imdsc1,  
                  dec(ixqnty / 1000000, 15, 6) as ixqnty, ixcpnb,  
                  (select sum(lipqoh) from proddta.f41021 where limcu = ixmmcu and liitm = ixitm) as pqoh,  
                  (select sum(lipqoh) - sum(lipcom) - sum(lihcom) - sum(lifcom) - sum(liqowo) from proddta.f41021 
                  where limcu = ixmmcu and liitm = ixitm) as avail, 
                  sum(dec(iexscr / 1000000, 15, 7)) as simCost
                , sum(dec(iecsl / 1000000, 15, 7)) as fznCost ,
                        coalesce(sum(case when left(iecost,1) = 'A' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simA1,
                        coalesce(sum(case when iecost = 'B2' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB2,
						coalesce(sum(case when iecost = 'B1' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB1,
						coalesce(sum(case when iecost = 'C3' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC3,
						coalesce(sum(case when iecost = 'C1' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC1,
                       coalesce(sum(case when iecost = 'C4' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC4,
						coalesce(sum(case when iecost = 'C2' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC2,
						coalesce(sum(case when left(iecost,1) = 'D' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simD0,
                        coalesce(sum(case when iecost = 'B2' then cast(iexscr/1000000*ibacq as decimal(15,7)) else 0 end),0) simSUTotal
                  from proddta.f3002 join proddta.f4102 on ixitm = ibitm and ixmmcu = ibmcu and left(ibglpt,2) in ('FG', 'CP', 'RM') 
                  join proddta.f4101 on imitm = ibitm 
                  left join proddta.f30026 on iemmcu = ibmcu and ieitm = ibitm and ieledg = ?
                  where ixtbm in ('M')
                and ixkit = ? 
                  and trim(ixmmcu) = ? 
                  group by    ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ibacq, imdsc1, ixqnty, ibstkt, ixcpnb, ibltmf, ibltcm, ixum";
        }
                else
                {
                    sqlScript = @"select	ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ixum, case when trim(ixmmcu)='780000' then case when ibacq=0 then 1 else ibacq end else ibacq end as ibacq, ibstkt, ibltmf, ibltcm,imdsc1,
                  CAST(dec(ixqnty / 1000000, 15, 6) AS INTEGER) as ixqnty, ixcpnb,  
                  ifnull((select sum(lipqoh) from proddta.f41021 where limcu = ixmmcu and liitm = ixitm),0) as pqoh,  
                  ifnull((select sum(lipqoh) - sum(lipcom) - sum(lihcom) - sum(lifcom) - sum(liqowo) from proddta.f41021 
                  where limcu = ixmmcu and liitm = ixitm),0) as avail, 
                  sum(dec(iexscr / 1000000, 15, 7)) as simCost
                , sum(dec(iecsl / 1000000, 15, 7)) as fznCost ,
                        coalesce(sum(case when iecost = 'A1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznA1,
                        coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB2,
						coalesce(sum(case when iecost = 'B1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB1,
						coalesce(sum(case when iecost = 'C3' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC3,
						coalesce(sum(case when iecost = 'C1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC1,
                        coalesce(sum(case when iecost = 'C4' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC4,
						coalesce(sum(case when iecost = 'C2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC2,
						coalesce(sum(case when left(iecost,1) = 'D' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznD0,
                        coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000*ibacq as decimal(15,7)) else 0 end),0) as fznSUTotal
                  from proddta.f3002 join proddta.f4102 on ixitm = ibitm and ixmmcu = ibmcu and left(ibglpt,2) in ('FG', 'CP', 'RM') 
                  join proddta.f4101 on imitm = ibitm 
                  left join proddta.f30026 on iemmcu = ibmcu and ieitm = ibitm and ieledg = ?
                  where ixtbm in ('M')
                and ixkit = ? 
                  and trim(ixmmcu) = ? 
                  group by    ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ibacq, imdsc1, ixqnty, ibstkt, ixcpnb, ibltmf, ibltcm, ixum";
                }
                //and dec(ixqnty / 1000000, 15, 6)>=1
                using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                {
                    command.Parameters.Add(new OdbcParameter("@cost_year", cost_reference_id));
                    command.Parameters.Add(new OdbcParameter("@item_num", model.GenerateQuoteHeader.Item_ID));
                    command.Parameters.Add(new OdbcParameter("@division", model.GenerateQuoteHeader.Division));

                  using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                ComponentFound item = new ComponentFound()
                                {
                                    //reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                                    ItemKey = int_num,
                                    ComponentNum = reader.GetString(4), //ixitm
                                    ComponentUOM = reader.GetString(5), //ixum
                                    ComponentDesc = reader.GetString(10), //imdsc1
                                    ComponentStock = reader.GetString(7), //ibstkt
                                    ComponentACQ = reader.IsDBNull(6) ? 0 : (decimal)reader.GetValue(6),
                                    ComponentMfgLT = reader.IsDBNull(8) ? 0 : (decimal)reader.GetValue(8),
                                    ComponentQty = reader.GetInt32(11), //ixqnty
                                    ChangeQuantity = reader.GetInt32(11), //ixqnty
                                    ComponentOnHand = reader.GetInt32(13), //pqoh
                                    ComponentAvailQt = reader.GetInt32(14), //avail
                                    //ComponentCost = reader.GetDecimal(15), //fzncost
                                    ComponentCostFzn = reader.IsDBNull(16) ? 0 : (decimal)reader.GetValue(16),
                                    CompMtlCost = reader.IsDBNull(17) ? 0 : (decimal)reader.GetValue(17),
                                    CompSetup =reader.IsDBNull(18) ? 0 : (decimal)reader.GetValue(18),
                                    CompLabor = reader.IsDBNull(19) ? 0 : (decimal)reader.GetValue(19),
                                    CompLbrVOH = reader.IsDBNull(20) ? 0 : (decimal)reader.GetValue(20),
                                    CompMchVOH = reader.IsDBNull(21) ? 0 : (decimal)reader.GetValue(21),
                                    CompLbrFOH = reader.IsDBNull(22) ? 0 : (decimal)reader.GetValue(22),
                                    CompMchFOH = reader.IsDBNull(23) ? 0 : (decimal)reader.GetValue(23),
                                    CompOSP = reader.IsDBNull(24) ? 0 : (decimal)reader.GetValue(24),
                                    CompSUTotal = reader.IsDBNull(25) ? 0 : (decimal)reader.GetValue(25),
                                    FznCPQtyCostSU = Math.Round((reader.IsDBNull(25) ? 0 : (decimal)reader.GetValue(25)),5) / (reader.IsDBNull(6) ? 0 : (decimal)reader.GetValue(6)),
                                    FznCPQtyCostVar = (reader.IsDBNull(17) ? 0 : (decimal)reader.GetValue(17)) + (reader.IsDBNull(19) ? 0 : (decimal)reader.GetValue(19)) + (reader.IsDBNull(20) ? 0 : (decimal)reader.GetValue(20)) + (reader.IsDBNull(21) ? 0 : (decimal)reader.GetValue(21)) + (reader.IsDBNull(24) ? 0 : (decimal)reader.GetValue(24)), //fzna1 fznb1 fznD0 fznc1 fznc3
                                    FznCPQtyCostFOH = (((reader.IsDBNull(23) ? 0 : (decimal)reader.GetValue(23)) + (reader.IsDBNull(22) ? 0 : (decimal)reader.GetValue(22))) * (reader.IsDBNull(6) ? 0 : (decimal)reader.GetValue(6))) / (reader.IsDBNull(6) ? 0 : (decimal)reader.GetValue(6))
                                    //loop through?
                                };

                                if (Convert.ToDecimal(item.ChangeQuantity) > 0)
                                {
                                    string test_compNum = item.ComponentNum;

                                    int test_compqty = item.ComponentQty;// = reader.GetInt32(8), //ixqnty

                                    int test_availqt = item.ComponentAvailQt; //= reader.GetInt32(11), //avail

                                    decimal test_CostFzn = item.ComponentCostFzn; //= reader.GetDecimal(12), //fzna1
                                    decimal test_FznRecalc = item.ComponentCostFznRecalc; //= reader.GetDecimal(12), //fzna1
                                    decimal test_MtlCost = item.CompMtlCost; //= reader.GetDecimal(14), //fznb2
                                    decimal test_ComLabor = item.CompLabor; //= reader.GetDecimal(13), //fzna1
                                    decimal test_ComSetup = item.CompSetup;
                                    decimal test_CompLbrVOH = item.CompLbrVOH; //= reader.GetDecimal(16), //fznc3
                                    decimal test_CompMchVOH = item.CompMchVOH; //= reader.GetDecimal(17), //fznc1
                                    decimal test_CompLbrFOH = item.CompLbrFOH; //= reader.GetDecimal(18), //fznc4
                                    decimal test_CompMchFOH = item.CompMchFOH; //= reader.GetDecimal(19), //fznc2
                                    decimal test_CompOSP = item.CompOSP; // = reader.GetDecimal(20), //fznd0
                                    decimal test_compSU = item.CompSUTotal; //= reader.GetDecimal(21), //fznsutotal

                                    //if (item.FznCPQtyCostVar != 0 && item.FznCPQtyCostSU != 0)
                                    //{

                                    model.GenerateQuoteHeader.totalCPQtyCostVar = (item.FznCPQtyCostVar * item.ChangeQuantity) + model.GenerateQuoteHeader.totalCPQtyCostVar;
                                    model.GenerateQuoteHeader.totalCPQtyCostSU = (item.FznCPQtyCostSU * item.ChangeQuantity) + model.GenerateQuoteHeader.totalCPQtyCostSU;
                                    model.GenerateQuoteHeader.totalCPQtyCostFOH = (item.FznCPQtyCostFOH * item.ChangeQuantity) + model.GenerateQuoteHeader.totalCPQtyCostFOH;
        
                                }
                                else
                                {
                                    if (item.FznCPQtyCostVar != 0 && item.FznCPQtyCostSU != 0)
                                    {
                                        model.GenerateQuoteHeader.totalCPQtyCostVar = (item.FznCPQtyCostVar) + model.GenerateQuoteHeader.totalCPQtyCostVar;
                                        model.GenerateQuoteHeader.totalCPQtyCostSU = (item.FznCPQtyCostSU) + model.GenerateQuoteHeader.totalCPQtyCostSU;
                                        model.GenerateQuoteHeader.totalCPQtyCostFOH = (item.FznCPQtyCostFOH) + model.GenerateQuoteHeader.totalCPQtyCostFOH;
                                    }
                                    else
                                    {
                                        model.GenerateQuoteHeader.totalCPQtyCostVar = 0 + model.GenerateQuoteHeader.totalCPQtyCostVar;
                                        model.GenerateQuoteHeader.totalCPQtyCostSU = 0 + model.GenerateQuoteHeader.totalCPQtyCostSU;
                                        model.GenerateQuoteHeader.totalCPQtyCostFOH = 0 + model.GenerateQuoteHeader.totalCPQtyCostFOH;
                                    }
                                }
                                int_num = int_num + 1;
                                Components.Add(item);


                            }
                        }
                       
                    else
                    {
                    //loop through my list and find all of the ACQ that is available.

                    foreach (var item in Components)
                    {
                        model.GenerateQuoteHeader.totalCPQtyCostVar = 0;
                        model.GenerateQuoteHeader.totalCPQtyCostSU = 0;
                        model.GenerateQuoteHeader.totalCPQtyCostFOH = 0;

                        model.GenerateQuoteHeader.totalCPQtyCostVar = (item.FznCPQtyCostVar * item.ChangeQuantity) + model.GenerateQuoteHeader.totalCPQtyCostVar;
                        model.GenerateQuoteHeader.totalCPQtyCostSU = (item.FznCPQtyCostSU * item.ChangeQuantity) + model.GenerateQuoteHeader.totalCPQtyCostSU;
                        model.GenerateQuoteHeader.totalCPQtyCostFOH = (item.FznCPQtyCostFOH * item.ChangeQuantity) + model.GenerateQuoteHeader.totalCPQtyCostFOH;

                    }


                }
                        reader.Close();
                    }

                }

            }

        //    int CUSTOMER_RECENT_PRICE_FLAG = 0;
        //int BASE_PRICE_FLAG = 0;
        //decimal BASE_PRICE = 0;
        //int RESERVE_FLAG = 0;
        //int APPLY_VARIANCE_ADJUSTMENT = 0;


            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                cnn.Open();

                string customer_name_convert_search = model.GenerateQuoteHeader.CustomerName;
                //string customer_number_convert;
                string customer_name = null;
                string customer_num = null;
                int customer_name_length = 0;

        

        if (customer_name_convert_search != null)
                {
                    customer_name_length = customer_name_convert_search.Length - 9;
                }
                int customer_num_length_start;

                if (model.GenerateQuoteHeader.CustomerName != null)
                {
                    customer_name = customer_name_convert_search.Substring(0, customer_name_length);
                    customer_num_length_start = customer_name_length + 3;
                    customer_num = customer_name_convert_search.Substring(customer_num_length_start, 6);
                }

                int[] qty_breaks = { 10, 25, 50, 75, 100, 250, 500, 750, 1000, 2000, 2500, 5000, 10000, 20000, 25000, 50000, 100000, 25000, 50000, 100000, model.GenerateQuoteHeader.QuoteQuantity };
                int n = 0;
                
                foreach (int y in qty_breaks)
                {

          //string sqlScript_adj = @"SELECT isnull(SUM(ADJUSTMENT),0) TOTAL_ADJUSTMENT, CUSTOMER_RECENT_PRICE_FLAG, BASE_PRICE_FLAG, BASE_PRICE, RESERVE_FLAG, APPLY_VARIANCE_ADJUSTMENT
          //                      , VALUE_STREAM, PRODUCT_GROUP, PRODUCT_CODE, CUSTOMER_NUMBER, INTERNAL_PART_NUMBER, CUSTOMER_PART_NUMBER, QUANTITY_BREAK_RANGE_START
          //                    , QUANTITY_BREAK_RANGE_END, OTHER
          //                  FROM STRATEGIC_PRICING_ADJUSTMENTS where ([status]=2 OR [status]=1) AND DIVISION=@DIVISION ";
          //string sqlScript_div = @" and DIVISION=@DIVISION   ";
          //string sqlScript_strt = @"  AND ( ";
          //string sqlScript_vs = @"  VALUE_STREAM =@VALUE_STREAM ";
          //string sqlScript_pg = @" OR product_Group=@product_group ";
          //string sqlScript_pc = @" OR product_code=@product_code ";
          //string sqlScript_cn = @" OR customer_number=@customer_number ";
          //string sqlScript_ipn = @" OR internal_part_number=@internal_part_number ";
          //string sqlScript_cpn = @" OR customer_part_number=@customer_part_number ";
          //string sqlScript_close = @"  ) and @qty_of_interest between QUANTITY_BREAK_RANGE_START and QUANTITY_BREAK_RANGE_END GROUP BY CUSTOMER_RECENT_PRICE_FLAG, BASE_PRICE_FLAG, BASE_PRICE, RESERVE_FLAG, APPLY_VARIANCE_ADJUSTMENT,VALUE_STREAM, PRODUCT_GROUP, PRODUCT_CODE, CUSTOMER_NUMBER, INTERNAL_PART_NUMBER, CUSTOMER_PART_NUMBER, QUANTITY_BREAK_RANGE_START, QUANTITY_BREAK_RANGE_END, OTHER ";
          //string sqlScript_true_end = @"; ";
          ////string sqlScript_other = @" and other=@other ";

          //string sqlScript = sqlScript_adj + sqlScript_div;
          //int used_or_function = 0;
          //if (model.GenerateQuoteHeader.ValueStream != null || model.GenerateQuoteHeader.ProductCode != null || model.GenerateQuoteHeader.ProductGroup != null || model.GenerateQuoteHeader.CustomerNum != null || model.GenerateQuoteHeader.PartNumber != null)
          //{
          //    sqlScript = sqlScript + sqlScript_strt;
          //    if (model.GenerateQuoteHeader.ValueStream != null)
          //    {
          //        sqlScript = sqlScript + sqlScript_vs;
          //        used_or_function = 1;
          //    }
          //    else
          //    {
          //        model.GenerateQuoteHeader.ValueStream = "NA";
          //        sqlScript = sqlScript + sqlScript_vs;
          //        used_or_function = 1;
          //    }
          //    if (model.GenerateQuoteHeader.ProductCode != null)
          //    {
          //        sqlScript = sqlScript + sqlScript_pc;
          //        used_or_function = 1;
          //    }
          //    if (model.GenerateQuoteHeader.ProductGroup != null)
          //    {
          //        sqlScript = sqlScript + sqlScript_pg;
          //        used_or_function = 1;
          //    }
          //    if (customer_num != null)
          //    {
          //        sqlScript = sqlScript + sqlScript_cn;
          //        used_or_function = 1;
          //    }
          //    if (model.GenerateQuoteHeader.PartNumber != null)
          //    {
          //        sqlScript = sqlScript + sqlScript_ipn;
          //        used_or_function = 1;
          //    }
          //    if (model.GenerateQuoteHeader.CustomerPartNum != null)
          //    {
          //        sqlScript = sqlScript + sqlScript_cpn;
          //        used_or_function = 1;
          //    }
          //    sqlScript = sqlScript + sqlScript_close;
          //}
          //sqlScript = sqlScript + sqlScript_true_end;


          string sqlScript = @"SELECT isnull(SUM(ADJUSTMENT),0) TOTAL_ADJUSTMENT, CUSTOMER_RECENT_PRICE_FLAG, BASE_PRICE_FLAG, BASE_PRICE, RESERVE_FLAG, APPLY_VARIANCE_ADJUSTMENT
                                          , VALUE_STREAM, PRODUCT_GROUP, PRODUCT_CODE, CUSTOMER_NUMBER, INTERNAL_PART_NUMBER, CUSTOMER_PART_NUMBER, QUANTITY_BREAK_RANGE_START
                                        , QUANTITY_BREAK_RANGE_END, OTHER, LEAD_TIME_FLAG, MINIMUM_ORDER_QTY_FLAG, MINIMUM_ORDER_QUANTITY
                                      FROM STRATEGIC_PRICING_ADJUSTMENTS where [status]=1 and DIVISION=@DIVISION AND (   VALUE_STREAM =@VALUE_STREAM  OR product_Group=@product_group
                                  OR product_code=@product_code ";
                                  string sqlscript2 = @" ) and @qty_of_interest between QUANTITY_BREAK_RANGE_START and QUANTITY_BREAK_RANGE_END ";
          string sqlscript3 = @" and customer_number=@customer_number ";
                    string sqlScript4 = @" and internal_part_number=@internal_part_number "; 
                    string sqlScript5=@" and customer_part_number=@customer_part_number  ";
                            string sqlScriptEnd=@" GROUP BY CUSTOMER_RECENT_PRICE_FLAG, BASE_PRICE_FLAG, BASE_PRICE, RESERVE_FLAG, APPLY_VARIANCE_ADJUSTMENT,VALUE_STREAM, PRODUCT_GROUP, PRODUCT_CODE
                              , CUSTOMER_NUMBER, INTERNAL_PART_NUMBER, CUSTOMER_PART_NUMBER, QUANTITY_BREAK_RANGE_START, QUANTITY_BREAK_RANGE_END, OTHER, LEAD_TIME_FLAG, MINIMUM_ORDER_QTY_FLAG, MINIMUM_ORDER_QUANTITY ";

          
                    if (customer_num!=null)
          {

                        if (model.GenerateQuoteHeader.CustomerPartNum != "")
                        {
                            sqlScript = sqlScript + sqlscript2 + sqlscript3 + sqlScript4 + sqlScript5 + sqlScriptEnd;
                        }
                        else
                        {
                            sqlScript = sqlScript + sqlscript2 + sqlscript3 + sqlScript4 + sqlScriptEnd;
                        }

          }
                    else
          {
                            if (model.GenerateQuoteHeader.CustomerPartNum != "")
                            {
                                sqlScript = sqlScript + sqlscript2 + sqlScript4 + sqlScript5 + sqlScriptEnd;
                            }

                            else
                        {
                            sqlScript = sqlScript + sqlscript2 + sqlScript4 + sqlScriptEnd;
                        }

          }
          

          using (var command = new SqlCommand(sqlScript, cnn))
                    {
                        //command.Parameters.Add(new SqlParameter("@cost_year", cost_reference_id));

                        command.Parameters.Add(new SqlParameter("@value_stream", String.IsNullOrEmpty(model.GenerateQuoteHeader.ValueStream) ? "" : model.GenerateQuoteHeader.ValueStream.ToString()));
                        command.Parameters.Add(new SqlParameter("@product_code", String.IsNullOrEmpty(model.GenerateQuoteHeader.ProductCode) ? "" : model.GenerateQuoteHeader.ProductCode.ToString()));
                        command.Parameters.Add(new SqlParameter("@product_group", String.IsNullOrEmpty(model.GenerateQuoteHeader.ProductGroup) ? "" : model.GenerateQuoteHeader.ProductGroup.ToString()));
            if (customer_num != null)
            {
              command.Parameters.Add(new SqlParameter("@customer_number", String.IsNullOrEmpty(customer_num) ? "" : customer_num.ToString()));
            }
            //String.IsNullOrEmpty(gqh_model.CUSTOMER_NUMBER) ? "" : gqh_model.CUSTOMER_NUMBER.ToString() //QuoteWorkBench.QCLineComments; + '%'
            command.Parameters.Add(new SqlParameter("@internal_part_number", model.GenerateQuoteHeader.PartNumber));
                        command.Parameters.Add(new SqlParameter("@DIVISION", model.GenerateQuoteHeader.Division));
                        if (model.GenerateQuoteHeader.CustomerPartNum != "")
                        {
                            command.Parameters.Add(new SqlParameter("@customer_part_number", model.GenerateQuoteHeader.CustomerPartNum));
                        }
                        SqlParameter qty_param = new SqlParameter
                        {
                            ParameterName = "@qty_of_interest",
                            Value = y
                        };
                        command.Parameters.Add(qty_param);

                        using (var reader = command.ExecuteReader())
                        {
                            try
                            {

                  if (reader.HasRows)
                  {
                    while (reader.Read())
                    {
                      if (y <= 17.5 && n == 0)
                    {
                      model.GenerateQuoteHeader.factor_adj_10 = reader.GetDecimal(0);
                      //   adj_amount = reader.GetDecimal(0);
                    }
                    else if (y > 17.5 && y <= 37.5 && n == 0)
                    {
                      model.GenerateQuoteHeader.factor_adj_25 = reader.GetDecimal(0);
                      //   adj_amount = reader.GetDecimal(0);
                    }
                    else if (y > 37.5 && y <= 62.5 && n == 0)
                    {
                      model.GenerateQuoteHeader.factor_adj_50 = reader.GetDecimal(0);
                      //  adj_amount = reader.GetDecimal(0);
                    }
                    else if (y > 62.5 && y <= 87.5 && n == 0)
                    {
                      model.GenerateQuoteHeader.factor_adj_75 = reader.GetDecimal(0);
                      // adj_amount = reader.GetDecimal(0);
                    }
                    else if (y > 87.5 && y <= 187.5 && n == 0)
                    {
                      model.GenerateQuoteHeader.factor_adj_100 = reader.GetDecimal(0);
                      //   adj_amount = reader.GetDecimal(0);
                    }
                    else if (y > 187.5 && y <= 375 && n == 0)
                    {
                      model.GenerateQuoteHeader.factor_adj_250 = reader.GetDecimal(0);
                      //  adj_amount = reader.GetDecimal(0);
                    }
                    else if (y > 375 && y <= 675 && n == 0)
                    {
                      model.GenerateQuoteHeader.factor_adj_500 = reader.GetDecimal(0);
                      //  adj_amount = reader.GetDecimal(0);
                    }
                    else if (y > 675 && y <= 875 && n == 0)
                    {
                      model.GenerateQuoteHeader.factor_adj_750 = reader.GetDecimal(0);
                      // adj_amount = reader.GetDecimal(0);
                    }
                    else if (y > 875 && y <= 1500 && n == 0)
                    {
                      model.GenerateQuoteHeader.factor_adj_1000 = reader.GetDecimal(0);
                      // adj_amount = reader.GetDecimal(0);
                    }
                    else if (y > 1500 && y <= 2250 && n == 0)
                    {
                      model.GenerateQuoteHeader.factor_adj_2000 = reader.GetDecimal(0);
                      //adj_amount = reader.GetDecimal(0);
                    }
                    else if (y > 2250 && y <= 3750 && n == 0)
                    {
                      model.GenerateQuoteHeader.factor_adj_2500 = reader.GetDecimal(0);
                      //adj_amount = reader.GetDecimal(0);
                    }
                    else if (y > 3750 && y <= 7500 && n == 0)
                    {
                      model.GenerateQuoteHeader.factor_adj_5000 = reader.GetDecimal(0);
                      //adj_amount = reader.GetDecimal(0);
                    }
                    else if (y > 7500 && y <= 15000 && n == 0)
                    {
                      model.GenerateQuoteHeader.factor_adj_10000 = reader.GetDecimal(0);
                      //adj_amount = reader.GetDecimal(0);
                    }
                    else if (y > 15000 && y <= 22500 && n == 0)
                    {
                      model.GenerateQuoteHeader.factor_adj_20000 = reader.GetDecimal(0);
                      //adj_amount = reader.GetDecimal(0);
                    }
                    else if (y > 22500 && y <= 37500 && n == 0)
                    {
                      model.GenerateQuoteHeader.factor_adj_25000 = reader.GetDecimal(0);
                      //adj_amount = reader.GetDecimal(0);
                    }
                    else if (y > 37500 && y <= 87500 && n == 0)
                    {
                      model.GenerateQuoteHeader.factor_adj_50000 = reader.GetDecimal(0);
                      //adj_amount = reader.GetDecimal(0);
                    }
                    else if (y > 87500 && n == 0)
                    {
                      model.GenerateQuoteHeader.factor_adj_100000 = reader.GetDecimal(0);
                      n = 1;
                    }
                    else if (y <= 17.5 && n == 1)
                    {
                      //model.GenerateQuoteHeader.factor_adj_10 = reader.GetDecimal(0);
                      adj_amount = reader.GetDecimal(0);
                      model.GenerateQuoteHeader.CUSTOMER_RECENT_PRICE_FLAG = reader.GetInt32(1);
                      model.GenerateQuoteHeader.BASE_PRICE_FLAG = reader.GetInt32(2);
                      model.GenerateQuoteHeader.BASE_PRICE = reader.GetDecimal(3);
                      model.GenerateQuoteHeader.RESERVE_FLAG = reader.GetInt32(4);
                      model.GenerateQuoteHeader.APPLY_VARIANCE_ADJUSTMENT = reader.GetInt32(5);
                      model.GenerateQuoteHeader.VALUE_STREAM_ADJ = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                      model.GenerateQuoteHeader.CUSTOMER_NUMBER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                      model.GenerateQuoteHeader.INTERNAL_PART_NUMBER = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                      model.GenerateQuoteHeader.CUSTOMER_PART_NUMBER = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_START = reader.GetInt32(12);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_END = reader.GetInt32(13);
                      model.GenerateQuoteHeader.OTHER_ADJ = reader.IsDBNull(14) ? "" : (string)reader.GetValue(14);
                      model.GenerateQuoteHeader.LEAD_TIME_FLAG = reader.GetInt32(15);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY_FLAG = reader.GetInt32(16);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);

                      //VALUE_STREAM, PRODUCT_GROUP, PRODUCT_CODE, CUSTOMER_NUMBER, INTERNAL_PART_NUMBER, CUSTOMER_PART_NUMBER, QUANTITY_BREAK_RANGE_START
                      //, QUANTITY_BREAK_RANGE_END, OTHER, BASE_PRICE_FLAG, BASE_PRICE, RESERVE_FLAG, APPLY_VARIANCE_ADJUSTMENT
                    }
                    else if (y > 17.5 && y <= 37.5 && n == 1)
                    {
                      //model.GenerateQuoteHeader.factor_adj_25 = reader.GetDecimal(0);
                      adj_amount = reader.GetDecimal(0);
                      model.GenerateQuoteHeader.CUSTOMER_RECENT_PRICE_FLAG = reader.GetInt32(1);
                      model.GenerateQuoteHeader.BASE_PRICE_FLAG = reader.GetInt32(2);
                      model.GenerateQuoteHeader.BASE_PRICE = reader.GetDecimal(3);
                      model.GenerateQuoteHeader.RESERVE_FLAG = reader.GetInt32(4);
                      model.GenerateQuoteHeader.APPLY_VARIANCE_ADJUSTMENT = reader.GetInt32(5);
                      model.GenerateQuoteHeader.VALUE_STREAM_ADJ = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                      model.GenerateQuoteHeader.CUSTOMER_NUMBER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                      model.GenerateQuoteHeader.INTERNAL_PART_NUMBER = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                      model.GenerateQuoteHeader.CUSTOMER_PART_NUMBER = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_START = reader.GetInt32(12);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_END = reader.GetInt32(13);
                      model.GenerateQuoteHeader.OTHER_ADJ = reader.IsDBNull(14) ? "" : (string)reader.GetValue(14);
                      model.GenerateQuoteHeader.LEAD_TIME_FLAG = reader.GetInt32(15);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY_FLAG = reader.GetInt32(16);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                    }
                    else if (y > 37.5 && y <= 62.5 && n == 1)
                    {
                      //model.GenerateQuoteHeader.factor_adj_50 = reader.GetDecimal(0);
                      adj_amount = reader.GetDecimal(0);
                      model.GenerateQuoteHeader.CUSTOMER_RECENT_PRICE_FLAG = reader.GetInt32(1);
                      model.GenerateQuoteHeader.BASE_PRICE_FLAG = reader.GetInt32(2);
                      model.GenerateQuoteHeader.BASE_PRICE = reader.GetDecimal(3);
                      model.GenerateQuoteHeader.RESERVE_FLAG = reader.GetInt32(4);
                      model.GenerateQuoteHeader.APPLY_VARIANCE_ADJUSTMENT = reader.GetInt32(5);
                      model.GenerateQuoteHeader.VALUE_STREAM_ADJ = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                      model.GenerateQuoteHeader.CUSTOMER_NUMBER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                      model.GenerateQuoteHeader.INTERNAL_PART_NUMBER = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                      model.GenerateQuoteHeader.CUSTOMER_PART_NUMBER = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_START = reader.GetInt32(12);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_END = reader.GetInt32(13);
                      model.GenerateQuoteHeader.OTHER_ADJ = reader.IsDBNull(14) ? "" : (string)reader.GetValue(14);
                      model.GenerateQuoteHeader.LEAD_TIME_FLAG = reader.GetInt32(15);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY_FLAG = reader.GetInt32(16);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                    }
                    else if (y > 62.5 && y <= 87.5 && n == 1)
                    {
                      //model.GenerateQuoteHeader.factor_adj_75 = reader.GetDecimal(0);
                      adj_amount = reader.GetDecimal(0);
                      model.GenerateQuoteHeader.CUSTOMER_RECENT_PRICE_FLAG = reader.GetInt32(1);
                      model.GenerateQuoteHeader.BASE_PRICE_FLAG = reader.GetInt32(2);
                      model.GenerateQuoteHeader.BASE_PRICE = reader.GetDecimal(3);
                      model.GenerateQuoteHeader.RESERVE_FLAG = reader.GetInt32(4);
                      model.GenerateQuoteHeader.APPLY_VARIANCE_ADJUSTMENT = reader.GetInt32(5);
                      model.GenerateQuoteHeader.VALUE_STREAM_ADJ = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                      model.GenerateQuoteHeader.CUSTOMER_NUMBER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                      model.GenerateQuoteHeader.INTERNAL_PART_NUMBER = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                      model.GenerateQuoteHeader.CUSTOMER_PART_NUMBER = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_START = reader.GetInt32(12);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_END = reader.GetInt32(13);
                      model.GenerateQuoteHeader.OTHER_ADJ = reader.IsDBNull(14) ? "" : (string)reader.GetValue(14);
                      model.GenerateQuoteHeader.LEAD_TIME_FLAG = reader.GetInt32(15);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY_FLAG = reader.GetInt32(16);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                    }
                    else if (y > 87.5 && y <= 187.5 && n == 1)
                    {
                      //model.GenerateQuoteHeader.factor_adj_100 = reader.GetDecimal(0);
                      adj_amount = reader.GetDecimal(0);
                      model.GenerateQuoteHeader.CUSTOMER_RECENT_PRICE_FLAG = reader.GetInt32(1);
                      model.GenerateQuoteHeader.BASE_PRICE_FLAG = reader.GetInt32(2);
                      model.GenerateQuoteHeader.BASE_PRICE = reader.GetDecimal(3);
                      model.GenerateQuoteHeader.RESERVE_FLAG = reader.GetInt32(4);
                      model.GenerateQuoteHeader.APPLY_VARIANCE_ADJUSTMENT = reader.GetInt32(5);
                      model.GenerateQuoteHeader.VALUE_STREAM_ADJ = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                      model.GenerateQuoteHeader.CUSTOMER_NUMBER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                      model.GenerateQuoteHeader.INTERNAL_PART_NUMBER = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                      model.GenerateQuoteHeader.CUSTOMER_PART_NUMBER = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_START = reader.GetInt32(12);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_END = reader.GetInt32(13);
                      model.GenerateQuoteHeader.OTHER_ADJ = reader.IsDBNull(14) ? "" : (string)reader.GetValue(14);
                      model.GenerateQuoteHeader.LEAD_TIME_FLAG = reader.GetInt32(15);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY_FLAG = reader.GetInt32(16);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                    }
                    else if (y > 187.5 && y <= 375 && n == 1)
                    {
                      //model.GenerateQuoteHeader.factor_adj_250 = reader.GetDecimal(0);
                      adj_amount = reader.GetDecimal(0);
                      model.GenerateQuoteHeader.CUSTOMER_RECENT_PRICE_FLAG = reader.GetInt32(1);
                      model.GenerateQuoteHeader.BASE_PRICE_FLAG = reader.GetInt32(2);
                      model.GenerateQuoteHeader.BASE_PRICE = reader.GetDecimal(3);
                      model.GenerateQuoteHeader.RESERVE_FLAG = reader.GetInt32(4);
                      model.GenerateQuoteHeader.APPLY_VARIANCE_ADJUSTMENT = reader.GetInt32(5);
                      model.GenerateQuoteHeader.VALUE_STREAM_ADJ = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                      model.GenerateQuoteHeader.CUSTOMER_NUMBER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                      model.GenerateQuoteHeader.INTERNAL_PART_NUMBER = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                      model.GenerateQuoteHeader.CUSTOMER_PART_NUMBER = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_START = reader.GetInt32(12);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_END = reader.GetInt32(13);
                      model.GenerateQuoteHeader.OTHER_ADJ = reader.IsDBNull(14) ? "" : (string)reader.GetValue(14);
                      model.GenerateQuoteHeader.LEAD_TIME_FLAG = reader.GetInt32(15);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY_FLAG = reader.GetInt32(16);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                    }
                    else if (y > 375 && y <= 675 && n == 1)
                    {
                      //model.GenerateQuoteHeader.factor_adj_500 = reader.GetDecimal(0);
                      adj_amount = reader.GetDecimal(0);
                      model.GenerateQuoteHeader.CUSTOMER_RECENT_PRICE_FLAG = reader.GetInt32(1);
                      model.GenerateQuoteHeader.BASE_PRICE_FLAG = reader.GetInt32(2);
                      model.GenerateQuoteHeader.BASE_PRICE = reader.GetDecimal(3);
                      model.GenerateQuoteHeader.RESERVE_FLAG = reader.GetInt32(4);
                      model.GenerateQuoteHeader.APPLY_VARIANCE_ADJUSTMENT = reader.GetInt32(5);
                      model.GenerateQuoteHeader.VALUE_STREAM_ADJ = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                      model.GenerateQuoteHeader.CUSTOMER_NUMBER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                      model.GenerateQuoteHeader.INTERNAL_PART_NUMBER = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                      model.GenerateQuoteHeader.CUSTOMER_PART_NUMBER = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_START = reader.GetInt32(12);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_END = reader.GetInt32(13);
                      model.GenerateQuoteHeader.OTHER_ADJ = reader.IsDBNull(14) ? "" : (string)reader.GetValue(14);
                      model.GenerateQuoteHeader.LEAD_TIME_FLAG = reader.GetInt32(15);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY_FLAG = reader.GetInt32(16);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                    }
                    else if (y > 675 && y <= 875 && n == 1)
                    {
                      //model.GenerateQuoteHeader.factor_adj_750 = reader.GetDecimal(0);
                      adj_amount = reader.GetDecimal(0);
                      model.GenerateQuoteHeader.CUSTOMER_RECENT_PRICE_FLAG = reader.GetInt32(1);
                      model.GenerateQuoteHeader.BASE_PRICE_FLAG = reader.GetInt32(2);
                      model.GenerateQuoteHeader.BASE_PRICE = reader.GetDecimal(3);
                      model.GenerateQuoteHeader.RESERVE_FLAG = reader.GetInt32(4);
                      model.GenerateQuoteHeader.APPLY_VARIANCE_ADJUSTMENT = reader.GetInt32(5);
                      model.GenerateQuoteHeader.VALUE_STREAM_ADJ = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                      model.GenerateQuoteHeader.CUSTOMER_NUMBER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                      model.GenerateQuoteHeader.INTERNAL_PART_NUMBER = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                      model.GenerateQuoteHeader.CUSTOMER_PART_NUMBER = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_START = reader.GetInt32(12);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_END = reader.GetInt32(13);
                      model.GenerateQuoteHeader.OTHER_ADJ = reader.IsDBNull(14) ? "" : (string)reader.GetValue(14);
                      model.GenerateQuoteHeader.LEAD_TIME_FLAG = reader.GetInt32(15);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY_FLAG = reader.GetInt32(16);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                    }
                    else if (y > 875 && y <= 1500 && n == 1)
                    {
                      //  model.GenerateQuoteHeader.factor_adj_1000 = reader.GetDecimal(0);
                      adj_amount = reader.GetDecimal(0);
                      model.GenerateQuoteHeader.CUSTOMER_RECENT_PRICE_FLAG = reader.GetInt32(1);
                      model.GenerateQuoteHeader.BASE_PRICE_FLAG = reader.GetInt32(2);
                      model.GenerateQuoteHeader.BASE_PRICE = reader.GetDecimal(3);
                      model.GenerateQuoteHeader.RESERVE_FLAG = reader.GetInt32(4);
                      model.GenerateQuoteHeader.APPLY_VARIANCE_ADJUSTMENT = reader.GetInt32(5);
                      model.GenerateQuoteHeader.VALUE_STREAM_ADJ = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                      model.GenerateQuoteHeader.CUSTOMER_NUMBER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                      model.GenerateQuoteHeader.INTERNAL_PART_NUMBER = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                      model.GenerateQuoteHeader.CUSTOMER_PART_NUMBER = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_START = reader.GetInt32(12);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_END = reader.GetInt32(13);
                      model.GenerateQuoteHeader.OTHER_ADJ = reader.IsDBNull(14) ? "" : (string)reader.GetValue(14);
                      model.GenerateQuoteHeader.LEAD_TIME_FLAG = reader.GetInt32(15);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY_FLAG = reader.GetInt32(16);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                    }
                    else if (y > 1500 && y <= 2250 && n == 1)
                    {
                      //  model.GenerateQuoteHeader.factor_adj_2000 = reader.GetDecimal(0);
                      adj_amount = reader.GetDecimal(0);
                      model.GenerateQuoteHeader.CUSTOMER_RECENT_PRICE_FLAG = reader.GetInt32(1);
                      model.GenerateQuoteHeader.BASE_PRICE_FLAG = reader.GetInt32(2);
                      model.GenerateQuoteHeader.BASE_PRICE = reader.GetDecimal(3);
                      model.GenerateQuoteHeader.RESERVE_FLAG = reader.GetInt32(4);
                      model.GenerateQuoteHeader.APPLY_VARIANCE_ADJUSTMENT = reader.GetInt32(5);
                      model.GenerateQuoteHeader.VALUE_STREAM_ADJ = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                      model.GenerateQuoteHeader.CUSTOMER_NUMBER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                      model.GenerateQuoteHeader.INTERNAL_PART_NUMBER = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                      model.GenerateQuoteHeader.CUSTOMER_PART_NUMBER = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_START = reader.GetInt32(12);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_END = reader.GetInt32(13);
                      model.GenerateQuoteHeader.OTHER_ADJ = reader.IsDBNull(14) ? "" : (string)reader.GetValue(14);
                      model.GenerateQuoteHeader.LEAD_TIME_FLAG = reader.GetInt32(15);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY_FLAG = reader.GetInt32(16);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                    }
                    else if (y > 2250 && y <= 3750 && n == 1)
                    {
                      // model.GenerateQuoteHeader.factor_adj_2500 = reader.GetDecimal(0);
                      adj_amount = reader.GetDecimal(0);
                      model.GenerateQuoteHeader.CUSTOMER_RECENT_PRICE_FLAG = reader.GetInt32(1);
                      model.GenerateQuoteHeader.BASE_PRICE_FLAG = reader.GetInt32(2);
                      model.GenerateQuoteHeader.BASE_PRICE = reader.GetDecimal(3);
                      model.GenerateQuoteHeader.RESERVE_FLAG = reader.GetInt32(4);
                      model.GenerateQuoteHeader.APPLY_VARIANCE_ADJUSTMENT = reader.GetInt32(5);
                      model.GenerateQuoteHeader.VALUE_STREAM_ADJ = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                      model.GenerateQuoteHeader.CUSTOMER_NUMBER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                      model.GenerateQuoteHeader.INTERNAL_PART_NUMBER = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                      model.GenerateQuoteHeader.CUSTOMER_PART_NUMBER = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_START = reader.GetInt32(12);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_END = reader.GetInt32(13);
                      model.GenerateQuoteHeader.OTHER_ADJ = reader.IsDBNull(14) ? "" : (string)reader.GetValue(14);
                      model.GenerateQuoteHeader.LEAD_TIME_FLAG = reader.GetInt32(15);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY_FLAG = reader.GetInt32(16);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                    }
                    else if (y > 3750 && y <= 7500 && n == 1)
                    {
                      // model.GenerateQuoteHeader.factor_adj_5000 = reader.GetDecimal(0);
                      adj_amount = reader.GetDecimal(0);
                      model.GenerateQuoteHeader.CUSTOMER_RECENT_PRICE_FLAG = reader.GetInt32(1);
                      model.GenerateQuoteHeader.BASE_PRICE_FLAG = reader.GetInt32(2);
                      model.GenerateQuoteHeader.BASE_PRICE = reader.GetDecimal(3);
                      model.GenerateQuoteHeader.RESERVE_FLAG = reader.GetInt32(4);
                      model.GenerateQuoteHeader.APPLY_VARIANCE_ADJUSTMENT = reader.GetInt32(5);
                      model.GenerateQuoteHeader.VALUE_STREAM_ADJ = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                      model.GenerateQuoteHeader.CUSTOMER_NUMBER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                      model.GenerateQuoteHeader.INTERNAL_PART_NUMBER = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                      model.GenerateQuoteHeader.CUSTOMER_PART_NUMBER = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_START = reader.GetInt32(12);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_END = reader.GetInt32(13);
                      model.GenerateQuoteHeader.OTHER_ADJ = reader.IsDBNull(14) ? "" : (string)reader.GetValue(14);
                      model.GenerateQuoteHeader.LEAD_TIME_FLAG = reader.GetInt32(15);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY_FLAG = reader.GetInt32(16);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                    }
                    else if (y > 7500 && y <= 15000 && n == 1)
                    {
                      //  model.GenerateQuoteHeader.factor_adj_10000 = reader.GetDecimal(0);
                      adj_amount = reader.GetDecimal(0);
                      model.GenerateQuoteHeader.CUSTOMER_RECENT_PRICE_FLAG = reader.GetInt32(1);
                      model.GenerateQuoteHeader.BASE_PRICE_FLAG = reader.GetInt32(2);
                      model.GenerateQuoteHeader.BASE_PRICE = reader.GetDecimal(3);
                      model.GenerateQuoteHeader.RESERVE_FLAG = reader.GetInt32(4);
                      model.GenerateQuoteHeader.APPLY_VARIANCE_ADJUSTMENT = reader.GetInt32(5);
                      model.GenerateQuoteHeader.VALUE_STREAM_ADJ = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                      model.GenerateQuoteHeader.CUSTOMER_NUMBER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                      model.GenerateQuoteHeader.INTERNAL_PART_NUMBER = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                      model.GenerateQuoteHeader.CUSTOMER_PART_NUMBER = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_START = reader.GetInt32(12);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_END = reader.GetInt32(13);
                      model.GenerateQuoteHeader.OTHER_ADJ = reader.IsDBNull(14) ? "" : (string)reader.GetValue(14);
                      model.GenerateQuoteHeader.LEAD_TIME_FLAG = reader.GetInt32(15);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY_FLAG = reader.GetInt32(16);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                    }
                    else if (y > 15000 && y <= 22500 && n == 1)
                    {
                      // model.GenerateQuoteHeader.factor_adj_20000 = reader.GetDecimal(0);
                      adj_amount = reader.GetDecimal(0);
                      model.GenerateQuoteHeader.CUSTOMER_RECENT_PRICE_FLAG = reader.GetInt32(1);
                      model.GenerateQuoteHeader.BASE_PRICE_FLAG = reader.GetInt32(2);
                      model.GenerateQuoteHeader.BASE_PRICE = reader.GetDecimal(3);
                      model.GenerateQuoteHeader.RESERVE_FLAG = reader.GetInt32(4);
                      model.GenerateQuoteHeader.APPLY_VARIANCE_ADJUSTMENT = reader.GetInt32(5);
                      model.GenerateQuoteHeader.VALUE_STREAM_ADJ = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                      model.GenerateQuoteHeader.CUSTOMER_NUMBER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                      model.GenerateQuoteHeader.INTERNAL_PART_NUMBER = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                      model.GenerateQuoteHeader.CUSTOMER_PART_NUMBER = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_START = reader.GetInt32(12);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_END = reader.GetInt32(13);
                      model.GenerateQuoteHeader.OTHER_ADJ = reader.IsDBNull(14) ? "" : (string)reader.GetValue(14);
                      model.GenerateQuoteHeader.LEAD_TIME_FLAG = reader.GetInt32(15);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY_FLAG = reader.GetInt32(16);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                    }
                    else if (y > 22500 && y <= 37500 && n == 1)
                    {
                      //  model.GenerateQuoteHeader.factor_adj_25000 = reader.GetDecimal(0);
                      adj_amount = reader.GetDecimal(0);
                      model.GenerateQuoteHeader.CUSTOMER_RECENT_PRICE_FLAG = reader.GetInt32(1);
                      model.GenerateQuoteHeader.BASE_PRICE_FLAG = reader.GetInt32(2);
                      model.GenerateQuoteHeader.BASE_PRICE = reader.GetDecimal(3);
                      model.GenerateQuoteHeader.RESERVE_FLAG = reader.GetInt32(4);
                      model.GenerateQuoteHeader.APPLY_VARIANCE_ADJUSTMENT = reader.GetInt32(5);
                      model.GenerateQuoteHeader.VALUE_STREAM_ADJ = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                      model.GenerateQuoteHeader.CUSTOMER_NUMBER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                      model.GenerateQuoteHeader.INTERNAL_PART_NUMBER = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                      model.GenerateQuoteHeader.CUSTOMER_PART_NUMBER = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_START = reader.GetInt32(12);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_END = reader.GetInt32(13);
                      model.GenerateQuoteHeader.OTHER_ADJ = reader.IsDBNull(14) ? "" : (string)reader.GetValue(14);
                      model.GenerateQuoteHeader.LEAD_TIME_FLAG = reader.GetInt32(15);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY_FLAG = reader.GetInt32(16);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                    }
                    else if (y > 37500 && y <= 87500 && n == 1)
                    {
                      //  model.GenerateQuoteHeader.factor_adj_50000 = reader.GetDecimal(0);
                      adj_amount = reader.GetDecimal(0);
                      model.GenerateQuoteHeader.CUSTOMER_RECENT_PRICE_FLAG = reader.GetInt32(1);
                      model.GenerateQuoteHeader.BASE_PRICE_FLAG = reader.GetInt32(2);
                      model.GenerateQuoteHeader.BASE_PRICE = reader.GetDecimal(3);
                      model.GenerateQuoteHeader.RESERVE_FLAG = reader.GetInt32(4);
                      model.GenerateQuoteHeader.APPLY_VARIANCE_ADJUSTMENT = reader.GetInt32(5);
                      model.GenerateQuoteHeader.VALUE_STREAM_ADJ = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                      model.GenerateQuoteHeader.CUSTOMER_NUMBER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                      model.GenerateQuoteHeader.INTERNAL_PART_NUMBER = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                      model.GenerateQuoteHeader.CUSTOMER_PART_NUMBER = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_START = reader.GetInt32(12);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_END = reader.GetInt32(13);
                      model.GenerateQuoteHeader.OTHER_ADJ = reader.IsDBNull(14) ? "" : (string)reader.GetValue(14);
                      model.GenerateQuoteHeader.LEAD_TIME_FLAG = reader.GetInt32(15);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY_FLAG = reader.GetInt32(16);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                    }
                    else if (y > 87500 && n == 1)
                    {
                      //  model.GenerateQuoteHeader.factor_adj_100000 = reader.GetDecimal(0);
                      adj_amount = reader.GetDecimal(0);
                      model.GenerateQuoteHeader.CUSTOMER_RECENT_PRICE_FLAG = reader.GetInt32(1);
                      model.GenerateQuoteHeader.BASE_PRICE_FLAG = reader.GetInt32(2);
                      model.GenerateQuoteHeader.BASE_PRICE = reader.GetDecimal(3);
                      model.GenerateQuoteHeader.RESERVE_FLAG = reader.GetInt32(4);
                      model.GenerateQuoteHeader.APPLY_VARIANCE_ADJUSTMENT = reader.GetInt32(5);
                      model.GenerateQuoteHeader.VALUE_STREAM_ADJ = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                      model.GenerateQuoteHeader.PRODUCT_CODE = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                      model.GenerateQuoteHeader.CUSTOMER_NUMBER = reader.IsDBNull(9) ? "" : (string)reader.GetValue(9);
                      model.GenerateQuoteHeader.INTERNAL_PART_NUMBER = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                      model.GenerateQuoteHeader.CUSTOMER_PART_NUMBER = reader.IsDBNull(11) ? "" : (string)reader.GetValue(11);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_START = reader.GetInt32(12);
                      model.GenerateQuoteHeader.QUANTITY_BREAK_RANGE_END = reader.GetInt32(13);
                      model.GenerateQuoteHeader.OTHER_ADJ = reader.IsDBNull(14) ? "" : (string)reader.GetValue(14);
                      model.GenerateQuoteHeader.LEAD_TIME_FLAG = reader.GetInt32(15);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY_FLAG = reader.GetInt32(16);
                      model.GenerateQuoteHeader.MINIMUM_ORDER_QUANTITY = reader.GetInt32(17);
                    }
                    //y++;
                  }
                }
                                //reader.Close();
                            }


                            //if (model.GenerateQuoteHeader.QuoteQuantity < 1000)
                            //{
                            //    model.GenerateQuoteHeader.Margin = model.GenerateQuoteHeader.margin;

                            //else
                            //{
                            //    model.GenerateQuoteHeader.Margin = model.GenerateQuoteHeader.margin;
                            //}


                            catch (Exception ex)
                            {
                                MessageBox.Show("Error loading the margins that have been configured.");
                                return View();
                            }
                            //reader.Close();
                            finally
                            {
                                reader.Close();
                            }

                        }

                    }
                }

            }

            //
            
            using (OdbcConnection cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString))
            {
                string sqlScript;
                cnn_odbc.Open();
                if (cnn_odbc == null || cnn_odbc.State == ConnectionState.Closed)
                {

                    cnn_odbc.Open();
                }

                //if (model.GenerateQuoteHeader.Division == "710000")
                //{

                decimal fznMatl; //fzna1
                decimal fznLbr; //fznb1
                decimal fznb2;
                decimal fznc1;
                decimal fznc2;
                decimal fznc3;
                decimal fznc4;
                decimal fznOS; //fznd0
                decimal fznlc;
                decimal fznSUTotal;
                decimal fznLCTotal;



        //if (model.GenerateQuoteHeader.Division == "710000")
        //{
        //    sqlScript =  @"select decimal(coalesce(sum(case when left(iecost, 1) = 'A' and trim(iemcu) <> 'V78LOT001' then cast(iecsl * .000001 as decimal(15, 7)) else 0 end), 0),10,5) as fznA1, 
        //                         decimal(coalesce(sum(case when iecost = 'B1' and trim(iemcu) <> 'V78LOT001' then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznB1, 
        //                         decimal(coalesce(sum(case when iecost = 'B2' and trim(iemcu) <> 'V78LOT001' then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznB2, 
        //                         decimal(coalesce(sum(case when iecost = 'C1' and trim(iemcu) <> 'V78LOT001' then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC1, 
        //                         decimal(coalesce(sum(case when iecost = 'C2' and trim(iemcu) <> 'V78LOT001' then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC2, 
        //                         decimal(coalesce(sum(case when iecost = 'C3' and trim(iemcu) <> 'V78LOT001' then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC3, 
        //                         decimal(coalesce(sum(case when iecost = 'C4' and trim(iemcu) <> 'V78LOT001' then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC4, 
        //                         decimal(coalesce(sum(case when left(iecost, 1) = 'D' then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznD0,    
        //                         decimal(coalesce(sum(case when trim(iemcu) = 'V78LOT001' then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznLC,
        //                         decimal(coalesce(sum(case when iecost = 'B2' then cast((iecsl * .000001 * CAST(? AS INTEGER)) as decimal(15, 5)) else 0 end), 0),10,5) as fznSUTotal, 
        //                        decimal(coalesce( sum(case when iecost = 'A1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznA1Net,
        //         decimal(coalesce( sum(case when iecost = 'B1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB1Net,
        //         decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB2Net,
        //         decimal(coalesce( sum(case when iecost = 'C1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC1Net,
        //         decimal(coalesce( sum(case when iecost = 'C2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC2Net,
        //         decimal(coalesce( sum(case when iecost = 'C3' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC3Net,
        //         decimal(coalesce( sum(case when iecost = 'C4' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC4Net,
        //         decimal(coalesce( sum(case when left(iecost,1) = 'D' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznD0Net,
        //         decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 * CAST(? AS INTEGER) as decimal(15,5)) else 0 end),0),10,5) 
        //          as fznSUTotalNet , decimal(coalesce(sum(case when trim(iemcu) = 'V78LOT001' then cast((iecsl / 1000000 * CAST(? AS INTEGER)) as decimal(15, 5)) else 0 end), 0),10,5) as fznLCTotal, 
        //         decimal(coalesce( sum(case when iecost = 'A2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznA2Net,
        //            decimal(coalesce( sum(case when iecost = 'D3' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznD3Net
        //                            from proddta.f300261
        //                            where ieledg = ?
        //                            and trim(iemmcu) in ('710000')
        //                            and ieitm = ?"; 
        //}
        if (model.GenerateQuoteHeader.Division == "710000")
        {
          sqlScript = @"select coalesce(sum(case when left(iecost,1) = 'A' and trim(iewmcu) <> 'V78LOT001' then cast(iecsl*.000001 as decimal(15,7)) else 0 end),0) as simA1,
						coalesce(sum(case when iecost = 'B1' and trim(iewmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB1,
						coalesce(sum(case when iecost = 'B2' and trim(iewmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB2,
						coalesce(sum(case when iecost = 'C1' and trim(iewmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC1,
						coalesce(sum(case when iecost = 'C2' and trim(iewmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC2,
						coalesce(sum(case when iecost = 'C3' and trim(iewmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC3,
						coalesce(sum(case when iecost = 'C4' and trim(iewmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC4,
                                         coalesce(sum(case when left(iecost,1) = 'D' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simD0,
                                         coalesce(sum(case when trim(iewmcu) = 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simLC,
                                         coalesce(sum(case when iecost = 'B2' then cast(iexscr/1000000*CAST(? AS INTEGER) as decimal(15,7)) else 0 end),0) 
							as simSUTotal, 
                                        decimal(coalesce( sum(case when iecost = 'A1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznA1Net,
                         decimal(coalesce( sum(case when iecost = 'B1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB1Net,
                         decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB2Net,
                         decimal(coalesce( sum(case when iecost = 'C1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC1Net,
                         decimal(coalesce( sum(case when iecost = 'C2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC2Net,
                         decimal(coalesce( sum(case when iecost = 'C3' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC3Net,
                         decimal(coalesce( sum(case when iecost = 'C4' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC4Net,
                         decimal(coalesce( sum(case when left(iecost,1) = 'D' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznD0Net,
                         decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 * CAST(? AS INTEGER) as decimal(15,5)) else 0 end),0),10,5) 
                          as fznSUTotalNet , coalesce(sum(case when trim(iewmcu) = 'V78LOT001' then cast(iexscr/1000000*CAST(? as INTEGER) as decimal(15,7)) else 0 end),0) simLCTotal, 
                         decimal(coalesce( sum(case when iecost = 'A2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznA2Net,
                            decimal(coalesce( sum(case when iecost = 'D3' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznD3Net,
                            decimal(coalesce( sum(case when iecost = 'D2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznD2Net
                                            from proddta.f30026
                                            where ieledg = ?
                                            and trim(iemmcu) in ('710000')
                                            and ieitm = ?";
        }
        else
                {
                    sqlScript = @"select decimal(coalesce(sum(case when iecost = 'A1'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznA1, 
                                         decimal(coalesce(sum(case when iecost = 'B1'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznB1, 
                                         decimal(coalesce(sum(case when iecost = 'B2'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznB2, 
                                         decimal(coalesce(sum(case when iecost = 'C1'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC1, 
                                         decimal(coalesce(sum(case when iecost = 'C2'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC2, 
                                         decimal(coalesce(sum(case when iecost = 'C3'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC3, 
                                         decimal(coalesce(sum(case when iecost = 'C4'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC4, 
                                         decimal(coalesce(sum(case when left(iecost, 1) = 'D' then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznD0,    
                                         decimal(coalesce(sum(case when iecost = 'B2' then cast((iecsl * .000001 * CAST(? AS INTEGER)) as decimal(15, 5)) else 0 end), 0),10,5) as fznSUTotal, 
                                        decimal(coalesce( sum(case when iecost = 'A1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznA1Net,
					                    decimal(coalesce( sum(case when iecost = 'B1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB1Net,
					                    decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB2Net,
					                    decimal(coalesce( sum(case when iecost = 'C1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC1Net,
					                    decimal(coalesce( sum(case when iecost = 'C2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC2Net,
					                    decimal(coalesce( sum(case when iecost = 'C3' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC3Net,
					                    decimal(coalesce( sum(case when iecost = 'C4' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC4Net,
					                    decimal(coalesce( sum(case when left(iecost,1) = 'D' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznD0Net,
					                    decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 * CAST(? AS INTEGER) as decimal(15,5)) else 0 end),0),10,5) as fznSUTotalNet
                                            from proddta.f30026
                                            where ieledg = ?
                                            and trim(iemmcu) in ('730000','710000','750000','720000','780000','734000')
                                            and ieitm = ?";
                }
                using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                {
                    command.Parameters.Add(new OdbcParameter("@ACQ", model.GenerateQuoteHeader.ACQ));
                    command.Parameters.Add(new OdbcParameter("@ACQ2", model.GenerateQuoteHeader.ACQ));
                    //command.Parameters.Add(new OdbcParameter("@ACQ3", model.GenerateQuoteHeader.ACQ));
                    if (model.GenerateQuoteHeader.Division == "710000")
                    {
                        command.Parameters.Add(new OdbcParameter("@ACQ3", model.GenerateQuoteHeader.ACQ));
                    }
                    command.Parameters.Add(new OdbcParameter("@year", cost_reference_id));
                    //command.Parameters.Add(new OdbcParameter("@division", model.GenerateQuoteHeader.Division));
                    if (model.GenerateQuoteHeader.Division == "710000")
                    {
                        command.Parameters.Add(new OdbcParameter("@item_num", model.GenerateQuoteHeader.ibitm));
                    }
                    else
                    {
                        command.Parameters.Add(new OdbcParameter("@item_num", model.GenerateQuoteHeader.ibitm));
                    }

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                if (model.GenerateQuoteHeader.Division == "710000")
                                {

                                    fznMatl = reader.GetDecimal(0);
                                    fznLbr = reader.GetDecimal(1);
                                    fznb2 = reader.GetDecimal(2);
                                    fznc1 = reader.GetDecimal(3);
                                    fznc2 = reader.GetDecimal(4);
                                    fznc3 = reader.GetDecimal(5);
                                    fznc4 = reader.GetDecimal(6);
                                    fznOS = reader.GetDecimal(7);
                                    fznlc = reader.GetDecimal(8);
                                    fznSUTotal = reader.GetDecimal(9);
                                    model.GenerateQuoteHeader.fznA1Net = reader.GetDecimal(10); //fznmatlnet
                                    model.GenerateQuoteHeader.fznB1Net = reader.GetDecimal(11); //fznlbrnet
                                    model.GenerateQuoteHeader.fznB2Net = reader.GetDecimal(12);
                                    model.GenerateQuoteHeader.fznc1net = reader.GetDecimal(13);
                                    model.GenerateQuoteHeader.fznc2net = reader.GetDecimal(14);
                                    model.GenerateQuoteHeader.fznc3net = reader.GetDecimal(15);
                                    model.GenerateQuoteHeader.fznc4net = reader.GetDecimal(16);
                                    model.GenerateQuoteHeader.fznD0Net = reader.GetDecimal(17); //fznosnet
                                    model.GenerateQuoteHeader.fznSUTotalNet = reader.IsDBNull(18) ? 0 : (decimal)reader.GetValue(18);
                                    model.GenerateQuoteHeader.fznLCTotal = reader.GetDecimal(19);
                                    model.GenerateQuoteHeader.fznA2Net = reader.GetDecimal(20);
                                    model.GenerateQuoteHeader.fznD2Net = reader.GetDecimal(22);
                                    
                                    //model.GenerateQuoteHeader.total = fznMatl + fznLbr + fznOS + fznb2 + fznc1 + fznc3 + fznc2 + fznc4; //fznfoh fznvoh
                                    model.GenerateQuoteHeader.material = fznMatl;
                                    model.GenerateQuoteHeader.setup = fznb2; //setup
                                    model.GenerateQuoteHeader.labor = fznLbr;
                                    model.GenerateQuoteHeader.lbr_voh = model.GenerateQuoteHeader.fznc3net;
                                    model.GenerateQuoteHeader.mch_voh = model.GenerateQuoteHeader.fznc1net;
                                    model.GenerateQuoteHeader.lbr_foh = model.GenerateQuoteHeader.fznc4net;
                                    model.GenerateQuoteHeader.mch_foh = fznc2;
                                    model.GenerateQuoteHeader.osp = model.GenerateQuoteHeader.fznD0Net;
                                    //model.GenerateQuoteHeader.UnitCost = (fznMatl + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS + (fznSUTotal / model.GenerateQuoteHeader.ACQ) + (fznlc / model.GenerateQuoteHeader.ACQ));
                                    //gqh_model.calc_500_fzn_unit_cost = (fznMatl + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS + (fznSUTotal / 500) + (fznLCTotal / 500));
                                    model.GenerateQuoteHeader.UnitCost = fznMatl +  model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net ;
                                    
                                }
                                else
                                {
                                    fznMatl = reader.GetDecimal(0); //a1
                                    fznLbr = reader.GetDecimal(1); //b1
                                    fznb2 = reader.GetDecimal(2);
                                    fznc1 = reader.GetDecimal(3);
                                    fznc2 = reader.GetDecimal(4);
                                    fznc3 = reader.GetDecimal(5);
                                    fznc4 = reader.GetDecimal(6);
                                    fznOS = reader.GetDecimal(7); //d0
                                                                  //fznlc = reader.GetDecimal(8);
                                    fznSUTotal = reader.GetDecimal(2) * model.GenerateQuoteHeader.ACQ;
                                    //if (reader.GetDecimal(9) != 0)
                                    //{
                                    model.GenerateQuoteHeader.fznA1Net = reader.GetDecimal(9); //fznmatlnet
                                                                                               //}
                                                                                               //else
                                                                                               //{
                                                                                               //    model.GenerateQuoteHeader.fznA1Net = fznMatl;
                                                                                               //}
                                    model.GenerateQuoteHeader.fznB1Net = reader.GetDecimal(10); //fznlbrnet
                                    model.GenerateQuoteHeader.fznB2Net = reader.GetDecimal(11);
                                    model.GenerateQuoteHeader.fznc1net = reader.GetDecimal(12);
                                    model.GenerateQuoteHeader.fznc2net = reader.GetDecimal(13);
                                    model.GenerateQuoteHeader.fznc3net = reader.GetDecimal(14);
                                    model.GenerateQuoteHeader.fznc4net = reader.GetDecimal(15);
                                    model.GenerateQuoteHeader.fznD0Net = reader.GetDecimal(16); //fznosnet
                                    model.GenerateQuoteHeader.fznSUTotalNet = reader.IsDBNull(17) ? 0 : (decimal)reader.GetValue(17);
                                    if (model.GenerateQuoteHeader.Division == "710000")
                                    {
                                        model.GenerateQuoteHeader.fznLCTotal = reader.GetDecimal(19); //model.GenerateQuoteHeader.total = fznMatl + fznLbr + fznOS + fznb2 + fznc1 + fznc3 + fznc2 + fznc4; //fznfoh fznvoh
                                    }
                                    model.GenerateQuoteHeader.material = fznMatl;
                                    model.GenerateQuoteHeader.setup = fznb2; //setup
                                    model.GenerateQuoteHeader.labor = fznLbr;
                                    model.GenerateQuoteHeader.lbr_voh = fznc3;
                                    model.GenerateQuoteHeader.mch_voh = fznc1;
                                    model.GenerateQuoteHeader.lbr_foh = fznc4;
                                    model.GenerateQuoteHeader.mch_foh = fznc2;
                                    model.GenerateQuoteHeader.osp = fznOS;
                                    model.GenerateQuoteHeader.UnitCost = fznMatl + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS + fznb2;
                                }
                                if (model.GenerateQuoteHeader.Division == "730000")
                                {
                                    if (Components.Count() > 0)
                                    {
                                        model.GenerateQuoteHeader.calc_500_fzn_unit_cost = (((500 * (model.GenerateQuoteHeader.fznA1Net + model.GenerateQuoteHeader.factor_adj_500 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 500) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 500;
                                        model.GenerateQuoteHeader.calc_1000_fzn_unit_cost = (((1000 * (model.GenerateQuoteHeader.fznA1Net + model.GenerateQuoteHeader.factor_adj_1000 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 1000) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 1000;
                                        model.GenerateQuoteHeader.calc_2000_fzn_unit_cost = (((2000 * (model.GenerateQuoteHeader.fznA1Net + model.GenerateQuoteHeader.factor_adj_2000 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 2000) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 2000;
                                        model.GenerateQuoteHeader.calc_2500_fzn_unit_cost = (((2500 * (model.GenerateQuoteHeader.fznA1Net + model.GenerateQuoteHeader.factor_adj_2500 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 2500) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 2500;
                                        model.GenerateQuoteHeader.calc_5000_fzn_unit_cost = (((5000 * (model.GenerateQuoteHeader.fznA1Net + model.GenerateQuoteHeader.factor_adj_5000 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 5000) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 5000;
                                        model.GenerateQuoteHeader.calc_10000_fzn_unit_cost = (((10000 * (model.GenerateQuoteHeader.fznA1Net + model.GenerateQuoteHeader.factor_adj_10000 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 10000) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 10000;
                                        model.GenerateQuoteHeader.calc_20000_fzn_unit_cost = (((20000 * (model.GenerateQuoteHeader.fznA1Net + model.GenerateQuoteHeader.factor_adj_20000 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 20000) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 20000;
                                        model.GenerateQuoteHeader.calc_25000_fzn_unit_cost = (((25000 * (model.GenerateQuoteHeader.fznA1Net + model.GenerateQuoteHeader.factor_adj_25000 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 25000) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 25000;
                                        model.GenerateQuoteHeader.calc_50000_fzn_unit_cost = (((50000 * (model.GenerateQuoteHeader.fznA1Net + model.GenerateQuoteHeader.factor_adj_50000 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 50000) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 50000;
                                        model.GenerateQuoteHeader.calc_100000_fzn_unit_cost = (((100000 * (model.GenerateQuoteHeader.fznA1Net + model.GenerateQuoteHeader.factor_adj_100000 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 100000) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 100000;

                                        model.GenerateQuoteHeader.calc_dynam_qty = model.GenerateQuoteHeader.QuoteQuantity;
                                        if (model.GenerateQuoteHeader.QuoteQuantity > 0)
                                        {
                                            model.GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (((model.GenerateQuoteHeader.QuoteQuantity * (model.GenerateQuoteHeader.fznA1Net + adj_amount + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / model.GenerateQuoteHeader.QuoteQuantity) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / model.GenerateQuoteHeader.QuoteQuantity;
                                        }
                                        else
                                        {
                                            model.GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (((model.GenerateQuoteHeader.QuoteQuantity * (model.GenerateQuoteHeader.fznA1Net + adj_amount + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net)) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH + (model.GenerateQuoteHeader.fznSUTotalNet)))));
                                        }
                                    }
                                    else
                                    {
                                        model.GenerateQuoteHeader.calc_500_fzn_unit_cost = (((500 * (fznMatl + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.factor_adj_500 + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 500) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 500;
                                        model.GenerateQuoteHeader.calc_1000_fzn_unit_cost = (((1000 * (fznMatl + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.factor_adj_1000 + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 1000) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 1000;
                                        model.GenerateQuoteHeader.calc_2000_fzn_unit_cost = (((2000 * (fznMatl + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.factor_adj_2000 + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 2000) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 2000;
                                        model.GenerateQuoteHeader.calc_2500_fzn_unit_cost = (((2500 * (fznMatl + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.factor_adj_2500 + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 2500) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 2500;
                                        model.GenerateQuoteHeader.calc_5000_fzn_unit_cost = (((5000 * (fznMatl + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.factor_adj_5000 + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 5000) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 5000;
                                        model.GenerateQuoteHeader.calc_10000_fzn_unit_cost = (((10000 * (fznMatl + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.factor_adj_10000 + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 10000) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 10000;
                                        model.GenerateQuoteHeader.calc_20000_fzn_unit_cost = (((20000 * (fznMatl + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.factor_adj_20000 + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 20000) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 20000;
                                        model.GenerateQuoteHeader.calc_25000_fzn_unit_cost = (((25000 * (fznMatl + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.factor_adj_25000 + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 25000) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 25000;
                                        model.GenerateQuoteHeader.calc_50000_fzn_unit_cost = (((50000 * (fznMatl + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.factor_adj_50000 + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 50000) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 50000;
                                        model.GenerateQuoteHeader.calc_100000_fzn_unit_cost = (((100000 * (fznMatl + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.factor_adj_100000 + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / 100000) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / 100000;
                                        //here is where the error is occuring
                                        model.GenerateQuoteHeader.calc_dynam_qty = model.GenerateQuoteHeader.QuoteQuantity;
                                        if (model.GenerateQuoteHeader.QuoteQuantity > 0)
                                        {
                                            model.GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (((model.GenerateQuoteHeader.QuoteQuantity * (fznMatl + model.GenerateQuoteHeader.fznB1Net + adj_amount + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / model.GenerateQuoteHeader.QuoteQuantity) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH)) + model.GenerateQuoteHeader.fznSUTotalNet)) / model.GenerateQuoteHeader.QuoteQuantity;
                                        }
                                        else
                                        {
                                            model.GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (((model.GenerateQuoteHeader.QuoteQuantity * (fznMatl + model.GenerateQuoteHeader.fznB1Net + adj_amount + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net)) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH + (model.GenerateQuoteHeader.fznSUTotalNet)))));
                                        }
                                    }
                                }
                                else if (Components.Count() > 0 && model.GenerateQuoteHeader.Division == "720000")
                                {
                                    model.GenerateQuoteHeader.calc_10_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_10) +(fznMatl + fznLbr + fznc2 + fznc1 + fznc3 + fznc4 + fznOS) + fznSUTotal / 10;
                                    model.GenerateQuoteHeader.calc_25_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_25) + (fznMatl + fznLbr + fznc2 + fznc1 + fznc3 + fznc4 + fznOS) + fznSUTotal / 25;
                                    model.GenerateQuoteHeader.calc_50_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_50) + (fznMatl + fznLbr + fznc2 + fznc1 + fznc3 + fznc4 + fznOS) + fznSUTotal / 50;
                                    model.GenerateQuoteHeader.calc_75_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_75) + (fznMatl + fznLbr + fznc2 + fznc1 + fznc3 + fznc4 + fznOS) + fznSUTotal / 75;
                                    model.GenerateQuoteHeader.calc_100_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_100) + (fznMatl + fznLbr + fznc2 + fznc1 + fznc3 + fznc4 + fznOS) + fznSUTotal / 100;
                                    model.GenerateQuoteHeader.calc_250_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_250) + (fznMatl + fznLbr + fznc2 + fznc1 + fznc3 + fznc4 + fznOS) + fznSUTotal / 250;
                                    model.GenerateQuoteHeader.calc_500_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_500) + (fznMatl + fznLbr + fznc2 + fznc1+ fznc3+fznc4 + fznOS) + fznSUTotal / 500;
                                    model.GenerateQuoteHeader.calc_750_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_750) + (fznMatl + fznLbr + fznc2 + fznc1 + fznc3 + fznc4 + fznOS) + fznSUTotal / 750; //750
                                    model.GenerateQuoteHeader.calc_1000_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_1000) + (fznMatl + fznLbr + fznc2 + fznc1+ fznc3+fznc4 + fznOS) + fznSUTotal / 1000;
                                    //model.GenerateQuoteHeader.calc_2000_fzn_unit_cost =  (fznMatl + fznLbr + fznc2 + fznc1+ fznc3+fznc4 + fznOS) + fznSUTotal / 2000;
                                    model.GenerateQuoteHeader.calc_2500_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_2500) + (fznMatl + fznLbr + fznc2 + fznc1+ fznc3+fznc4 + fznOS) + fznSUTotal / 2500;
                                    model.GenerateQuoteHeader.calc_5000_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_5000) + (fznMatl + fznLbr + fznc2 + fznc1+ fznc3+fznc4 + fznOS) + fznSUTotal / 5000;
                                    model.GenerateQuoteHeader.calc_10000_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_10000) + (fznMatl + fznLbr + fznc2 + fznc1+ fznc3+fznc4 + fznOS) + fznSUTotal / 10000;
                                    //model.GenerateQuoteHeader.calc_20000_fzn_unit_cost = (fznMatl + fznLbr + fznc2 + fznc1+ fznc3+fznc4 + fznOS) + fznSUTotal / 20000;
                                    model.GenerateQuoteHeader.calc_25000_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_25000) + (fznMatl + fznLbr + fznc2 + fznc1+ fznc3+fznc4 + fznOS) + fznSUTotal / 25000;
                                    //model.GenerateQuoteHeader.calc_50000_fzn_unit_cost = (fznMatl + fznLbr + fznc2 + fznc1+ fznc3+fznc4 + fznOS) + fznSUTotal / 50000;
                                    //model.GenerateQuoteHeader.calc_100000_fzn_unit_cost =(fznMatl + fznLbr + fznc2 + fznc1 + fznc3 + fznc4 + fznOS) + fznSUTotal / 100000;
                                    //here is where the error is occuring
                                    model.GenerateQuoteHeader.calc_dynam_qty = model.GenerateQuoteHeader.QuoteQuantity;

                                    if (model.GenerateQuoteHeader.QuoteQuantity > 0)
                                    {
                                        //model.GenerateQuoteHeader.calc_dynam_fzn_unit_cost = ((model.GenerateQuoteHeader.fznA1Net + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznD0Net + ((model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc4net) / model.GenerateQuoteHeader.QuoteQuantity) + model.GenerateQuoteHeader.totalCPQtyCostVar + model.GenerateQuoteHeader.totalCPQtyCostSU + model.GenerateQuoteHeader.totalCPQtyCostFOH + (model.GenerateQuoteHeader.fznSUTotalNet/model.GenerateQuoteHeader.QuoteQuantity)) / model.GenerateQuoteHeader.QuoteQuantity);
                                        model.GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (adj_amount) + (fznMatl + fznLbr + fznc2 + fznc1 +fznc3+fznc4+ fznOS) + fznSUTotal / model.GenerateQuoteHeader.QuoteQuantity;
                                        // mtlnet + lbrnet + vohnet + osnet + (fznfohnettotal / qty) + totalcpqtycostvar + totalcpqtycostsu + totalcpqtycostfoh + (fznsutotalnet / qty))/ 0.8))
                                        //(((getPrice.fznMatlNet+getPrice.fznLbrNet+getPrice.fznVOHNet+getPrice.fznOSNet+(getPrice.fznFOHNetTotal / qty99)+totalCPQtyCostVar + totalCPQtyCostSU + totalCPQtyCostFOH + (getPrice.fznSUTotalNet / qty99))/ .8),"999.99")
                                    }
                                    //else
                                    //{
                                    //    model.GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (fznMatl + fznLbr + fznc2 + fznc1 + fznc3 + fznc4 + fznOS) + fznSUTotal / 100000;
                                    //}
                                }
                                else if (Components.Count() == 0 && model.GenerateQuoteHeader.Division == "720000")
                                {
                                    model.GenerateQuoteHeader.calc_10_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_10) + ((10 * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 +fznc3 + fznOS)) + fznSUTotal) / 10;
                                    model.GenerateQuoteHeader.calc_25_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_25) + ((25 * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal) / 25;
                                    model.GenerateQuoteHeader.calc_50_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_50) + ((50 * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal) / 50;
                                    model.GenerateQuoteHeader.calc_75_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_75) + ((75 * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal) / 75;
                                    model.GenerateQuoteHeader.calc_100_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_100) + ((100 * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal) / 100;
                                    model.GenerateQuoteHeader.calc_250_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_250) + ((250 * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal) / 250;
                                    model.GenerateQuoteHeader.calc_500_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_500) + ((500 * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal) / 500;
                                    model.GenerateQuoteHeader.calc_750_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_750) + ((750 * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal) / 750;
                                    model.GenerateQuoteHeader.calc_1000_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_1000) + ((1000 * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal) / 1000;
                                    model.GenerateQuoteHeader.calc_2500_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_2500) + ((2500 * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal) / 2500;
                                    model.GenerateQuoteHeader.calc_5000_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_5000) + ((5000 * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal) / 5000;
                                    model.GenerateQuoteHeader.calc_10000_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_10000) + ((10000 * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal) / 10000;
                                    model.GenerateQuoteHeader.calc_25000_fzn_unit_cost = (model.GenerateQuoteHeader.factor_adj_25000) + ((25000 * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal) / 25000;
                                    model.GenerateQuoteHeader.calc_dynam_qty = model.GenerateQuoteHeader.QuoteQuantity;
                                    if (model.GenerateQuoteHeader.QuoteQuantity > 0)
                                    {
                                        model.GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (adj_amount) + ((model.GenerateQuoteHeader.QuoteQuantity * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal) / model.GenerateQuoteHeader.QuoteQuantity;
                                    }
                                    else
                                    {
                                        model.GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (adj_amount) + (( (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal);
                                    }
                                }
                                else if (model.GenerateQuoteHeader.Division=="710000")
                                {
                                    //model.GenerateQuoteHeader.UnitCost = fznMatl +  model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net ;

                                    //model.GenerateQuoteHeader.calc_10_fzn_unit_cost = ((10 * (fznMatl + model.GenerateQuoteHeader.factor_adj_10 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net))) / 10; //+ (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 10;
                                    //model.GenerateQuoteHeader.calc_25_fzn_unit_cost = ((25 * (fznMatl + model.GenerateQuoteHeader.factor_adj_25 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net))) / 25; //+ (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal))/ 25;
                                    //model.GenerateQuoteHeader.calc_50_fzn_unit_cost = ((50 * (fznMatl + model.GenerateQuoteHeader.factor_adj_50 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net ))) / 50; //+ (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 50;
                                    //model.GenerateQuoteHeader.calc_75_fzn_unit_cost = ((75 * (fznMatl + model.GenerateQuoteHeader.factor_adj_75 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net )) )/ 75; //+ (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 75;
                                    //model.GenerateQuoteHeader.calc_100_fzn_unit_cost = ((100 * (fznMatl + model.GenerateQuoteHeader.factor_adj_100 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net ))) / 100; // + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 100;
                                    //model.GenerateQuoteHeader.calc_250_fzn_unit_cost = ((250 * (fznMatl + model.GenerateQuoteHeader.factor_adj_250 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net ))) / 250; //+ (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 250;
                                    //model.GenerateQuoteHeader.calc_500_fzn_unit_cost = ((500 * (fznMatl + model.GenerateQuoteHeader.factor_adj_500 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net )) )/ 500; //+ (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 500;
                                    //model.GenerateQuoteHeader.calc_750_fzn_unit_cost = ((750 * (fznMatl + model.GenerateQuoteHeader.factor_adj_750 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net )) )/ 750; //+ (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 750;
                                    //model.GenerateQuoteHeader.calc_1000_fzn_unit_cost = ((1000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_1000 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net ))) / 1000; // + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 1000;
                                    //model.GenerateQuoteHeader.calc_2000_fzn_unit_cost = ((2000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_2000 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net ))) / 2000; //+ (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 2000;
                                    //model.GenerateQuoteHeader.calc_2500_fzn_unit_cost = ((2500 * (fznMatl + model.GenerateQuoteHeader.factor_adj_2500 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net )) )/ 2500; // + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 2500;
                                    //model.GenerateQuoteHeader.calc_5000_fzn_unit_cost = ((5000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_5000 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net )) )/ 5000; // + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 5000;
                                    //model.GenerateQuoteHeader.calc_10000_fzn_unit_cost = ((10000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_10000 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net ))) / 10000; // + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 10000;
                                    //model.GenerateQuoteHeader.calc_20000_fzn_unit_cost = ((20000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_20000 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net ))) / 20000; // + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 20000;
                                    //model.GenerateQuoteHeader.calc_25000_fzn_unit_cost = ((25000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_25000 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net ))) / 25000; //+ (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 25000;
                                    //model.GenerateQuoteHeader.calc_50000_fzn_unit_cost = ((50000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_50000 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net )) )/ 50000; // + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 50000;
                                    //model.GenerateQuoteHeader.calc_100000_fzn_unit_cost = ((100000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_100000 + model.GenerateQuoteHeader.fznB1Net + model.GenerateQuoteHeader.fznB2Net + model.GenerateQuoteHeader.fznc1net + model.GenerateQuoteHeader.fznc2net + model.GenerateQuoteHeader.fznc3net + model.GenerateQuoteHeader.fznc4net + model.GenerateQuoteHeader.fznD0Net ))) / 100000; //+ (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 100000;

                                    model.GenerateQuoteHeader.calc_10_fzn_unit_cost = ((10 * (fznMatl + model.GenerateQuoteHeader.factor_adj_10 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 10;
                                    model.GenerateQuoteHeader.calc_25_fzn_unit_cost = ((25 * (fznMatl + model.GenerateQuoteHeader.factor_adj_25 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 25;
                                    model.GenerateQuoteHeader.calc_50_fzn_unit_cost = ((50 * (fznMatl + model.GenerateQuoteHeader.factor_adj_50 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 50;
                                    model.GenerateQuoteHeader.calc_75_fzn_unit_cost = ((75 * (fznMatl + model.GenerateQuoteHeader.factor_adj_75 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 75;
                                    model.GenerateQuoteHeader.calc_100_fzn_unit_cost = ((100 * (fznMatl + model.GenerateQuoteHeader.factor_adj_100 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 100;
                                    model.GenerateQuoteHeader.calc_250_fzn_unit_cost = ((250 * (fznMatl + model.GenerateQuoteHeader.factor_adj_250 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 250;
                                    model.GenerateQuoteHeader.calc_500_fzn_unit_cost = ((500 * (fznMatl + model.GenerateQuoteHeader.factor_adj_500 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 500;
                                    model.GenerateQuoteHeader.calc_750_fzn_unit_cost = ((750 * (fznMatl + model.GenerateQuoteHeader.factor_adj_750 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 750;
                                    model.GenerateQuoteHeader.calc_1000_fzn_unit_cost = ((1000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_1000 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 1000;
                                    model.GenerateQuoteHeader.calc_2000_fzn_unit_cost = ((2000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_2000 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 2000;
                                    model.GenerateQuoteHeader.calc_2500_fzn_unit_cost = ((2500 * (fznMatl + model.GenerateQuoteHeader.factor_adj_2500 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 2500;
                                    model.GenerateQuoteHeader.calc_5000_fzn_unit_cost = ((5000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_5000 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 5000;
                                    model.GenerateQuoteHeader.calc_10000_fzn_unit_cost = ((10000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_10000 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 10000;
                                    model.GenerateQuoteHeader.calc_20000_fzn_unit_cost = ((20000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_20000 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 20000;
                                    model.GenerateQuoteHeader.calc_25000_fzn_unit_cost = ((25000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_25000 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 25000;
                                    model.GenerateQuoteHeader.calc_50000_fzn_unit_cost = ((50000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_50000 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 50000;
                                    model.GenerateQuoteHeader.calc_100000_fzn_unit_cost = ((100000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_100000 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 100000;

                                    model.GenerateQuoteHeader.calc_dynam_qty = model.GenerateQuoteHeader.QuoteQuantity;
                                    if (model.GenerateQuoteHeader.QuoteQuantity > 0)
                                    {
                                        model.GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (adj_amount) + ((model.GenerateQuoteHeader.QuoteQuantity * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal) / model.GenerateQuoteHeader.QuoteQuantity;
                                    }
                                    else
                                    {
                                        model.GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (adj_amount) + (((fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal);
                                    }
                                }
                                else
                                {

                                    model.GenerateQuoteHeader.calc_10_fzn_unit_cost = ((10 * (fznMatl + model.GenerateQuoteHeader.factor_adj_10 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 10;
                                    model.GenerateQuoteHeader.calc_25_fzn_unit_cost = ((25 * (fznMatl + model.GenerateQuoteHeader.factor_adj_25 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 25;
                                    model.GenerateQuoteHeader.calc_50_fzn_unit_cost = ((50 * (fznMatl + model.GenerateQuoteHeader.factor_adj_50 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 50;
                                    model.GenerateQuoteHeader.calc_75_fzn_unit_cost = ((75 * (fznMatl + model.GenerateQuoteHeader.factor_adj_75 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 75;
                                    model.GenerateQuoteHeader.calc_100_fzn_unit_cost = ((100 * (fznMatl + model.GenerateQuoteHeader.factor_adj_100 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 100;
                                    model.GenerateQuoteHeader.calc_250_fzn_unit_cost = ((250 * (fznMatl + model.GenerateQuoteHeader.factor_adj_250 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 250;
                                    model.GenerateQuoteHeader.calc_500_fzn_unit_cost = ((500 * (fznMatl + model.GenerateQuoteHeader.factor_adj_500 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 500;
                                    model.GenerateQuoteHeader.calc_750_fzn_unit_cost = ((750 * (fznMatl + model.GenerateQuoteHeader.factor_adj_750 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 750;
                                    model.GenerateQuoteHeader.calc_1000_fzn_unit_cost = ((1000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_1000 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 1000;
                                    model.GenerateQuoteHeader.calc_2000_fzn_unit_cost = ((2000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_2000 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 2000;
                                    model.GenerateQuoteHeader.calc_2500_fzn_unit_cost = ((2500 * (fznMatl + model.GenerateQuoteHeader.factor_adj_2500 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 2500;
                                    model.GenerateQuoteHeader.calc_5000_fzn_unit_cost = ((5000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_5000 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 5000;
                                    model.GenerateQuoteHeader.calc_10000_fzn_unit_cost = ((10000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_10000 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 10000;
                                    model.GenerateQuoteHeader.calc_20000_fzn_unit_cost = ((20000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_20000 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 20000;
                                    model.GenerateQuoteHeader.calc_25000_fzn_unit_cost = ((25000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_25000 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 25000;
                                    model.GenerateQuoteHeader.calc_50000_fzn_unit_cost = ((50000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_50000 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 50000;
                                    model.GenerateQuoteHeader.calc_100000_fzn_unit_cost = ((100000 * (fznMatl + model.GenerateQuoteHeader.factor_adj_100000 + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS)) + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal)) / 100000;

                                    model.GenerateQuoteHeader.calc_dynam_qty = model.GenerateQuoteHeader.QuoteQuantity;
                                    if (model.GenerateQuoteHeader.QuoteQuantity > 0)
                                    {
                                        model.GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (fznMatl + adj_amount + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS + (fznSUTotal / model.GenerateQuoteHeader.QuoteQuantity) + (model.GenerateQuoteHeader.fznLCTotal / model.GenerateQuoteHeader.QuoteQuantity));
                                    }
                                    else
                                    {
                                        model.GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (fznMatl + adj_amount + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS + (fznSUTotal) + (model.GenerateQuoteHeader.fznLCTotal));
                                    }

                                }
                            }

                        }
                        else
                        {
                            MessageBox.Show("Failed to obtain frozen cost.");
                        }
                        reader.Close();
                    }
                }


                string customer_name_convert_search = model.GenerateQuoteHeader.CustomerName;
                //string customer_number_convert;
                string customer_name = null;
                string customer_num = null;
                int customer_name_length = 0;
                if (customer_name_convert_search!=null)
                {
                    customer_name_length=customer_name_convert_search.Length - 9;
                }
                int customer_num_length_start;

                if (model.GenerateQuoteHeader.CustomerName != null)
                {
                    customer_name = customer_name_convert_search.Substring(0, customer_name_length);
                    customer_num_length_start = customer_name_length + 3;
                    customer_num = customer_name_convert_search.Substring(customer_num_length_start, 6);
                }



                int additional_lta_count = 0;

                if (customer_num != null)
                {
                    sqlScript = @"select count(*) from (select distinct tv_header.mcu, tv_header.supname 
                , tv_header.item, tv_header.minqty, tv_header.maxqty,tv_header.amu, tv2.getMaxDate, tv_header.an8 supplier_code 
                , v.abalph Customer_Name, tv_header.itm
                    from coldfuse.tvlta tv_header  join 
                        (select max(tvlta.rptDate) as priorDate 
                    , tvlta.an8, tvlta.supname, tv.getMaxDate 
                        from        coldfuse.tvlta  tvlta 
                        left outer join(select distinct max(rptdate) getMaxDate, supname, an8 
                                from coldfuse.tvlta tv 
                                group by supname, an8) tv 
                            on tv.an8 = tvlta.an8 
                        where       1 = 1 and tvlta.an8 in ('152632', '140991', '175238', '174984', '141448', '140991', '142097', '141448') 
                        and date(tvlta.rptDate) < Date(tv.getMaxDate) 
                        group by tvlta.an8, tvlta.supname, tv.getMaxDate 
                        ) tv2 
                        on tv_header.an8 = tv2.an8 
                        and tv_header.rptdate = tv2.getMaxDate 
                    left outer join proddta.f0101 v 
                        on tv_header.an8 = v.aban8 
                        where 1 = 1 
                     and tv_header.itm = ? 
                     and tv_header.mcu in ('730000','710000','750000','720000','Q720000','780000','734000') and tv_header.an8=?) lta_table ";
                }
                else
                {
                    
                        sqlScript = @"select count(*) from (select distinct tv_header.mcu, tv_header.supname 
                , tv_header.item, tv_header.minqty, tv_header.maxqty,tv_header.amu, tv2.getMaxDate, tv_header.an8 supplier_code 
                , v.abalph Customer_Name , tv_header.itm
                    from coldfuse.tvlta tv_header  join 
                        (select max(tvlta.rptDate) as priorDate 
                    , tvlta.an8, tvlta.supname, tv.getMaxDate 
                        from        coldfuse.tvlta  tvlta 
                        left outer join(select distinct max(rptdate) getMaxDate, supname, an8 
                                from coldfuse.tvlta tv 
                                group by supname, an8) tv 
                            on tv.an8 = tvlta.an8 
                        where       1 = 1 and tvlta.an8 in ('152632', '140991', '175238', '174984', '141448', '140991', '142097', '141448') 
                        and date(tvlta.rptDate) < Date(tv.getMaxDate) 
                        group by tvlta.an8, tvlta.supname, tv.getMaxDate 
                        ) tv2 
                        on tv_header.an8 = tv2.an8 
                        and tv_header.rptdate = tv2.getMaxDate 
                    left outer join proddta.f0101 v 
                        on tv_header.an8 = v.aban8 
                        where 1 = 1 
                     and tv_header.itm = ? 
                     and tv_header.mcu in ('730000','710000','750000','720000','Q720000','780000','734000') ) lta_table ";
                    
                }
                using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                {
                    command.Parameters.Add(new OdbcParameter("@item_num", model.GenerateQuoteHeader.ibitm));
                    if (customer_num != null)
                    {
                        command.Parameters.Add(new OdbcParameter("@customer_num",customer_num));
                    }
                    
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {

                            while (reader.Read())
                            {
                                additional_lta_count = reader.GetInt32(0);
                            }
                            if (additional_lta_count > 0 && model.GenerateQuoteHeader.LTACount<1)
                            {
                                model.GenerateQuoteHeader.LTACount = additional_lta_count + model.GenerateQuoteHeader.LTACount;
                            }
                        
                        }
                        reader.Close();
                    }
                
                }

                int i = 1;
                if (customer_num != null)
                {
         
          sqlScript = @"select trim(soh.Division) Division,date(to_Date(soh.Order_Date,'YYYY-MM-DD')) Order_Date, soh.Order_Num
                                , sum(soh.Quantity) Quantity
                                , soh.Unit_Price
                                , soh.Status
                                , soh.Customer_Name, soh.Customer_No, Unit_Cost  from (select sdmcu Division, sddoco Order_Num, so.sddcto Order_Type
	                                , dba.juliantodate(sdtrdj) as Order_Date
                                                    , trim(sdlitm) as Part_Number
	                                , sduorg Quantity
	                                , dec(sduprc * .000001, 15, 4) as Unit_Price  
                	                                , case when sdnxtr = '999' then 'Closed' else 'Open' end as Status 
                                                     , sdan8 Customer_No
	                                , (abalph) as Customer_Name , SDUNCS*.000001 Unit_Cost
                                                         from        PRODDTA.f4211 so join PRODDTA.f0101 on sdan8 = aban8 
                                                             left outer join zpemjde.calendar_dim cal on 
                                                                 so.sdtrdj = cal.juliandate 
                                                         where    1 = 1            and sditm=? and trim(sdmcu) in ('730000','710000','750000','720000','780000','780015','734000')
                                                           and sddcto in ('SO','SX') 
                                                         and sdlttr<> '980' 
                                                         and DATE(cal.date)<= (current date) 
                                                         union all 
                                                         select sdmcu Division, sddoco Order_Num, sddcto Order_Type,  dba.juliantodate(sdtrdj) Order_Date, 
                                                                 trim(sdlitm) as Part_Number, sduorg Quantity, dec(sduprc * .000001, 15, 4) as Unit_Price,  
                                                                 case when sdnxtr = '999' then 'Closed' else 'Open' end as Status, 
                                                                 sdan8 Customer_No, trim(abalph) as Customer_Name, SDUNCS*.000001 Unit_Cost
                                                         from        PRODDTA.f42119 soh join PRODDTA.f0101 on sdan8 = aban8 
                                                             left outer join 
                                                             zpemjde.calendar_dim cal2 on 
                                                             soh.sdtrdj = cal2.juliandate 
                                                       where sditm =?
                                                             and trim(sdmcu) in ('730000','710000','750000','720000','780000','780015','734000')
                                                       and sddcto in ('SO','SX') 
                                                         and sdlttr<> '980') soh WHERE soh.customer_no=?
                                group by Division, Order_Date, Order_Num, Unit_Price, Status, Customer_Name, Customer_No, Unit_Cost order by Order_Date desc";
                }
                else
                {
          sqlScript = @"select trim(soh.Division) Division,date(to_Date(soh.Order_Date,'YYYY-MM-DD')) Order_Date, soh.Order_Num
                                , sum(soh.Quantity) Quantity
                                , soh.Unit_Price
                                , soh.Status
                                , soh.Customer_Name, soh.Customer_No, Unit_Cost  from (select sdmcu Division, sddoco Order_Num, so.sddcto Order_Type
	                                , dba.juliantodate(sdtrdj) as Order_Date
                                                    , trim(sdlitm) as Part_Number
	                                , sduorg Quantity
	                                , dec(sduprc * .000001, 15, 4) as Unit_Price  
                	                                , case when sdnxtr = '999' then 'Closed' else 'Open' end as Status 
                                                     , sdan8 Customer_No
	                                , (abalph) as Customer_Name , SDUNCS*.000001 Unit_Cost
                                                         from        PRODDTA.f4211 so join PRODDTA.f0101 on sdan8 = aban8 
                                                             left outer join zpemjde.calendar_dim cal on 
                                                                 so.sdtrdj = cal.juliandate 
                                                         where    1 = 1            and sditm=? and trim(sdmcu) in ('730000','710000','750000','720000','780000','780015','734000')
                                                           and sddcto in ('SO','SX') 
                                                         and sdlttr<> '980' 
                                                         and DATE(cal.date)<= (current date) 
                                                         union all 
                                                         select sdmcu Division, sddoco Order_Num, sddcto Order_Type,  dba.juliantodate(sdtrdj) Order_Date, 
                                                                 trim(sdlitm) as Part_Number, sduorg Quantity, dec(sduprc * .000001, 15, 4) as Unit_Price,  
                                                                 case when sdnxtr = '999' then 'Closed' else 'Open' end as Status, 
                                                                 sdan8 Customer_No, trim(abalph) as Customer_Name, SDUNCS*.000001 Unit_Cost
                                                         from        PRODDTA.f42119 soh join PRODDTA.f0101 on sdan8 = aban8 
                                                             left outer join 
                                                             zpemjde.calendar_dim cal2 on 
                                                             soh.sdtrdj = cal2.juliandate 
                                                       where sditm =?
                                                             and trim(sdmcu) in ('730000','710000','750000','720000','780000','780015','734000')
                                                       and sddcto in ('SO','SX') 
                                                         and sdlttr<> '980') soh 
                                group by Division, Order_Date, Order_Num, Unit_Price, Status, Customer_Name, Customer_No, Unit_Cost order by Order_Date desc";
        }
                using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                {
                    command.Parameters.Add(new OdbcParameter("@item_num", model.GenerateQuoteHeader.ibitm));
                    //if (customer_num != null)
                    //{
                    //    command.Parameters.Add(new OdbcParameter("@customer_num", customer_num));
                    //}

                    command.Parameters.Add(new OdbcParameter("@item_num2", model.GenerateQuoteHeader.ibitm));
                    if (customer_num != null)
                    {
                        command.Parameters.Add(new OdbcParameter("@customer_num", customer_num));
                    }
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                SalesOrder item = new SalesOrder()
                                {
                                    SOLineNum = i,
                                    SOOrderDate = reader.GetDateTime(1),
                                    SOOrderNum = reader.GetString(2),
                                    SOQuantity = reader.GetInt32(3),
                                    SOUnitPrice = Convert.ToDecimal(string.Format("{0:0.00}", Math.Round(reader.GetDecimal(4), 2))),
                                    SOStatus = reader.GetString(5),
                                    SOCustomerName = reader.GetString(6),
                                    SOCustomerNum = reader.GetInt32(7),
                                    SOExtPrice = reader.GetDecimal(4) * reader.GetInt32(3),
                                  SOUnitCost = Convert.ToDecimal(string.Format("{0:0.00}", Math.Round(reader.GetDecimal(8), 2)))
                                };
                                model2.Add(item);
                                model.GenerateQuoteHeader.Wins = model2.Count();
                                i++;
                            }
                        }
                        reader.Close();

                    }
                }

        //prepare the new formula
        //float y = 0;
        //int t = 0;
        //float y_zero = 0;

                i = 1;
                if (customer_num != null)
                {
          
          sqlScript = @"select	sdmcu, sddoco Order_Num, sddcto Order_Type, dec(sdlnid/1000,8,3) as sdlnid, dba.juliantodate(sdtrdj) as Order_Date, 
                         trim(sdlitm) as sdlitm, sduorg Quantity, dec(sduprc * .000001, 15, 4) as Unit_Price,  
                         case when sdnxtr = '999' then 'Closed' else 'Open' end as Status, 
                         dec(sdan8,15,0) as Customer_No, trim(abalph) as Customer_Name  , sduncs*.000001 Unit_Cost
                                 from PRODDTA.f4211 so join PRODDTA.f0101 on sdan8 = aban8 
                          left outer join zpemjde.calendar_dim cal on 
                                                     so.sdtrdj = cal.juliandate 
                                 where 1=1
                                 and sditm = ?
                                and trim(sdmcu) in ('730000','710000','750000','720000','780000','780015','734000')
                                 and sddcto in ('SQ') 
                                 and sdlttr<> '980' 
                                                and date(cal.date)<= (current date)  
                                                     and date(cal.date)>= (current date - 2 YEAR) 
                                    and dec(sdan8,15,0)=?
                                 union all 
                                 select sdmcu, sddoco, sddcto, dec(sdlnid / 1000, 8, 3) as sdlnid, dba.juliantodate(sdtrdj) Order_Date,  
                                             trim(sdlitm) as sdlitm, sduorg, dec(sduprc * .000001, 15, 4) as sduprc,  
                                 			case when sdnxtr = '999' then 'Closed' else 'Open' end as sdnxtr, 
                                             dec(sdan8,15,0) as Customer_No, trim(abalph) as abalph , sduncs*.000001 Unit_Cost
                                from PRODDTA.f42119 soh join PRODDTA.f0101 on sdan8 = aban8  
                          left outer join zpemjde.calendar_dim cal2 on soh.sdtrdj = cal2.juliandate 
                                 where 1=1
                                 and sditm = ?
                                    and trim(sdmcu) in ('730000','710000','750000','720000','780000','780015','734000')
                                 and sddcto in ('SQ') 
                                and  dec(sdan8,15,0)=?
                                 and sdlttr<> '980' order by Order_Date desc";
                }
                else
                {
         
          sqlScript = @"select	sdmcu, sddoco Order_Num, sddcto Order_Type, dec(sdlnid/1000,8,3) as sdlnid, dba.juliantodate(sdtrdj) as Order_Date, 
                         trim(sdlitm) as sdlitm, sduorg Quantity, dec(sduprc * .000001, 15, 4) as Unit_Price,  
                         case when sdnxtr = '999' then 'Closed' else 'Open' end as Status, 
                         dec(sdan8, 15, 0) as Customer_No, trim(abalph) as Customer_Name  , sduncs * .000001 Unit_Cost
                                     from PRODDTA.f4211 so join PRODDTA.f0101 on sdan8 = aban8
                          left outer join zpemjde.calendar_dim cal on
                                                     so.sdtrdj = cal.juliandate
                                 where 1 = 1
                                 and sditm = ?
                                and trim(sdmcu) in ('730000', '710000', '750000', '720000','780000','780015','734000')
                                 and sddcto in ('SQ')
                                 and sdlttr<> '980'
                                                and date(cal.date)<= (current date)  
                                                     and date(cal.date)>= (current date - 2 YEAR) 
                                 union all
                                 select sdmcu, sddoco, sddcto, dec(sdlnid / 1000, 8, 3) as sdlnid, dba.juliantodate(sdtrdj) Order_Date,  
                                             trim(sdlitm) as sdlitm, sduorg, dec(sduprc * .000001, 15, 4) as sduprc,  
                                 			case when sdnxtr = '999' then 'Closed' else 'Open' end as sdnxtr, 
                                             dec(sdan8, 15, 0) as Customer_No, trim(abalph) as abalph , sduncs * .000001 Unit_Cost
                                    from PRODDTA.f42119 soh join PRODDTA.f0101 on sdan8 = aban8
                          left outer join zpemjde.calendar_dim cal2 on soh.sdtrdj = cal2.juliandate
                                 where 1 = 1
                                 and sditm = ?
                                    and trim(sdmcu) in ('730000', '710000', '750000', '720000','780000','780015','734000')
                                 and sddcto in ('SQ')
                                 and sdlttr<> '980' order by Order_Date desc";
                }
                using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                {
                    command.Parameters.Add(new OdbcParameter("@item_num", model.GenerateQuoteHeader.ibitm));
                    //command.Parameters.Add(new OdbcParameter("@division", model.GenerateQuoteHeader.Division));
                    if (customer_num != null)
                    {
                        command.Parameters.Add(new OdbcParameter("@customer_num", customer_num));
                    }
                    command.Parameters.Add(new OdbcParameter("@item_num", model.GenerateQuoteHeader.ibitm));
                    if (customer_num != null)
                    {
                        command.Parameters.Add(new OdbcParameter("@customer_num", customer_num));
                    }
                    
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Quote item = new Quote()
                                {
                                    QHLineNum = i,
                                    QHQuoteNum = reader.GetString(reader.GetOrdinal("Order_Num")),
                                    QHQuoteDate = reader.GetDate(reader.GetOrdinal("Order_Date")),
                                    QHQuantity = reader.GetInt32(reader.GetOrdinal("Quantity")),
                                    QHUnitPrice = reader.GetDecimal(reader.GetOrdinal("Unit_Price")),
                                    QHStatus = reader.GetString(reader.GetOrdinal("Status")),
                                    QHCustomerNum = reader.GetInt32(9),
                                    QHCustomerName = reader.GetString(reader.GetOrdinal("Customer_Name")),
                                    QHExtPrice = reader.GetDecimal(reader.GetOrdinal("Unit_Price")) * reader.GetInt32(reader.GetOrdinal("Quantity")),
                                    QHUnitCost=reader.GetDecimal(reader.GetOrdinal("Unit_Cost"))
                                };

                                quotes.Add(item);
                                model.GenerateQuoteHeader.Losses = quotes.Count();
                                i++;
                            }

                        }

                    }

                    i = 1;

                }


                model.GenerateQuoteHeader.TotalOrders = model.GenerateQuoteHeader.Wins + model.GenerateQuoteHeader.Losses;
                if (model.GenerateQuoteHeader.TotalOrders > 0 & model.GenerateQuoteHeader.Losses > 0)
                {
                    decimal win_calc = (((model.GenerateQuoteHeader.Wins) / (model.GenerateQuoteHeader.Losses)) * 100);
                    model.GenerateQuoteHeader.WinsCalc = Math.Round(win_calc, 2);
                    //model.GenerateQuoteHeader.LossesCalc = model.GenerateQuoteHeader.Losses / model.GenerateQuoteHeader.TotalOrders;
                    //model.GenerateQuoteHeader.WinsPercent = (model.GenerateQuoteHeader.Wins / model.GenerateQuoteHeader.TotalOrders);
                }
            }


            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                cnn.Open();
                string sqlScript;

                sqlScript = @"insert into LOGIN_ACTIVITY ([user_id],LAST_LOGIN_DATETIME, MODULE_VISITED, PART_SEARCHED, VALUE_STREAM
                               , PRODUCT_GROUP, PRODUCT_CLASS, ACQ, FIRST_OP_DEPT, QUANTITY_ON_HAND, QUANTITY_AVAILABLE, PRODUCT_TYPE
                            ,FZN_UNIT_COST, COST_500, MARGIN_500, COST_1000, MARGIN_1000, COST_2000, MARGIN_2000
                            ,COST_2500, MARGIN_2500, COST_5000, MARGIN_5000, COST_10000, MARGIN_10000, COST_20000, MARGIN_20000, COST_25000, MARGIN_25000
                            , COST_50000, MARGIN_50000, COST_100000, MARGIN_100000) values (@user_id,getdate(),'PRICING TOOL',@PART_SEARCHED
                            , @VALUE_STREAM
                               , @PRODUCT_GROUP, @PRODUCT_CLASS, @ACQ, @FIRST_OP_DEPT, @QUANTITY_ON_HAND, @QUANTITY_AVAILABLE, @PRODUCT_TYPE
                            ,@FZN_UNIT_COST, @COST_500, @MARGIN_500, @COST_1000, @MARGIN_1000, @COST_2000, @MARGIN_2000
                            ,@COST_2500, @MARGIN_2500, @COST_5000, @MARGIN_5000, @COST_10000, @MARGIN_10000, @COST_20000, @MARGIN_20000, @COST_25000, @MARGIN_25000 
                            , @COST_50000, @MARGIN_50000, @COST_100000, @MARGIN_100000 )";
                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    command.Parameters.Add(new SqlParameter("@user_id", model.GenerateQuoteHeader.UserID));
                    if (string.IsNullOrEmpty(model.GenerateQuoteHeader.PartNumber) == true && string.IsNullOrEmpty(model.GenerateQuoteHeader.CustomerPartNum) == true)
                    {
                        command.Parameters.Add(new SqlParameter("@PART_sEARCHED", "NO PART ENTERED"));
                    }
                    else if (string.IsNullOrEmpty(model.GenerateQuoteHeader.CustomerPartNum) == true)
                    {
                        command.Parameters.Add(new SqlParameter("@PART_SEARCHED", model.GenerateQuoteHeader.PartNumber));
                    }
                    else
                    {
                        command.Parameters.Add(new SqlParameter("@PART_sEARCHED", model.GenerateQuoteHeader.CustomerPartNum));
                    }

                    //command.Parameters.Add(new SqlParameter("@VALUE_STREAM", model.GenerateQuoteHeader.ValueStream));
                    SqlParameter value_stream_param = new SqlParameter
                    {
                        ParameterName = "@VALUE_STREAM",
                        Value = String.IsNullOrEmpty(model.GenerateQuoteHeader.ValueStream) ? "" : model.GenerateQuoteHeader.ValueStream.ToString()
                    };
                    command.Parameters.Add(value_stream_param);
                    //command.Parameters.Add(new SqlParameter("@PRODUCT_GROUP", model.GenerateQuoteHeader.ProductGroup));
                    SqlParameter product_group_param = new SqlParameter
                    {
                        ParameterName = "@PRODUCT_GROUP",
                        Value = String.IsNullOrEmpty(model.GenerateQuoteHeader.ProductGroup) ? "" : model.GenerateQuoteHeader.ProductGroup.ToString()
                    };
                    command.Parameters.Add(product_group_param);
                    //command.Parameters.Add(new SqlParameter("@PROUCT_CLASS", model.GenerateQuoteHeader.ProductCode));
                    SqlParameter product_class_param = new SqlParameter
                    {
                        ParameterName = "@PRODUCT_CLASS",
                        Value = String.IsNullOrEmpty(model.GenerateQuoteHeader.ProductCode) ? "" : model.GenerateQuoteHeader.ProductCode.ToString()
                    };
                    command.Parameters.Add(product_class_param);
                    command.Parameters.Add(new SqlParameter("@ACQ", model.GenerateQuoteHeader.ACQ));
                    //command.Parameters.Add(new SqlParameter("@FIRST_OP_DEPT", model.GenerateQuoteHeader.GetFirstOp));
                    SqlParameter first_op_dept_param = new SqlParameter
                    {
                        ParameterName = "@FIRST_OP_DEPT",
                        Value = String.IsNullOrEmpty(model.GenerateQuoteHeader.GetFirstOp) ? "" : model.GenerateQuoteHeader.GetFirstOp.ToString()
                    };
                    command.Parameters.Add(first_op_dept_param);
                    command.Parameters.Add(new SqlParameter("@QUANTITY_ON_HAND", model.GenerateQuoteHeader.QuantityOnHand));
                    command.Parameters.Add(new SqlParameter("@QUANTITY_AVAILABLE", model.GenerateQuoteHeader.QtyAvail));
                    //command.Parameters.Add(new SqlParameter("@PRODUCT_TYPE", model.GenerateQuoteHeader.ProductType));
                    SqlParameter product_type_parma = new SqlParameter
                    {
                        ParameterName = "@PRODUCT_TYPE",
                        Value = String.IsNullOrEmpty(model.GenerateQuoteHeader.ProductType) ? "" : model.GenerateQuoteHeader.ProductType.ToString()
                    };
                    command.Parameters.Add(product_type_parma);
                    command.Parameters.Add(new SqlParameter("@FZN_UNIT_COST", model.GenerateQuoteHeader.UnitCost));
                    command.Parameters.Add(new SqlParameter("@COST_500", model.GenerateQuoteHeader.calc_500_fzn_unit_cost));
                    command.Parameters.Add(new SqlParameter("@MARGIN_500", model.GenerateQuoteHeader.margin_500));
                    command.Parameters.Add(new SqlParameter("@COST_1000", model.GenerateQuoteHeader.calc_1000_fzn_unit_cost));
                    command.Parameters.Add(new SqlParameter("@MARGIN_1000", model.GenerateQuoteHeader.margin_1000));
                    command.Parameters.Add(new SqlParameter("@COST_2000", model.GenerateQuoteHeader.calc_2000_fzn_unit_cost));
                    command.Parameters.Add(new SqlParameter("@MARGIN_2000", model.GenerateQuoteHeader.margin_2000));
                    command.Parameters.Add(new SqlParameter("@COST_2500", model.GenerateQuoteHeader.calc_2500_fzn_unit_cost));
                    command.Parameters.Add(new SqlParameter("@MARGIN_2500", model.GenerateQuoteHeader.margin_2500));
                    command.Parameters.Add(new SqlParameter("@COST_5000", model.GenerateQuoteHeader.calc_5000_fzn_unit_cost));
                    command.Parameters.Add(new SqlParameter("@MARGIN_5000", model.GenerateQuoteHeader.margin_5000));
                    command.Parameters.Add(new SqlParameter("@COST_10000", model.GenerateQuoteHeader.calc_10000_fzn_unit_cost));
                    command.Parameters.Add(new SqlParameter("@MARGIN_10000", model.GenerateQuoteHeader.margin_10000));
                    command.Parameters.Add(new SqlParameter("@COST_20000", model.GenerateQuoteHeader.calc_20000_fzn_unit_cost));
                    command.Parameters.Add(new SqlParameter("@MARGIN_20000", model.GenerateQuoteHeader.margin_20000));
                    command.Parameters.Add(new SqlParameter("@COST_25000", model.GenerateQuoteHeader.calc_25000_fzn_unit_cost));
                    command.Parameters.Add(new SqlParameter("@MARGIN_25000", model.GenerateQuoteHeader.margin_25000));
                    command.Parameters.Add(new SqlParameter("@COST_50000", model.GenerateQuoteHeader.calc_50000_fzn_unit_cost));
                    command.Parameters.Add(new SqlParameter("@MARGIN_50000", model.GenerateQuoteHeader.margin_50000));
                    command.Parameters.Add(new SqlParameter("@COST_100000", model.GenerateQuoteHeader.calc_100000_fzn_unit_cost));
                    command.Parameters.Add(new SqlParameter("@MARGIN_100000", model.GenerateQuoteHeader.margin_100000));


                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        //messagebox?

                    }
                    finally
                    {
                        cnn.Close();
                    }
                }


            }
            

            var ourModel = new OriginalPricingTool
            {
                GenerateQuoteHeader = model.GenerateQuoteHeader,
                SalesOrders = model2,
                cl = cl,
                Competitors = Comps,
                CustParts=CustParts,
                Components = Components,
                ActualCosts = ActCost,
                Comments = Comments,
                Quotes = quotes,
                LTAs = LTAs,
                LTAParts=LTAParts,
                QuoteWorkbenchList= bench
            };
            //overallTime.Stop(this.HttpContext);
            return View(ourModel);
            //return View();

        }

   

        [HttpPost]
        public ActionResult _CommentsAddOrig(OriginalPricingTool model, GenerateQuoteHeader GenerateQuoteHeader, NewCommentDetail NewCommentDetail)
        {


        


            //int user_id = 0;
            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                cnn.Open();
        
                string sqlScript = @"insert into comment_header (created_by_user_id, creation_date,last_updated_date,quote_part_link,active_flag) VALUES (@user_id,convert(date,getdate()),convert(date,getdate()),@item_num,1); 
                insert into comment_lines (comment_header_id,comment,created_by_user_id,creation_date,active_flag) values (SCOPE_IDENTITY(),@comment,@user_id,convert(date,getdate()),1);";

                //insert into comment_header(created_by_user_id, creation_date, last_updated_date, quote_part_link, active_flag) VALUES(@user_id, convert(date, getdate()), convert(date, getdate()), @item_num, 1); " +
                //"insert into comment_lines (comment_header_id,comment,created_by_user_id,creation_date,active_flag) values (SCOPE_IDENTITY(),@comment,@user_id,convert(date,getdate()),1);";

                using (var command = new SqlCommand(sqlScript, cnn))
                {

                    command.Parameters.Add(new SqlParameter("@user_id", GenerateQuoteHeader.UserID));
                    command.Parameters.Add(new SqlParameter("@item_num", GenerateQuoteHeader.PartNumber));
                    command.Parameters.Add(new SqlParameter("@comment", NewCommentDetail.NewPartComment));
                    //reader.IsDBNull(16) ? 0 : (decimal)reader.GetValue(16),

                    try
                    {
                        command.ExecuteNonQuery();
                        //command.Dispose();
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show("Could not insert the new comment.");
                        return View();
                    }
                    finally
                    {
                        cnn.Close();
                        cnn.Dispose();
                    }
                }

            }
            //cnn.Close();

            //OdbcConnection cnn_odbc;
            //cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString);
            //List<SelectListItem> CostReference = new List<SelectListItem>();
            //using (cnn_odbc)
            //{
            //    cnn_odbc.Open();
            //    string query = "SELECT c.ieledg COST_ID,case when c.IELEDG = '07' then 'Simulated Cost' " +
            //                    " when c.IELEDG = 'P1' then 'Frozen Cost' " +
            //                    " end Cost_Description " +
            //                    " FROM(SELECT DISTINCT(IELEDG) from PRODDTA.F30026 WHERE IELEDG IN('07', 'P1') " +
            //                    " ) c";
            //    OdbcCommand cmd = new OdbcCommand(query, cnn_odbc);
            //    //cnn_odbc.Open();
            //    using (OdbcDataReader reader = cmd.ExecuteReader())
            //    {
            //        try
            //        {
            //            while (reader.Read())
            //            {
            //                CostReference.Add(new SelectListItem
            //                {
            //                    Text = reader["Cost_Description"].ToString(),
            //                    Value = reader["COST_ID"].ToString(),
            //                });
            //            }
            //        }
            //        catch (Exception ex)
            //        {

            //            //cnn_odbc.Close();
            //            //cnn_odbc.Dispose();
            //        }
            //        finally
            //        {
            //            cnn_odbc.Close();
            //            cnn_odbc.Dispose();
            //        }
            //    }
            //    //cnn_odbc.Close();
            //    model.GenerateQuoteHeader.CostReferenceList = CostReference;
            //    //cnn_odbc.Close();
            //}
            return new HttpStatusCodeResult(HttpStatusCode.OK);


            //var ourModel = new OriginalPricingTool
            //{
            //    //GenerateQuoteHeader = header_model,
            //    SalesOrders = null,
            //    Competitors = null,
            //    Components = null,
            //    Comments = null,
            //    LTAs = null,
            //    //ActualCosts = null,
            //    //QuoteWorkbench = null,
            //    DemandList = null
            //};
            //return View("~/Views/Home/OriginalPricingTool.cshtml", ourModel);
        
            

        }

        [HttpPost]
        public ActionResult UpdateComponentACQ(OriginalPricingTool model)
        {
            //update model for view recalculation
            model.GenerateQuoteHeader.calc_dynam_qty = model.GenerateQuoteHeader.QuoteQuantity;
            model.GenerateQuoteHeader.calc_dynam_fzn_unit_cost = 0.05m;
            model.GenerateQuoteHeader.calc_500_fzn_unit_cost = 0.08m;
            model.GenerateQuoteHeader.calc_1000_fzn_unit_cost = 0.08m;
            model.GenerateQuoteHeader.calc_2000_fzn_unit_cost = 0.07m;
            model.GenerateQuoteHeader.calc_2500_fzn_unit_cost = 0.07m;
            model.GenerateQuoteHeader.calc_5000_fzn_unit_cost = 0.07m;
            model.GenerateQuoteHeader.calc_10000_fzn_unit_cost = 0.07m;
            model.GenerateQuoteHeader.calc_20000_fzn_unit_cost = 0.06m;
            model.GenerateQuoteHeader.calc_25000_fzn_unit_cost = 0.06m;
            model.GenerateQuoteHeader.calc_50000_fzn_unit_cost = 0.05m;
            model.GenerateQuoteHeader.calc_100000_fzn_unit_cost = 0.05m;

            return new JsonResult() { Data = model };
        }

        [HttpPost]
        public ActionResult _CommentSalesOrder(Comment model)
        {
            //update model for view recalculation
            model.OrderVal = model.OrderVal;
            

            return new JsonResult() { Data = model };
        }

        [HttpPost]
        public ActionResult RecalcBasedOnComponentUpdate(ComponentFound Components, OriginalPricingTool model, GenerateQuoteHeader GenerateQuoteHeader)
        {

            decimal updated_comp_qty;
            updated_comp_qty = 0;

            updated_comp_qty = Components.ChangeQuantity;
            string part_num = Components.ComponentNum;
            //string cost_ref = GenerateQuoteHeader.CostYearDesc;
            string cost_ref ="07";

            List<ComponentFound> comp_list = new List<ComponentFound>();

            //recalculate component valuation
            using (OdbcConnection cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString))
            {
                cnn_odbc.Open();

                //if (model.GenerateQuoteHeader.totalCPQtyCostVar < 1)
                //{
                    int int_num = 1;
        string sqlScript = "";
        if (model.GenerateQuoteHeader.Division != "710000")
        {
          sqlScript = @"select	ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ixum, ibacq, ibstkt, ibltmf, ibltcm, imdsc1,  
                  dec(ixqnty / 1000000, 15, 6) as ixqnty, ixcpnb,  
                  (select sum(lipqoh) from proddta.f41021 where limcu = ixmmcu and liitm = ixitm) as pqoh,  
                  (select sum(lipqoh) - sum(lipcom) - sum(lihcom) - sum(lifcom) - sum(liqowo) from proddta.f41021 
                  where trim(limcu) = trim(ixmmcu) and liitm = ixitm) as avail, 
                  sum(dec(iexscr / 1000000, 15, 7)) as simCost
                , sum(dec(iecsl / 1000000, 15, 7)) as fznCost ,
                        coalesce(sum(case when iecost = 'A1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznA1,
                        coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB2,
						coalesce(sum(case when iecost = 'B1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB1,
						coalesce(sum(case when iecost = 'C3' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC3,
						coalesce(sum(case when iecost = 'C1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC1,
                        coalesce(sum(case when iecost = 'C4' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC4,
						coalesce(sum(case when iecost = 'C2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC2,
						coalesce(sum(case when left(iecost,1) = 'D' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznD0,
                        coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000*ibacq as decimal(15,7)) else 0 end),0) as fznSUTotal
                  from proddta.f3002 join proddta.f4102 on ixitm = ibitm and ixmmcu = ibmcu and left(ibglpt,2) in ('FG', 'CP') 
                  join proddta.f4101 on imitm = ibitm 
                  left join proddta.f30026 on iemmcu = ibmcu and ieitm = ibitm and ieledg = ?
                  where ixtbm = 'M' 
                and trim(ixkitl) = ? 
                  and trim(ixmmcu) = ? 
                  group by    ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ibacq, imdsc1, ixqnty, ibstkt, ixcpnb, ibltmf, ibltcm, ixum";
        }
        else
        {
          sqlScript = @"select	ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ixum, ibacq, ibstkt, ibltmf, ibltcm, imdsc1,  
                  dec(ixqnty / 1000000, 15, 6) as ixqnty, ixcpnb,  
                  (select sum(lipqoh) from proddta.f41021 where limcu = ixmmcu and liitm = ixitm) as pqoh,  
                  (select sum(lipqoh) - sum(lipcom) - sum(lihcom) - sum(lifcom) - sum(liqowo) from proddta.f41021 
                  where trim(limcu) = trim(ixmmcu) and liitm = ixitm) as avail, 
                  sum(dec(iexscr / 1000000, 15, 7)) as simCost
                , sum(dec(iecsl / 1000000, 15, 7)) as fznCost ,
                        coalesce(sum(case when left(iecost,1) = 'A' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simA1,
                        coalesce(sum(case when iecost = 'B2' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB2,
						coalesce(sum(case when iecost = 'B1' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB1,
						coalesce(sum(case when iecost = 'C3' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC3,
						coalesce(sum(case when iecost = 'C1' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC1,
                        coalesce(sum(case when iecost = 'C4' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC4,
						coalesce(sum(case when iecost = 'C2' and trim(iemcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC2,
						coalesce(sum(case when left(iecost,1) = 'D' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simD0,
                        coalesce(sum(case when iecost = 'B2' then cast(iexscr/1000000*#validatePN.ibacq# as decimal(15,7)) else 0 end),0) as simSUTotal
                  from proddta.f3002 join proddta.f4102 on ixitm = ibitm and ixmmcu = ibmcu and left(ibglpt,2) in ('FG', 'CP') 
                  join proddta.f4101 on imitm = ibitm 
                  left join proddta.f30026 on iemmcu = ibmcu and ieitm = ibitm and ieledg = ?
                  where ixtbm = 'M' 
                and trim(ixkitl) = ? 
                  and trim(ixmmcu) = ? 
                  group by    ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ibacq, imdsc1, ixqnty, ibstkt, ixcpnb, ibltmf, ibltcm, ixum";
        }
                    using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                    {
                        command.Parameters.Add(new OdbcParameter("@cost_year", model.GenerateQuoteHeader.CostReference));
                        command.Parameters.Add(new OdbcParameter("@item_num", part_num));
                        command.Parameters.Add(new OdbcParameter("@division", cost_ref));
                        //command.Parameters.Add(new OdbcParameter("@acq", updated_comp_qty));
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    ComponentFound item = new ComponentFound()
                                    {
                                        ItemKey = int_num,
                                        ComponentNum = reader.GetString(4), //ixitm
                                        ComponentUOM = reader.GetString(5), //ixum
                                        ComponentDesc = reader.GetString(10), //imdsc1
                                        ComponentStock = reader.GetString(7), //ibstkt
                                        ComponentACQ = updated_comp_qty,
                                        ComponentMfgLT = reader.GetDecimal(8), //ibltmf
                                        ComponentQty = reader.GetInt32(11), //ixqnty
                                        ChangeQuantity = reader.GetInt32(11), //ixqnty
                                        ComponentOnHand = reader.GetInt32(13), //pqoh
                                        ComponentAvailQt = reader.GetInt32(14), //avail
                                                                                //ComponentCost = reader.GetDecimal(15), //fzncost
                                                                                //ComponentCostFzn = reader.GetDecimal(16), //fzna1
                                                                                //ComponentCostFznRecalc = reader.GetDecimal(16), //fzna1
                                        ComponentCostFzn = reader.IsDBNull(16) ? 0 : (decimal)reader.GetValue(16),
                                        ComponentCostFznRecalc = reader.IsDBNull(16) ? 0 : (decimal)reader.GetValue(16),

                                        CompMtlCost = reader.GetDecimal(17), //fznb2
                                        CompLabor = reader.GetDecimal(18), //fznb1
                                        CompLbrVOH = reader.GetDecimal(19), //fznc3
                                        CompMchVOH = reader.GetDecimal(20), //fznc1
                                        CompLbrFOH = reader.GetDecimal(21), //fznc4
                                        CompMchFOH = reader.GetDecimal(22), //fznc2
                                        CompOSP = reader.GetDecimal(23), //fznd0
                                        CompSUTotal = reader.GetDecimal(24), //fznsutotal
                                        FznCPQtyCostSU = (reader.GetDecimal(24) / reader.GetDecimal(6)),
                                        FznCPQtyCostVar = reader.GetDecimal(17) + reader.GetDecimal(19) + reader.GetDecimal(21) + reader.GetDecimal(20) + reader.GetDecimal(24), //fzna1 fznb1 fznD0 fznc1 fznc3
                                        FznCPQtyCostFOH = ((reader.GetDecimal(22) + reader.GetDecimal(23)) * (reader.GetDecimal(6))) / reader.GetDecimal(6) //compmchfoh //composp //compacq //acq
                                                                                                                                                            //loop through?
                                    };

                                    if (Convert.ToDecimal(item.ChangeQuantity) > 0)
                                    {
                                        model.GenerateQuoteHeader.totalCPQtyCostVar = (item.FznCPQtyCostVar * item.ChangeQuantity) + model.GenerateQuoteHeader.totalCPQtyCostVar;
                                        model.GenerateQuoteHeader.totalCPQtyCostSU = (item.FznCPQtyCostSU * item.ChangeQuantity) + model.GenerateQuoteHeader.totalCPQtyCostSU;
                                        model.GenerateQuoteHeader.totalCPQtyCostFOH = (item.FznCPQtyCostFOH * item.ChangeQuantity) + model.GenerateQuoteHeader.totalCPQtyCostFOH;
                                    }
                                    else
                                    {
                                        model.GenerateQuoteHeader.totalCPQtyCostVar = (item.FznCPQtyCostVar) + model.GenerateQuoteHeader.totalCPQtyCostVar;
                                        model.GenerateQuoteHeader.totalCPQtyCostSU = (item.FznCPQtyCostSU) + model.GenerateQuoteHeader.totalCPQtyCostSU;
                                        model.GenerateQuoteHeader.totalCPQtyCostFOH = (item.FznCPQtyCostFOH) + model.GenerateQuoteHeader.totalCPQtyCostFOH;
                                    }
                                    int_num = int_num + 1;
                                    comp_list.Add(item);




                                }
                            }
                            reader.Close();
                        }
                    }
        
                cnn_odbc.Close();
                cnn_odbc.Dispose();
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult _CommentDeletion(Comment com, OriginalPricingTool model)
        {

            int comment_id=0;
            //string comment;
            //comment = com.PartComment;
            //List<Comment> comments = new List<Comment>;
            comment_id = com.PostID;
            using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
            {
                cnn.Open();
                string sqlScript = @"delete from comment_header where comment_id=@comment_id_val; 
                                     delete from comment_lines where comment_header_id = @comment_id_val;";

                using (var command = new SqlCommand(sqlScript, cnn))
                {
                    command.Parameters.Add(new SqlParameter("@comment_id_val", comment_id));
                    try
                    {
                        command.ExecuteNonQuery();
                        command.Dispose();
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show("Could not insert the new comment.");
                        return View();
                    }
                    finally
                    {
                        cnn.Close();
                        cnn.Dispose();
                    }
                }

            }
    
            return new HttpStatusCodeResult(HttpStatusCode.OK);

        }

        [HttpGet]
        public ActionResult PartDescriptionAnalysis(OriginalPricingTool model)
        {
            //var part_number_partial = Request.Params["PartNumber"];//model.GenerateQuoteHeader.PartNumber;

            if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
            {
                using (SqlConnection cnn_sql_con = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                {
                    cnn_sql_con.Open();
                    model.GenerateQuoteHeader.Costing = this.GetCostYearList(cnn_sql_con);
                }
            }
      try
      {
        List<PartDescription> pd = new List<PartDescription>();
        SharePointConnectionUpdated dc = new SharePointConnectionUpdated();

        pd = (from m in dc.sT_PartDetails_JDE
              where m.IBLITM.StartsWith(model.GenerateQuoteHeader.PartNumber)
              where m.IBLITM.Contains("*OP") == false
              orderby m.IBLITM
              select new PartDescription
              {
                PartID = m.ID,
                PartNumber = m.IBLITM,
                PartDesc = m.IMDSC1
              }).ToList();
        return View("PartDescriptionAnalysis", pd);



        //var ourModel = new OriginalPricingTool
        //{
        //  GenerateQuoteHeader = model.GenerateQuoteHeader,
        //  SalesOrders = null,
        //  Competitors = null,
        //  CustParts = null,
        //  Components = null,
        //  ActualCosts = null,
        //  Comments = null,
        //  Quotes = null,
        //  LTAs = null,
        //  LTAParts = null,
        //  QuoteWorkbenchList = null
        //};
        //return View(ourModel);
      }
      catch
      {
        //MessageBox.Show("failed to load 4.1");
        return View();
      }
        }

        [HttpPost]
        public ActionResult PartDescriptionAnalysis(PartDescription pd)
        {
      //var part_number_partial = Request.Params["PartPartial"];
                //var RefreshModel = new OriginalPricingTool
                //{
                //  GenerateQuoteHeader =null,
                //  SalesOrders = null,
                //  Competitors = null,
                //  Components = null,
                //  DemandList = null,
                //  Comments = null,
                //  LTAs = null,
                //  LTAParts = null
                  
                
      return View();
    }

        [HttpPost]
        public ActionResult _CommentsAddOrigSO(OriginalPricingTool model, SalesOrder SalesOrder, GenerateQuoteHeader GenerateQuoteHeader, Comment Comment)
        {

            //GenerateQuoteHeader header = new GenerateQuoteHeader();
            string so_num = SalesOrder.SOOrderNum;
            string part_num = GenerateQuoteHeader.PartNumber;
            string cust_part_num = GenerateQuoteHeader.CustomerPartNum;


            string comment_text;

            //if the comment entered was null
            if (model.NewComment.NewPartComment == null)
            {
                GenerateQuoteHeader gqh = new GenerateQuoteHeader();
                SqlConnection cnn;
                //gqh.LoadingPricingValue = 0;

                gqh.UnitCost = 0;
                gqh.UnitPrice = 0;

                if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                {
                    using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                    {
                        cnn.Open();
                        gqh.Costing = this.GetCostYearList(cnn);
                    }
                }
                var RefreshModel = new OriginalPricingTool
                {
                    GenerateQuoteHeader = gqh,
                    SalesOrders = null,
                    Competitors = null,
                    Components = null,
                    DemandList = null,
                    Comments = null,
                    LTAs = null,
                    LTAParts=null

                };
                return View("OriginalPricingTool", RefreshModel);
            }
            else
            {
                comment_text = model.NewComment.NewPartComment;

                if (model.GenerateQuoteHeader.PartNumber is null)
                {
                    string customer_part_num;
                    customer_part_num = model.GenerateQuoteHeader.CustomerPartNum;

                    using (OdbcConnection cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString))
                    {
                        cnn_odbc.Open();

                        string sqlScript;
                        sqlScript = @"select im.imdsc1,cr.ivcitm, case when im.imstkt in ('I','O') then 'Obselete' else im.imstkt end as imstkt, TRIM(im.imaitm) customer_part_num From PRODDTA.f4101 im 
                            left outer join(select cr_sub.ivan8, cr_sub.ivitm, cr_sub.ivcitm from PRODDTA.F4104 cr_sub where cr_sub.ivxrt = 'CR') cr on 
                            cr.ivitm = im.imitm 
                            where 1 = 1 
                            and TRIM(im.imaitm) =? ";
                        using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                        {
                            command.Parameters.Add(new OdbcParameter("@cust_item_num", customer_part_num));
                            using (var reader = command.ExecuteReader())
                            {
                                try
                                {
                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            model.GenerateQuoteHeader.PartNumber = reader.GetString(1);
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Item does not exist. Please try again.");
                                        cnn_odbc.Close();
                                        cnn_odbc.Dispose();
                                        GenerateQuoteHeader gqh = new GenerateQuoteHeader();
                                        SqlConnection cnn;
                                        //gqh.LoadingPricingValue = 0;

                                        gqh.UnitCost = 0;
                                        gqh.UnitPrice = 0;

                                        //SqlConnection cnn;
                                        if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                                        {
                                            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                                            {
                                                cnn.Open();
                                                gqh.Costing = this.GetCostYearList(cnn);
                                            }
                                        }
                                        var RefreshModel = new OriginalPricingTool
                                        {
                                            GenerateQuoteHeader = gqh,
                                            SalesOrders = null,
                                            Competitors = null,
                                            Components = null,
                                            DemandList = null,
                                            Comments = null,
                                            LTAs = null,
                                          LTAParts = null
                                        };
                                        return View(RefreshModel);
                                    }
                                }
                                catch
                                {
                                    MessageBox.Show("Item does not exist. Please try again.");
                                    MessageBox.Show("The connection is step 8: " + cnn_odbc.ToString());
                                    cnn_odbc.Close();
                                    cnn_odbc.Dispose();
                                    GenerateQuoteHeader gqh = new GenerateQuoteHeader();
                                    SqlConnection cnn;
                                    //gqh.LoadingPricingValue = 0;

                                    gqh.UnitCost = 0;
                                    gqh.UnitPrice = 0;

                                    if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                                    {
                                        using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                                        {
                                            cnn.Open();
                                            gqh.Costing = this.GetCostYearList(cnn);
                                        }
                                    }
                                    var RefreshModel = new OriginalPricingTool
                                    {
                                        GenerateQuoteHeader = gqh,
                                        SalesOrders = null,
                                        Competitors = null,
                                        Components = null,
                                        DemandList = null,
                                        Comments = null,
                                        LTAs = null,
                                      LTAParts = null
                                    };
                                    return View(RefreshModel);
                                }
                                //reader.Close();
                            }
                        }
                        cnn_odbc.Close();
                        cnn_odbc.Dispose();
                    }
                }
                else { part_num = model.GenerateQuoteHeader.PartNumber; }

                part_num = model.GenerateQuoteHeader.PartNumber;

                int user_id = 0;
                using (SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                {
                    cnn.Open();

                    string sqlScript = "select user_id from users where user_name=@user_name ";

                    using (var command = new SqlCommand(sqlScript, cnn))
                    {
                        command.Parameters.Add(new SqlParameter("@user_name", Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)));

                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                if (reader.Read())
                                {
                                    user_id = reader.GetInt32(0);
                                }
                            }
                            reader.Close();
                        }
                    }

                    sqlScript = @"insert into comment_header (created_by_user_id, creation_date,last_updated_date,QUOTE_PART_LINK,SALES_HIST_HEAD_LINK,active_flag) 
                            VALUES (@user_id,convert(date,getdate()),convert(date,getdate()),@item_num,@so_num,1); 
                            insert into comment_lines (comment_header_id,comment,created_by_user_id,creation_date,active_flag) 
                            values (SCOPE_IDENTITY(),@comment,@user_id,convert(date,getdate()),1);";

                    using (var command = new SqlCommand(sqlScript, cnn))
                    {

                        command.Parameters.Add(new SqlParameter("@user_id", user_id));
                        command.Parameters.Add(new SqlParameter("@so_num", so_num));
                        command.Parameters.Add(new SqlParameter("@item_num", part_num));
                        command.Parameters.Add(new SqlParameter("@comment", comment_text));

                        try
                        {
                            command.ExecuteNonQuery();
                            command.Dispose();
                        }

                        catch (Exception ex)
                        {
                            MessageBox.Show("Could not insert the new comment.");
                            return View();
                        }
                        finally
                        {
                            cnn.Close();
                            cnn.Dispose();
                        }
                    }


                    return new HttpStatusCodeResult(HttpStatusCode.OK);


                    //var ourModel = new OriginalPricingTool
                    //{
                    //    //GenerateQuoteHeader = header_model,
                    //    SalesOrders = null,
                    //    Competitors = null,
                    //    Components = null,
                    //    Comments = null,
                    //    LTAs = null,
                    //    //ActualCosts = null,
                    //    //QuoteWorkbench = null,
                    //    DemandList = null
                    //};
                    //return View("~/Views/Home/OriginalPricingTool.cshtml", ourModel);

                }



            }

        }

    [HttpPost]
    public ActionResult QuoteWorkbenchLineDelete(QuoteWorkBench QuoteWorkBench, UserModel users, OriginalPricingTool model, GenerateQuoteHeader GenerateQuoteHeader)
    {

      SqlConnection cnn;
      string sqlScript;

      cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
      cnn.Open();
      using (cnn)
      {
        sqlScript = "select user_id from users where user_name=@username ";
        SqlCommand command = new SqlCommand(sqlScript, cnn);
        SqlParameter username_val = new SqlParameter
        {
          ParameterName = "@username",
          Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
        };
        command.Parameters.Add(username_val);
        SqlDataReader reader = command.ExecuteReader();
        try
        {
          while (reader.Read())
          {
            users.user_id = reader.GetInt32(0);
          }
          reader.Close();
        }
        catch (Exception)
        {
          MessageBox.Show("No user found. Exiting.");
          return View();
        }
        reader.Close();
      }
      cnn.Close();

      int workbench_exist;
      int i = 0;
      workbench_exist = 0;
      int workbench_line_count = 0;


      //does workbench exist?
      cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
      cnn.Open();
      using (cnn)
      {
        i = 1;
        sqlScript = @"select w.workbench_id, isnull((select count(workbench_id) from workbench_lines l
				            where l.WORKBENCH_ID=w.WORKBENCH_ID),0) workbench_lines
				            From WORKBENCH_HEADER w left outer join users u ON w.created_by_user_id=u.user_id 
			              WHERE u.user_name=@username;";
        SqlCommand command = new SqlCommand(sqlScript, cnn);
        SqlParameter username_val = new SqlParameter
        {
          ParameterName = "@username",
          Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
        };
        command.Parameters.Add(username_val);
        SqlDataReader reader = command.ExecuteReader();

        try
        {
          while (reader.Read())
          {
            workbench_exist = reader.GetInt32(0);
            workbench_line_count = reader.GetInt32(1);
          }
          reader.Close();
        }
        catch (Exception)
        {
          MessageBox.Show("No workbench exists. Creating...");
          return View();
        }
        reader.Close();
      }
      cnn.Close();

      //if (workbench_exist < 1 && line_num > 0)
      
      if (workbench_exist != 0)
      {
        cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
        cnn.Open();
        workbench_line_count = workbench_line_count + 1;
        using (cnn)
        {
          i = 1;
          sqlScript = @"  delete from WORKBENCH_LINES  where WORKBENCH_ID=@workbench_id and WORKBENCH_LINE_NUM=@workbench_line_num ;";
          SqlCommand command = new SqlCommand(sqlScript, cnn);
          
          SqlParameter workbench_header_id = new SqlParameter
          {
            ParameterName = "@workbench_id",
            Value = workbench_exist
          };
          command.Parameters.Add(workbench_header_id);
          SqlParameter workbench_line_num_param = new SqlParameter
          {
            ParameterName = "@workbench_line_num",
            Value = QuoteWorkBench.QCLineNum
          };
          command.Parameters.Add(workbench_line_num_param);
          try
          {
            command.ExecuteNonQuery();
            command.Dispose();
          }

          catch (Exception ex)
          {
            //MessageBox.Show("Could not create workbench as the customer number or customer name may have been left blank. Please try agian or contact your sys admin." + ex.ToString());
            //return View();
          }
          cnn.Close();
        }


        
      }


      return new HttpStatusCodeResult(HttpStatusCode.OK);

    }

        




    }

}
    
