using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Mvc;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using RequestForQuoteSharePointWeb.Models;
using System.Data.Odbc;
using Microsoft.Ajax.Utilities;
using System.Web.Configuration;
using System.Web.WebPages;


//using System.Web.UI.WebControls;

namespace RequestForQuoteSharePointWeb.Controllers
{
  [Authorize]
  public class UploadController : BaseController
    {

        [HttpGet]
        public ActionResult UploadFile()
        {
            return View();
        }

    [HttpGet]
    public ActionResult UploadWorkbenchFile()
    {
      return View();
    }


    [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            SqlConnection cnn;

            cnn=new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            //cnn = new SqlConnection(UserInfo.connectionString);
            int division = 0;
            int user_id = 0;
            //string username = string.Empty;
            string startFileLocation;
            string endFileLocation;

            endFileLocation = @"\Downloads\";
            startFileLocation=@"C:\Users\";

            cnn.Open();
            using (cnn)
            {
                string sqlScript = @"select d.org_id, u.user_id from users u
                                    left outer join ORGANIZATION d on u.org_id = d.org_id
                                    where user_name = @username ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter user_param = new SqlParameter
                {
                    ParameterName = "@username",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(user_param);

                SqlDataReader reader;
                reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        division = reader.GetInt32(0);
                        user_id = reader.GetInt32(1);
                        //username = reader.GetString(2);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Could not find the user's information.");
                    return View();
                }
                reader.Close();

            }
            cnn.Close();

            string filePath = string.Empty;
            if (file != null)
            {
                //string path = Server.MapPath("~/Uploads/");

                if (Request.UserAgent.IndexOf("Edge") > -1)
                {
                    filePath = file.FileName.ToString();
                    
                    filePath=string.Concat(startFileLocation, Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1), endFileLocation,filePath);
                }
                else if (Request.UserAgent.IndexOf("Chrome") > -1)
                {
                    FileInfo filePath_test = new FileInfo(file.FileName.ToString());
                    //filePath = filePath_test.FullName;
                    filePath=string.Concat(startFileLocation, Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1), endFileLocation, file.FileName.ToString());
                }
                else
                {
                    filePath = file.FileName.ToString();
                }

                //string path = "C:/Users/bmount/Downloads/";

                //if (!Directory.Exists(path))
                //{
                //    Directory.CreateDirectory(path);
                //}

                //filePath = path + Path.GetFileName(file.FileName);
                string extension = Path.GetExtension(file.FileName);
                file.SaveAs(filePath);

                //Create a DataTable.
                DataTable dt = new DataTable();
               
                dt.Columns.AddRange(new DataColumn[10] {
                                //new DataColumn("Division_ID", typeof(int)),
                                new DataColumn("Customer_Number", typeof(int)),
                                new DataColumn("Item_Number_3",typeof(string)),
                                new DataColumn("Item_Number_2", typeof(string)),
                                new DataColumn("Type", typeof(string)),
                                new DataColumn("Effective_Start_Date", typeof(DateTime)),
                                new DataColumn("Effective_End_Date", typeof(DateTime)),
                                new DataColumn("Price", typeof(double)),
                                new DataColumn("UOM", typeof(string)),
                                new DataColumn("Customer_AMU", typeof(int)),
                                new DataColumn("Total_Forecast", typeof(int)) });
                       
                string csvData = System.IO.File.ReadAllText(filePath);

                //Execute a loop over the rows.
                foreach (string row in csvData.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        dt.Rows.Add();
                        int i = 0;

                        //Execute a loop over the columns.
                        foreach (string cell in row.Split(','))
                        {
                            dt.Rows[dt.Rows.Count-1][i] = cell;
                            i++;
                        }
                    }
                }

                string conString = ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(conString);

                using (con)
                {
                    con.Open();
                    string sqlScript = @"CREATE TABLE #temp_UploadLTA ([Customer_Number] [int] NULL,
	                            [Item_Number_3] [nvarchar](255) NULL,
	                            [Item_Number_2] [nvarchar](255) NULL,
	                            [Type] [nvarchar](255) NULL,
	                            [Effective_Start_Date] [datetime] NULL,
	                            [Effective_End_Date] [datetime] NULL,
	                            [Price] [decimal](18, 2) NOT NULL,
	                            [UOM] [nvarchar](255) NULL,
	                            [Customer_AMU] [int] NULL,
	                            [Total_Forecast] [int] NULL);";

                        string insert_sql = @"insert into iT_Sales_LTAUpload (Division_ID, Customer_Number, Item_Number_3, Item_Number_2, Type, Effective_Start_Date, Effective_End_Date
                                        , Price, UOM, Customer_AMU, Total_Forecast, Last_Updated_By, Last_Updated) select '730000', tp.*, 1, getdate() from #temp_UploadLTA tp;";
                        SqlCommand command = new SqlCommand(sqlScript, con);

                        try
                        {
                            command.ExecuteNonQuery();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {

                                try
                                {
                                    sqlBulkCopy.DestinationTableName = "dbo.#temp_UploadLTA";
                                    sqlBulkCopy.ColumnMappings.Add("Customer_Number", "Customer_Number");
                                    sqlBulkCopy.ColumnMappings.Add("Item_Number_3", "Item_Number_3");
                                    sqlBulkCopy.ColumnMappings.Add("Item_Number_2", "Item_Number_2");
                                    sqlBulkCopy.ColumnMappings.Add("Type", "Type");
                                    sqlBulkCopy.ColumnMappings.Add("Effective_Start_Date", "Effective_Start_Date");
                                    sqlBulkCopy.ColumnMappings.Add("Effective_End_Date", "Effective_End_Date");
                                    sqlBulkCopy.ColumnMappings.Add("Price", "Price");
                                    sqlBulkCopy.ColumnMappings.Add("UOM", "UOM");
                                    sqlBulkCopy.ColumnMappings.Add("Customer_AMU", "Customer_AMU");
                                    sqlBulkCopy.ColumnMappings.Add("Total_Forecast", "Total_Forecast");
                                    sqlBulkCopy.WriteToServer(dt);

                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("Failed to upload respective data extract.");
                                    con.Close();
                                    con.Dispose();
                                    return View();
                                }
                                finally
                                {
                                    SqlCommand cmd = new SqlCommand(insert_sql, con);
                                    try
                                    {
                                        cmd.ExecuteNonQuery();
                                        cmd.Dispose();
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show("Could not load information into main table.");
                                        con.Close();
                                        con.Dispose();
                                        //return View();
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Could not create temp table.");
                            return View();
                        }

                    //update all forecast data that has been entered as null set to 0, same with customer amu

                    sqlScript = @"update iT_Sales_LTAUpload set Customer_AMU=0 where Customer_AMU is null;
                                    update iT_Sales_LTAUpload set Total_Forecast=0 where Total_Forecast is null;";
                    command = new SqlCommand(sqlScript, con);
                    try
                    {
                        //    command.ExecuteNonQuery();
                        //    SqlCommand cmd = new SqlCommand(insert_sql, con);
                        //    try
                        //    {
                        command.ExecuteNonQuery();
                        command.Dispose();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Could not load information into main table.");
                            con.Close();
                            con.Dispose();
                            return View();
                        }

                    //}
                    //catch (Exception ex)
                    //{
                    //    MessageBox.Show("Could not update LTA load.");
                    //    con.Dispose();
                    //    con.Close();
                    //    return View();
                    //}

                    sqlScript = @"delete from iT_Sales_LTAUpload where Total_Forecast is null;";
                    command = new SqlCommand(sqlScript, con);
                    try
                    {
                        command.ExecuteNonQuery();
                                SqlCommand cmd = new SqlCommand(insert_sql, con);
                                try 
                                {
                            command.ExecuteNonQuery();
                            command.Dispose();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("Could not load information into main table.");
                                    con.Close();
                                    con.Dispose();
                            return View();
                                }
                        
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Could not create temp table.");
                        con.Dispose();
                        con.Close();
                        return View();
                    }
                    finally
                    {
                        con.Dispose();
                        con.Close();
                    }


                }
                    //}
            }

            //LTAModifications lta = new LTAModifications();

            //return new HttpStatusCodeResult(HttpStatusCode.OK);

            return View("~/Views/Home/DataManagementConsole.cshtml");

        }

        [HttpPost]
        public ActionResult UploadWorkbenchFile(HttpPostedFileBase file, GenerateQuoteHeader GenerateQuoteHeader)
        {
            SqlConnection cnn;

            cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
            //cnn = new SqlConnection(UserInfo.connectionString);
            int division = 0;
            int user_id = 0;
            int workbench_id = 0;
            //string username = string.Empty;
            string startFileLocation;
            string endFileLocation;
            string sqlScript = "";
            string sqlScripttext;
            string division_num = "";
            int qty_break = 0;
            decimal unit_price = 0;
            decimal margin = 0;

            //string test_upload_option = GenerateQuoteHeader.QuoteUploadMethod.ToString();

            //string upload_option_selected_text = UploadFile.QuoteUploadMethod;
            //bool upload_option_selected = Convert.ToBoolean(upload_option_selected_text);

            endFileLocation = @"\Downloads\";
            startFileLocation = @"C:\Users\";

            cnn.Open();
            using (cnn)
            {
                sqlScript = @"select d.org_id, u.user_id, h.workbench_id,d.division from users u
                                    left outer join ORGANIZATION d on u.org_id = d.org_id
                                      left outer join workbench_header h on u.[user_id]=h.created_by_user_id
                                    where user_name = @username ";
                SqlCommand command = new SqlCommand(sqlScript, cnn);
                SqlParameter user_param = new SqlParameter
                {
                    ParameterName = "@username",
                    Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                };
                command.Parameters.Add(user_param);

                SqlDataReader reader;
                reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        division = reader.GetInt32(0);
                        user_id = reader.GetInt32(1);
                        workbench_id = reader.GetInt32(2);
                        division_num = reader.GetString(3);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Could not find the user's information or your workbench doesn't exist.");
                    //return View();
                }
                reader.Close();

            }
            cnn.Close();

            if (workbench_id != 0)
            {
                string filePath = string.Empty;
                if (file != null)
                {
                    //string path = Server.MapPath("~/Uploads/");

                    if (Request.UserAgent.IndexOf("Edge") > -1)
                    {
                        filePath = file.FileName.ToString();

                        filePath = string.Concat(startFileLocation, Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1), endFileLocation, filePath);
                    }
                    else if (Request.UserAgent.IndexOf("Chrome") > -1)
                    {
                        FileInfo filePath_test = new FileInfo(file.FileName.ToString());
                        filePath = filePath_test.FullName;
                        //filePath = string.Concat(startFileLocation, Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1), endFileLocation, file.FileName.ToString());
                    }
                    else
                    {
                        filePath = file.FileName.ToString();
                    }


                    string extension = Path.GetExtension(file.FileName);
                    file.SaveAs(filePath);

                    //Create a DataTable.
                    DataTable dt = new DataTable();

                        dt.Columns.AddRange(new DataColumn[7] {
                                new DataColumn("Part_Number", typeof(string)),
                                new DataColumn("Cust_Part_Number",typeof(string)),
                                new DataColumn("Quantity", typeof(int)),
                                new DataColumn("Price", typeof(double)),
                                new DataColumn("Lead_Time",typeof(string)),
                                new DataColumn("Cost", typeof(double)),
                                new DataColumn("Cost_Ref",typeof(string))
                                 });

                        string csvData = System.IO.File.ReadAllText(filePath);

                        //Execute a loop over the rows.
                        foreach (string row in csvData.Split('\n'))
                        {
                            if (!string.IsNullOrEmpty(row))
                            {
                                dt.Rows.Add();
                                int i = 0;

                                //Execute a loop over the columns.
                                foreach (string cell in row.Split(','))
                                {
                                    dt.Rows[dt.Rows.Count - 1][i] = cell;
                                    i++;
                                }
                            }
                        }

                        string conString = ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString;
                        SqlConnection con = new SqlConnection(conString);
                        //con.Open();
                        using (con)
                        {
                            con.Open();
                            sqlScript = @"CREATE TABLE #temp_UploadWorkbench ([PART_NUMBER] [nvarchar](255) NULL,
	                            [CUST_PART_NUMBER] [nvarchar](255) NULL,
	                            [QUANTITY] [int] NOT NULL,
	                            [PRICE] [numeric](18,5) NOT NULL,
                                [LEAD_TIME] [nvarchar](255) NULL,
                                [COST] [numeric](18,5) NULL,
                                [COST_REF] [nvarchar](255) NULL);";

                            string insert_sql = @"insert into workbench_lines (WORKBENCH_LINE_NUM, WORKBENCH_ID, CREATION_DATE, LAST_UPDATED_DATE, PART_NUMBER, CUST_PART_NUMBER, LEAD_TIME
                                        , COMMENT, QUANTITY, UNIT_PRICE, UNIT_PRICE_CALCULATOR, UNIT_COST, COST_REF, COST_OVERRIDE, PRICING_TOOL_COST)  select ROW_NUMBER() OVER (ORDER BY PART_NUMBER) as WORKBENCH_LINE_NUM,@workbench_id, convert(Date,getdate())
                                         , convert(date,getdate()), tw.part_number , tw.cust_part_number, tw.lead_time,'',tw.quantity,tw.price,0
                                                                                    ,isnull((select top 1 a.unit_cost from (select uc.councs*.000001 unit_cost, uc.colitm item_num, 
                                                case when ltrim(uc.comcu) in ('Q720000','T720000') then '720000'
                                                when  ltrim(uc.comcu) in ('V4001'
                                                      ,'V4002'
                                                       ,'V4003'
                                                       ,'V4004'
                                                       ,'V4005'
                                                       ,'V4006'
                                                       ,'V4007') then '730000'
                                                when  ltrim(uc.comcu) in ('710V000') then '720000'
                                                when  ltrim(uc.comcu) in ('750015','750010','750020') then '750000'
                                                when  ltrim(uc.comcu) in ('780014','780015') then '780000'
                                                when  ltrim(uc.comcu) in ('Q710000','710V03','710V02','710V01') then '710000'
                                                else ltrim(uc.comcu) end division
                                                 from [DataWarehouse].[dbo].[iT_ItemCostFile_F4105] uc join organization o on o.division=(case when ltrim(uc.comcu) in ('Q720000','T720000') then '720000'
                                                when  ltrim(uc.comcu) in ('V4001'
                                                      ,'V4002'
                                                       ,'V4003'
                                                       ,'V4004'
                                                       ,'V4005'
                                                       ,'V4006'
                                                       ,'V4007') then '730000'
                                                when  ltrim(uc.comcu) in ('710V000') then '720000'
                                                when  ltrim(uc.comcu) in ('750015','750010','750020') then '750000'
                                                when  ltrim(uc.comcu) in ('780014','780015') then '780000'
                                                when  ltrim(uc.comcu) in ('Q710000','710V03','710V02','710V01') then '710000'
                                                else ltrim(uc.comcu) end)
                                                where 1=1 
                                            AND  (uc.councs*.000001)>0
                                                and uc.coledg='07' and  ltrim(uc.comcu)<>''  and o.org_id=@division_id) a 
												where a.item_num=tw.part_number ),convert(float,isnull(tw.cost,0))), tw.cost_ref,  isnull((select TOP 1 0 from (select uc.councs*.000001 unit_cost, uc.colitm item_num, 
                                                case when ltrim(uc.comcu) in ('Q720000','T720000') then '720000'
                                                when  ltrim(uc.comcu) in ('V4001'
                                                      ,'V4002'
                                                       ,'V4003'
                                                       ,'V4004'
                                                       ,'V4005'
                                                       ,'V4006'
                                                       ,'V4007') then '730000'
                                                when  ltrim(uc.comcu) in ('710V000') then '720000'
                                                when  ltrim(uc.comcu) in ('750015','750010','750020') then '750000'
                                                when  ltrim(uc.comcu) in ('780014','780015') then '780000'
                                                when  ltrim(uc.comcu) in ('Q710000','710V03','710V02','710V01') then '710000'
                                                else ltrim(uc.comcu) end division
                                                 from [DataWarehouse].[dbo].[iT_ItemCostFile_F4105] uc join organization o on o.division=(case when ltrim(uc.comcu) in ('Q720000','T720000') then '720000'
                                                when  ltrim(uc.comcu) in ('V4001'
                                                      ,'V4002'
                                                       ,'V4003'
                                                       ,'V4004'
                                                       ,'V4005'
                                                       ,'V4006'
                                                       ,'V4007') then '730000'
                                                when  ltrim(uc.comcu) in ('710V000') then '720000'
                                                when  ltrim(uc.comcu) in ('750015','750010','750020') then '750000'
                                                when  ltrim(uc.comcu) in ('780014','780015') then '780000'
                                                when  ltrim(uc.comcu) in ('Q710000','710V03','710V02','710V01') then '710000'
                                                else ltrim(uc.comcu) end)
                                                where 1=1 
                                            AND  (uc.councs*.000001)>0
                                                and uc.coledg='07' and  ltrim(uc.comcu)<>''  and o.org_id=@division_id) a 
												where a.item_num=tw.part_number ),1),0
                                        from   #temp_UploadWorkbench tw  ;";


                            SqlCommand command = new SqlCommand(sqlScript, con);
                            try
                            {
                                command.ExecuteNonQuery();
                                command.CommandTimeout = 0;
                                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                {
                                    //sqlBulkCopy.DestinationTableName = "dbo.#temp_UploadWorkbench";

                                    try
                                    {
                                        sqlBulkCopy.DestinationTableName = "dbo.#temp_UploadWorkbench";
                                        sqlBulkCopy.ColumnMappings.Add("PART_NUMBER", "PART_NUMBER");
                                        sqlBulkCopy.ColumnMappings.Add("CUST_PART_NUMBER", "CUST_PART_NUMBER");
                                        sqlBulkCopy.ColumnMappings.Add("QUANTITY", "QUANTITY");
                                        sqlBulkCopy.ColumnMappings.Add("PRICE", "PRICE");
                                        sqlBulkCopy.ColumnMappings.Add("LEAD_TIME", "LEAD_TIME");
                                        sqlBulkCopy.ColumnMappings.Add("COST", "COST");
                                        sqlBulkCopy.ColumnMappings.Add("COST_REF", "COST_REF");
                                        sqlBulkCopy.WriteToServer(dt);

                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message + ". Please contact your system administrator for assistance.");
                                        con.Close();
                                        con.Dispose();
                                        //return View();
                                    }
                                    finally
                                    {
                                        SqlCommand cmd = new SqlCommand(insert_sql, con);
                                        cmd.CommandTimeout = 0;
                                        SqlParameter division_param = new SqlParameter
                                        {
                                            ParameterName = "@division_id",
                                            Value = division
                                        };
                                        cmd.Parameters.Add(division_param);
                                        SqlParameter workbench_id_param = new SqlParameter
                                        {
                                            ParameterName = "@workbench_id",
                                            Value = workbench_id
                                        };
                                        cmd.Parameters.Add(workbench_id_param);
                                        try
                                        {
                                            cmd.ExecuteNonQuery();
                                            cmd.Dispose();
                                        }
                                        catch (Exception ex)
                                        {
                                            MessageBox.Show("Could not load information into main table." + ex.Message);
                                            con.Close();
                                            con.Dispose();
                                            //return View("~/Home/OriginalPricingTool");
                                        }

                                        con.Dispose();
                                        con.Close();


                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Could not create temp table." + ex.Message);
                                return View();
                            }



                        }
                        //}
                    }




                GenerateQuoteHeader gqh = new GenerateQuoteHeader();
                //gqh.LoadingPricingValue = 0;

                gqh.UnitCost = 0;
                gqh.UnitPrice = 0;

                //SqlConnection cnn;
                if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                {
                    using (SqlConnection cnn_again = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                    {
                        cnn_again.Open();
                        gqh.Costing = this.GetCostYearList(cnn_again);
                    }
                }
                var RefreshModel = new OriginalPricingTool
                {
                    GenerateQuoteHeader = gqh,
                    SalesOrders = null,
                    Competitors = null,
                    Components = null,
                    DemandList = null,
                    Comments = null,
                    LTAs = null,
                    LTAParts = null
                };

                //return new HttpStatusCodeResult(HttpStatusCode.NoContent);
                return Redirect(Request.UrlReferrer.ToString());

            }
            else
            {
                MessageBox.Show("Enter the header level workbench and one line item first before using this feature.");

                GenerateQuoteHeader gqh = new GenerateQuoteHeader();
                //gqh.LoadingPricingValue = 0;

                gqh.UnitCost = 0;
                gqh.UnitPrice = 0;

                //SqlConnection cnn;
                if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                {
                    using (SqlConnection cnn_again = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                    {
                        cnn_again.Open();
                        gqh.Costing = this.GetCostYearList(cnn_again);
                    }
                }
                var RefreshModel = new OriginalPricingTool
                {
                    GenerateQuoteHeader = gqh,
                    SalesOrders = null,
                    Competitors = null,
                    Components = null,
                    DemandList = null,
                    Comments = null,
                    LTAs = null,
                    LTAParts = null
                };
                //MessageBox.Show("About to refresh step 1.");
                //return View("~/Home/OriginalPricingTool.cshtml",RefreshModel);
                //return new HttpStatusCodeResult(HttpStatusCode.OK);
                return Redirect(Request.UrlReferrer.ToString());

                //return new HttpStatusCodeResult(HttpStatusCode.OK);

            }
        }

            [HttpPost]
            public ActionResult UploadWorkbenchFileNew(HttpPostedFileBase file, GenerateQuoteHeader GenerateQuoteHeader)
            {
                SqlConnection cnn;

                cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
                //cnn = new SqlConnection(UserInfo.connectionString);
                int division = 0;
                int user_id = 0;
                int workbench_id = 0;
                //string username = string.Empty;
                string startFileLocation;
                string endFileLocation;
                string sqlScript = "";
                string sqlScripttext;
                string division_num = "";
                int qty_break = 0;
                decimal unit_price = 0;
                decimal margin = 0;
                decimal adj_amount = 0;

                endFileLocation = @"\Downloads\";
                startFileLocation = @"C:\Users\";

                cnn.Open();
                using (cnn)
                {
                    sqlScript = @"select d.org_id, u.user_id, h.workbench_id,d.division from users u
                                    left outer join ORGANIZATION d on u.org_id = d.org_id
                                      left outer join workbench_header h on u.[user_id]=h.created_by_user_id
                                    where user_name = @username ";
                    SqlCommand command = new SqlCommand(sqlScript, cnn);
                    SqlParameter user_param = new SqlParameter
                    {
                        ParameterName = "@username",
                        Value = Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1)
                    };
                    command.Parameters.Add(user_param);

                    SqlDataReader reader;
                    reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            division = reader.GetInt32(0);
                            user_id = reader.GetInt32(1);
                            workbench_id = reader.GetInt32(2);
                            division_num = reader.GetString(3);
                        }
                        reader.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Could not find the user's information or your workbench doesn't exist.");
                        //return View();
                    }
                    reader.Close();

                }
                cnn.Close();

                if (workbench_id != 0)
                {
                    string filePath = string.Empty;
                    if (file != null)
                    {
                        //string path = Server.MapPath("~/Uploads/");

                        if (Request.UserAgent.IndexOf("Edge") > -1)
                        {
                            filePath = file.FileName.ToString();

                            filePath = string.Concat(startFileLocation, Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1), endFileLocation, filePath);
                        }
                        else if (Request.UserAgent.IndexOf("Chrome") > -1)
                        {
                            FileInfo filePath_test = new FileInfo(file.FileName.ToString());
                            filePath = filePath_test.FullName;
                           //filePath = string.Concat(startFileLocation, Request.LogonUserIdentity.Name.Substring(Request.LogonUserIdentity.Name.LastIndexOf(@"\") + 1), endFileLocation, file.FileName.ToString());
                        }
                        else
                        {
                            filePath = file.FileName.ToString();
                        }


                        string extension = Path.GetExtension(file.FileName);
                        file.SaveAs(filePath);

                        //Create a DataTable.
                        DataTable dt = new DataTable();
                        

                            dt.Columns.AddRange(new DataColumn[5] {
                               // new DataColumn("Part_Number", typeof(string)),
                                new DataColumn("Cust_Part_Number",typeof(string)),
                                new DataColumn("Quantity", typeof(int)),
                                new DataColumn("Lead_Time",typeof(string)),
                                new DataColumn("Cost", typeof(double)),
                                new DataColumn("Cost_Ref",typeof(string))
                                 });

                            string csvData = System.IO.File.ReadAllText(filePath);

                            //Execute a loop over the rows.
                            foreach (string row in csvData.Split('\n'))
                            {
                                if (!string.IsNullOrEmpty(row))
                                {
                                    dt.Rows.Add();
                                    int i = 0;

                                    //Execute a loop over the columns.
                                    foreach (string cell in row.Split(','))
                                    {
                                        dt.Rows[dt.Rows.Count - 1][i] = cell;
                                        i++;
                                    }
                                }
                            }

                            string conString = ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString;
                            SqlConnection con = new SqlConnection(conString);

                            using (con)
                            {
                                con.Open();
                        //[PART_NUMBER] [nvarchar](255) NULL,

                        sqlScripttext = @"CREATE TABLE #temp_UploadWorkbench (  [CUST_PART_NUMBER] [nvarchar](255) NULL,
	                            [QUANTITY] [int] NOT NULL,
                                [LEAD_TIME] [nvarchar](255) NULL,
                                [COST] [numeric](18,5) NULL,
                                [COST_REF] [nvarchar](255) NULL)
                                    ";


                      

                        string insert_sql = @"insert into workbench_lines (WORKBENCH_LINE_NUM, WORKBENCH_ID, CREATION_DATE
, LAST_UPDATED_DATE, PART_NUMBER, CUST_PART_NUMBER, LEAD_TIME
    , COMMENT, QUANTITY
	, UNIT_COST, PRICING_TOOL_COST, COST_REF, COST_OVERRIDE
	, UNIT_PRICE, UNIT_PRICE_CALCULATOR) 
    select ROW_NUMBER() OVER (ORDER BY CUST_PART_NUMBER) as WORKBENCH_LINE_NUM,@workbench_id, convert(Date,getdate())
        , convert(date,getdate()), isnull(isnull((select top 1 iblitm 
        from [DataWarehouse].[dbo].[iT_ItemBranchFile_F4102] itm where 1=1 and ltrim(itm.ibmcu)<>'Q710000'
		and itm.ibaitm=rtrim(ltrim(tw.cust_part_number)) and ltrim(itm.ibmcu)=@division_num)
		,(select top 1 iblitm from [DataWarehouse].[dbo].[iT_ItemBranchFile_F4102] itm 
		where 1=1 and itm.iblitm=tw.cust_part_number and ltrim(itm.ibmcu)=@division_num and ltrim(itm.ibmcu)<>'Q710000')),'NEW ITEM QUOTE')
            ,rtrim(ltrim(tw.cust_part_number))
			, tw.lead_time,'',tw.quantity
                                                ,isnull((select a.unit_cost from 
												(select top 1 uc.councs*.000001 unit_cost, uc.colitm item_num 
            ,ltrim(uc.comcu)  division
                from [DataWarehouse].[dbo].[iT_ItemCostFile_F4105] uc join organization o 
				on o.division=ltrim(uc.comcu)
            where 1=1 
    and o.division=@division_num
    and ltrim(comcu)<>'Q710000'
        AND  (uc.councs*.000001)>0
            and uc.coledg='07' and  ltrim(uc.comcu)<>''  and o.org_id=@division_id) a 
			where a.item_num=isnull(isnull((select top 1 iblitm from [DataWarehouse].[dbo].[iT_ItemBranchFile_F4102] itm 
			where 1=1 and itm.ibaitm=tw.cust_part_number and ltrim(itm.ibmcu)=@division_num),(select top 1 iblitm 
			from [DataWarehouse].[dbo].[iT_ItemBranchFile_F4102] itm where 1=1 and ltrim(itm.ibmcu)<>'Q710000'
			and itm.iblitm=tw.cust_part_number and ltrim(itm.ibmcu)=@division_num)),'NEW ITEM QUOTE') )
			,convert(float,isnull(tw.cost,0))) 
			,isnull((select top 1 unit_cost from (select uc.councs*.000001 unit_cost, uc.colitm item_num, 
            ltrim(uc.comcu) division
                from [DataWarehouse].[dbo].[iT_ItemCostFile_F4105] uc join organization o 
				on o.division=(ltrim(uc.comcu))
            where 1=1 
 and ltrim(comcu)<>'Q710000'
and o.division=@division_num
        AND  (uc.councs*.000001)>0
            and uc.coledg='07' and  ltrim(uc.comcu)<>''  and o.org_id=@division_id) a 
			where a.item_num=isnull(isnull(isnull((select top 1 iblitm from [DataWarehouse].[dbo].[iT_ItemBranchFile_F4102] itm 
			where 1=1 and itm.ibmcu<>'Q710000' and itm.ibaitm=tw.cust_part_number  and ltrim(itm.ibmcu)=@division_num)
			,(select top 1  iblitm from [DataWarehouse].[dbo].[iT_ItemBranchFile_F4102] itm where 1=1 and ltrim(itm.ibmcu)<>'Q710000'
			and itm.iblitm=tw.cust_part_number and ltrim(itm.ibmcu)=@division_num)) 
			,CONCAT('Z-',(select top 1 iblitm from [DataWarehouse].[dbo].[iT_ItemBranchFile_F4102] itm 
			where 1=1 and itm.ibaitm=tw.cust_part_number and ltrim(itm.ibmcu)=@division_num))),'NEW ITEM QUOTE')),0)
			,tw.cost_ref,0,0,0
    from   #temp_UploadWorkbench tw ;";


                        SqlCommand command = new SqlCommand(sqlScripttext, con);
                                try
                                {
                                    command.ExecuteNonQuery();
                                    command.CommandTimeout = 0;
                                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                                    {
                                        //sqlBulkCopy.DestinationTableName = "dbo.#temp_UploadWorkbench";

                                        try
                                        {
                                            sqlBulkCopy.DestinationTableName = "dbo.#temp_UploadWorkbench";
                                            sqlBulkCopy.ColumnMappings.Add("CUST_PART_NUMBER", "CUST_PART_NUMBER");
                                            sqlBulkCopy.ColumnMappings.Add("QUANTITY", "QUANTITY");
                                            sqlBulkCopy.ColumnMappings.Add("LEAD_TIME", "LEAD_TIME");
                                            sqlBulkCopy.ColumnMappings.Add("COST", "COST");
                                            sqlBulkCopy.ColumnMappings.Add("COST_REF", "COST_REF");
                                            sqlBulkCopy.WriteToServer(dt);

                                        }
                                        catch (Exception ex)
                                        {
                                            MessageBox.Show(ex.Message + ". Please contact your system administrator for assistance.");
                                            con.Close();
                                            con.Dispose();
                                            //return View();
                                        }
                                        finally
                                        {
                                            SqlCommand cmd = new SqlCommand(insert_sql, con);
                                            cmd.CommandTimeout = 0;
                                            SqlParameter division_param = new SqlParameter
                                            {
                                                ParameterName = "@division_id",
                                                Value = division
                                            };
                                            cmd.Parameters.Add(division_param);
                                            SqlParameter workbench_id_param = new SqlParameter
                                            {
                                                ParameterName = "@workbench_id",
                                                Value = workbench_id
                                            };
                                            cmd.Parameters.Add(workbench_id_param);
                                    SqlParameter div_num_param = new SqlParameter
                                    {
                                        ParameterName = "@division_num",
                                        Value = division_num
                                    };
                                    cmd.Parameters.Add(div_num_param);
                                    try
                                            {
                                                cmd.ExecuteNonQuery();
                                                cmd.Dispose();
                                            }
                                            catch (Exception ex)
                                            {
                                                MessageBox.Show("Could not load information into main table." + ex.Message);
                                                con.Close();
                                                con.Dispose();

                                            }




                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("Could not create temp table." + ex.Message);
                                    return View();
                                }
                            }

                            //update workbench lines with the pricing found

                            List<ComponentFound> Components = new List<ComponentFound>();
                            List<UploadWorkbenchPricing> workbench_items = new List<UploadWorkbenchPricing>();

                            //fill workbench_items for auto-pricing

                            sqlScripttext = @"select part_number, quantity From workbench_lines where workbench_id=@workbench_id";

                    //SqlConnection cnn;
                    con = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
                    con.Open();

                    using (var command = new SqlCommand(sqlScripttext, con))
                        {
                            command.Parameters.Add(new SqlParameter("@workbench_id", workbench_id));

                            using (var reader = command.ExecuteReader())

                            {
                                try
                                {
                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            UploadWorkbenchPricing item = new UploadWorkbenchPricing()
                                            {
                                                item_num = reader.GetString(0),
                                                qty_break = reader.GetInt32(1)
                                            };
                                            workbench_items.Add(item);


                                            
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("Error was: " + ex.Message);
                                }
                            finally
                            {
                                reader.Close();
                            }

                            }
                        }


                        //now go get the item id of the fg inquirying about




                        foreach (var item_var in workbench_items)
                        {



                        con = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString);
                        
                        con.Open();
                        using (con)
                        { 

                        if (division_num != "750000" || division_num != "780000")
                            {
                                sqlScript = @"select top 1 pd.ibmcu, pd.IBACQ ACQ
                        , pd.ibltmf MfgLT
                        , pd.IBLTCM CumLT
                        , pd.QOH
                        , pd.PRODUCT_CODE
                        , pd.PRODUCT_GROUP
                        , pd.IBSRP5 ValueStream
                        , pd.IBSRP1 ProductType   
                        , convert(int,ltrim(pd.ibitm)) ibitm
                        , pd.IBMERL PartRevision
                        From sT_PartDetails_JDE  pd
                            where pd.iblitm=@item_num and pd.ibmcu=@division";
                            }
                            else
                            {
                                sqlScript = @"select top 1 ibmcu, IBACQ ACQ
                        , ibltmf MfgLT
                        , pd.IBLTCM CumLT
                        , pd.QOH
                        , pd.PRODUCT_CODE
                        , pd.PRODUCT_GROUP
                        , pd.IBSRP2 ValueStream
                        , pd.IBSRP1 ProductType   
                        , convert(int,ltrim(pd.ibitm)) ibitm
                        , pd.IBMERL PartRevision
                        From sT_PartDetails_JDE pd 
						where pd.iblitm=@item_num and pd.ibmcu=@division";
                            }
                            using (var commandx = new SqlCommand(sqlScript, con))
                            {

                                SqlParameter div = new SqlParameter
                                {
                                    ParameterName = "@division",
                                    Value = division_num
                                };
                                commandx.Parameters.Add(div);
                                SqlParameter item_num_param = new SqlParameter
                                {
                                    ParameterName = "@item_num",
                                    Value = item_var.item_num
                                };
                                commandx.Parameters.Add(item_num_param);

                                using (var reader = commandx.ExecuteReader())
                                {
                                    try
                                    {
                                        if (reader.HasRows)
                                        {
                                            while (reader.Read())
                                            {
                                                GenerateQuoteHeader.ACQ = Convert.ToInt32(reader.GetDecimal(1));
                                                GenerateQuoteHeader.ProductGroup = reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                                                GenerateQuoteHeader.ValueStream = reader.IsDBNull(7) ? "" : (string)reader.GetValue(7);
                                                GenerateQuoteHeader.ProductType = reader.IsDBNull(8) ? "" : (string)reader.GetValue(8);
                                                GenerateQuoteHeader.ibitm = reader.GetInt32(9);
                                                GenerateQuoteHeader.PartRevision = reader.IsDBNull(10) ? "" : (string)reader.GetValue(10);
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Could not find. Error.");
                                        }
                                        reader.Close();
                                    }
                                    catch
                                    {

                                        //do nothing

                                    }
                                    finally
                                    {
                                        cnn.Close();
                                        cnn.Dispose();
                                    }
                                }
                            }
                        }

                        
                    

                                //now go get the component costs for Bristol if applicable
                                using (OdbcConnection cnn_odbc = new OdbcConnection(ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString))
                                {

                                    cnn_odbc.Open();
                                    if (cnn_odbc == null || cnn_odbc.State == ConnectionState.Closed)
                                    {

                                        cnn_odbc.Open();
                                    }

                                    int int_num = 1;

                                    GenerateQuoteHeader.totalCPQtyCostVar = 0;
                                    GenerateQuoteHeader.totalCPQtyCostSU = 0;
                                    GenerateQuoteHeader.totalCPQtyCostFOH = 0;

                                    if (division_num == "730000" || division_num == "734000")
                                    {
                                        sqlScript = @"select	ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ixum, ibacq, ibstkt, ibltmf, ibltcm, imdsc1,  
                  dec(ixqnty / 1000000, 15, 6) as ixqnty, ixcpnb,  
                  (select sum(lipqoh) from proddta.f41021 where limcu = ixmmcu and liitm = ixitm) as pqoh,  
                  (select sum(lipqoh) - sum(lipcom) - sum(lihcom) - sum(lifcom) - sum(liqowo) from proddta.f41021 
                  where limcu = ixmmcu and liitm = ixitm) as avail, 
                  sum(dec(iexscr / 1000000, 15, 7)) as simCost
                , sum(dec(iecsl / 1000000, 15, 7)) as fznCost ,
                        coalesce(sum(case when iecost = 'A1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznA1,
                        coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB2,
						coalesce(sum(case when iecost = 'B1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB1,
						coalesce(sum(case when iecost = 'C3' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC3,
						coalesce(sum(case when iecost = 'C1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC1,
                        coalesce(sum(case when iecost = 'C4' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC4,
						coalesce(sum(case when iecost = 'C2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC2,
						coalesce(sum(case when left(iecost,1) = 'D' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznD0,
                        coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000*ibacq as decimal(15,7)) else 0 end),0) as fznSUTotal
                  from proddta.f3002 join proddta.f4102 on ixitm = ibitm and ixmmcu = ibmcu and left(ibglpt,2) in ('FG', 'CP') 
                  join proddta.f4101 on imitm = ibitm 
                  left join proddta.f30026 on iemmcu = ibmcu and ieitm = ibitm and ieledg = ?
                  where ixtbm in ('M')
                and ixkit = ? 
                  and trim(ixmmcu) = ? 
                  group by    ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ibacq, imdsc1, ixqnty, ibstkt, ixcpnb, ibltmf, ibltcm, ixum";
                                    }
                                    else if (division_num == "710000")
                                    {
                                        sqlScript = @"select	ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ixum, ibacq, ibstkt, ibltmf, ibltcm, imdsc1,  
                  dec(ixqnty / 1000000, 15, 6) as ixqnty, ixcpnb,  
                  (select sum(lipqoh) from proddta.f41021 where limcu = ixmmcu and liitm = ixitm) as pqoh,  
                  (select sum(lipqoh) - sum(lipcom) - sum(lihcom) - sum(lifcom) - sum(liqowo) from proddta.f41021 
                  where limcu = ixmmcu and liitm = ixitm) as avail, 
                  sum(dec(iexscr / 1000000, 15, 7)) as simCost
                , sum(dec(iecsl / 1000000, 15, 7)) as fznCost ,
                        coalesce(sum(case when left(iecost,1) = 'A' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simA1,
                        coalesce(sum(case when iecost = 'B2' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB2,
						coalesce(sum(case when iecost = 'B1' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB1,
						coalesce(sum(case when iecost = 'C3' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC3,
						coalesce(sum(case when iecost = 'C1' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC1,
                       coalesce(sum(case when iecost = 'C4' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC4,
						coalesce(sum(case when iecost = 'C2' and trim(iemmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC2,
						coalesce(sum(case when left(iecost,1) = 'D' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simD0,
                        coalesce(sum(case when iecost = 'B2' then cast(iexscr/1000000*ibacq as decimal(15,7)) else 0 end),0) simSUTotal
                  from proddta.f3002 join proddta.f4102 on ixitm = ibitm and ixmmcu = ibmcu and left(ibglpt,2) in ('FG', 'CP', 'RM') 
                  join proddta.f4101 on imitm = ibitm 
                  left join proddta.f30026 on iemmcu = ibmcu and ieitm = ibitm and ieledg = ?
                  where ixtbm in ('M')
                and ixkit = ? 
                  and trim(ixmmcu) = ? 
                  group by    ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ibacq, imdsc1, ixqnty, ibstkt, ixcpnb, ibltmf, ibltcm, ixum";
                                    }
                                    else
                                    {
                                        sqlScript = @"select	ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ixum, case when trim(ixmmcu)='780000' then case when ibacq=0 then 1 else ibacq end else ibacq end as ibacq, ibstkt, ibltmf, ibltcm,imdsc1,
                  CAST(dec(ixqnty / 1000000, 15, 6) AS INTEGER) as ixqnty, ixcpnb,  
                  ifnull((select sum(lipqoh) from proddta.f41021 where limcu = ixmmcu and liitm = ixitm),0) as pqoh,  
                  ifnull((select sum(lipqoh) - sum(lipcom) - sum(lihcom) - sum(lifcom) - sum(liqowo) from proddta.f41021 
                  where limcu = ixmmcu and liitm = ixitm),0) as avail, 
                  sum(dec(iexscr / 1000000, 15, 7)) as simCost
                , sum(dec(iecsl / 1000000, 15, 7)) as fznCost ,
                        coalesce(sum(case when iecost = 'A1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznA1,
                        coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB2,
						coalesce(sum(case when iecost = 'B1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznB1,
						coalesce(sum(case when iecost = 'C3' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC3,
						coalesce(sum(case when iecost = 'C1' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC1,
                        coalesce(sum(case when iecost = 'C4' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC4,
						coalesce(sum(case when iecost = 'C2' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznC2,
						coalesce(sum(case when left(iecost,1) = 'D' then cast(iecsl/1000000 as decimal(15,7)) else 0 end),0) as fznD0,
                        coalesce(sum(case when iecost = 'B2' then cast(iecsl/1000000*ibacq as decimal(15,7)) else 0 end),0) as fznSUTotal
                  from proddta.f3002 join proddta.f4102 on ixitm = ibitm and ixmmcu = ibmcu and left(ibglpt,2) in ('FG', 'CP', 'RM') 
                  join proddta.f4101 on imitm = ibitm 
                  left join proddta.f30026 on iemmcu = ibmcu and ieitm = ibitm and ieledg = ?
                  where ixtbm in ('M')
                and ixkit = ? 
                  and trim(ixmmcu) = ? 
                  group by    ixmmcu, ixkit, ixkitl, ixitm, ixlitm, ibacq, imdsc1, ixqnty, ibstkt, ixcpnb, ibltmf, ibltcm, ixum";
                                    }
                                    //and dec(ixqnty / 1000000, 15, 6)>=1
                                    using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                                    {
                                        command.Parameters.Add(new OdbcParameter("@cost_year", "07")); //only current standard cost year
                                        command.Parameters.Add(new OdbcParameter("@item_num", GenerateQuoteHeader.ibitm));
                                        command.Parameters.Add(new OdbcParameter("@division", division_num));

                                        using (var reader = command.ExecuteReader())
                                        {
                                            if (reader.HasRows)
                                            {
                                                while (reader.Read())
                                                {
                                                    ComponentFound item = new ComponentFound()
                                                    {
                                                        //reader.IsDBNull(6) ? "" : (string)reader.GetValue(6);
                                                        //ItemKey = int_num,
                                                        ComponentNum = reader.GetString(4), //ixitm
                                                        ComponentUOM = reader.GetString(5), //ixum
                                                        ComponentDesc = reader.GetString(10), //imdsc1
                                                        ComponentStock = reader.GetString(7), //ibstkt
                                                        ComponentACQ = reader.IsDBNull(6) ? 0 : (decimal)reader.GetValue(6),
                                                        ComponentMfgLT = reader.IsDBNull(8) ? 0 : (decimal)reader.GetValue(8),
                                                        ComponentQty = reader.GetInt32(11),
                                                        ChangeQuantity = reader.GetInt32(11),
                                                        ComponentOnHand = reader.GetInt32(13),
                                                        ComponentAvailQt = reader.GetInt32(14),
                                                        ComponentCostFzn = reader.IsDBNull(16) ? 0 : (decimal)reader.GetValue(16),
                                                        CompMtlCost = reader.IsDBNull(17) ? 0 : (decimal)reader.GetValue(17),
                                                        CompSetup = reader.IsDBNull(18) ? 0 : (decimal)reader.GetValue(18),
                                                        CompLabor = reader.IsDBNull(19) ? 0 : (decimal)reader.GetValue(19),
                                                        CompLbrVOH = reader.IsDBNull(20) ? 0 : (decimal)reader.GetValue(20),
                                                        CompMchVOH = reader.IsDBNull(21) ? 0 : (decimal)reader.GetValue(21),
                                                        CompLbrFOH = reader.IsDBNull(22) ? 0 : (decimal)reader.GetValue(22),
                                                        CompMchFOH = reader.IsDBNull(23) ? 0 : (decimal)reader.GetValue(23),
                                                        CompOSP = reader.IsDBNull(24) ? 0 : (decimal)reader.GetValue(24),
                                                        CompSUTotal = reader.IsDBNull(25) ? 0 : (decimal)reader.GetValue(25),
                                                        FznCPQtyCostSU = Math.Round((reader.IsDBNull(25) ? 0 : (decimal)reader.GetValue(25)), 5) / (reader.IsDBNull(6) ? 0 : (decimal)reader.GetValue(6)),
                                                        FznCPQtyCostVar = (reader.IsDBNull(17) ? 0 : (decimal)reader.GetValue(17)) + (reader.IsDBNull(19) ? 0 : (decimal)reader.GetValue(19)) + (reader.IsDBNull(20) ? 0 : (decimal)reader.GetValue(20)) + (reader.IsDBNull(21) ? 0 : (decimal)reader.GetValue(21)) + (reader.IsDBNull(24) ? 0 : (decimal)reader.GetValue(24)), //fzna1 fznb1 fznD0 fznc1 fznc3
                                                        FznCPQtyCostFOH = (((reader.IsDBNull(23) ? 0 : (decimal)reader.GetValue(23)) + (reader.IsDBNull(22) ? 0 : (decimal)reader.GetValue(22))) * (reader.IsDBNull(6) ? 0 : (decimal)reader.GetValue(6))) / (reader.IsDBNull(6) ? 0 : (decimal)reader.GetValue(6))
                                                        //loop through?
                                                    };

                                                    if (Convert.ToDecimal(item.ChangeQuantity) > 0)
                                                    {
                                                        string test_compNum = item.ComponentNum;
                                                        int test_compqty = item.ComponentQty;// = reader.GetInt32(8), //ixqnty
                                                        int test_availqt = item.ComponentAvailQt; //= reader.GetInt32(11), //avail
                                                        decimal test_CostFzn = item.ComponentCostFzn; //= reader.GetDecimal(12), //fzna1
                                                        decimal test_FznRecalc = item.ComponentCostFznRecalc; //= reader.GetDecimal(12), //fzna1
                                                        decimal test_MtlCost = item.CompMtlCost; //= reader.GetDecimal(14), //fznb2
                                                        decimal test_ComLabor = item.CompLabor; //= reader.GetDecimal(13), //fzna1
                                                        decimal test_ComSetup = item.CompSetup;
                                                        decimal test_CompLbrVOH = item.CompLbrVOH;
                                                        decimal test_CompMchVOH = item.CompMchVOH;
                                                        decimal test_CompLbrFOH = item.CompLbrFOH;
                                                        decimal test_CompMchFOH = item.CompMchFOH;
                                                        decimal test_CompOSP = item.CompOSP; // = reader.GetDecimal(20), //fznd0
                                                        decimal test_compSU = item.CompSUTotal; //= reader.GetDecimal(21), //fznsutotal
                                                        GenerateQuoteHeader.totalCPQtyCostVar = (item.FznCPQtyCostVar * item.ChangeQuantity) + GenerateQuoteHeader.totalCPQtyCostVar;
                                                        GenerateQuoteHeader.totalCPQtyCostSU = (item.FznCPQtyCostSU * item.ChangeQuantity) + GenerateQuoteHeader.totalCPQtyCostSU;
                                                        GenerateQuoteHeader.totalCPQtyCostFOH = (item.FznCPQtyCostFOH * item.ChangeQuantity) + GenerateQuoteHeader.totalCPQtyCostFOH;

                                                    }
                                                    else
                                                    {
                                                        if (item.FznCPQtyCostVar != 0 && item.FznCPQtyCostSU != 0)
                                                        {
                                                            GenerateQuoteHeader.totalCPQtyCostVar = (item.FznCPQtyCostVar) + GenerateQuoteHeader.totalCPQtyCostVar;
                                                            GenerateQuoteHeader.totalCPQtyCostSU = (item.FznCPQtyCostSU) + GenerateQuoteHeader.totalCPQtyCostSU;
                                                            GenerateQuoteHeader.totalCPQtyCostFOH = (item.FznCPQtyCostFOH) + GenerateQuoteHeader.totalCPQtyCostFOH;
                                                        }
                                                        else
                                                        {
                                                            GenerateQuoteHeader.totalCPQtyCostVar = 0 + GenerateQuoteHeader.totalCPQtyCostVar;
                                                            GenerateQuoteHeader.totalCPQtyCostSU = 0 + GenerateQuoteHeader.totalCPQtyCostSU;
                                                            GenerateQuoteHeader.totalCPQtyCostFOH = 0 + GenerateQuoteHeader.totalCPQtyCostFOH;
                                                        }
                                                    }
                                                    int_num = int_num + 1;
                                                    Components.Add(item);
                                                }
                                            }

                                            else
                                            {
                                                foreach (var item in Components)
                                                {
                                                    GenerateQuoteHeader.totalCPQtyCostVar = 0;
                                                    GenerateQuoteHeader.totalCPQtyCostSU = 0;
                                                    GenerateQuoteHeader.totalCPQtyCostFOH = 0;
                                                    GenerateQuoteHeader.totalCPQtyCostVar = (item.FznCPQtyCostVar * item.ChangeQuantity) + GenerateQuoteHeader.totalCPQtyCostVar;
                                                    GenerateQuoteHeader.totalCPQtyCostSU = (item.FznCPQtyCostSU * item.ChangeQuantity) + GenerateQuoteHeader.totalCPQtyCostSU;
                                                    GenerateQuoteHeader.totalCPQtyCostFOH = (item.FznCPQtyCostFOH * item.ChangeQuantity) + GenerateQuoteHeader.totalCPQtyCostFOH;

                                                }
                                            }
                                            reader.Close();
                                        }
                                    }

                                    decimal fznMatl; //fzna1
                                    decimal fznLbr; //fznb1
                                    decimal fznb2;
                                    decimal fznc1;
                                    decimal fznc2;
                                    decimal fznc3;
                                    decimal fznc4;
                                    decimal fznOS; //fznd0
                                    decimal fznlc;
                                    decimal fznSUTotal;
                                    decimal fznLCTotal;

                                    if (division_num=="710000")
                            {
                                sqlScript = @"select coalesce(sum(case when left(iecost,1) = 'A' and trim(ieMmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simA1,
						coalesce(sum(case when iecost = 'B1' and trim(ieMmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB1,
						coalesce(sum(case when iecost = 'B2' and trim(ieMmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simB2,
						coalesce(sum(case when iecost = 'C1' and trim(ieMmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC1,
						coalesce(sum(case when iecost = 'C2' and trim(ieMmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC2,
						coalesce(sum(case when iecost = 'C3' and trim(ieMmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC3,
						coalesce(sum(case when iecost = 'C4' and trim(ieMmcu) <> 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simC4,
                                         coalesce(sum(case when left(iecost,1) = 'D' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simD0,
                                         coalesce(sum(case when trim(ieMmcu) = 'V78LOT001' then cast(iexscr*.000001 as decimal(15,7)) else 0 end),0) as simLC,
                                         coalesce(sum(case when iecost = 'B2' then cast(iexscr/1000000*CAST(? AS INTEGER) as decimal(15,7)) else 0 end),0) 
							as simSUTotal, 
                                        decimal(coalesce( sum(case when iecost = 'A1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznA1Net,
                         decimal(coalesce( sum(case when iecost = 'B1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB1Net,
                         decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB2Net,
                         decimal(coalesce( sum(case when iecost = 'C1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC1Net,
                         decimal(coalesce( sum(case when iecost = 'C2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC2Net,
                         decimal(coalesce( sum(case when iecost = 'C3' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC3Net,
                         decimal(coalesce( sum(case when iecost = 'C4' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC4Net,
                         decimal(coalesce( sum(case when left(iecost,1) = 'D' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznD0Net,
                         decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 * CAST(? AS INTEGER) as decimal(15,5)) else 0 end),0),10,5) 
                          as fznSUTotalNet , coalesce(sum(case when trim(iemMcu) = 'V78LOT001' then cast(iexscr/1000000*CAST(? as INTEGER) as decimal(15,7)) else 0 end),0) simLCTotal, 
                         decimal(coalesce( sum(case when iecost = 'A2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznA2Net,
                            decimal(coalesce( sum(case when iecost = 'D3' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznD3Net
                                            from proddta.f30026
                                            where ieledg = ?
                                            and trim(iemmcu)=?
                                            and ieitm = ?";
                            }
                                    
                                    else
                                    {
                                        sqlScript = @"select decimal(coalesce(sum(case when iecost = 'A1'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznA1, 
                                         decimal(coalesce(sum(case when iecost = 'B1'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznB1, 
                                         decimal(coalesce(sum(case when iecost = 'B2'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznB2, 
                                         decimal(coalesce(sum(case when iecost = 'C1'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC1, 
                                         decimal(coalesce(sum(case when iecost = 'C2'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC2, 
                                         decimal(coalesce(sum(case when iecost = 'C3'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC3, 
                                         decimal(coalesce(sum(case when iecost = 'C4'  then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznC4, 
                                         decimal(coalesce(sum(case when left(iecost, 1) = 'D' then cast(iecsl * .000001 as decimal(15, 5)) else 0 end), 0),10,5) as fznD0,    
                                         decimal(coalesce(sum(case when iecost = 'B2' then cast((iecsl * .000001 * CAST(? AS INTEGER)) as decimal(15, 5)) else 0 end), 0),10,5) as fznSUTotal, 
                                        decimal(coalesce( sum(case when iecost = 'A1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznA1Net,
					                    decimal(coalesce( sum(case when iecost = 'B1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB1Net,
					                    decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznB2Net,
					                    decimal(coalesce( sum(case when iecost = 'C1' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC1Net,
					                    decimal(coalesce( sum(case when iecost = 'C2' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC2Net,
					                    decimal(coalesce( sum(case when iecost = 'C3' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC3Net,
					                    decimal(coalesce( sum(case when iecost = 'C4' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznC4Net,
					                    decimal(coalesce( sum(case when left(iecost,1) = 'D' then cast(iestdc/1000000 as decimal(15,5)) else 0 end),0),10,5) as fznD0Net,
					                    decimal(coalesce( sum(case when iecost = 'B2' then cast(iestdc/1000000 * CAST(? AS INTEGER) as decimal(15,5)) else 0 end),0),10,5) as fznSUTotalNet
                                            from proddta.f30026
                                            where ieledg = ?
                                            and trim(iemmcu) in (?)
                                            and trim(ielitm) = ?";
                                    }
                                    using (var command = new OdbcCommand(sqlScript, cnn_odbc))
                                    {
                                
                                    command.Parameters.Add(new OdbcParameter("@ACQ", GenerateQuoteHeader.ACQ));
                                
                                
                                    command.Parameters.Add(new OdbcParameter("@ACQ2", GenerateQuoteHeader.ACQ));
                                
                                        if (division_num != "730000")
                                        {
                                            command.Parameters.Add(new OdbcParameter("@ACQ3", GenerateQuoteHeader.ACQ));
                                        }
                                        command.Parameters.Add(new OdbcParameter("@year", "07"));
                                    command.Parameters.Add(new OdbcParameter("@division_num", division_num));
                                if (division_num == "710000")
                                        {
                                            command.Parameters.Add(new OdbcParameter("@item_num", GenerateQuoteHeader.ibitm));
                                        }
                                        else
                                        {
                                            command.Parameters.Add(new OdbcParameter("@item_num", item_var.item_num));
                                        }

                                        using (var reader = command.ExecuteReader())
                                        {
                                            if (reader.HasRows)
                                            {
                                                while (reader.Read())
                                                {
                                                   if (division_num == "710000")
                                                    {

                                                        fznMatl = reader.GetDecimal(0);
                                                        fznLbr = reader.GetDecimal(1);
                                                        fznb2 = reader.GetDecimal(2);
                                                        fznc1 = reader.GetDecimal(3);
                                                        fznc2 = reader.GetDecimal(4);
                                                        fznc3 = reader.GetDecimal(5);
                                                        fznc4 = reader.GetDecimal(6);
                                                        fznOS = reader.GetDecimal(7);
                                                        fznlc = reader.GetDecimal(8);
                                                        fznSUTotal = reader.GetDecimal(9);
                                                        GenerateQuoteHeader.fznA1Net = reader.GetDecimal(10); //fznmatlnet
                                                        GenerateQuoteHeader.fznB1Net = reader.GetDecimal(11); //fznlbrnet
                                                        GenerateQuoteHeader.fznB2Net = reader.GetDecimal(12);
                                                        GenerateQuoteHeader.fznc1net = reader.GetDecimal(13);
                                                        GenerateQuoteHeader.fznc2net = reader.GetDecimal(14);
                                                        GenerateQuoteHeader.fznc3net = reader.GetDecimal(15);
                                                        GenerateQuoteHeader.fznc4net = reader.GetDecimal(16);
                                                        GenerateQuoteHeader.fznD0Net = reader.GetDecimal(17); //fznosnet
                                                        GenerateQuoteHeader.fznSUTotalNet = reader.IsDBNull(18) ? 0 : (decimal)reader.GetValue(18);
                                                        GenerateQuoteHeader.fznLCTotal = reader.GetDecimal(19);
                                                        GenerateQuoteHeader.material = fznMatl;
                                                        GenerateQuoteHeader.setup = fznb2; //setup
                                                        GenerateQuoteHeader.labor = fznLbr;
                                                        GenerateQuoteHeader.lbr_voh = GenerateQuoteHeader.fznc3net;
                                                        GenerateQuoteHeader.mch_voh = GenerateQuoteHeader.fznc1net;
                                                        GenerateQuoteHeader.lbr_foh = GenerateQuoteHeader.fznc4net;
                                                        GenerateQuoteHeader.mch_foh = fznc2;
                                                        GenerateQuoteHeader.osp = GenerateQuoteHeader.fznD0Net;

                                                    }
                                                    else
                                                    {
                                                        fznMatl = reader.GetDecimal(0); //a1
                                                        fznLbr = reader.GetDecimal(1); //b1
                                                        fznb2 = reader.GetDecimal(2);
                                                        fznc1 = reader.GetDecimal(3);
                                                        fznc2 = reader.GetDecimal(4);
                                                        fznc3 = reader.GetDecimal(5);
                                                        fznc4 = reader.GetDecimal(6);
                                                        fznOS = reader.GetDecimal(7); //d0
                                                        fznSUTotal = reader.GetDecimal(2) * GenerateQuoteHeader.ACQ;
                                                        GenerateQuoteHeader.fznA1Net = reader.GetDecimal(9); //fznmatlnet
                                                        GenerateQuoteHeader.fznB1Net = reader.GetDecimal(10); //fznlbrnet
                                                        GenerateQuoteHeader.fznB2Net = reader.GetDecimal(11);
                                                        GenerateQuoteHeader.fznc1net = reader.GetDecimal(12);
                                                        GenerateQuoteHeader.fznc2net = reader.GetDecimal(13);
                                                        GenerateQuoteHeader.fznc3net = reader.GetDecimal(14);
                                                        GenerateQuoteHeader.fznc4net = reader.GetDecimal(15);
                                                        GenerateQuoteHeader.fznD0Net = reader.GetDecimal(16); //fznosnet
                                                        GenerateQuoteHeader.fznSUTotalNet = reader.IsDBNull(17) ? 0 : (decimal)reader.GetValue(17);
                                                        if (division_num == "710000")
                                                        {
                                                            GenerateQuoteHeader.fznLCTotal = reader.GetDecimal(19); //GenerateQuoteHeader.total = fznMatl + fznLbr + fznOS + fznb2 + fznc1 + fznc3 + fznc2 + fznc4; //fznfoh fznvoh
                                                        }
                                                        GenerateQuoteHeader.material = fznMatl;
                                                        GenerateQuoteHeader.setup = fznb2; //setup
                                                        GenerateQuoteHeader.labor = fznLbr;
                                                        GenerateQuoteHeader.lbr_voh = fznc3;
                                                        GenerateQuoteHeader.mch_voh = fznc1;
                                                        GenerateQuoteHeader.lbr_foh = fznc4;
                                                        GenerateQuoteHeader.mch_foh = fznc2;
                                                        GenerateQuoteHeader.osp = fznOS;
                                                        GenerateQuoteHeader.UnitCost = fznMatl + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS + fznb2;
                                                    }
                                            if (division_num == "730000")
                                            {
                                                //if (item_var.item_num == "BH01816-4") 
                                                //{
                                                //    int check_me=1;
                                                //    //check_me = 1;
                                                //    check_me = 1;
                                                //}
                                                        if (Components.Count() > 0)
                                                        {
                                                    GenerateQuoteHeader.calc_dynam_qty = item_var.qty_break;
                                                            if (item_var.qty_break > 0)
                                                            {
                                                                GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (((item_var.qty_break * (GenerateQuoteHeader.fznA1Net + adj_amount + GenerateQuoteHeader.fznB1Net + GenerateQuoteHeader.fznc1net + GenerateQuoteHeader.fznc3net + GenerateQuoteHeader.fznD0Net + ((GenerateQuoteHeader.fznc2net + GenerateQuoteHeader.fznc4net) / item_var.qty_break) + GenerateQuoteHeader.totalCPQtyCostVar + GenerateQuoteHeader.totalCPQtyCostSU + GenerateQuoteHeader.totalCPQtyCostFOH)) + GenerateQuoteHeader.fznSUTotalNet)) / item_var.qty_break;
                                                    }
                                                            else
                                                            {
                                                                GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (((item_var.qty_break * (GenerateQuoteHeader.fznA1Net + adj_amount + GenerateQuoteHeader.fznB1Net + GenerateQuoteHeader.fznc1net + GenerateQuoteHeader.fznc3net + GenerateQuoteHeader.fznD0Net + ((GenerateQuoteHeader.fznc2net + GenerateQuoteHeader.fznc4net)) + GenerateQuoteHeader.totalCPQtyCostVar + GenerateQuoteHeader.totalCPQtyCostSU + GenerateQuoteHeader.totalCPQtyCostFOH + (GenerateQuoteHeader.fznSUTotalNet)))));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            GenerateQuoteHeader.calc_dynam_qty = item_var.qty_break;
                                                    if (item_var.qty_break > 0)
                                                            {
                                                                GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (((item_var.qty_break * (fznMatl + GenerateQuoteHeader.fznB1Net + adj_amount + GenerateQuoteHeader.fznc1net + GenerateQuoteHeader.fznc3net + GenerateQuoteHeader.fznD0Net + ((GenerateQuoteHeader.fznc2net + GenerateQuoteHeader.fznc4net) / item_var.qty_break) + GenerateQuoteHeader.totalCPQtyCostVar + GenerateQuoteHeader.totalCPQtyCostSU + GenerateQuoteHeader.totalCPQtyCostFOH)) + GenerateQuoteHeader.fznSUTotalNet)) / item_var.qty_break;
                                                    }
                                                            else
                                                            {
                                                                GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (((item_var.qty_break * (fznMatl + GenerateQuoteHeader.fznB1Net + adj_amount + GenerateQuoteHeader.fznc1net + GenerateQuoteHeader.fznc3net + GenerateQuoteHeader.fznD0Net + ((GenerateQuoteHeader.fznc2net + GenerateQuoteHeader.fznc4net)) + GenerateQuoteHeader.totalCPQtyCostVar + GenerateQuoteHeader.totalCPQtyCostSU + GenerateQuoteHeader.totalCPQtyCostFOH + (GenerateQuoteHeader.fznSUTotalNet)))));
                                                            }
                                                        }
                                                    }
                                                    else if (Components.Count() > 0 && division_num == "720000")
                                                    {
                                                        GenerateQuoteHeader.calc_dynam_qty = item_var.qty_break;

                                                if (item_var.qty_break > 0)
                                                        {
                                                            GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (adj_amount) + (fznMatl + fznLbr + fznc2 + fznc1 + fznc3 + fznc4 + fznOS) + fznSUTotal / item_var.qty_break;
                                                }
                                                    }
                                                    else if (Components.Count() == 0 && division_num == "720000")
                                                    {
                                                        GenerateQuoteHeader.calc_dynam_qty = item_var.qty_break;
                                                if (item_var.qty_break > 0)
                                                        {
                                                            GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (adj_amount) + ((item_var.qty_break * (fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal) / item_var.qty_break;
                                                        }
                                                        else
                                                        {
                                                            GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (adj_amount) + (((fznMatl + fznLbr + fznc2 + fznc4 + fznc1 + fznc3 + fznOS)) + fznSUTotal);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        GenerateQuoteHeader.calc_dynam_qty = item_var.qty_break;
                                                if (item_var.qty_break > 0)
                                                        {
                                                            GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (fznMatl + adj_amount + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS + (fznSUTotal / item_var.qty_break) + (GenerateQuoteHeader.fznLCTotal / item_var.qty_break));
                                                        }
                                                        else
                                                        {
                                                            GenerateQuoteHeader.calc_dynam_fzn_unit_cost = (fznMatl + adj_amount + fznLbr + fznc1 + fznc2 + fznc3 + fznc4 + fznOS + (fznSUTotal) + (GenerateQuoteHeader.fznLCTotal));
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                MessageBox.Show("Failed to obtain frozen cost.");
                                            }
                                            reader.Close();
                                        }
                                    }
                                }


                                if (ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString != null)
                                {
                                    using (SqlConnection cnn_again = new SqlConnection(ConfigurationManager.ConnectionStrings["SharePointConnectionString"].ConnectionString))
                                    {
                                        cnn_again.Open();
                                        GenerateQuoteHeader.Costing = this.GetCostYearList(cnn_again);


                                        sqlScript = @"select top 1 m.MARGIN_VALUE, m.other from config_MARGIN m 
                                 left outer join organization o on m.DIVISION_ID = o.ORG_ID 
                                 WHERE 1 = 1 
                                 AND (o.DIVISION = isnull(@division_id,o.DIVISION) and m.VALUE_STREAM=isnull(@value_stream,m.Value_stream) or m.PRODUCT_GROUP=isnull(@product_group,m.product_group)
								)
								and @qty_of_interest between m.QUANTITY_BREAK_RANGE_START and m.QUANTITY_BREAK_RANGE_END order by m.margin_value; ";

                                        using (var command = new SqlCommand(sqlScript, cnn_again))
                                        {
                                            SqlParameter division_val = new SqlParameter
                                            {
                                                ParameterName = "@division_id",
                                                Value = division
                                            };
                                            command.Parameters.Add(division_val);

                                            SqlParameter value_stream_param = new SqlParameter
                                            {
                                                ParameterName = "@value_stream",
                                                Value = String.IsNullOrEmpty(GenerateQuoteHeader.ValueStream) ? "" : GenerateQuoteHeader.ValueStream.ToString()
                                            };
                                            command.Parameters.Add(value_stream_param);
                                            SqlParameter product_group_param = new SqlParameter
                                            {
                                                ParameterName = "@product_group",
                                                Value = String.IsNullOrEmpty(GenerateQuoteHeader.ProductGroup) ? "" : GenerateQuoteHeader.ProductGroup.ToString()
                                            };
                                            command.Parameters.Add(product_group_param);

                                            SqlParameter internal_part_number_param = new SqlParameter
                                            {
                                                ParameterName = "@part_number",
                                                Value = item_var.item_num
                                            };
                                            command.Parameters.Add(internal_part_number_param);

                                            SqlParameter qty_param = new SqlParameter
                                            {
                                                ParameterName = "@qty_of_interest",
                                                Value = item_var.qty_break
                                            };
                                            command.Parameters.Add(qty_param);

                                            using (SqlDataReader reader = command.ExecuteReader())
                                            {
                                                try
                                                {

                                                    while (reader.Read())
                                                    {
                                                        if (reader.HasRows)
                                                        {
                                                            margin = reader.GetDecimal(0);
                                                        }
                                                    }
                                                }

                                                catch (Exception)
                                                {
                                                    MessageBox.Show("Error loading the margins that have been configured.");
                                                    cnn_again.Close();
                                                    cnn_again.Dispose();
                                                    return View();
                                                }
                                                finally
                                                {
                                                    reader.Close();
                                                }
                                            }
                                        }

                                        unit_price =  GenerateQuoteHeader.calc_dynam_fzn_unit_cost/ (1-(margin/100));

                                        sqlScript = @"update workbench_lines set unit_price=@price, pricing_tool_cost=@pricing_tool_cost where workbench_id=@workbench_id and part_number=@item_num and quantity=@quantity and part_number<>'NEW ITEM QUOTE'; ";

                                        using (var command = new SqlCommand(sqlScript, cnn_again))
                                        {
                                    command.CommandTimeout = 0;
                                    SqlParameter internal_part_number_param = new SqlParameter
                                            {
                                                ParameterName = "@item_num",
                                                Value = item_var.item_num
                                            };
                                            command.Parameters.Add(internal_part_number_param);

                                            SqlParameter workbench_id_param = new SqlParameter
                                            {
                                                ParameterName = "@workbench_id",
                                                Value = workbench_id
                                            };
                                            command.Parameters.Add(workbench_id_param);
                                    //GenerateQuoteHeader.calc_dynam_fzn_unit_cost
                                    SqlParameter calc_unit_cost_param = new SqlParameter
                                    {
                                        ParameterName = "@pricing_tool_cost",
                                        Value = GenerateQuoteHeader.calc_dynam_fzn_unit_cost
                                    };
                                    command.Parameters.Add(calc_unit_cost_param);
                                    SqlParameter quantity_param = new SqlParameter
                                    {
                                        ParameterName = "@quantity",
                                        Value = item_var.qty_break
                                    };
                                    command.Parameters.Add(quantity_param);

                                    SqlParameter unit_price_param = new SqlParameter
                                    {
                                        ParameterName = "@price",
                                        Value = unit_price
                                    };
                                    command.Parameters.Add(unit_price_param);
                                    try
                                            {
                                                command.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                MessageBox.Show("Failed to find pricing to update. " + ex.Message);

                                            }
                                            finally
                                            {
                                                cnn_again.Close();
                                                cnn_again.Dispose();
                                            }

                                        }

                                    }



                                }
                            }
                        }
                    }


            return Redirect(Request.UrlReferrer.ToString());



        }

   

  }
}
