﻿using RequestForQuoteSharePointWeb.Models;
using RequestForQuoteSharePointWeb.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RequestForQuoteSharePointWeb.Controllers
{
    public class StatsController : BaseController
    {
        // GET: Stats
        public ActionResult Index()
        {
            List<TimerResultsViewModel> models = new List<TimerResultsViewModel>();
            foreach( string key in this.HttpContext.Application.Keys )
            {
                if( !string.IsNullOrEmpty(key) && key.StartsWith( CodeTimer.TIMER_PREFIX) )
                {
                    List<long> values = this.HttpContext.Application[key] as List<long>;
                    if( values != null )
                    {
                        models.Add(new TimerResultsViewModel() {
                            Name = key.Replace(CodeTimer.TIMER_PREFIX, ""),
                            Values = values
                        });
                    }
                }
            }

            return View(models);
        }
    }
}