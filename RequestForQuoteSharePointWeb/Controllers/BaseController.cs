using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using RequestForQuoteSharePointWeb.Models;

namespace RequestForQuoteSharePointWeb.Controllers
{
    public class BaseController : Controller
    {
        protected const int CACHE_TIME_MINS = 5;
    protected const string APP_VERSION = "1.0.0.39";

    public BaseController()
    {
      ViewBag.Version = APP_VERSION;
    }

    /* Generic Database Functions */

    /// <summary>
    /// Opens a command and a reader and then calls your function
    /// </summary>
    /// <typeparam name="T">the type of object to return</typeparam>
    /// <param name="sqlQuery">the sql query</param>
    /// <param name="connection">the already open connection</param>
    /// <param name="callback"> function to call with a reader</param>
    /// <returns>T a generic type</returns>
    public T DbReader<T>(string sqlQuery, SqlConnection connection, Func<SqlDataReader, T> callback)
        {
            // open a sql command using the query and connection
            using (SqlCommand cmd = new SqlCommand(sqlQuery, connection)) // sending in the connection
            {
                // open a reader on the command
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    // capture the callback results
                    T result = callback(sdr);
                    // close the reader
                    sdr.Close();
                    // return the results
                    return result;
                }
            }
        }

        /// <summary>
        /// Opens a command with a parameter and a reader and then calls your function
        /// </summary>
        /// <typeparam name="T">the type of object to return</typeparam>
        /// <param name="sqlQuery">the sql query</param>
        /// <param name="connection">the already open connection</param>
        /// <param name="param">the parameter</param>
        /// <param name="callback">you functio  to call</param>
        /// <returns>T a generic type</returns>
        public T DbReaderWithParam<T>(string sqlQuery, SqlConnection connection, SqlParameter param, Func<SqlDataReader, T> callback)
        {
            // open a sql command using the query and connection
            using (SqlCommand cmd = new SqlCommand(sqlQuery, connection)) // sending in the connection
            {
                // add the parameter
                cmd.Parameters.Add(param);
                // open a reader on the command
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    // capture the callback results
                    T result = callback(sdr);
                    // close the reader
                    sdr.Close();
                    // return the result
                    return result;
                }
            }
        }
        /// <summary>
        /// Opens a command with a list of parameters and a reader and then calls your function
        /// </summary>
        /// <typeparam name="T">the type of object to return</typeparam>
        /// <param name="sqlQuery">the sql query</param>
        /// <param name="connection">the already open connection</param>
        /// <param name="parameters">the list of paramenteres to add to the command</param>
        /// <param name="callback">you functio  to call</param>
        /// <returns>an instance of T </returns>
        public T DbReaderWithParams<T>(string sqlQuery, SqlConnection connection, List<SqlParameter> parameters, Func<SqlDataReader, T> callback)
        {
            // open a sql command using the query and connection
            using (SqlCommand cmd = new SqlCommand(sqlQuery, connection)) // sending in the connection
            {
                // add the parameters
                foreach( var param in parameters)
                    cmd.Parameters.Add(param);
                // open a reader on the command
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    // capture the callback results
                    T result = callback(sdr);
                    // close the reader
                    sdr.Close();
                    // return the result
                    return result;
                }
            }
        }

		public List<MenuItems> GetSiteMenu()
		{
			// Setup the menu

			List<RequestForQuoteSharePointWeb.Models.MenuItems> site_menu = new List<RequestForQuoteSharePointWeb.Models.MenuItems>();
			site_menu.Add(new RequestForQuoteSharePointWeb.Models.MenuItems()
			{
				IsHeroTool = true,
				Name = "Pricing Tool",
				Icon = "comments-dollar",
				Path = "/Home/OriginalPricingTool",
				Description = "Pricing Tool"
			});
			site_menu.Add(new RequestForQuoteSharePointWeb.Models.MenuItems()
			{
				IsHeroTool = false,
				Name = "Check your requests",
				Icon = "comments-dollar",
				Path = "/Home/CurrentQueue",
				Description = "Check your current listing of outstanding requests for new quotes, modify an existing request, or cancel a new part quote request."
			});
			site_menu.Add(new RequestForQuoteSharePointWeb.Models.MenuItems()
			{
				IsHeroTool = false,
				Name = "View Issued Quotes",
				Icon = "route",
				Path = "/Home/CurrentQuoteQueue",
				Description = "View quotes that are currently pending approval, approved, or issued and their respective pricnig."
			});
			site_menu.Add(new RequestForQuoteSharePointWeb.Models.MenuItems()
			{
				IsHeroTool = false,
				Name = "Manage Users",
				Icon = "users-cog",
				Path = "/Home/ManageUsers",
				Description = "Manage existing users, their access level, and any associated roles that you would like to associate."
			});
			site_menu.Add(new RequestForQuoteSharePointWeb.Models.MenuItems()
			{
				IsHeroTool = false,
				Name = "Reports",
				Icon = "table",
				Path = "/Home/Reports",
				Description = "Review previously issued quotes, combined with quotes currently being evaluated."
			});
			site_menu.Add(new RequestForQuoteSharePointWeb.Models.MenuItems()
			{
				IsHeroTool = false,
				Name = "Configuration",
				Icon = "cog",
				Path = "/Home/Configure",
				Description = "Set default parameters within the Pricing Tool."
			});
			site_menu.Add(new RequestForQuoteSharePointWeb.Models.MenuItems()
			{
				IsHeroTool = false,
				Name = "Data Management",
				Icon = "database",
				Path = "/Home/DataManagementConsole",
				Description = "Define factor variables for your respective location, update qualified parts, etc.."
			});

			return site_menu;
		}


        /// <summary>
        /// Retrive a list of material types
        /// </summary>
        /// <param name="connection">the already open connection</param>
        /// <returns>a list of types from the Material_Type table</returns>
        public List<SelectListItem> GetMaterialTypesList(SqlConnection connection)
        {
            string sKey = "_AppMaterialTypesList";
            var result = HttpContext.Cache[sKey] as List<SelectListItem>;
            if (result == null)
            {
                result = this.DbReader<List<SelectListItem>>("SELECT Material_Type_Desc, Material_Type_id FROM Material_Type", connection, (rdr) =>
                {
                    List<SelectListItem> items = new List<SelectListItem>();
                    while (rdr.Read())
                    {
                        items.Add(new SelectListItem
                        {
                            Text = rdr["Material_Type_Desc"].ToString(),
                            Value = rdr["Material_Type_id"].ToString()
                        });
                    }
                    return items;
                });
                if (result != null && result.Count > 0) // cache results for CACHE_TIME_MINS mins (if any)
                    HttpContext.Cache.Add(sKey, result, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CACHE_TIME_MINS, 0), System.Web.Caching.CacheItemPriority.Normal, null);
            }
            return result;
        }

        /// <summary>
        /// Retrive a list of roles
        /// </summary>
        /// <param name="connection">the already open connection</param>
        /// <returns>a list of roles from the vView_Quote_Routing table</returns>
        public List<SelectListItem> GetRolesList(SqlConnection connection)
        {
            string sKey = "_AppRolesList";
            var result = HttpContext.Cache[sKey] as List<SelectListItem>;
            if (result == null)
            {
                result = this.DbReader<List<SelectListItem>>("SELECT role_desc, role_id FROM vView_Quote_Routing", connection, (rdr) =>
                {
                    List<SelectListItem> roles = new List<SelectListItem>();
                    while (rdr.Read())
                    {
                        roles.Add(new SelectListItem
                        {
                            Text = rdr["Role_Desc"].ToString(),
                            Value = rdr["Role_ID"].ToString()
                        });
                    }
                    return roles;
                });
                if (result != null && result.Count > 0) // cache results for CACHE_TIME_MINS mins (if any)
                    HttpContext.Cache.Add(sKey, result, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CACHE_TIME_MINS, 0), System.Web.Caching.CacheItemPriority.Normal, null);
            }
            return result;
        }

        /// <summary>
        /// Retrive a list of roles
        /// </summary>
        /// <param name="connection">the already open connection</param>
        /// <returns>a list of roles from the vCreate_Quote_Routing table</returns>
        public List<SelectListItem> GetRolesListFromCreate(SqlConnection connection)
        {
            string sKey = "_AppRolesCreateList";
            var result = HttpContext.Cache[sKey] as List<SelectListItem>;
            if (result == null)
            {
                result = this.DbReader<List<SelectListItem>>("SELECT role_desc,role_id FROM vCreate_Quote_Routing", connection, (rdr) =>
                {
                    List<SelectListItem> roles = new List<SelectListItem>();
                    while (rdr.Read())
                    {
                        roles.Add(new SelectListItem
                        {
                            Text = rdr["role_desc"].ToString(),
                            Value = rdr["role_id"].ToString()
                        });
                    }
                    return roles;
                });
                if (result != null && result.Count > 0) // cache results for CACHE_TIME_MINS mins (if any)
                    HttpContext.Cache.Add(sKey, result, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CACHE_TIME_MINS, 0), System.Web.Caching.CacheItemPriority.Normal, null);
            }
            return result;
        }

        public List<SelectListItem> GetQualificationList(SqlConnection connection)
        {
            string sKey = "_AppQualificationTypesList";
            var result = HttpContext.Cache[sKey] as List<SelectListItem>;
            if (result == null)
            {
                result = this.DbReader<List<SelectListItem>>("SELECT qualification_testing_type_desc,qualification_testing_type_id FROM qualification_testing_type", connection, (rdr) =>
                {
                    List<SelectListItem> roles = new List<SelectListItem>();
                    while (rdr.Read())
                    {
                        roles.Add(new SelectListItem
                        {
                            Text = rdr["qualification_testing_type_Desc"].ToString(),
                            Value = rdr["qualification_testing_type_id"].ToString()
                        });
                    }
                    return roles;
                });
                if (result != null && result.Count > 0) // cache results for CACHE_TIME_MINS mins (if any)
                    HttpContext.Cache.Add(sKey, result, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CACHE_TIME_MINS, 0), System.Web.Caching.CacheItemPriority.Normal, null);
            }
            return result;
        }

        public List<SelectListItem> GetOppurtunitiesList(SqlConnection connection)
        {
            string sKey = "_AppOpportunitiesList";
            var result = HttpContext.Cache[sKey] as List<SelectListItem>;
            if (result == null)
            {
                result = this.DbReader<List<SelectListItem>>("SELECT opportunity_type_Desc, opportunity_type_id FROM opportunity_type", connection, (rdr) =>
                {
                    List<SelectListItem> roles = new List<SelectListItem>();
                    while (rdr.Read())
                    {
                        roles.Add(new SelectListItem
                        {
                            Text = rdr["opportunity_type_Desc"].ToString(),
                            Value = rdr["opportunity_type_id"].ToString()
                        });
                    }
                    return roles;
                });
                if (result != null && result.Count > 0) // cache results for CACHE_TIME_MINS mins (if any)
                    HttpContext.Cache.Add(sKey, result, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CACHE_TIME_MINS, 0), System.Web.Caching.CacheItemPriority.Normal, null);
            }
            return result;
        }

        public List<SelectListItem> GetPrioritiesList(SqlConnection connection)
        {
            string sKey = "_AppPrioritiesList";
            var result = HttpContext.Cache[sKey] as List<SelectListItem>;
            if (result == null)
            {
                result = this.DbReader<List<SelectListItem>>("SELECT Priority_Desc, Priority_ID FROM Priority", connection, (rdr) =>
                {
                    List<SelectListItem> roles = new List<SelectListItem>();
                    while (rdr.Read())
                    {
                        roles.Add(new SelectListItem
                        {
                            Text = rdr["Priority_Desc"].ToString(),
                            Value = rdr["Priority_ID"].ToString()
                        });
                    }
                    return roles;
                });
                if (result != null && result.Count > 0) // cache results for CACHE_TIME_MINS mins (if any)
                    HttpContext.Cache.Add(sKey, result, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CACHE_TIME_MINS, 0), System.Web.Caching.CacheItemPriority.Normal, null);
            }
            return result;
        }


        public List<SelectListItem> GetProductTypesList(SqlConnection connection)
        {
            string sKey = "_AppProductTypes";
            var result = HttpContext.Cache[sKey] as List<SelectListItem>;
            if (result == null)
            {
                result = this.DbReader<List<SelectListItem>>("SELECT product_type_desc, product_type_id FROM product_type", connection, (rdr) =>
                {
                    List<SelectListItem> roles = new List<SelectListItem>();
                    while (rdr.Read())
                    {
                        roles.Add(new SelectListItem
                        {
                            Text = rdr["product_type_desc"].ToString(),
                            Value = rdr["product_type_id"].ToString()
                        });
                    }
                    return roles;
                });
                if (result != null && result.Count > 0) // cache results for CACHE_TIME_MINS mins (if any)
                    HttpContext.Cache.Add(sKey, result, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CACHE_TIME_MINS, 0), System.Web.Caching.CacheItemPriority.Normal, null);
            }
            return result;
        }
        /// <summary>
        /// Retrieve a list of status from the vUpdate_Status table
        /// </summary>
        /// <param name="connection">the already open connection</param>
        /// <returns>a list of status</returns>
        public List<SelectListItem> GetUpdateStatusList(SqlConnection connection)
        {
            string sKey = "_AppUpdateStatusList";
            var result = HttpContext.Cache[sKey] as List<SelectListItem>;
            if (result == null)
            {
                result = this.DbReader<List<SelectListItem>>("SELECT status_desc, status_id FROM vUpdate_Status", connection, (reader) =>
                {
                    List<SelectListItem> roles = new List<SelectListItem>();
                    while (reader.Read())
                    {
                        roles.Add(new SelectListItem
                        {
                            Text = reader["Status_desc"].ToString(),
                            Value = reader["Status_ID"].ToString()
                        });
                    }
                    return roles;
                });
                if (result != null && result.Count > 0) // cache results for CACHE_TIME_MINS mins (if any)
                    HttpContext.Cache.Add(sKey, result, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CACHE_TIME_MINS, 0), System.Web.Caching.CacheItemPriority.Normal, null);
            }
            return result;
        }

        /// <summary>
        /// Retrieve a list of status from the status table
        /// </summary>
        /// <param name="connection">the already open connection</param>
        /// <returns>a list of status</returns>
        public List<SelectListItem> GetStatusList(SqlConnection connection)
        {
            string sKey = "_AppStatusList";
            var result = HttpContext.Cache[sKey] as List<SelectListItem>;
            if (result == null)
            {
                result = this.DbReader<List<SelectListItem>>("SELECT status_desc,status_id FROM status", connection, (reader) =>
                {
                    List<SelectListItem> roles = new List<SelectListItem>();
                    while (reader.Read())
                    {
                        roles.Add(new SelectListItem
                        {
                            Text = reader["Status_desc"].ToString(),
                            Value = reader["Status_ID"].ToString()
                        });
                    }
                    return roles;
                });
                if (result != null && result.Count > 0) // cache results for CACHE_TIME_MINS mins (if any)
                    HttpContext.Cache.Add(sKey, result, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CACHE_TIME_MINS, 0), System.Web.Caching.CacheItemPriority.Normal, null);
            }
            return result;
        }


        /// <summary>
        /// Retrive a list of material types
        /// </summary>
        /// <param name="connection">the already open connection</param>
        /// <returns>a list of types from the Material_Type table</returns>
        public List<SelectListItem> GetCostYearList(SqlConnection connection)
        {
            string sKey = "_AppCostYearTypesList";
            var result = HttpContext.Cache[sKey] as List<SelectListItem>;
            if (result == null)
            {
                result = this.DbReader<List<SelectListItem>>("select cost_year_id, cost_year from dT_Finance_CostYearLookup ", connection, (rdr) =>
                {
                    List<SelectListItem> CostYears = new List<SelectListItem>();
                    while (rdr.Read())
                    {
                        CostYears.Add(new SelectListItem
                        {
                            Text = rdr["Cost_Year"].ToString(),
                            Value = rdr["COST_Year_ID"].ToString()
                        });
                    }
                    return CostYears;
                });
                if (result != null && result.Count > 0) // cache results for CACHE_TIME_MINS mins (if any)
                    HttpContext.Cache.Add(sKey, result, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CACHE_TIME_MINS, 0), System.Web.Caching.CacheItemPriority.Normal, null);
            }
            return result;
        }

    }
}
